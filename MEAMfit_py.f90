!
! MEAMfit v2
! ----------
!

subroutine MEAMfit(object_func, b0,b1,b2)

   use m_optimization
   use m_filenames
   use m_geometry
   use m_datapoints
   use m_generalinfo
   use m_meamparameters
   use m_mpi_info

   implicit none
   real(8), intent(in) :: b0,b1,b2          ! Input 3 params
   real(8) object_func, b(19), c(19)        ! Size dependant on fitdbse configs to fit number (in this case 1-20)
   logical maxOK,exist
   integer ip,j,k,iseed,nattempts,nfreep
   real(8) tmp

   ! Initializes MPI
   call p_setup
   call getTime(tStart)

   ! MPI: make sure only the main process prints this
   call testargs !Check for command-line arguments

   !Check if settings file exists (other filenames are defined and their existence checked later)
   settingsfile='settings'
   inquire(file=trim(settingsfile),exist=exist)
   if (exist.eqv..false.) then
      settingsfileexist=.false.
   else
      settingsfileexist=.true.
   endif

   call readVerbosityInSettings !Pre-check the settings file for verbosity
   createdFitdbse=.false.
   createdSettings=.false.
   call setupfilenames  !Define all filenames

   if (createdFitdbse.eqv..true.) then
      ! This has been added for the python implementation. The 'stop' in setupfilenames doesn't allow the subroutine to end
      ! normally, so this is a workaround
!f2py    intent(out) object_func
      call deallocateAllocated
      return
   endif
   call checkInputFilesExist !Check if 'settings' file and, if applicable, potential files exist
   call getmaxspecies   !Determine 'maxspecies' from the vasprun files (the total number of species
                 !to be used). This needs doing in advance of readsettings for setting up arrays.
   if (readpotfile.eqv..true.) then
       call extractlmax
   endif
   if (settingsfileexist.eqv..false.) then
      call setupdefaultlmax
   endif

   call readsettings(iseed) !read settings from settings file
   if (createdSettings.eqv..true.) then
      call deallocateAllocated
      return
   endif

   if (settingsfileexist.eqv..true.) then
      call setuprandomseed(iseed)
   endif

   !---- Initial meam-parameter set-up ----
   call initializemeam_py(b0,b1,b2)       !Initialize initial meam parameters
   if (nLammpsFiles.gt.0) then
      !Read in LAMMPS potentials and set-up splines
      call initializeLammpsPot
   endif
   if ((readParasOnebyone.eqv..false.).and.(readpotfile).and.(noOpt.eqv..false.)) then
      !If a potential file is supplied by user, amend freep so that non-zero parameters
      !in this file are optimized (unless PARASTOOPT is specified in the settings file)
      call amendFreep
   endif
   call getRmax !Find the maximum interatomic separation that needs to be considered.
   if (optimizestruc) then
       !Optimize positons of ions, keeping the MEAM parameters fixed.
       !Note: currently unsupported - to be fully instated later
       call optimizestructure(.false.) !Index: optimize all structures?
       stop
   endif

   !---- Set-up structures and fitting database ----
   call initializestruc(0) !Read in atomic configurations from vasprun files.
   !Note that, depending on flags, LAMMPS input structure files may also be
   !generated here.
   
   call backupxyz !Store original positions so we can reset them after each optimization
   !cycle.

   createdCoulomb=.false.
   call readtargetdata  !Read energies, forces and stress tensor components from vasprun files
   if (createdCoulomb.eqv..true.) then
      call deallocateAllocated
      return
   endif
   if (analyzeVib.eqv..true.) then
      !If requested, analyze DFT input MD data and predict an appropriate
      !timestep for subsequent DFT calculations
      call analyzeVibrations
      return
   endif
   call calcSds       !Determine s.d's of energies and forces to rescale optfunc
   if (verbosity.gt.1) print *,'setting up nn tables'
   call setupnntables !Set up atom separation tables, also for forces (must
                      !come after 'readtargetdata' as this determines which
                      !forces are to be optimized)
   if (verbosity.gt.1) print *,'finding smallest separation'
   call findsmallestsepn !Determine smallest and largest interatomic separations
   if (verbosity.gt.1) print *,'...completed findsmallestsepn call'
   if ((procid.eq.0).and.(nsteps.eq.1).and.(readpotfile.eqv..false.).and.(nLammpsFiles.eq.0).and.(analyzeVib.eqv..false.)) then
      print *,'No optimization specified, and no potential filenames supplied:'
      print *,'Producing sepnHistogram file and STOPPING.'
      call p_finalize
      return
   endif
   !------------------------------------------------

   if ((settingsfileexist.eqv..false.).or. &
       (writeExPot.eqv..true.)) then
      !Create template file for 'potparas_best' if requested
      call createMeamParasTemplate
   endif
   !Check which parameters are to be optimized, and initiate nopt variables
   !accordingly
   !print *,'check which parameters are to be optimized'
   call signRadFuncs
   !NOTE: FROM here on, the worker process and the main process are no longer executing the same code!!!
   !Here the workerthreads will split off from the main thread
   !The worker threads will go into an endeless do loop calling the objective function
   !In the objective function they will only execute the main optimation loop if they get assigned by the main thread

   if (.not.procid.eq.0) then
       ! MPI: The Workerinit subroutine allocates the necessary arrays for the workers to get started in the objectivefunction
       call p_workerThread
   endif
   ! MPI: The main thread (proqid=0) continues sequentually

   if (randgenparas) then
      !Parameters to be randomly initialized: Read in bounds for initialization
      if (readparasfromsettings) then
         call readBoundsForRandParas
      else
         call defaultBoundsForRandParas
      endif
      print *
   endif
   !call displayBoundsForVarParas

   if (printoptparas.eqv..true.) then
      !If requested, write out sample PARASTOOPT and stop
      call writeoptparasSub
      print *
      print *,'Stopping.'
      stop
   endif

   call checkPotType
   if (nsteps.gt.1) then
      call checkParas !Check bounds for random parameter initialization as well
                      !as the parameters themselves if supplied by user
      !Determine the number of parameters to be optimized
      nfreep=0
      do ip=1,np
         if ((freep(ip).eq.1).or.(freep(ip).eq.2)) then
            nfreep=nfreep+1
         endif
      enddo
   endif

   if (contjob.eqv..false.) then
      n_optfunc=1
      allocate(bestoptfuncs(noptfuncstore),timeoptfuncHr(noptfuncstore), &
          timeoptfuncMin(noptfuncstore))
   else
      n_optfunc=noptfuncstore+1
   endif

   call allocateArrays

   if (nsteps.eq.1) then

      !if (MCMC.eqv..true.) then          ! Condition can be used to test of MCMC flag in settings
      !else
      !end if

      !print *
      !print *,'Calculation of properties (no optimization)'
      !print *,'-------------------------------------------'
      !print *
      call initialize_noptp
      !print *,'final out:'
      !call finalOutput
      call output_py(object_func)
      call deallocateAllocated
      return

   else

      !Display fitting data
      write(*,'(A12,I5,A58)') !' Fitting to ',ndatapointsTrue,' data-points (energies and force/stress tensor components)'
      write(*,'(A7,I5,A12)') !' using ',nfreep,' parameters.'
      fitPntsPerPara=dble(ndatapointsTrue)/dble(nfreep)
      if (fitPntsPerPara.gt.minFitPntsPerPara) then
         write(*,'(F10.5,A28,I5,A1)') fitPntsPerPara,' fit points per parameter (>',minFitPntsPerPara,')'
      else
         write(*,'(F10.5,A28,I5,A1)') fitPntsPerPara,' fit points per parameter (<',minFitPntsPerPara,')'
         write(*,'(A20,I5,A65)') ' WARNING: Less than ',minFitPntsPerPara,' fitting datapoints per potential parameter - likely to overfit!'
      endif
      call optimizeParameters(nfreep) !Main call: Optimize the parameters
      
      if (optComplete) then
         call finalOutput
      else
      endif

   endif

   call p_finalize
!f2py    intent(out) object_func
end subroutine MEAMfit



subroutine initializemeam_py(b0,b1,b2)

    !--------------------------------------------------------------c
    !
    !     Initialize (initial) MEAM parameters either using a
    !     potential file provided by the user; a series of
    !     potential files in case of a continuation job; or set
    !     them to zero (later to be assigned random values) if
    !     they are to be initialized using parameters from the
    !     settings file. MEAM parameters are read both into the 
    !     work arrays: cmin, cmax, etc; and also into the p() 
    !     array.
    !
    !     Calls:         readmeamparam,variables_to_p,
    !                 p_to_variables
    !     Files read:    'startparameterfile' (filename provided
    !                 by user), potparas_best#n (n=1-10),
    !                 bestoptfuncs
    !     Returns:       cmin,cmax,meamtau,
    !                 meamrhodecay,meame0,meamrho0,meamemb3,
    !                 meamemb4,pairpotparameter,rs,rc,enconst,p()
    !
    !     Andrew Duff Jan 2015
    !
    !     Copyright (c) 2018, STFC
    !
    !--------------------------------------------------------------c

    use m_meamparameters
    use m_atomproperties
    use m_filenames
    use m_generalinfo
    use m_optimization

    implicit none

    logical exist
    integer i,j,tmp
    character*1 tmp2
    character*4 char4
    character*20 string1
    character*80 filename,string
    real(8) b0,b1,b2

    if (verbosity.gt.1) then
       !print *
       !print *,'Potential initialization'
       !print *,'------------------------'
    endif

    if (.not.allocated(cmin)) then
        allocate( cmin(maxspecies,maxspecies,maxspecies), &
            cmax(maxspecies,maxspecies,maxspecies), &
            meamtau(1:lmax,maxspecies), &
            meamrhodecay(12,0:lmax,maxspecies,maxspecies), &
            meame0(maxspecies), &
            meamrho0(maxspecies), &
            meamemb3(maxspecies), &
            meamemb4(maxspecies), &
            pairpotparameter(32,maxspecies,maxspecies), &
            rs(maxspecies,maxspecies), &
            rc(maxspecies,maxspecies), &
            enconst(maxspecies)) !, &
            ! pairpotStr(splnNvals,maxspecies,maxspecies))
    endif

    cmin=0d0
    cmax=0d0
    meamtau=0d0
    meamrhodecay=0d0
    meame0=0d0
    meamrho0=0d0
    meamemb3=0d0
    meamemb4=0d0
    pairpotparameter=0d0
    rs=0d0
    rc=0d0
    enconst=0d0

    if (.not.allocated(p)) then
        allocate(p(np))
    endif

    p=0d0

    if (readpotfile.eqv..true.) then
       !if (verbosity.gt.1) print *,'Reading in potential parameters from ',trim(startparameterfile)
       call readmeamparam_py(startparameterfile, b0,b1,b2) !Read into sensibly-named variables (meamrhodecay, etc)
       call variables_to_p!(a,b,c) !Now copy these variables into the p() array (necc. for CG optimizer)

    else
       if (contjob.eqv..true.) then
          ! Continuation job: Read in potentials from files
          allocate(p_saved(np,noptfuncstore),bestoptfuncs(noptfuncstore), &
              timeoptfuncHr(noptfuncstore),timeoptfuncMin(noptfuncstore))
          if (verbosity.gt.1) print *,'Reading in files from potparas_best#n, with #n=1,NOPTFUNCSTORE'
          open(50,file='bestoptfuncs')
          read(50,*) string
          if (verbosity.gt.1) print *,'string=',string
          if (string(1:3).ne.'Top') then
             print *,'ERROR: You do not yet have a full set of potparas_best files, stopping.'
             stop
          endif
          do i=1,noptfuncstore
             !Setup filename for each potential in turn
             filename="potparas_best"
             if (i.lt.10) then
                 write(string1,'(I1)') i
             elseif (i.lt.100) then
                 write(string1,'(I2)') i
             else
                 print *,'ERROR: more than 100 files set to save; code needs'
                 print *,'changing, STOPPING.'
                 stop
             endif
             filename=trim(filename)//trim(string1)

             !Read potential parameters from file
             print *,'starting with ',trim(adjustl(filename))
             call readmeamparam(trim(adjustl(filename)))
             call variables_to_p
             do j=1,np
                 p_saved(j,i)=p(j)
             enddo
             !Copy corresponding optimization function into bestoptfuncs
             read(50,*) char4,bestoptfuncs(i)
             timeoptfuncHr(i)=0
             timeoptfuncMin(i)=0
          enddo
          print *,'Bestopfuncs read in:'
          do i=1,noptfuncstore
             print *,i,': ',bestoptfuncs(i)
          enddo
       else
          !print *,'Potential parameters to be optimized read from settings file.'
          !Set-up dummy values for now, and then after the distance analysis,
          !assign values to the cut-off radii.
          call p_to_variables
       endif
    endif

end subroutine initializemeam_py

subroutine readmeamparam_py(filename, b0,b1,b2)

    !---------------------------------------------------------------c
    !
    !     Read in a set of MEAM parameters from a file into the
    !     variables: cmin, etc. The name of the file is stored in
    !     the variable, filename.
    !
    !     Notes: lmaxPot is read in rather than lmax because lmax
    !     will already have been read in from the potential
    !     file (by extractlmax called from MEAMfit), but might have
    !     been changed by the settings file. E.g, we might want to
    !     optimize a MEAM potential but the input potential is EAM,
    !     in which case we want to keep lmax=3, but use a different
    !     variable, lmaxPot, to denote which angular momenta of
    !     data we should read in from the potential file.
    !
    !     Called by:     program MEAMfit,initializemeam,meamenergy
    !     Calls:         -
    !     Arguments:     filename
    !     Returns:       cmin,cmax,meamtau,
    !                 meamrhodecay,meame0,meamrho0,
    !                 meamemb3,meamemb4,pairpotparameter,
    !                 rs,rc
    !     Files read:    -
    !     Files written: -
    !
    !     Andrew Ian Duff 2014
    !
    !     Copyright (c) 2018, STFC
    !
    !---------------------------------------------------------------c

    use m_filenames
    use m_meamparameters
    use m_optimization
    use m_atomproperties

    implicit none

    integer i,j,l,lmaxPot
    real(8), allocatable:: meamtau_dummy(:,:)
    real(8) b0,b1,b2
    character*80 filename

    open(unit=1,file=trim(filename),status='old')
    read(1,*) lmaxPot
   !print *,'lmaxPot=',lmaxPot
    read(1,*) cmin !species 3x
   !print *,'cmin=',cmin
    read(1,*) cmax !species 3x
   !print *,'cmax=',cmax
    read(1,*) envdepmeamt !environ dep meamt's?
   !print *,'envdepmeamt=',envdepmeamt
    !meamtau is stored in the potential files with indices reversed (I.e., with
    !species first), for legacy reasons. Therefore first read into dummy array
    !before transfering to meamtau.
   !print *,'maxspecies=',maxspecies,' lmaxPot=',lmaxPot
    allocate(meamtau_dummy(1:maxspecies,1:lmaxPot))
    read(1,*) meamtau_dummy(1:maxspecies,1:lmaxPot)
   !print *,'read in meamtau_dummy(1:',maxspecies,'1:',lmaxPot,')=',meamtau_dummy(1:maxspecies,1:lmaxPot)
    do i=1,maxspecies
       do l=1,lmaxPot
          meamtau(l,i)=meamtau_dummy(i,l)
       enddo
    enddo
 deallocate(meamtau_dummy)
  !print *,'meamtau(l=0,1)=',meamtau(0,1)
  !print *,'meamtau(l=1,1)=',meamtau(1,1)
  !print *,'meamtau(l=2,1)=',meamtau(2,1)
  !print *,'meamtau(l=3,1)=',meamtau(3,1)
  !print *,'meamtau(l=0,2)=',meamtau(0,2)
  !print *,'meamtau(l=1,2)=',meamtau(1,2)
  !print *,'meamtau(l=2,2)=',meamtau(2,2)
  !print *,'meamtau(l=3,2)=',meamtau(3,2)
   !stop
    read(1,*) thiaccptindepndt
   !print *,'thiaccptindepnt=',thiaccptindepndt
    if (thiaccptindepndt.eqv..true.) then
        do i=1,maxspecies
            read(1,*) typethi(1,i)
            read(1,*) meamrhodecay(1:12,0:lmaxPot,1,i)
           !print *,'meamrhodecay=',meamrhodecay
        enddo
    else
        do i=1,maxspecies
            do j=1,maxspecies
                read(1,*) typethi(i,j)
                read(1,*) meamrhodecay(1:12,0:lmaxPot,i,j)
            enddo
        enddo
    endif
    read(1,*) embfunctype
    read(1,*) meame0 !species
   !print *,'meame0=',meame0
    read(1,*) meamrho0 !species
   !print *,'meamrho0=',meamrho0
    read(1,*) meamemb3 !species
    read(1,*) meamemb4 !species
    do i=1,maxspecies
        do j=1,maxspecies
            if (j.ge.i) then
                read(1,*) typepairpot(i,j)
                read(1,*) pairpotparameter(1:32,i,j)
   !print *,'pairpotparameter=',pairpotparameter
            endif
        enddo
    enddo
    read(1,*) rs !species 2x
   !print *,'rs=',rs
    read(1,*) rc !species 2
   !print *,'rc=',rc
    read(1,*) enconst
   !print *,'enconst=',enconst

    close(unit=1)


    ! Adding input 3 params to pairpotparameter array
    ! pairpotparameter(2,1,2) = b0
    ! pairpotparameter(4,1,2) = b1
    ! pairpotparameter(6,1,2) = b2


    
end subroutine readmeamparam_py

subroutine read_params_py(bout)
    use m_meamparameters
    use m_atomproperties
    use m_filenames
    use m_generalinfo
    use m_optimization
    use m_datapoints
    logical exist
    real(8), intent(out) :: bout(32,2,2)
    call testargs !Check for command-line arguments

    !Check if settings file exists (other filenames are defined and their existence checked later)
    settingsfile='settings'
    inquire(file=trim(settingsfile),exist=exist)
    if (exist.eqv..false.) then
       settingsfileexist=.false.
    else
       settingsfileexist=.true.
    endif

    call readVerbosityInSettings !Pre-check the settings file for verbosity

    call setupfilenames  !Define all filenames
    call checkInputFilesExist !Check if 'settings' file and, if applicable, potential files exist
    call getmaxspecies   !Determine 'maxspecies' from the vasprun files (the total number of species
                  !to be used). This needs doing in advance of readsettings for setting up arrays.
    if (readpotfile.eqv..true.) then
        call extractlmax
    endif
    if (settingsfileexist.eqv..false.) then
       call setupdefaultlmax
    endif
 
    call readsettings(iseed) !read settings from settings file
    if (settingsfileexist.eqv..true.) then
       call setuprandomseed(iseed)
    endif

    call initializemeam

    bout = pairpotparameter
    !print *, shape(pairpotparameter)

    deallocate(targetfiles)
    deallocate(strucnames)
    deallocate(weightsEn)
    deallocate(weightsFr)
    deallocate(weightsSt)
    deallocate(istrucInFile)
    deallocate(DFTdata)
    deallocate(computeForces)
    deallocate(rlxstruc)
    deallocate(computeForces_backup)
    deallocate(freeenergy)
    deallocate(refenergy)

    !f2py   intent(out) bout

    !if (verbosity.gt.1) print *,'Reading in potential parameters from ',trim(startparameterfile)
    !call readmeamparam(startparameterfile) !Read into sensibly-named variables (meamrhodecay, etc)
end subroutine read_params_py

subroutine output_py(object_func)
    use m_datapoints
    use m_filenames
    use m_optimization
    use m_generalinfo
    use m_atomproperties
    use m_plotfiles
    use m_geometry
    use m_meamparameters
    use m_objectiveFunction
    use m_observables


    implicit none
    integer isp
    character*80 filename
    character*20 string1,string2
    real(8), intent(out) :: object_func!, b(19),c(19)

    call objectiveFunction(.true.)

    if (verbosity.gt.0) call writemeamdatapoints(6,.false.)

    !call writederivedquantities(6)
    open(61,file='fitted_quantities.out')
    write(61,*) 'Variances of energies, forces and stress tensor components:'
    write(61,*) varEn,varFrc,varStr
    write(61,*) 'rms error on energies=',funcen
    write(61,*) 'rms error on forces=',funcforc
    write(61,*) 'rms error on stresses=',funcstr
    write(61,*)
    call writemeamdatapoints(61,.true.)
    close(61)

    object_func = F

    ! ANDY added 26/08/2022 to get stage 7_ of APD outputting .eam.alloy file
    if (writePot.eqv..true.) then

       if (lmax.eq.0) then
          !Open file for output of potential in LAMMPS format
          filename=""
          do isp=1,maxspecies
              write(string1,'(A2)') element(speciestoZ(isp))
              filename=trim(filename)//trim(string1)
          enddo
          string2='.eam.alloy'
          filename=trim(filename)//trim(string2)
          open(58,file=trim(adjustl(filename)))
          if (dlpolyOut) then
             open(70,file='TABEAM')
             open(71,file='FIELD')
          endif
       endif
                       
       !Output files (inc. LAMMPS, Camelon and potential plots
       call plotfunctions(.true.)

       if (lmax.eq.0) then
          close(58)
          if (dlpolyOut) then
             close(70)
             close(71)
          endif
       endif

       if (lmax.eq.3) then
          !LAMMPS MEAM potentials for use with Prashanth's altered
          !LAMMPS
          filename=""
          do isp=1,maxspecies
              write(string1,'(A2)') element(speciestoZ(isp))
              filename=trim(filename)//trim(string1)
          enddo
          string2='library.rfmeam.'
          filename=trim(string2)//trim(filename)
          open(2,file=trim(adjustl(filename)))
          call writemeamplammps
          close(2)
       endif

    endif

end subroutine output_py









subroutine start_mpi
    use m_mpi_info
 
    implicit none
  
    ! Header for MPI
    include 'mpif.h'
  
    call MPI_init(ierr)

end subroutine start_mpi


subroutine end_mpi
    use m_mpi_info
 
    implicit none
  
    ! Header for MPI
    include 'mpif.h'
    call mpi_finalize(ierr)
end subroutine end_mpi


subroutine p_setup

   ! This subroutine initialized MPI and the variables
   ! that are constant throughout the application
 
   use m_mpi_info
   use m_generalinfo

   implicit none
 
   ! Header for MPI
   include 'mpif.h'
 

   !if (MCMC.eqv..false.) then          ! MCMC Flag needs to be added
   ! call MPI_init(ierr)
   !end if
   !call MPI_init(ierr)
   call MPI_comm_size(MPI_COMM_WORLD, nprocs, ierr) ! sets nprocs to the amount
                                                    ! of processes running
   call MPI_comm_rank(MPI_COMM_WORLD, procid, ierr) ! sets procid to the current
                                                    ! process's ID. It procid=0
                                                    ! this is the main thread
 
   if (procid.eq.0) then
     !print *, '[MPI Info] Running MPI with ',nprocs,' nodes'
   endif
 
end subroutine p_setup
 

subroutine deallocateAllocated

    use m_mpi_info
    use m_filenames
    use m_geometry
    use m_datapoints
    use m_optimization
    use m_meamparameters
    use m_generalinfo
    !    use m_filenames
    use m_atomproperties
    use m_observables
    use m_electrondensity

    ! added these to cater for my extended list below
    use m_objectiveFunction
    use m_screening
    use m_objectiveFunctionShared
    use m_plotfiles
    use m_neighborlist
    use m_poscar

    implicit none
    
    include 'mpif.h'
 
    if (allocated(dfjlij_dxyz)) deallocate(dfjlij_dxyz)
    if (allocated(d2fjlij_dxyz_dpara)) deallocate(d2fjlij_dxyz_dpara)
    if (allocated(d2rhol_dxyz_dpara)) deallocate(d2rhol_dxyz_dpara)
    if (allocated(dmeamt_dxyz)) deallocate(dmeamt_dxyz)
    if (allocated(d2meamt_dxyz_dmeamtau)) deallocate(d2meamt_dxyz_dmeamtau)
    if (allocated(d2meamt_dxyz_dpara)) deallocate(d2meamt_dxyz_dpara)
    if (allocated(targetfiles)) deallocate(targetfiles)
    if (allocated(strucnames)) deallocate(strucnames)
    if (allocated(computeForces)) deallocate(computeForces)
    if (allocated(weightsEn)) deallocate(weightsEn)
    if (allocated(weightsFr)) deallocate(weightsFr)
    if (allocated(weightsSt)) deallocate(weightsSt)
    if (allocated(rlxstruc)) deallocate(rlxstruc)
    if (allocated(computeForces_backup)) deallocate(computeForces_backup)
    if (allocated(freeenergy)) deallocate(freeenergy)
    if (allocated(istrucInFile)) deallocate(istrucInFile)
    if (allocated(DFTdata)) deallocate(DFTdata)
    if (allocated(refenergy)) deallocate(refenergy)
    if (allocated(meamrhodecay_negvals)) deallocate(meamrhodecay_negvals)
    if (allocated(pairpotparameter_negvals)) deallocate(pairpotparameter_negvals)
    if (allocated(bestoptfuncs)) deallocate(bestoptfuncs)
    if (allocated(timeoptfuncHr)) deallocate(timeoptfuncHr)
    if (allocated(timeoptfuncMin)) deallocate(timeoptfuncMin)
    if (allocated(fjlij)) deallocate(fjlij)
    if (allocated(dfjlij_dpara)) deallocate(dfjlij_dpara)
    if (allocated(rhol)) deallocate(rhol)
    if (allocated(drhol_dpara)) deallocate(drhol_dpara)
    if (allocated(drhol_dxyz)) deallocate(drhol_dxyz)
    if (allocated(meam_t)) deallocate(meam_t)
    if (allocated(dmeamt_dmeamtau)) deallocate(dmeamt_dmeamtau)
    if (allocated(dmeamt_dpara)) deallocate(dmeamt_dpara)
    if (allocated(daux1_dpara)) deallocate(daux1_dpara)
    if (allocated(daux2_dpara)) deallocate(daux2_dpara)
    if (allocated(daux2a_dpara)) deallocate(daux2a_dpara)
    if (allocated(daux3a_dxyz)) deallocate(daux3a_dxyz)
    if (allocated(d2aux2_dxyz_dpara)) deallocate(d2aux2_dxyz_dpara)
    if (allocated(d2aux2a_dxyz_dpara)) deallocate(d2aux2a_dxyz_dpara)
    if (allocated(daux2_dxyz)) deallocate(daux2_dxyz)
    if (allocated(daux3_dpara)) deallocate(daux3_dpara)
    if (allocated(daux3a_dpara)) deallocate(daux3a_dpara)
    if (allocated(daux3_dxyz)) deallocate(daux3_dxyz)
    if (allocated(d2aux3_dxyz_dpara)) deallocate(d2aux3_dxyz_dpara)
    if (allocated(d2aux3a_dxyz_dpara)) deallocate(d2aux3a_dxyz_dpara)
    if (allocated(d2aux1_dxyz_dpara)) deallocate(d2aux1_dxyz_dpara)
    if (allocated(rho_i)) deallocate(rho_i)
    if (allocated(drhoi_dpara)) deallocate(drhoi_dpara)
    if (allocated(drhoi_dmeamtau)) deallocate(drhoi_dmeamtau)
    if (allocated(drhoi_dxyz)) deallocate(drhoi_dxyz)
    if (allocated(d2rhoi_dxyz_dpara)) deallocate(d2rhoi_dxyz_dpara)
    if (allocated(d2rhoi_dxyz_dmeamtau)) deallocate(d2rhoi_dxyz_dmeamtau)
    if (allocated(meam_f)) deallocate(meam_f)
    if (allocated(dmeamf_dpara)) deallocate(dmeamf_dpara)
    if (allocated(dmeamf_dmeame0)) deallocate(dmeamf_dmeame0)
    if (allocated(dmeamf_dmeamrho0)) deallocate(dmeamf_dmeamrho0)
    if (allocated(dmeamf_dmeamemb3)) deallocate(dmeamf_dmeamemb3) 
    if (allocated(dmeamf_dmeamemb4)) deallocate(dmeamf_dmeamemb4) 
    if (allocated(dmeamf_dmeamtau)) deallocate(dmeamf_dmeamtau)
    if (allocated(dmeamf_dxyz)) deallocate(dmeamf_dxyz)
    if (allocated(d2meamf_dxyz_dpara)) deallocate(d2meamf_dxyz_dpara)
    if (allocated(d2meamf_dxyz_dmeame0)) deallocate(d2meamf_dxyz_dmeame0)
    if (allocated(d2meamf_dxyz_dmeamrho0)) deallocate(d2meamf_dxyz_dmeamrho0)
    if (allocated(d2meamf_dxyz_dmeamemb3)) deallocate(d2meamf_dxyz_dmeamemb3)
    if (allocated(d2meamf_dxyz_dmeamemb4)) deallocate(d2meamf_dxyz_dmeamemb4)
    if (allocated(d2meamf_dxyz_dmeamtau)) deallocate(d2meamf_dxyz_dmeamtau)
    if (allocated(meam_paire)) deallocate(meam_paire)
    if (allocated(dsummeamf_dmeamtau)) deallocate(dsummeamf_dmeamtau)
    if (allocated(dsummeamf_dmeame0)) deallocate(dsummeamf_dmeame0)
    if (allocated(dsummeamf_dmeamemb4)) deallocate(dsummeamf_dmeamemb4) 
    if (allocated(dsummeam_paire_dpara_dummy)) deallocate(dsummeam_paire_dpara_dummy) 
    if (allocated(dsummeamf_dmeamtau_ref)) deallocate(dsummeamf_dmeamtau_ref)
    if (allocated(dsummeamf_dmeame0_ref)) deallocate(dsummeamf_dmeame0_ref)
    if (allocated(dsummeamf_dmeamemb4_ref)) deallocate(dsummeamf_dmeamemb4_ref)
    if (allocated(dintEn_fit_dmeamtau)) deallocate(dintEn_fit_dmeamtau)
    if (allocated(dintEn_fit_dmeame0)) deallocate(dintEn_fit_dmeame0) 
    if (allocated(dintEn_fit_dmeamemb4)) deallocate(dintEn_fit_dmeamemb4) 
    if (allocated(dsummeamf_dpara)) deallocate(dsummeamf_dpara)
    if (allocated(dsummeamf_dmeamrho0)) deallocate(dsummeamf_dmeamrho0)
    if (allocated(dsummeamf_dmeamemb3)) deallocate(dsummeamf_dmeamemb3)
    if (allocated(dsummeam_paire_dpara)) deallocate(dsummeam_paire_dpara)
    if (allocated(dsummeamf_dpara_ref)) deallocate(dsummeamf_dpara_ref) 
    if (allocated(dsummeamf_dmeamrho0_ref)) deallocate(dsummeamf_dmeamrho0_ref) 
    if (allocated(dsummeamf_dmeamemb3_ref)) deallocate(dsummeamf_dmeamemb3_ref)
    if (allocated(dsummeam_paire_dpara_ref)) deallocate(dsummeam_paire_dpara_ref)
    if (allocated(dintEn_fit_draddens)) deallocate(dintEn_fit_draddens)
    if (allocated(dintEn_fit_dmeamrho0)) deallocate(dintEn_fit_dmeamrho0) 
    if (allocated(dintEn_fit_dmeamemb3)) deallocate(dintEn_fit_dmeamemb3) 
    if (allocated(dintEn_fit_dpairpot)) deallocate(dintEn_fit_dpairpot) 
    if (allocated(q)) deallocate(q) 
    if (allocated(optmeamtau)) deallocate(optmeamtau)
    if (allocated(noptpairpotCoeff)) deallocate(noptpairpotCoeff)
    if (allocated(noptpairpotCutoff)) deallocate(noptpairpotCutoff)
    if (allocated(noptdensityCoeff)) deallocate(noptdensityCoeff)
    if (allocated(noptdensityCutoff)) deallocate(noptdensityCutoff) 
    if (allocated(ioptpairpotCoeff)) deallocate(ioptpairpotCoeff) 
    if (allocated(ioptpairpotCutoff)) deallocate(ioptpairpotCutoff)
    if (allocated(ioptdensityCoeff)) deallocate(ioptdensityCoeff)
    if (allocated(ioptdensityCutoff)) deallocate(ioptdensityCutoff)
    if (allocated(ncutoffDens)) deallocate(ncutoffDens)
    if (allocated(ncutoffPairpot)) deallocate(ncutoffPairpot)
    if (allocated(gxyz_backup)) deallocate(gxyz_backup)
    if (allocated(daux2a_dxyz)) deallocate(daux2a_dxyz) 

    ! below I did a copy paste jobs on all 'allocatables' in the fortran code,
    ! then edited with vi. includes some of those above as well (can edit these
    ! out later)
    !if (allocated(NITYP)) deallocate(NITYP)
    !if (allocated(POSION)) deallocate(POSION)
    !if (allocated(prevForce)) deallocate(prevForce)
    !if (allocated(dgamma_dpara)) deallocate(dgamma_dpara)
    !if (allocated(atom_tested)) deallocate(atom_tested)
    !if (allocated(zz_tmp)) deallocate(zz_tmp)
    !if (allocated(coords_tmp)) deallocate(coords_tmp)
    !if (allocated(numsepnsinbin)) deallocate(numsepnsinbin)
    !if (allocated(tofileSml)) deallocate(tofileSml)
    !if (allocated(words)) deallocate(words)
    if (allocated(freeenergy)) deallocate(freeenergy)
    if (allocated(rlxstruc)) deallocate(rlxstruc)
    if (allocated(truedata)) deallocate(truedata)
    if (allocated(optforce)) deallocate(optforce)
    if (allocated(fjlij)) deallocate(fjlij)
    if (allocated(daux1_dxyz)) deallocate(daux1_dxyz)
    if (allocated(targetfiles)) deallocate(targetfiles)
    if (allocated(DFTdata)) deallocate(DFTdata)
    if (allocated(gn_inequivalentsites)) deallocate(gn_inequivalentsites)
    if (allocated(gxyz)) deallocate(gxyz)
    if (allocated(strWghtPar)) deallocate(strWghtPar)
    if (allocated(start_i_stored)) deallocate(start_i_stored)
    if (allocated(cmin)) deallocate(cmin)
    if (allocated(cmax)) deallocate(cmax)
    if (allocated(meamtau)) deallocate(meamtau)
    if (allocated(meamrhodecay)) deallocate(meamrhodecay)
    if (allocated(meame0)) deallocate(meame0)
    if (allocated(meamrho0)) deallocate(meamrho0)
    if (allocated(meamemb3)) deallocate(meamemb3)
    if (allocated(meamemb4)) deallocate(meamemb4)
    if (allocated(rs)) deallocate(rs)
    if (allocated(rc)) deallocate(rc)
    if (allocated(enconst)) deallocate(enconst)
    if (allocated(dmeamt_dmeamtau)) deallocate(dmeamt_dmeamtau)
    if (allocated(pairpotparameter)) deallocate(pairpotparameter)
    if (allocated(pot_samespecies)) deallocate(pot_samespecies)
    if (allocated(typethi)) deallocate(typethi)
    if (allocated(r_pairpotXXarr)) deallocate(r_pairpotXXarr)
    if (allocated(NrhoLmps)) deallocate(NrhoLmps)
    if (allocated(embfuncSpline)) deallocate(embfuncSpline)
    if (allocated(useSpline1PartFns)) deallocate(useSpline1PartFns)
    if (allocated(q)) deallocate(q)
    if (allocated(send_arr)) deallocate(send_arr)
    if (allocated(recv_arr)) deallocate(recv_arr)
    if (allocated(recv_all_1darr)) deallocate(recv_all_1darr)
    if (allocated(send_all_1darr)) deallocate(send_all_1darr)
    if (allocated(send_all_2darr)) deallocate(send_all_2darr)
    if (allocated(recv_all_2darr)) deallocate(recv_all_2darr)
    if (allocated(recv_draddens_3darr)) deallocate(recv_draddens_3darr)
    if (allocated(send_draddens_3darr)) deallocate(send_draddens_3darr)
    if (allocated(recv_dpairpot_3darr)) deallocate(recv_dpairpot_3darr)
    if (allocated(send_dpairpot_3darr)) deallocate(send_dpairpot_3darr)
    if (allocated(send_noptdensitycoeff)) deallocate(send_noptdensitycoeff)
    if (allocated(recv_noptdensitycoeff)) deallocate(recv_noptdensitycoeff)
    if (allocated(recv_fitdata)) deallocate(recv_fitdata)
    if (allocated(species)) deallocate(species)
    if (allocated(xyz)) deallocate(xyz)
    if (allocated(dF_dmeamtau)) deallocate(dF_dmeamtau)
    if (allocated(dF_dmeame0)) deallocate(dF_dmeame0)
    if (allocated(dF_dpairpot)) deallocate(dF_dpairpot)
    if (allocated(dF_dpara)) deallocate(dF_dpara)
    if (allocated(dF_dpoptStore)) deallocate(dF_dpoptStore)
    if (allocated(dFen_dmeamtau)) deallocate(dFen_dmeamtau)
    if (allocated(dFen_draddens)) deallocate(dFen_draddens)
    if (allocated(dFen_dmeame0)) deallocate(dFen_dmeame0)
    if (allocated(dFen_dpairpot)) deallocate(dFen_dpairpot)
    if (allocated(dFen_denconst)) deallocate(dFen_denconst)
    if (allocated(dFen2_dmeamtau)) deallocate(dFen2_dmeamtau)
    if (allocated(dFen2_draddens)) deallocate(dFen2_draddens)
    if (allocated(dFen2_dmeame0)) deallocate(dFen2_dmeame0)
    if (allocated(dFen2_dpairpot)) deallocate(dFen2_dpairpot)
    if (allocated(dFfrc_dmeamtau)) deallocate(dFfrc_dmeamtau)
    if (allocated(dFfrc_draddens)) deallocate(dFfrc_draddens)
    if (allocated(dFfrc_dmeame0)) deallocate(dFfrc_dmeame0)
    if (allocated(dFfrc_dmeamrho0)) deallocate(dFfrc_dmeamrho0)
    if (allocated(dFfrc_dmeamemb3)) deallocate(dFfrc_dmeamemb3)
    if (allocated(dFfrc_dmeamemb4)) deallocate(dFfrc_dmeamemb4)
    if (allocated(dFfrc_dpairpot)) deallocate(dFfrc_dpairpot)
    if (allocated(dFstr_dmeamtau)) deallocate(dFstr_dmeamtau)
    if (allocated(dFstr_draddens)) deallocate(dFstr_draddens)
    if (allocated(dFstr_dmeame0)) deallocate(dFstr_dmeame0)
    if (allocated(dFstr_dmeamrho0)) deallocate(dFstr_dmeamrho0)
    if (allocated(dFstr_dmeamemb3)) deallocate(dFstr_dmeamemb3)
    if (allocated(dFstr_dmeamemb4)) deallocate(dFstr_dmeamemb4)
    if (allocated(dFstr_dpairpot)) deallocate(dFstr_dpairpot)
    if (allocated(dFcutoffPen_draddens)) deallocate(dFcutoffPen_draddens)
    !if (allocated(dFlowdensPen_draddens)) deallocate(dFlowdensPen_draddens)
    if (allocated(dsummeamf_dmeamtau)) deallocate(dsummeamf_dmeamtau)
    if (allocated(dintEn_fit_dmeamtau)) deallocate(dintEn_fit_dmeamtau)
    if (allocated(dsummeamf_dmeamtau_ref)) deallocate(dsummeamf_dmeamtau_ref)
    if (allocated(forces)) deallocate(forces)
    if (allocated(dstressTensor_dpairpot)) deallocate(dstressTensor_dpairpot)
    if (allocated(freep)) deallocate(freep)
    if (allocated(optmeamtau)) deallocate(optmeamtau)
    if (allocated(noptpairpotCoeff)) deallocate(noptpairpotCoeff)
    if (allocated(ncutoffDens)) deallocate(ncutoffDens)
    if (allocated(cutoffDens)) deallocate(cutoffDens)
    if (allocated(positivep)) deallocate(positivep)
    if (allocated(istrucInFile)) deallocate(istrucInFile)
    if (allocated(p)) deallocate(p)
    if (allocated(fitdata)) deallocate(fitdata)
    if (allocated(bestfitdata)) deallocate(bestfitdata)
    if (allocated(summeam_paireStr)) deallocate(summeam_paireStr)
    if (allocated(weightsEn)) deallocate(weightsEn)
    if (allocated(timeoptfuncHr)) deallocate(timeoptfuncHr)
    if (allocated(bestoptfuncs)) deallocate(bestoptfuncs)
    !if (allocated(pairpotStr)) deallocate(pairpotStr)
    if (allocated(meamtau_minorder)) deallocate(meamtau_minorder)
    if (allocated(meamrhodecay_minradius)) deallocate(meamrhodecay_minradius)
    if (allocated(z)) deallocate(z)
    if (allocated(coordinates)) deallocate(coordinates)
    if (allocated(screening)) deallocate(screening)
    if (allocated(dmeam_paire_dxyz)) deallocate(dmeam_paire_dxyz)
    if (allocated(d2meam_paire_dxyz_dpara)) deallocate(d2meam_paire_dxyz_dpara)

    if (allocated(forces)) deallocate(forces)
    if (allocated(dforce_dpairpot)) deallocate(dforce_dpairpot)
    if (allocated(dforce_draddens)) deallocate(dforce_draddens)
    if (allocated(dforce_dmeamtau)) deallocate(dforce_dmeamtau)
    if (allocated(dforce_dmeame0)) deallocate(dforce_dmeame0)
    if (allocated(dforce_dmeamrho0)) deallocate(dforce_dmeamrho0)
    if (allocated(dforce_dmeamemb3)) deallocate(dforce_dmeamemb3)
    if (allocated(dforce_dmeamemb4)) deallocate(dforce_dmeamemb4)
    if (allocated(dstressTensor_draddens)) deallocate(dstressTensor_draddens)
    if (allocated(dstressTensor_dmeamtau)) deallocate(dstressTensor_dmeamtau)
    if (allocated(dstressTensor_dmeame0)) deallocate(dstressTensor_dmeame0)
    if (allocated(dstressTensor_dmeamrho0)) deallocate(dstressTensor_dmeamrho0)
    if (allocated(dstressTensor_dmeamemb3)) deallocate(dstressTensor_dmeamemb3)
    if (allocated(dstressTensor_dmeamemb4)) deallocate(dstressTensor_dmeamemb4)


    !if (allocated(acopy)) deallocate(acopy)
    !if (allocated(rho_i_backup)) deallocate(rho_i_backup)
    !if (allocated(rhol_prev)) deallocate(rhol_prev)
    !if (allocated(meam_t_prev)) deallocate(meam_t_prev)
    !if (allocated(meam_paire_prev)) deallocate(meam_paire_prev)
    !if (allocated(dsummeam_paire_dpara_prev)) deallocate(dsummeam_paire_dpara_prev)
    !if (allocated(dmeam_paire_dxyz_prev)) deallocate(dmeam_paire_dxyz_prev)
    !if (allocated(d2meam_paire_dxyz_dpara_prev)) deallocate(d2meam_paire_dxyz_dpara_prev)
    !if (allocated(fjlij2)) deallocate(fjlij2)
    !if (allocated(dfjlij_dxyz_prev)) deallocate(dfjlij_dxyz_prev)
    !if (allocated(d2fjlij_dxyz_dpara_prev)) deallocate(d2fjlij_dxyz_dpara_prev)
    !if (allocated(rhol_prev)) deallocate(rhol_prev)
    !if (allocated(meam_t_prev)) deallocate(meam_t_prev)
    !if (allocated(dmeamt_dpara_prev)) deallocate(dmeamt_dpara_prev)
    !if (allocated(drhol_dxyz_prev)) deallocate(drhol_dxyz_prev)
    !if (allocated(drhol_dpara_prev)) deallocate(drhol_dpara_prev)
    !if (allocated(d2rhol_dxyz_dpara_prev)) deallocate(d2rhol_dxyz_dpara_prev)
    !if (allocated(rho_i_prev)) deallocate(rho_i_prev)
    !if (allocated(meam_f_prev)) deallocate(meam_f_prev)
    !if (allocated(popt)) deallocate(popt)
    !if (allocated(vv)) deallocate(vv)
    !if (allocated(signflipped)) deallocate(signflipped)
    !if (allocated(x)) deallocate(x)
    !if (allocated(words)) deallocate(words)
    !if (allocated(meamtau_dummy)) deallocate(meamtau_dummy)
    !if (allocated(truedata_readin)) deallocate(truedata_readin)
    !if (allocated(coulombForce)) deallocate(coulombForce)
    !if (allocated(words)) deallocate(words)
    !if (allocated(nconf)) deallocate(nconf)
    !if (allocated(files)) deallocate(files)
    !if (allocated(seed)) deallocate(seed)
    !if (allocated(ac)) deallocate(ac)
    if (allocated(truedata)) deallocate(truedata)
    if (allocated(optforce)) deallocate(optforce)
    !if (allocated(truedata_readin)) deallocate(truedata_readin)
    !if (allocated(coulombForce)) deallocate(coulombForce)
    if (allocated(summeam_paireStr)) deallocate(summeam_paireStr)
    !if (allocated(meamtau_dummy)) deallocate(meamtau_dummy)

    ! in the above have commented out some which apparently do not have a type :
    ! (if these aren't in modules (i've checked -they aren't) perhaps I should
    ! make sure they are deallocated in their subroutines?)
    !NITYP  <- not allocatable anymore
    !POSION -"-
    !prevForce <- added a deallocate
    !dgamma_dpara <- added a deallocate
    !atom_tested < has a deallocate
    !zz_tmp <- no issue
    !coords_tmp <- no issue
    !numsepnsinbin <- added a deallocate
    !tofileSml <- -"-
    !words <- added deallocates where necessary

end subroutine deallocateAllocated


subroutine p_finalize
 
    use m_mpi_info
    use m_filenames
    use m_geometry
    use m_datapoints
    use m_optimization
    use m_meamparameters
    use m_generalinfo
    !    use m_filenames
    use m_atomproperties
    use m_observables
    use m_electronDensity

    implicit none
    
    include 'mpif.h'
  
    !after the subroutine is finished, the buffer arrays used to synchronize the results can be deallocated

    if (allocated(send_arr)) deallocate(send_arr)
    if (allocated(recv_arr)) deallocate(recv_arr)
    if (allocated(send_all_1darr)) deallocate(send_all_1darr)
    if (allocated(recv_all_1darr)) deallocate(recv_all_1darr)
    if (allocated(send_all_2darr)) deallocate(send_all_2darr)
    if (allocated(recv_all_2darr)) deallocate(recv_all_2darr)
    if (allocated(send_draddens_3darr)) deallocate(send_draddens_3darr)
    if (allocated(recv_draddens_3darr)) deallocate(recv_draddens_3darr)
    if (allocated(send_dpairpot_3darr)) deallocate(send_dpairpot_3darr)
    if (allocated(recv_dpairpot_3darr)) deallocate(recv_dpairpot_3darr)
    if (allocated(recv_fitdata)) deallocate(recv_fitdata)

    !Also need to deallocate other arrays
    !Note, not all of these arrays will have been allocated depending on the run type, hence they are checked first before
    !deallocating
    call deallocateAllocated

    ! Kill all workers
    if (procid.eq.0) then
        p_killWorkers = .true.
        call MPI_bcast(p_killWorkers, 1, MPI_LOGICAL, 0, MPI_COMM_WORLD, ierr)
    endif
    
    !MPI is finalized. The worker threads will be terminated before the main thread ends the program.
    !call mpi_finalize(ierr)
  
    !stop
end subroutine p_finalize


 
subroutine p_allocateArrays
 
   ! This subroutine allocates all arrays used to send around data between nodes
 
   use m_optimization
   use m_atomproperties
   use m_meamparameters
   use m_mpi_info
   use m_datapoints
 
   implicit none

   ! allocating send and receiving buffer arrays for single variables.
   ! There are 10 reals to send, so the length of the array is 10
   allocate(send_arr(10))
   allocate(recv_arr(10))
 
   ! allocating send and receive arrays for the communication of 1d arrays. The length is dependent on
   ! whether objFuncType=2:    if objFuncType =    2 --> all 1d dFen2_... should be allocated
   !                           if objFuncType !=   2 --> all 1d dFen2_... should NOT be allocated
   if (objFuncType.eq.2) then
       allocate(recv_all_1darr(17*maxspecies))
       allocate(send_All_1darr(17*maxspecies))
   else
       allocate(recv_all_1darr(13*maxspecies))
       allocate(send_All_1darr(13*maxspecies))
   endif
 
   ! Maybe add "if((noopt.eqv..false.)" later on?
   ! allocating send and receive arrays for the communication of 2d arrays. The length is dependent on
   ! -whether objFuncType=2:   if objFuncType =    2 --> all 2d dFen2_... should be allocated
   !                           if objFuncType !=   2 --> all 2d dFen2_... should NOT be allocated
   ! -whether lmax > 0:        if lmax !>          0 -->
   !                           if lmax >           0 -->
    if ((objFuncType.eq.2) .AND. (lmax .gt.0)) then
        allocate(send_all_2darr(maxspecies*4*lmax))
        allocate(recv_all_2darr(maxspecies*4*lmax))
    elseif (objFuncType.eq.2) then
        allocate(send_all_2darr(maxspecies*2*lmax))
        allocate(recv_all_2darr(maxspecies*2*lmax))
    elseif (lmax.gt.0) then
        allocate(send_all_2darr(maxspecies*3*lmax))
        allocate(recv_all_2darr(maxspecies*3*lmax))
    else
        allocate(send_all_2darr(maxspecies*lmax))
        allocate(recv_all_2darr(maxspecies*lmax))
    endif
 
 
   ! allocating send and receive arrays for the communication of 3d arrays. The length is dependent on
   ! whether objFuncType=2:    if objFuncType =    2 --> all 3d dFen2_... should be allocated
   !                           if objFuncType !=   2 --> all 3d dFen2_... should NOT be allocated
   if (objFuncType.eq.2) then
       allocate(recv_draddens_3darr(12*(lmax+1)*maxspecies*4))
       allocate(send_draddens_3darr(12*(lmax+1)*maxspecies*4))
       allocate(recv_dpairpot_3darr(32*maxspecies*maxspecies*4))
       allocate(send_dpairpot_3darr(32*maxspecies*maxspecies*4))
   else
       allocate(recv_draddens_3darr(12*(lmax+1)*maxspecies*3))
       allocate(send_draddens_3darr(12*(lmax+1)*maxspecies*3))
       allocate(recv_dpairpot_3darr(32*maxspecies*maxspecies*3))
       allocate(send_dpairpot_3darr(32*maxspecies*maxspecies*3))
   endif
 
   ! Allocate variables needed for MPI communication
   allocate(q(np))
   
   allocate(recv_fitdata(ndatapoints))

end subroutine p_allocateArrays

 
subroutine p_workerThread
   ! This routine contains the main loop of the worker threads
   ! Initialize the workers once
   ! This includes allocating all required arrays
     
   call p_workerSetup
 
   ! The worker threads keep on executing
   ! the objectiveFunction indefinitely
   ! Inside the objective function they will
   ! wait to receive data from the main thread
   do
       call objectiveFunction(.false.)
   end do
   ! Once the main thread terminates,
   ! the worker threads will be killed automatically
   ! This is not a problem, since the workers have
   ! already shared any important data the might contain by then
 
end subroutine p_workerThread
 
 
subroutine p_workerSetup
   ! MvE
   ! The workerInit subroutine is used to allocate
   ! required arrays for the worker processes,
   ! needed in the objectivfunction routine
 
   use m_datapoints
   use m_geometry
   use m_optimization
   use m_generalinfo
   use m_meamparameters
   use m_filenames
   use m_poscar
   use m_electrondensity
   use m_atomproperties
   use m_plotfiles
   use m_objectiveFunction
   use m_observables
   use m_mpi_info
   call allocateArrays
   
   ! copied from optimizeParameters
   allocate(dF_dmeamtau(1:lmax,maxspecies))
   allocate(dF_draddens(12,0:lmax,maxspecies))
   allocate(dF_dmeame0(maxspecies),dF_dmeamrho0(maxspecies), &
       dF_dmeamemb3(maxspecies),dF_dmeamemb4(maxspecies))
   allocate(dF_dpairpot(32,maxspecies,maxspecies))
   allocate(dF_denconst(maxspecies))
   allocate(dF_dpara(np))
   !Also for objective function components
   allocate(dFen_dmeamtau(1:lmax,maxspecies))
   allocate(dFen_draddens(12,0:lmax,maxspecies))
   allocate(dFen_dmeame0(maxspecies),dFen_dmeamrho0(maxspecies), &
       dFen_dmeamemb3(maxspecies),dFen_dmeamemb4(maxspecies))
   allocate(dFen_dpairpot(32,maxspecies,maxspecies))
   allocate(dFen_denconst(maxspecies))
   if (objFuncType.eq.2) then
      !The following are for Tom's objective function
      allocate(dFen2_dmeamtau(1:lmax,maxspecies))
      allocate(dFen2_draddens(12,0:lmax,maxspecies))
      allocate(dFen2_dmeame0(maxspecies),dFen2_dmeamrho0(maxspecies), &
          dFen2_dmeamemb3(maxspecies),dFen2_dmeamemb4(maxspecies))
      allocate(dFen2_dpairpot(32,maxspecies,maxspecies))
   endif
 
   if (lmax.gt.0) then
      allocate(dFfrc_dmeamtau(1:lmax,maxspecies))
   endif
   allocate(dFfrc_draddens(12,0:lmax,maxspecies))
   allocate(dFfrc_dmeame0(maxspecies),dFfrc_dmeamrho0(maxspecies), &
       dFfrc_dmeamemb3(maxspecies),dFfrc_dmeamemb4(maxspecies))
   allocate(dFfrc_dpairpot(32,maxspecies,maxspecies))
   if (lmax.gt.0) then
      allocate(dFstr_dmeamtau(1:lmax,maxspecies))
   endif
   allocate(dFstr_draddens(12,0:lmax,maxspecies))
   allocate(dFstr_dmeame0(maxspecies),dFstr_dmeamrho0(maxspecies), &
       dFstr_dmeamemb3(maxspecies),dFstr_dmeamemb4(maxspecies))
   allocate(dFstr_dpairpot(32,maxspecies,maxspecies))
   allocate(dFcutoffPen_draddens(12,0:lmax,maxspecies), &
       dFcutoffPen_dpairpot(32,maxspecies,maxspecies)) !, &
       !dFlowDensPen_draddens(12,0:lmax,maxspecies))
 
   allocate(positivep(np),p_orig(np))
   if (contjob.eqv..false.) then
      allocate(p_saved(np,noptfuncstore))
   endif
 
   ! copied from some other function
   allocate( meamtau_minorder(1:lmax,maxspecies), &
       meamtau_maxorder(1:lmax,maxspecies), &
       meamrhodecay_minradius(0:lmax,maxspecies,maxspecies), &
       meamrhodecay_maxradius(0:lmax,maxspecies,maxspecies), &
       meamrhodecay_minorder(1:6,0:lmax,maxspecies,maxspecies), &
       meamrhodecay_maxorder(1:6,0:lmax,maxspecies,maxspecies), &
       meame0_minorder(maxspecies), meame0_maxorder(maxspecies), &
       meamrho0_minorder(maxspecies), meamrho0_maxorder(maxspecies), &
       meamemb3_minorder(maxspecies), meamemb3_maxorder(maxspecies), &
       meamemb4_minorder(maxspecies), meamemb4_maxorder(maxspecies), &
       pairpotparameter_minradius(maxspecies,maxspecies), &
       pairpotparameter_maxradius(maxspecies,maxspecies), &
       pairpotparameter_minorder(1:8,maxspecies,maxspecies), &
       pairpotparameter_maxorder(1:8,maxspecies,maxspecies), &
       minordervaluepairpot(1:16), &
       maxordervaluepairpot(1:16), &
       nfreeppairpot(maxspecies,maxspecies), &
       enconst_minorder(maxspecies), enconst_maxorder(maxspecies) )
 
   !Set the maxradius variables to zero (then only those 'in use' will be used
   !in the 'checkParas' subroutine).
   meamrhodecay_maxradius=0d0
   pairpotparameter_maxradius=0d0
 
   ! copied from meamfit.f90
   if (contjob.eqv..false.) then
      n_optfunc=1
      allocate(bestoptfuncs(noptfuncstore),timeoptfuncHr(noptfuncstore), &
          timeoptfuncMin(noptfuncstore))
   else
      n_optfunc=noptfuncstore+1
   endif
 
   call initialize_noptp
 
end subroutine p_workerSetup
 
  
subroutine p_beforeLoop(finalOutput)
 
   use m_datapoints
   use m_geometry
   use m_optimization
   use m_generalinfo
   use m_meamparameters
   use m_filenames
   use m_poscar
   use m_electrondensity
   use m_atomproperties
   use m_plotfiles
   use m_objectiveFunction
   use m_objectiveFunctionShared
   use m_observables
   use m_mpi_info
 
   implicit none
 
   ! Header for MPI
   include 'mpif.h'
   
   logical finalOutput
   integer iat
   
   !start_i and stop_i are the starting and stopping points for the processes to make sure each process does a part of the loop
   start_i = int(real(nstruct*procid)/real(nprocs)) + 1
   stop_i = int(real(nstruct*(procid+1))/real(nprocs))
 
   ! Make sure all structs are taken into account, giving the last process the remaining iterations
   if ((procid+1).eq.nprocs) then
       stop_i = nstruct
   endif
   
   ! Normally this is set to false
   ! When the main thread terminates however, it broadcasts true and all workers will terminate
   p_killWorkers = .false.
   call MPI_bcast(p_killWorkers, 1, MPI_LOGICAL, 0, MPI_COMM_WORLD, ierr)
   
   ! If we get the kill command from the main thread, stop here
   if (p_killWorkers) then
       call p_finalize
       stop
   endif
 
   ! Here, variables_to_p and p_to_variables are used to put the variables that need to be communicated to the workers,
   ! are communicated before the loop. Putting them into the vector p makes communication easier. Afterwards, p_to_variables
   ! is called to put the values back in the variables for the workers.
   if (procid.eq.0) then ! In the main thread we backup p and put the variables to be sent into p
       q(:) = p(:)
       call variables_to_p
   endif
 
   call MPI_bcast(p, np, MPI_DOUBLE, 0, MPI_COMM_WORLD, ierr)
 
   if (procid.eq.0) then ! In the main thread we restore p
       p(:) = q(:)
   else
       call p_to_variables
   endif
   
   ! For the workers finalOutput is always false.
   ! After this bcast however, p_finalOutput is true if (and only if) finalOutput was true in the main thread
   p_finalOutput = finalOutput
   call MPI_bcast(p_finalOutput, 1, MPI_LOGICAL, 0, MPI_COMM_WORLD, ierr)
 
   ! The workers do not start at the beginning of the loop over nstruct, so they're missing important data
   ! in the loop below this important data is calculated for all workers. 
   ! Note: it might be that some important variables are still missing. They can then be added in a similar fashion
   idatapoint=1
   do istr=1,start_i-1
         
     if (weightsEn(istr).ne.0d0) then
     
         if (refenergy(istr).ge.1) then
            !We only want the energy here, but we still need to call meamforce if
            !structures have been set up with force fitting in mind (see
            !'meamforce' subroutine for more info)
            if (computeForces(istr)) then
               call meamforce
            else
               call meamenergy
            endif
            !call meamenergy
            fitdata(idatapoint)=energy
            print *,' setting up ref energies for procid=',procid,' fitdata(',idatapoint,')=',fitdata(idatapoint)
            ref_datapoint(refenergy(istr))=idatapoint
            ref_strucnum(refenergy(istr))=istr
            nRef=MAX(nRef,refenergy(istr))
 
            if (noOpt.eqv..false.) then
               dsummeamf_dmeamtau_ref(refenergy(istr),:,:)=dsummeamf_dmeamtau(:,:)
               dsummeamf_dpara_ref(refenergy(istr),:,:,:,:)=dsummeamf_dpara(:,:,:,:)
               dsummeamf_dmeame0_ref(refenergy(istr),:)=dsummeamf_dmeame0(:)
               dsummeamf_dmeamrho0_ref(refenergy(istr),:)=dsummeamf_dmeamrho0(:)
               dsummeamf_dmeamemb3_ref(refenergy(istr),:)=dsummeamf_dmeamemb3(:)
               dsummeamf_dmeamemb4_ref(refenergy(istr),:)=dsummeamf_dmeamemb4(:)
               dsummeam_paire_dpara_ref(refenergy(istr),:,:,:)=dsummeam_paire_dpara(:,:,:)
            endif
       endif
       
       idatapoint = idatapoint + 1
     endif
         
     if (weightsFr(istr).gt.0d0) then
        idatapoint = idatapoint + 3*gn_forces(istr)
     endif
     
     if (weightsSt(istr).gt.0d0) then
        idatapoint = idatapoint + 6
     endif
     
   enddo    
 
end subroutine p_beforeLoop
 
 
subroutine p_synchronizeResults
 
   use m_datapoints
   use m_geometry
   use m_optimization
   use m_generalinfo
   use m_meamparameters
   use m_filenames
   use m_poscar
   use m_electrondensity
   use m_atomproperties
   use m_plotfiles
   use m_objectiveFunction
   use m_objectiveFunctionShared
   use m_observables
   use m_mpi_info
 
   implicit none
 
   ! Header for MPI
   include 'mpif.h'
   
   ! these variables are used when combining fitdata for the final output
   integer idatapoint_start, idatapoint_stop, fitdata_start_i, fitdata_stop_i, iat
 
   !filling the send array with the variables that need to be communicated
       send_arr(1) = funcforc
       send_arr(2) = funcen
       send_arr(3) = funcstr
       send_arr(4) = funcforcdenom
       send_arr(5) = funcendenom
       send_arr(6) = funcstrdenom
       send_arr(7) = Fen
       send_arr(8) = Ffrc
       send_arr(9) = avg_deltaE
       send_arr(10)= Fstr

       if (noOpt.eqv..false.) then
          !filling a 1d array with all the 1d arrays that need to be communicated
          if (objFuncType.eq.2) then
             send_all_1darr=[dFen_dmeame0,dFen_dmeamrho0,dFen_dmeamemb3,dFen_dmeamemb4,dFen_denconst,dFfrc_dmeame0,dFfrc_dmeamrho0,dFfrc_dmeamemb3,dFfrc_dmeamemb4,dFstr_dmeame0,dFstr_dmeamrho0,dFstr_dmeamemb3,dFstr_dmeamemb4,dFen2_dmeame0,dFen2_dmeamrho0,dFen2_dmeamemb3,dFen2_dmeamemb4]
          else
             send_all_1darr=[dFen_dmeame0,dFen_dmeamrho0,dFen_dmeamemb3,dFen_dmeamemb4,dFen_denconst,dFfrc_dmeame0,dFfrc_dmeamrho0,dFfrc_dmeamemb3,dFfrc_dmeamemb4,dFstr_dmeame0,dFstr_dmeamrho0,dFstr_dmeamemb3,dFstr_dmeamemb4]
          endif
 
          !Filling a 1d array with all the 2d arrays that need to be communicated
          if ((objFuncType.eq.2) .AND. (lmax .gt.0)) then
             Send_all_2darr = [dFen_dmeamtau, dFfrc_dmeamtau, dFstr_dmeamtau, dFen2_dmeamtau]
          elseif (objFuncType.eq.2) then
             send_all_2darr = [dFen_dmeamtau, dFen2_dmeamtau]
          elseif (lmax.gt.0) then
             send_all_2darr = [dFen_dmeamtau, dFfrc_dmeamtau, dFstr_dmeamtau]
          else
             send_all_2darr = [dFen_dmeamtau]
          endif
 
          !Filling a 1d array with all the 3d arrays that need to be communicated
          if (objFuncType.eq.2) then
             send_draddens_3darr=[dFen_draddens,dFfrc_draddens,dFstr_draddens,dFen2_draddens]
             send_dpairpot_3darr=[dFen_dpairpot,dFfrc_dpairpot,dFstr_dpairpot,dFen2_dpairpot]
          else
             send_draddens_3darr=[dFen_draddens,dFfrc_draddens,dFstr_draddens]
             send_dpairpot_3darr=[dFen_dpairpot,dFfrc_dpairpot,dFstr_dpairpot]
          endif
 
       endif
 
       !communicating the results. So the arrays send_arr, send_all_1darr, send_all_2d_arr, send_draddens_3darr, send_dpairpot_3darr are sent and received
       !for loop over all process id's
       do i_node = 1, nprocs-1
          !the process that currently sends his result enters this if statement
          if (i_node.eq.procid) then
 
             !Sending the array with the variables to other processes
             call MPI_Send(send_arr, 10, MPI_DOUBLE, 0,1, MPI_COMM_WORLD,ierr)
 
             if (noOpt.eqv..false.) then
                !Sending the array with the 1d arrays to other processes, its size is dependent on whether objfunctype=2 or not.
                if (objFuncType.eq.2) then
                    call MPI_Send(send_all_1darr,17*maxspecies,MPI_DOUBLE, 0, 1 ,MPI_COMM_WORLD, ierr)
                else
                    call MPI_Send(send_all_1darr,13*maxspecies,MPI_DOUBLE, 0,1,MPI_COMM_WORLD,ierr)
                endif
 
                !Sending the array with the 2d arrays to other processes
                if ((objFuncType.eq.2) .AND. (lmax .gt.0)) then
                    call MPI_Send(send_all_2darr,4*maxspecies*lmax,MPI_DOUBLE,0,1,MPI_COMM_WORLD,ierr)
                elseif (objFuncType.eq.2) then
                    call MPI_Send(send_all_2darr,2*maxspecies*lmax,MPI_DOUBLE,0,1,MPI_COMM_WORLD,ierr)
                elseif (lmax.gt.0) then
                    call MPI_Send(send_all_2darr,3*maxspecies*lmax,MPI_DOUBLE,0,1,MPI_COMM_WORLD,ierr)
                else
                    call MPI_Send(send_all_2darr,1*maxspecies*lmax,MPI_DOUBLE,0,1,MPI_COMM_WORLD,ierr)
                endif
 
                !Sending the two arrays with the 3d arrays to other processes
                if (objFuncType.eq.2) then
                    call MPI_Send(send_draddens_3darr,12*(lmax+1)*maxspecies*4,MPI_DOUBLE, 0,1,MPI_COMM_WORLD, ierr)
                    call MPI_Send(send_dpairpot_3darr,32*maxspecies*maxspecies*4,MPI_DOUBLE,0,1,MPI_COMM_WORLD, ierr)
                else
                    call MPI_Send(send_draddens_3darr,12*(lmax+1)*maxspecies*3,MPI_DOUBLE, 0,1,MPI_COMM_WORLD,ierr)
                    call MPI_Send(send_dpairpot_3darr,32*maxspecies*maxspecies*3,MPI_DOUBLE, 0,1,MPI_COMM_WORLD, ierr)
                endif
             endif
             
             !Synchronize fitdata points, because these are written to file
             if (p_finalOutput) then
               
                 call MPI_send(fitdata, ndatapoints, MPI_DOUBLE, 0, 1, MPI_COMM_WORLD, ierr)
               
             endif
 
          !all processes that receive in the current iteration enter the else statement
          elseif (procid.eq.0) then
             !Receiving the array with the variables from other processes
             call MPI_Recv(recv_arr, 10, MPI_DOUBLE, i_node, 1 ,MPI_COMM_WORLD,MPI_STATUS_IGNORE, ierr)
 
             if (noOpt.eqv..false.) then
                !Receiving the array with the 1d arrays to other processes, its size is dependent on whether objfunctype=2 or not.
                if (objFuncType.eq.2) then
                    call MPI_Recv(recv_all_1darr,17*maxspecies,MPI_DOUBLE, i_node,1,MPI_COMM_WORLD,MPI_STATUS_IGNORE, ierr)
                else
                    call MPI_Recv(recv_all_1darr,13*maxspecies,MPI_DOUBLE, i_node,1,MPI_COMM_WORLD,MPI_STATUS_IGNORE,ierr)
                endif
 
                !Receiving the array with the 2d arrays from other processes
                if ((objFuncType.eq.2) .AND. (lmax .gt.0)) then
                    call MPI_Recv(recv_all_2darr,4*maxspecies*lmax,MPI_DOUBLE,i_node,1,MPI_COMM_WORLD,MPI_STATUS_IGNORE,ierr)
                elseif (objFuncType.eq.2) then
                    call MPI_Recv(recv_all_2darr,2*maxspecies*lmax,MPI_DOUBLE,i_node,1,MPI_COMM_WORLD,MPI_STATUS_IGNORE,ierr)
                elseif (lmax.gt.0) then
                    call MPI_Recv(recv_all_2darr,3*maxspecies*lmax,MPI_DOUBLE,i_node,1,MPI_COMM_WORLD,MPI_STATUS_IGNORE,ierr)
                else
                    call MPI_Recv(recv_all_2darr,1*maxspecies*lmax,MPI_DOUBLE,i_node,1,MPI_COMM_WORLD,MPI_STATUS_IGNORE,ierr)
                endif
 
                !Receiving the two arrays with the 3d arrays from other processes
                if (objFuncType.eq.2) then
                   call MPI_Recv(recv_draddens_3darr,12*(lmax+1)*maxspecies*4,MPI_DOUBLE, i_node,1,MPI_COMM_WORLD,MPI_STATUS_IGNORE, ierr)
                   call MPI_Recv(recv_dpairpot_3darr,32*maxspecies*maxspecies*4,MPI_DOUBLE, i_node,1,MPI_COMM_WORLD,MPI_STATUS_IGNORE, ierr)
                else
                   call MPI_Recv(recv_draddens_3darr,12*(lmax+1)*maxspecies*3,MPI_DOUBLE, i_node,1,MPI_COMM_WORLD,MPI_STATUS_IGNORE,ierr)
                   call MPI_Recv(recv_dpairpot_3darr,32*maxspecies*maxspecies*3,MPI_DOUBLE, i_node,1,MPI_COMM_WORLD,MPI_STATUS_IGNORE, ierr)
                endif
             endif
             
             if (p_finalOutput) then
               
               fitdata_start_i = (nstruct/nprocs)*i_node + 1
               fitdata_stop_i = (nstruct/nprocs)*(i_node+1)
               print *,'fitdata_start_i=',fitdata_start_i
               print *,'fitdata_stop_i=',fitdata_stop_i
               print *,'nstruct=',nstruct,' nprocs=',nprocs,' i_node=',i_node
 
               if ((i_node+1).eq.nprocs) then
                   fitdata_stop_i = nstruct
               endif
               
               
               ! use these to figure out where to stop/start in the array when combining results from workers
               idatapoint = 0
               
               do istr=1,fitdata_start_i-1
                   if (weightsEn(istr).ne.0d0) then
                       idatapoint = idatapoint + 1
                   endif
                   if (weightsFr(istr).gt.0d0) then
                       do iat=1,gn_forces(istr)
                          idatapoint = idatapoint + 3
                       enddo
                   endif
                   if (weightsSt(istr).gt.0d0) then
                       idatapoint = idatapoint + 6
                   endif
               end do
               
               idatapoint_start = idatapoint + 1
               
               do istr=fitdata_start_i,fitdata_stop_i
                   if (weightsEn(istr).ne.0d0) then
                       idatapoint = idatapoint + 1
                   endif
                   if (weightsFr(istr).gt.0d0) then
                       do iat=1,gn_forces(istr)
                          idatapoint = idatapoint + 3
                       enddo
                   endif
                   if (weightsSt(istr).gt.0d0) then
                       idatapoint = idatapoint + 6
                   endif
               end do
               
               idatapoint_stop = idatapoint
               
               print *,'receiving from ', i_node, ' start=',idatapoint_start,', stop=',idatapoint_stop
               
               call MPI_recv(recv_fitdata, ndatapoints, MPI_DOUBLE, i_node, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE, ierr)
               
               fitdata(idatapoint_start:idatapoint_stop) = recv_fitdata(idatapoint_start:idatapoint_stop)
 
             endif
 
             !All the received results are added to the result of this process
             !first the results of the individual variables
             funcforc = funcforc + recv_arr(1)
             funcen = funcen + recv_arr(2)
             funcstr = funcstr + recv_arr(3)
             funcforcdenom = funcforcdenom + recv_arr(4)
             funcendenom = funcendenom + recv_arr(5)
             funcstrdenom = funcstrdenom + recv_arr(6)
             Fen = Fen + recv_arr(7)
             Ffrc = Ffrc + recv_arr(8)
             avg_deltaE = avg_deltaE + recv_arr(9)
             Fstr = Fstr + recv_arr(10)
 
             if (noOpt.eqv..false.) then
                !Second the results of the 1d arrays
                dFen_dmeame0(:) = dFen_dmeame0(:) + recv_all_1darr(1:maxspecies)
                dFen_dmeamrho0(:) = dFen_dmeamrho0(:) + recv_all_1darr(maxspecies+1:2*maxspecies)
                dFen_dmeamemb3(:) = dFen_dmeamemb3(:) + recv_all_1darr(2*maxspecies+1:3*maxspecies)
                dFen_dmeamemb4(:) = dFen_dmeamemb4(:) + recv_all_1darr(3*maxspecies+1:4*maxspecies)
                dFen_denconst(:) = dFen_denconst(:) + recv_all_1darr(4*maxspecies+1:5*maxspecies)
                dFfrc_dmeame0(:) = dFfrc_dmeame0(:) + recv_all_1darr(5*maxspecies+1:6*maxspecies)
                dFfrc_dmeamrho0(:) = dFfrc_dmeamrho0(:) + recv_all_1darr(6*maxspecies+1:7*maxspecies)
                dFfrc_dmeamemb3(:) = dFfrc_dmeamemb3(:) + recv_all_1darr(7*maxspecies+1:8*maxspecies)
                dFfrc_dmeamemb4(:) = dFfrc_dmeamemb4(:) + recv_all_1darr(8*maxspecies+1:9*maxspecies)
                dFstr_dmeame0(:) = dFstr_dmeame0(:) + recv_all_1darr(9*maxspecies+1:10*maxspecies)
                dFstr_dmeamrho0(:) = dFstr_dmeamrho0(:) + recv_all_1darr(10*maxspecies+1:11*maxspecies)
                dFstr_dmeamemb3(:) = dFstr_dmeamemb3(:) + recv_all_1darr(11*maxspecies+1:12*maxspecies)
                dFstr_dmeamemb4(:) = dFstr_dmeamemb4(:) + recv_all_1darr(12*maxspecies+1:13*maxspecies)
                if (objFuncType.eq.2) then
                    dFen2_dmeame0(:) = dFen2_dmeame0(:) + recv_all_1darr(13*maxspecies+1:14*maxspecies)
                    dFen2_dmeamrho0(:) = dFen2_dmeamrho0(:) + recv_all_1darr(14*maxspecies+1:15*maxspecies)
                    dFen2_dmeamemb3(:) = dFen2_dmeamemb3(:) + recv_all_1darr(15*maxspecies+1:16*maxspecies)
                    dFen2_dmeamemb4(:) = dFen2_dmeamemb4(:) + recv_all_1darr(16*maxspecies+1:17*maxspecies)
                endif
 
                !Third the results of the 2d arrays
                dFen_dmeamtau(:,:) = dFen_dmeamtau(:,:) + reshape((recv_all_2darr(1:maxspecies*lmax)),(/lmax,maxspecies/))
                if (lmax.gt.0) then
                    dFfrc_dmeamtau(:,:) = dFfrc_dmeamtau(:,:) + reshape((recv_all_2darr(maxspecies*lmax+1:2*maxspecies*lmax)),(/lmax,maxspecies/))
                    dFstr_dmeamtau(:,:) = dFstr_dmeamtau(:,:) + reshape((recv_all_2darr(2*maxspecies*lmax+1:3*maxspecies*lmax)),(/lmax,maxspecies/))
                    if (objFuncType.eq.2) then
                        dFen2_dmeamtau(:,:) = dFen2_dmeamtau(:,:) + reshape((recv_all_2darr(3*maxspecies*lmax+1:4*maxspecies*lmax)),(/lmax,maxspecies/))
                    endif
                else
                    if (objFuncType.eq.2) then
                        dFen2_dmeamtau(:,:) = dFen2_dmeamtau(:,:) + reshape ((recv_all_2darr(maxspecies*lmax+1:2*maxspecies*lmax)),(/lmax,maxspecies/))
                    endif
                endif
 
                !Fourth the results of the 3d arrays
                dFen_draddens(:,:,:) = dFen_draddens(:,:,:) + reshape((recv_draddens_3darr(1:12*(lmax+1)*maxspecies)), (/12,lmax+1,maxspecies/))
                dFfrc_draddens(:,:,:) = dFfrc_draddens(:,:,:) + reshape((recv_draddens_3darr(12*(lmax+1)*maxspecies+1:12*(lmax+1)*maxspecies*2)), (/12,lmax+1,maxspecies/))
                dFstr_draddens(:,:,:) = dFstr_draddens(:,:,:) + reshape((recv_draddens_3darr(12*(lmax+1)*maxspecies*2+1:12*(lmax+1)*maxspecies*3)), (/12,lmax+1,maxspecies/))
                if (objFuncType.eq.2) then
                    dFen2_draddens(:,:,:) = dFen2_draddens(:,:,:) + reshape((recv_draddens_3darr(12*(lmax+1)*maxspecies*3+1:12*(lmax+1)*maxspecies*4)), (/12,lmax+1,maxspecies/))
                endif
                dFen_dpairpot(:,:,:) = dFen_dpairpot(:,:,:) + reshape((recv_dpairpot_3darr(1:32*maxspecies*maxspecies)), (/32,maxspecies,maxspecies/))
                dFfrc_dpairpot(:,:,:) = dFfrc_dpairpot(:,:,:) + reshape((recv_dpairpot_3darr(32*maxspecies*maxspecies+1:32*maxspecies*maxspecies*2)), (/32,maxspecies,maxspecies/))
                dFstr_dpairpot(:,:,:) = dFstr_dpairpot(:,:,:) + reshape((recv_dpairpot_3darr(32*maxspecies*maxspecies*2+1:32*maxspecies*maxspecies*3)), (/32,maxspecies,maxspecies/))
                if (objFuncType.eq.2) then
                    dFen2_dpairpot(:,:,:) = dFen2_dpairpot(:,:,:) + reshape((recv_dpairpot_3darr(32*maxspecies*maxspecies*3+1:32*maxspecies*maxspecies*4)), (/32,maxspecies,maxspecies/))
                endif
             endif
 
          endif
       enddo
       !End of the synchronization of the results
end subroutine p_synchronizeResults
 
 
subroutine getMEAM_EnergyForce(NIONS,NTYP,NITYP,elementNames,POSION,A,energyMEAM,forcesMEAM)

    !----------------------------------------------------------------------c
    !
    !  Subroutine to calculate the energy and atomic forces for a given
    !  configuration of ions.
    !
    !  This code is adapted from the main code in 'MEAMfit.90', and was
    !  designed originally for the purposes of calculating MEAM energies
    !  and forces from within VASP, within the TU-TILD approach.
    !
    !  Andrew Duff, Blazej Grabowski 2017
    !
    !     Copyright (c) 2018, STFC
    !
    !----------------------------------------------------------------------c

    use m_poscar
    use m_optimization
    use m_filenames
    use m_geometry
    use m_datapoints
    use m_generalinfo
    use m_meamparameters
    use m_atomproperties
    use m_observables
    use m_atomproperties

    implicit none

    logical maxOK,ok
    integer i,iseed,nattempts,istruc
    real(8) tmp

    !---- initiailize VASP variables and use to fill m_poscar ----
    integer NIONS, & !Number of atoms
            NTYP, &  !Number of species
            iatom,j
    integer NITYP(NTYP), & !Number of atoms of each species
            atomicNumbers(NTYP)     !Atomic number of each species
    CHARACTER (LEN=2) :: elementNames(NTYP)
    real(8) energyMEAM,aux1(3),aux2(3)
    real(8) A(3,3)
    real(8) POSION(1:3,NIONS),forcesMEAM(1:3,NIONS),ff(3)
    !Variables for analytic derivatives of meam energy. Not necessary to compute
    !here, but need to put in the call to meamenergy

    ok = .FALSE.
    do i=1,NTYP
      do j=1,size(element)
        if (elementNames(i).eq.element(j)) then
          atomicNumbers(i)=j
          ok=.TRUE.
        endif
      enddo
      if (.NOT.ok) then
        write (6,*) "ERROR: element ",elementNames(i)," not found in list"
        stop
      endif
    enddo

    natoms=NIONS
    maxspecies=NTYP
    nspecies=NTYP
    if (allocated(z)) deallocate(z)
    !if (allocated(nat)) deallocate(nat)
    allocate (z(maxspecies))!,nat(maxspecies))
    z=atomicNumbers
    !Set up zz array using NITYPE and atomicNumbers
    if (allocated(zz)) deallocate(zz)
    allocate (zz(natoms))
    iatom=1
    do i=1,maxspecies
       !nat(i)=NITYP(i)   <--commented this and the above out as I no longer use
       !nat. NEed to update this subroutine if I want to use it again.
       do j=1,NITYP(i)
          zz(iatom)=atomicNumbers(i)
          iatom=iatom+1
       enddo
    enddo
 !  print *,'atomicNumbers=',atomicNumbers,' NITYP=',NITYP
 !  print *,'zz=',zz
 !  stop
    tr=A
    if (allocated(coordinates)) deallocate(coordinates)
    allocate(coordinates(3,natoms))
    coordinates=POSION
    !Convert direct coordinates to cartesian
    do i=1,natoms
        aux1 = coordinates(1:3,i)
        aux2 = matmul(tr,aux1)
        coordinates(1:3,i) = aux2
    enddo
    if (allocated(coords_old)) deallocate(coords_old)
    allocate(coords_old(3,natoms))
    natoms_old=natoms
    do i=1,natoms
        coords_old(1:3,i)=coordinates(1:3,i)
    enddo
    !-------------------------------------------------------------

    call getTime(tStart)

    if (verbosity.gt.1) print *,'MEAMfit: calculating energies and atomic forces'

    !---- set up filenames; and set up for an energy calculation
    !followed ----
    !     by an atomic force calculation
    settingsfile='settings'
    nTargetFiles=2
    nstruct=2

    if (allocated(strucnames)) deallocate(strucnames)
    if (allocated(computeForces)) deallocate(computeForces)
    if (allocated(rlxstruc)) deallocate(rlxstruc)
    if (allocated(freeenergy)) deallocate(freeenergy)
    if (allocated(istrucInFile)) deallocate(istrucInFile)
    if (allocated(weightsEn)) deallocate(weightsEn)
    if (allocated(weightsFr)) deallocate(weightsFr)
    if (allocated(weightsSt)) deallocate(weightsSt)
    allocate( strucnames(nstruct),computeForces(nstruct),rlxstruc(nstruct),freeenergy(nstruct),istrucInFile(nstruct), &
              weightsEn(nstruct),weightsFr(nstruct),weightsSt(nstruct) )

    strucnames=""
    weightsEn=0d0
    weightsFr=0d0
    weightsSt=0d0
    weightsEn(1)=1d0     !Not used for the optfunc, but necessary for forces to be computed in Fnew
    weightsFr(2)=1d0
    forcefit=.true.
    computeForces(1)=.false.
    computeForces(2)=.true.
    freeenergy(1)=.true.
    freeenergy(2)=.false.
    istrucInFile(1)=1
    istrucInFile(2)=1
    rlxstruc(1)=0
    rlxstruc(2)=0
    !-------------------------------------------------------------------------

    call checkInputFilesExist !Check if 'fitdbse' and 'settings' files exist
    if (readpotfile.eqv..true.) then
        call extractlmax
    endif
    if (settingsfileexist.eqv..false.) then
       call setupdefaultlmax
    endif

    call readsettings(iseed) !read settings from settings file
    if (settingsfileexist.eqv..true.) then
       call setuprandomseed(iseed)
    endif
    !---- Initial meam-parameter set-up ----
    call initializemeam       !Initialize initial meam parameters

    if ((readParasOnebyone.eqv..false.).and.(readpotfile)) then
       !If a potential file is supplied by user, amend freep so that
       !non-zero parameters
       !in this file are optimized (unless PARASTOOPT is specified in
       !the settings file)
       call amendFreep
    endif
    call getRmax !Find the maximum interatomic separation that needs to be considered.

    !---- Set-up structures and fitting database ----
    call initializestruc_fromarrays(0) !Read in atomic configurations from vasprun files
    optforce=.true.
    if (verbosity.gt.1) print *,'outside initializestruc sub'
    !Determine the number of datapoints to read in from the outcar
    !files. This will depend on whether we are fitting to energies or
    !forces for each of the outcar files.

    !The following was originally in a 'readtargetdata' call, but in the
    !following we extract just the ndatapoints set up and array
    !initialization
    ndatapoints=0
    do istruc=1,nstruct
        if (weightsEn(istruc).ne.0d0) then
            ndatapoints=ndatapoints+1
        endif
        if (weightsFr(istruc).gt.0d0) then
            ndatapoints=ndatapoints+3*gn_forces(istruc)
        endif
    enddo
    !Allocate fitting arrays.
    if (allocated(truedata)) deallocate(truedata)
    if (allocated(fitdata)) deallocate(fitdata)
    if (allocated(summeam_paireStr)) deallocate(summeam_paireStr)
    if (allocated(summeamfStr)) deallocate(summeamfStr)
    if (allocated(bestfitdata)) deallocate(bestfitdata)
    if (allocated(smallestsepnStr)) deallocate(smallestsepnStr)
    if (allocated(smlsepnperspcStr)) deallocate(smlsepnperspcStr)
    if (allocated(lrgsepnperspcStr)) deallocate(lrgsepnperspcStr)
    allocate(truedata(ndatapoints),fitdata(ndatapoints), &
        bestfitdata(ndatapoints),summeam_paireStr(ndatapoints), &
        summeamfStr(ndatapoints),smallestsepnStr(ndatapoints), &
        smlsepnperspcStr(15,ndatapoints), &
        lrgsepnperspcStr(15,ndatapoints) )

    if (maxspecies.gt.5) then
       print *,'ERROR: Please increase smlsepnperspcStr and lrgsepnperspcStr arrays, STOPPING.'
       stop
    endif

    call calcSds       !Determine s.d's of energies and forces to rescale optfunc
    if (verbosity.gt.1) print *,'setting up nn tables'
    call setupnntables !Set up atom separation tables, also for forces (must
                       !come after 'readtargetdata' as this determines
                       !which forces are to be optimized)
    if (verbosity.gt.1) print *,'finding smallest sepn'
    call findsmallestsepn !Determine smallest and largest interatomic separations
    if ((randgenparas.eqv..false.).and.(readpotfile.eqv..false.)) then
       print *,'sepnHistogram file produced. No potential filename supplied and'
       print *,'-noopt/-no flag at command-line, STOPPING.'
       stop
    endif
    !------------------------------------------------
    if (verbosity.gt.1) print *,'checking pot'
    call checkPotType
    call allocateArrays
    !Note: before I introduced the call immediately above, Blazej found it
    !necessary to add the following lines to fix a bug:
    if (verbosity.gt.1) print *,'initializing noptp'
    if (.NOT.allocated(optmeamtau)) call initialize_noptp
    !If there is an issue with the code when running getMEAM_EnergyForce, 
    !please check these lines carefully

    !Print out energy and atomic forces and then stop (nsteps=1 makes
    !sure of this)
    istr=1
    call meamenergy
    energyMEAM=energy
    istr=2
    call meamforce
    forcesMEAM=forces

end subroutine getMEAM_EnergyForce
