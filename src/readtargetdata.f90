subroutine readtargetdata

    !----------------------------------------------------------------------c
    !
    !     Reads in target data (energes and/or forces and/or stress tensors
    !     from DFT data files and stores in the 'truedata' array. Note
    !     that if excludeCoulomb=true then for each structure we also run
    !     LAMMPS (through the external script 'getCoulombObservables') 
    !     to calculate the Coulomb contributions to the energies, forces 
    !     and stress tensor components.
    !     Note that MEAMfit should be run once, in serial, to get the
    !     'coulombObservablesFull' file. Then it can be run in parallel.
    !     excludeCoulombMode=0 corresponds to former, =1 to latter. Since
    !     this subroutine is currently called by all processes (each
    !     process does a separate initialization, which is a bit wasteful)
    !     doing the LAMMPS bit in parallel is messy, because each process
    !     writes files in the same directory. Ultimately the initialization
    !     should be coded up so that it is shared between nodes, or else
    !     only one node performs the job.
    !
    !     Called by:     program MEAMfit
    !     Calls:         readtargetforces,readtargetenergy
    !     Returns:       ndatapoints,truedata
    !     Files read:    -
    !     Files written: -
    !
    !     Andy Duff, Jan 2008
    !
    !     Copyright (c) 2018, STFC
    !
    !----------------------------------------------------------------------c

    use m_generalinfo
    use m_filenames
    use m_datapoints
    use m_optimization
    use m_geometry
    use m_atomproperties
    use m_mpi_info !if excludeCoulomb=true, use this to check we are running in
                   !serial mode if we are running in excludeCoulombMode=0

    implicit none

    logical noForcesFlagged,file_exists
    character*1 snglchar
    character*5 string6
    character*9 string,string2
    character*80 targetfile_prev,string3,string4,string5,line,aux(1:4)
    character*106 lineLong,coulombObservablesFile
    integer iat,istruc,idatapoint,istrucInFile_prev,iconf,idata, &
         idatapointEn,idatapointFr,idatapointSt,IOstatus,iexit, &
         excludeCoulombMode,iiat,n_finiteBasis
    real(8), allocatable:: truedata_readin(:)
    real(8), parameter:: hartreeovereV=27.211396d0
    real(8), parameter:: hartreebohrovereVangstrom=51.421d0
    !The following are used if excludeCoulomb=true
    character*200 runCommand
    real(8) coulombEnergy,forcemagnitude,forcemagnitudeAverage
    real(8), allocatable:: coulombForce(:,:)
    real(8) coulombStressTensor(6),coulombStressTensorDummy(6) ! The latter is to read in stress*volume (which we don't need in MEAMfit), but is included in coulombObservablesFull for interaction with the MLIP code

    if (verbosity.gt.1) print *,'Preparing to read data from DFT input files'

    !Determine the number of datapoints to read in from the outcar
    !files. This will depend on whether we are fitting to energies or
    !forces for each of the vasprun files.
    !Note: where selective force fitting is used, 'ndatapoints' also includes
    !those forces that aren't being optimized, so that fitdata(), truedata(),
    !etc arrays contain unused elements. This is necessary because by this point
    !in the code we have to size the arrays, but only know the numbers of atoms
    !(from initializestruc) and not which (if any) atoms are to have selective 
    !force fitting applied.
    !We also define 'ndatapointsTrue' later which contains the ACTUAL number of
    !fitting points, but this is only used to print out the nubmer of fitting
    !points per parameter in MEAMfit
    ndatapoints=0
    do istruc=1,nstruct
        if (weightsEn(istruc).ne.0d0) then
            ndatapoints=ndatapoints+1
        endif
        if (weightsFr(istruc).gt.0d0) then
            ndatapoints=ndatapoints+3*gn_forces(istruc)
        endif
        if (weightsSt(istruc).gt.0d0) then
            ndatapoints=ndatapoints+6
        endif
    enddo

    !Allocate fitting arrays.
    if (allocated(truedata)) deallocate(truedata)
    if (allocated(fitdata)) deallocate(fitdata)
    if (allocated(summeam_paireStr)) deallocate(summeam_paireStr)
    if (allocated(summeamfStr)) deallocate(summeamfStr)
    if (allocated(bestfitdata)) deallocate(bestfitdata)
    if (allocated(smallestsepnStr)) deallocate(smallestsepnStr)
    if (allocated(smlsepnperspcStr)) deallocate(smlsepnperspcStr)
    if (allocated(lrgsepnperspcStr)) deallocate(lrgsepnperspcStr)
    allocate(truedata(ndatapoints),fitdata(ndatapoints), &
        bestfitdata(ndatapoints),summeam_paireStr(ndatapoints), &
        summeamfStr(ndatapoints),smallestsepnStr(ndatapoints), &
        smlsepnperspcStr(30,ndatapoints), &
        lrgsepnperspcStr(30,ndatapoints) )

    if (maxspecies.gt.7) then
       print *,'ERROR: Please increase smlsepnperspcStr and lrgsepnperspcStr arrays, STOPPING.'
       stop
    endif

    idatapoint=1
    open(unit=1,file=trim(targetfiles(1)))
    iconf=0
    if (excludeCoulomb.eqv..true.) then
       !See which 'mode' we are running under
       inquire(file="coulombObservablesFull", EXIST=file_exists)
       if (file_exists.eqv..true.) then
          if (verbosity.gt.1) print *,'coulombObservablesFull file exists - ready for optimization'
          excludeCoulombMode=1
       else
          if (verbosity.gt.0) print *,'coulombObservablesFull file does not exist - creating it. Progress:'
          excludeCoulombMode=0
          !Check we are running in serial
          if (procid>0) then
             print *,'ERROR: You need to run MEAMfit first in serial to generate the coulombObservablesFull file, STOPPING.'
             stop
          endif
       endif
       open(unit=3,file="coulombObservablesFull")
    endif

    !Loop through all structures present in the DFT datafiles

    do istruc=1,nstruct
       !print *,'istruc=',istruc !DEBUG

       if (excludeCoulomb.eqv..true.) then

          allocate(coulombForce(gn_forces(istruc),3))

          if (excludeCoulombMode==0) then

             if ((verbosity.gt.0).and.(mod(istruc,10).eq.0)) then
                print *,dble(istruc)/dble(nstruct)*100d0,'%'
             endif
             
             ! Copy LAMMPS structure file over. These were generated in 'initializestruc'
             if (istruc.lt.10) then
                write(string,'(I1)') istruc
             elseif (istruc.lt.100) then
                write(string,'(I2)') istruc
             elseif (istruc.lt.1000) then
                write(string,'(I3)') istruc
             elseif (istruc.lt.10000) then
                write(string,'(I4)') istruc
             endif
             runCommand="mv positions.eam_" // trim(string) // " struc.lammps"
             call execute_command_line (trim(runCommand), exitstat=iexit)
             ! print *, "Exit status of copy command was ", iexit

             ! Run getCoulombObservables.sh
             runCommand = "./getCoulombObservables.sh"
             call execute_command_line (trim(runCommand), exitstat=iexit)
             ! print *, "Exit status of ./getCoulombObservables.sh run was ", iexit

             ! Now we have a file, 'coulombObservables' containing the Coulomb energy, forces 
             ! and stress tensor components, read these into internal variables.
             open(unit=2,file="coulombObservables")   !need to check '2' is free...
             read(2,*)
             read(2,*) coulombEnergy
             read(2,*)
             do iat=1,gn_forces(istruc)
                read(2,*) coulombForce(iat,1:3)
             enddo
             read(2,*)
             read(2,*) coulombStressTensor(1:6)
             close(2)

             !Write out the data to coulombObservablesFull
             if (weightsEn(istruc).ne.0d0) then
                write(3,*) coulombEnergy
             endif
             if (weightsFr(istruc).gt.0d0) then
                do iat=1,gn_forces(istruc)
                   write(3,*) coulombForce(iat,1:3)
                enddo
             endif
             if (weightsSt(istruc).gt.0d0) then
                write(3,*) coulombStressTensor(1:6)
                write(3,*) coulombStressTensor(1:6)*g_vol(istruc)
             endif

          else

             !Read in the data from coulombObservablesFull
             if (weightsEn(istruc).ne.0d0) then
                read(3,*) coulombEnergy
             endif
             if (weightsFr(istruc).gt.0d0) then
                do iat=1,gn_forces(istruc)
                   read(3,*) coulombForce(iat,1:3)
                enddo
             endif
             if (weightsSt(istruc).gt.0d0) then
                read(3,*) coulombStressTensor(1:6)
                read(3,*) coulombStressTensorDummy(1:6)
             endif

             ! Then below we can subtract these observables from the 
             ! energy, forces, and stress components.

          endif

       endif
       !print *,'istruc=',istruc,' coulombEnergy=',coulombEnergy

       if (istruc.gt.1) then
          !Decide if we need to open new DFT datafile or rewind the current one
          if (trim(targetfiles(istruc)).ne.targetfile_prev) then
             close(1)
             open(unit=1,file=trim(targetfiles(istruc)))
             iconf=0
          elseif (istrucInFile(istruc).le.istrucInFile_prev) then
             rewind(unit=1)
             iconf=0
          endif
       endif

       if (DFTdata(istruc).eq.0) then !vasprun files

          !In MEAMfit I use: energy; force; stress tensor ordering in the
          !fitdata and truedata arrays. VASP instead uses: force; stress tensor;
          !energy. Use following to set starting point for each quantity:
          if (weightsEn(istruc).ne.0d0) then
             idatapointEn=idatapoint
          endif
          if (weightsFr(istruc).gt.0d0) then
             if (weightsEn(istruc).ne.0d0) then
                idatapointFr=idatapoint+1
             else
                idatapointFr=idatapoint
             endif
          endif
          if (weightsSt(istruc).gt.0d0) then
             if (weightsFr(istruc).gt.0d0) then
                idatapointSt=idatapointFr+3*gn_forces(istruc)
             else
                if (weightsEn(istruc).ne.0d0) then
                   idatapointSt=idatapoint+1
                else
                   idatapointSt=idatapoint
                endif
             endif
          endif

          !Forces: (these appear first in the vasprun.xml file)
          !Even if we aren't fitting, use forces to index ionic configs
          !print *,'searching for forces'
          do
             read(1,'(a80)') line
             string=line(17:25)
             string2=line(18:26)
             if ( (string(1:6).eq.'forces') .or. &
                  (string2(1:6).eq.'forces') ) then
                iconf=iconf+1
                if (iconf.eq.istrucInFile(istruc)) then
                   exit
                endif
             endif
          enddo
          if (weightsFr(istruc).gt.0d0) then
             noForcesFlagged=.true.
!             print *,'forces:' !21_10
             do iat=1,gn_forces(istruc)
                !forces of form "   <v>       0.00000213      0.00000295         -0.00000143</v>"
                read(1,*) line
                if ((line(1:1).eq.'T').or.(line(1:1).eq.'t')) then
                   optforce(iat,istruc)=.true.
                   noForcesFlagged=.false.
                   backspace(unit=1)
                   read(1,*) snglchar,string3,truedata(idatapointFr:idatapointFr+1),string4
                else
                   optforce(iat,istruc)=.false.
                   backspace(unit=1)
                   read(1,*) string3,truedata(idatapointFr:idatapointFr+1),string4
                endif
                call keepNumber(string4)
                string4=trim(string4)
                read(string4,*) truedata(idatapointFr+2)
!                print *,truedata(idatapointFr:idatapointFr+2) !21_10
                !if (iat.le.3) then
                !   print *,' truedata(',idatapointFr,'-',idatapointFr+2,') [=forces(iat=',iat,')] =',truedata(idatapointFr:idatapointFr+2)
                !endif
                if (excludeCoulomb.eqv..true.) then
                   !Subtract the Coulomb forces from the VASP forces
                   truedata(idatapointFr)=truedata(idatapointFr)-coulombForce(iat,1)
                   truedata(idatapointFr+1)=truedata(idatapointFr+1)-coulombForce(iat,2)
                   truedata(idatapointFr+2)=truedata(idatapointFr+2)-coulombForce(iat,3)
                endif
                idatapointFr=idatapointFr+3
                idatapoint=idatapoint+3
             enddo
             if (noForcesFlagged) then
                 if (verbosity.gt.1) write(*,'(A29,I2)') ' Fitting all forces for file ',istruc
                 do iat=1,gn_forces(istruc)
                    optforce(iat,istruc)=.true.
                 enddo
             else
                 write(*,'(A47,I2)') 'Fitting only manually selected forces for file ',istruc
             endif
          else
             ! !If not fiting forces, skip past the lines
             ! do iat=1,gn_forces(istruc)
             !    print *,'iat=',iat
             !    read(1,*) line
             ! enddo
             ! print *,'skipping forces'
          endif

          !Stress-tensor:
          if (weightsSt(istruc).gt.0d0) then
             !Search for stress-tensor (perform search rather than simply scan
             !three lines down, since forces might not have been read in)
             do
                read(1,'(a80)',IOSTAT=IOstatus) line
                if (IOstatus.lt.0) then
                   print *,'ERROR: Stress-tensor fitting specified for ',istrucInFile(istruc),'th ionic configuration '
                   print *,'of ',targetfiles(istruc),' but no stress data found, STOPPING.'
                   stop
                endif
                string=line(17:25)
                string2=line(18:26)
                if ( (string(1:6).eq.'stress') .or. &
                     (string2(1:6).eq.'stress') ) then
                   exit
                endif
             enddo
             read(1,*) string3,truedata(idatapointSt),string4,string5
             read(1,*) string3,truedata(idatapointSt+3),truedata(idatapointSt+1),string4
             read(1,*) string3,truedata(idatapointSt+4),truedata(idatapointSt+5),string4
             call keepNumber(string4)
             string4=trim(string4)
             read(string4,*) truedata(idatapointSt+2)
             !Convert stress from kBar to eV/Angstrom^3
             do idata=0,5
                truedata(idatapointSt+idata) = truedata(idatapointSt+idata) * kB2eVperAng3
             enddo
             !print *,' truedata(',idatapointSt,'-',idatapointSt+5,')/kB2evperAng3 (=stress-tensor, in vasp units) =',truedata(idatapointSt:idatapointSt+5)/kB2eVperAng3
             if (excludeCoulomb.eqv..true.) then
                !Subtract the Coulomb stress component tensors from the VASP values
                do idata=0,5
                   truedata(idatapointSt+idata)=truedata(idatapointSt+idata) - coulombStressTensor(idata+1)
                   !Hopefully LAMMPS outputs in same order as in MEAMfit these
                   !components. At least in
                   !MEAMfit_fordistribution_V2_latest/SampleCalculation/RF-MEAM_LAMMPStest/testMEAMfit_vs_LAMMPS_vasprun4B.sh
                   !I outputted the MEAMfit vs the LAMMPS output sequentially,
                   !so hopefully this is the case.
                enddo
             endif
             idatapoint=idatapoint+6
          endif

          !Energies:
          if (weightsEn(istruc).ne.0d0) then
             if (freeenergy(istruc)) then
                !Scan for e_fr_energy
                !print *,'about to scane for energy'
                do
                   read(1,'(a80)',IOSTAT=IOstatus) line
                   if (IOstatus.lt.0) then
                      print *,'ERROR: Free energy fitting specified for ',istrucInFile(istruc),'th ionic configuration '
                      print *,'of ',targetfiles(istruc),' but no Free energy data found, STOPPING.'
                      stop
                   endif
                   if ((line(13:23).eq."e_fr_energy")) then
                      exit
                   endif
                enddo
             else
                !e_wo_entrp should appear two lines down - check this
                do
                   read(1,'(a80)',IOSTAT=IOstatus) line
                   if (IOstatus.lt.0) then
                      print *,'ERROR: E0 energy fitting specified for ',istrucInFile(istruc),'th ionic configuration '
                      print *,'of ',targetfiles(istruc),' but no E0 energy data found, STOPPING.'
                      print *,'(line instead reads:',line,')'
                      stop
                   endif
                   if ((line(13:22).eq."e_wo_entrp")) then
                      exit
                   endif
                enddo
             endif
             read(line,*) aux(1:2),string3
             call keepNumber(string3)
             string3=trim(string3)
             read(string3,*) truedata(idatapointEn)
!             print *,'energy = ',truedata(idatapointEn) !21_10
!             stop !21_10

             if (excludeCoulomb.eqv..true.) then
                !print *,'Adjusting truedata(',idatapointEn,') to ',truedata(idatapointEn),' - ',coulombEnergy,' = ', truedata(idatapointEn) - coulombEnergy
                truedata(idatapointEn)=truedata(idatapointEn) - coulombEnergy
             endif

             ! MEAMfit fits energies per atom rather than per supercell
             if (computeForces(istruc)) then
                !print *,'gn_forces(',istruc,')=',gn_forces(istruc)
                truedata(idatapointEn)=truedata(idatapointEn)/gn_forces(istruc)
             else
                !print *,'gn_inequivalentsites((',istruc,')=',gn_inequivalentsites(istruc)
                truedata(idatapointEn)=truedata(idatapointEn)/gn_inequivalentsites(istruc)
             endif
             !print *,'truedata(',idatapointEn,'=',truedata(idatapointEn)

             idatapoint=idatapoint+1
          endif

       elseif (DFTdata(istruc).eq.1) then !CASTEP .geom files  

          !In MEAMfit I use: energy; force; stress tensor ordering in the
          !fitdata and truedata arrays. CASTEP instead uses: energy; stress
          !tensor; forces. Use following to set starting point for each
          !quantity:
          if (weightsEn(istruc).ne.0d0) then
             idatapointEn=idatapoint
          endif
          if (weightsFr(istruc).gt.0d0) then
             if (weightsEn(istruc).ne.0d0) then
                idatapointFr=idatapoint+1
             else
                idatapointFr=idatapoint
             endif
          endif
          if (weightsSt(istruc).gt.0d0) then
             if (weightsFr(istruc).gt.0d0) then
                idatapointSt=idatapointFr+3*gn_forces(istruc)
             else
                if (weightsEn(istruc).ne.0d0) then
                   idatapointSt=idatapoint+1
                else
                   idatapointSt=idatapoint
                endif
             endif
          endif

          !Even if we aren't fitting, use energies to index ionic configs
          do
             read(1,'(a106)') lineLong
             if (lineLong(102:106)=='<-- E') then
                iconf=iconf+1
                if (iconf.eq.istrucInFile(istruc)) then
                   exit
                endif
             endif
          enddo

          !Energies
          if (weightsEn(istruc).ne.0d0) then
             read(lineLong,*) truedata(idatapointEn)
             !print *,'Energy read in: truedata(',idatapointEn,')=',truedata(idatapointEn)
             truedata(idatapointEn)=truedata(idatapointEn)*hartreeovereV
             idatapoint=idatapoint+1
          endif

          !Skip past lattice parameters
          read(1,*)
          read(1,*)
          read(1,*)

          !Stress-tensor
          if (weightsSt(istruc).gt.0d0) then
             read(1,'(a106)') lineLong
             if (lineLong(102:106)=='<-- S') then
                read(lineLong,*) truedata(idatapointSt)
                read(1,'(a106)') lineLong
                read(lineLong,*) truedata(idatapointSt+3),truedata(idatapointSt+1)
                read(1,'(a106)') lineLong
                read(lineLong,*) truedata(idatapointSt+4),truedata(idatapointSt+5),truedata(idatapointSt+2)
                !Convert stress from ... to eV/Angstrom^3
                do idata=0,5
                   truedata(idatapointSt+idata) = truedata(idatapointSt+idata) ! * <-- Instert factor here !kB2eVperAng3
                enddo
                print *,'STOPPING: need to add stress tensor factor'
                stop
                print *,'Stress-tensor read in:',truedata(idatapointSt:idatapointSt+5)
                idatapoint=idatapoint+6
                read(1,'(a106)') lineLong
             else
                print *,'ERROR: Stress-tensor fitting specified for ',istrucInFile(istruc),'th ionic configuration '
                print *,'of ',targetfiles(istruc),' but no stress data found, STOPPING.'
                stop
             endif
          else
             read(1,'(a106)') lineLong
             if (lineLong(102:106)=='<-- S') then
                read(1,*)
                read(1,*)
                read(1,'(a106)') lineLong
             endif
          endif

          !Skip past positions
          do iat=1,gn_forces(istruc)-1 !<-- we've already read in the first atomic position line 26_10_2020. It might be necessary to add 9 here for MD runs
             read(1,*)
          enddo

          !Forces
          if (weightsFr(istruc).gt.0d0) then
             noForcesFlagged=.true.
             do iat=1,gn_forces(istruc)
                !forces of form "  Zr              1    4.5646308913749582E-007                 !-1.2890260232349243E-004   -3.4578071283619795E-005  <-- F". Note, replace 'F' with 'f' for selective forces.
                read(1,'(a106)') lineLong
                if (lineLong(102:106)=='<-- f') then
                   optforce(iat,istruc)=.true.
                   noForcesFlagged=.false.
                else
                   optforce(iat,istruc)=.false.
                endif
                read(lineLong,*) string3,string4,truedata(idatapointFr:idatapointFr+2)
                !print *,'Force read in, truedata(',idatapointFr,':',idatapointFr+2,')=',truedata(idatapointFr:idatapointFr+2)
                truedata(idatapointFr:idatapointFr+2)=truedata(idatapointFr:idatapointFr+2)*hartreebohrovereVangstrom
                if ((fitLargeForces.eqv..true.).and. &
                        (sqrt( truedata(idatapointFr)**2 +  &
                               truedata(idatapointFr+1)**2 +  &
                               truedata(idatapointFr+2)**2 ).gt.0.1d0 )) then
                   optforce(iat,istruc)=.true.
                   noForcesFlagged=.false.
                endif
                idatapointFr=idatapointFr+3
                idatapoint=idatapoint+3
             enddo
             if (noForcesFlagged.and.(fitLargeForces.eqv..false.)) then
                 if (verbosity.gt.1) write(*,'(A29,I2)') ' Fitting all forces for file ',istruc
                 do iat=1,gn_forces(istruc)
                    optforce(iat,istruc)=.true.
                 enddo
             else
                 write(*,'(A47,I2)') 'Fitting only manually selected forces for file ',istruc
             endif

          endif

       elseif (DFTdata(istruc).eq.2) then !Quantum Espresso files  

          !In MEAMfit I use: energy; force; stress tensor ordering in the
          !fitdata and truedata arrays. QE instead uses: energy; stress
          !tensor; forces. Use following to set starting point for each
          !quantity:
          if (weightsEn(istruc).ne.0d0) then
             idatapointEn=idatapoint
          endif
          if (weightsFr(istruc).gt.0d0) then
             if (weightsEn(istruc).ne.0d0) then
                idatapointFr=idatapoint+1
             else
                idatapointFr=idatapoint
             endif
          endif
          if (weightsSt(istruc).gt.0d0) then
             if (weightsFr(istruc).gt.0d0) then
                idatapointSt=idatapointFr+3*gn_forces(istruc)
             else
                if (weightsEn(istruc).ne.0d0) then
                   idatapointSt=idatapoint+1
                else
                   idatapointSt=idatapoint
                endif
             endif
          endif

          !Even if we aren't fitting, use energies to index ionic configs
          do
             read(1,'(a106)') lineLong
             if (lineLong(1:28)=='                total energy') then
                iconf=iconf+1
                if (iconf.eq.istrucInFile(istruc)) then
                   exit
                endif
             endif
          enddo

          !Energies
          if (weightsEn(istruc).ne.0d0) then
             read(lineLong,*) string3,string4,snglchar,truedata(idatapointEn)
             !print *,'Energy read in: truedata(',idatapointEn,')=',truedata(idatapointEn),' Hartree a.u.'
             truedata(idatapointEn)=truedata(idatapointEn)*hartreeovereV
             !print *,'Energy read in: truedata(',idatapointEn,')=',truedata(idatapointEn),' eV'
             !stop
             idatapoint=idatapoint+1
          endif

          if (weightsSt(istruc).gt.0d0) then
             !Stress-tensor
             do
                read(1,'(a106)') lineLong
                if (lineLong(1:21)=='   Total stress (GPa)') then
                   exit
                endif
                !Check if we have reached 'ATOMIC_POSITIONS'. If so, then we have
                !gone too far, and a stress tensor was not present for this config
                if (lineLong(1:19)=='   ATOMIC_POSITIONS') then
                   print *,'ERROR: Stress-tensor fitting specified for ',istrucInFile(istruc),'th ionic configuration '
                   print *,'of ',targetfiles(istruc),' but no stress data found, STOPPING.'
                   stop
                endif
             enddo

             read(1,*) truedata(idatapointSt)
             read(1,*) truedata(idatapointSt+3),truedata(idatapointSt+1)
             read(1,*) truedata(idatapointSt+4),truedata(idatapointSt+5),truedata(idatapointSt+2)
             !Convert units of stress tensor
             !Units of GPa. 1 GPa = 10 kBar. kBar to eV/Angstrom^3 using 'kb2eVperAng3'
             do idata=0,5
                truedata(idatapointSt+idata) = truedata(idatapointSt+idata) * 10*kB2eVperAng3
             enddo
             !print *,'Stress-tensor read in:',truedata(idatapointSt:idatapointSt+5)
             !stop
             idatapoint=idatapoint+6
          endif

          !Forces
          if (weightsFr(istruc).gt.0d0) then

             do
                read(1,'(a106)') lineLong
                if (lineLong(1:31)=='   Forces acting on atoms (au):') then
                   exit
                endif
             enddo

             noForcesFlagged=.true.
             do iat=1,gn_forces(istruc)
                !forces of form "   Zr    4.5646308913749582E-007   -1.2890260232349243E-004   -3.4578071283619795E-005". Note, an 'f' at the front can be added by the user if they want selective forces. 
                read(1,'(a106)') lineLong
                if (lineLong(1:1)=='f') then
                   optforce(iat,istruc)=.true.
                   noForcesFlagged=.false.
                   read(lineLong,*) string3,string4,truedata(idatapointFr:idatapointFr+2)
                else
                   optforce(iat,istruc)=.false.
                   read(lineLong,*) string3,truedata(idatapointFr:idatapointFr+2)
                endif

                truedata(idatapointFr:idatapointFr+2)=truedata(idatapointFr:idatapointFr+2)*hartreebohrovereVangstrom
                if ((fitLargeForces.eqv..true.).and. &
                        (sqrt( truedata(idatapointFr)**2 +  &
                               truedata(idatapointFr+1)**2 +  &
                               truedata(idatapointFr+2)**2 ).gt.0.1d0 )) then
                   optforce(iat,istruc)=.true.
                   noForcesFlagged=.false.
                endif

                !MS2020   this must be the proper place to compute "weightFr"  !4Andy
                if (forcemagnitudepower.ne.0d0) then
                    print *,'DEBUG CODE: applying force modifier'
                    if(weightsFr(istruc) .ne. 0d0) then
                      ! if(weightsFr(iat,istruc) .ne. 0d0) then  <- ideally we would apply this on a by atom basis, but weightsFr is currently only defined per structure. (note, structure here refers to each separate structure e.g. from md runs) Therefore take an average of forces. This will certainly work for relaxed structures where the average should be small, but may not 'weed out' the large forces...
                      forcemagnitudeAverage=0d0
                      do iiat=1,gn_forces(istruc)
                        forcemagnitudeAverage=forcemagnitudeAverage+sqrt( truedata(idatapointFr)**2 + truedata(idatapointFr+1)**2 + truedata(idatapointFr+2)**2 )
                      enddo
                      forcemagnitudeAverage=forcemagnitudeAverage/dble(gn_forces(istruc))
                      ! forcemagnitude=sqrt( truedata(idatapointFr)**2 + truedata(idatapointFr+1)**2 + truedata(idatapointFr+2)**2 )
                      if(forcemagnitudeAverage .lt. excludeforcemax) then
                        if(forcemagnitudeAverage .gt. excludeforcemin) then
                          weightsFr(istruc)=weightsFr(istruc)*(forcemagnitudeAverage**forcemagnitudepower)
                        endif
                      endif
                    endif
                endif

                idatapointFr=idatapointFr+3
                idatapoint=idatapoint+3
             enddo
             if (noForcesFlagged.and.(fitLargeForces.eqv..false.)) then
                 if (verbosity.gt.1) write(*,'(A29,I2)') ' Fitting all forces for file ',istruc
                 do iat=1,gn_forces(istruc)
                    optforce(iat,istruc)=.true.
                 enddo
             else
                 write(*,'(A47,I2)') 'Fitting only manually selected forces for file ',istruc
             endif

          endif

       elseif (DFTdata(istruc).eq.4) then !Quantum Espresso - PWscf format

          !In MEAMfit I use: energy; force; stress tensor ordering in the
          !fitdata and truedata arrays. QE-PWscf also uses: energy; forces
          !stress tensor. Use following to set starting point for each
          !quantity:
          if (weightsEn(istruc).ne.0d0) then
             idatapointEn=idatapoint
          endif
          if (weightsFr(istruc).gt.0d0) then
             if (weightsEn(istruc).ne.0d0) then
                idatapointFr=idatapoint+1
             else
                idatapointFr=idatapoint
             endif
          endif
          if (weightsSt(istruc).gt.0d0) then
             if (weightsFr(istruc).gt.0d0) then
                idatapointSt=idatapointFr+3*gn_forces(istruc)
             else
                if (weightsEn(istruc).ne.0d0) then
                   idatapointSt=idatapoint+1
                else
                   idatapointSt=idatapoint
                endif
             endif
          endif

          !Use atomic positions to index ionic configs
          print *,'lookign for atomicpositions'
          do
             read(1,'(a106)') lineLong
             if (lineLong(40:48)=='positions') then
                iconf=iconf+1
             endif
             if (lineLong(1:16)=='ATOMIC_POSITIONS') then
                iconf=iconf+1
             endif
             if (iconf.eq.istrucInFile(istruc)) then
                exit
             endif
          enddo
                print *,'positions found'
          !Energies
          if (weightsEn(istruc).ne.0d0) then
             do
                read(1,'(a106)') lineLong
                if (lineLong(1:17)=='!    total energy') then
                   exit
                endif
             enddo
             !backspace(unit=1)
             string3=linelong(33:50)
             string3=trim(string3)
             read(string3,fmt=*) truedata(idatapointEn)
             !print *,'Energy read in: truedata(',idatapointEn,')=',truedata(idatapointEn),' Rydbergs'
             truedata(idatapointEn)=truedata(idatapointEn)*hartreeovereV/2d0
             !print *,'Energy read in: truedata(',idatapointEn,')=',truedata(idatapointEn),' eV'
             !stop
             idatapoint=idatapoint+1
          endif
                        print *,'energy found'
          !Forces
          if (weightsFr(istruc).gt.0d0) then
             !print *,'Forces: (Ry/au)'
             do
                read(1,'(a106)') lineLong
                if (lineLong(6:11)=='Forces') then
                   exit
                endif
             enddo
             read(1,'(a106)') lineLong

             noForcesFlagged=.true.
             do iat=1,gn_forces(istruc)
                read(1,'(a106)') lineLong
                if (lineLong(1:1)=='f') then
                   optforce(iat,istruc)=.true.
                   noForcesFlagged=.false.
                else
                   optforce(iat,istruc)=.false.
                endif
                string3=linelong(33:75)
                string3=trim(string3)
                read(string3,fmt=*) truedata(idatapointFr:idatapointFr+2)
                !print *,truedata(idatapointFr:idatapointFr+2)
                truedata(idatapointFr:idatapointFr+2)=truedata(idatapointFr:idatapointFr+2)*hartreebohrovereVangstrom/2d0
                if ((fitLargeForces.eqv..true.).and. &
                        (sqrt( truedata(idatapointFr)**2 +  &
                               truedata(idatapointFr+1)**2 +  &
                               truedata(idatapointFr+2)**2 ).gt.0.1d0 )) then
                   optforce(iat,istruc)=.true.
                   noForcesFlagged=.false.
                endif
                idatapointFr=idatapointFr+3
                idatapoint=idatapoint+3
             enddo
             if (noForcesFlagged.and.(fitLargeForces.eqv..false.)) then
                 write(*,'(A29,I2)') ' Fitting all forces for file ',istruc
                 do iat=1,gn_forces(istruc)
                    optforce(iat,istruc)=.true.
                 enddo
             else
                 write(*,'(A47,I2)') 'Fitting only manually selected forces for file ',istruc
             endif

          endif
                print *,'forces found'
          if (weightsSt(istruc).gt.0d0) then
             !Stress-tensor
             do
                read(1,'(a106)') lineLong
                if (lineLong(11:24)=='total   stress') then
                   exit
                endif
             enddo
             print *,'to be coded'
             stop
             idatapoint=idatapoint+6
          endif

       elseif (DFTdata(istruc).eq.3) then !CASTEP .castep files  

          !In MEAMfit I use: energy; force; stress tensor ordering in the
          !fitdata and truedata arrays. .castep files use: energy; then
          !forces. Stress tensors in CASTEP have opposite sign to VASP
          !(and MEAMfit) so add a negative when reading them in.
          if (weightsEn(istruc).ne.0d0) then
             idatapointEn=idatapoint
          endif
          if (weightsFr(istruc).gt.0d0) then
             if (weightsEn(istruc).ne.0d0) then
                idatapointFr=idatapoint+1
             else
                idatapointFr=idatapoint
             endif
          endif
          if (weightsSt(istruc).gt.0d0) then
             if (weightsFr(istruc).gt.0d0) then
                idatapointSt=idatapointFr+3*gn_forces(istruc)
             else
                if (weightsEn(istruc).ne.0d0) then
                   idatapointSt=idatapoint+1
                else
                   idatapointSt=idatapoint
                endif
             endif
          endif

          !Use 'Forces' to index ionic configs (can't use Final Free energy
          !as sometimes CASTEP does an ENCUT test at the start)
          do
             read(1,'(a106)') lineLong
             if ((lineLong(41:46)=='Forces').or.(lineLong(29:34)=='Forces').or.(lineLong(35:40)=='Forces').or.(lineLong(47:52)=='Forces')) then
                !print *,' Forces identified, iconf=',iconf ! DEBUG
                iconf=iconf+1
                if (iconf.eq.istrucInFile(istruc)) then
                   !print *,' = istrucInFile(',istruc,')= ',istrucInFile(istruc),' ...exiting' ! DEBUG
                   exit
                endif
             endif
          enddo

          !Read energy (need to go backwards in the file to do this)
          if (weightsEn(istruc).ne.0d0) then
             !print *,'looking back for energy:' ! DEBUG
             do
                backspace(1)
                backspace(1)
                read(1,'(a106)',IOSTAT=IOstatus) lineLong
                !print *,lineLong
                if (IOstatus.lt.0) then
                   print *,'ERROR: Free energy fitting specified for ',istrucInFile(istruc),'th ionic configuration '
                   print *,'of ',targetfiles(istruc),' but no Free energy data found, STOPPING.'
                   stop
                endif
                if (lineLong(1:17)=='Final free energy') then !later generalize
!                    for E0/Fr distinguishing. Here is Fr, Final energy, E is 'E0'
                   string3=linelong(30:50)
                   string3=trim(string3)
                   read(string3,fmt=*) truedata(idatapointEn)
                   !print *,'found free energy, = ',truedata(idatapointEn) ! DEBUG
                   !truedata(idatapointEn)=truedata(idatapointEn)*hartreeovereV  in
                   !castep file it is in eV unless units changed in settings perhaps.
                   !add test?

                   if (excludeCoulomb.eqv..true.) then
                      print *,' excludeCoulomb not yet supported for CASTEP please add'
                      stop
                   endif

                   ! MEAMfit fits energies per atom rather than per supercell
                   if (computeForces(istruc)) then
                      !print *,'gn_forces(',istruc,')=',gn_forces(istruc) ! DEBUG
                      truedata(idatapointEn)=truedata(idatapointEn)/gn_forces(istruc)
                   else
                      !print *,'gn_inequivalentsites((',istruc,')=',gn_inequivalentsites(istruc) ! DEBUG
                      truedata(idatapointEn)=truedata(idatapointEn)/gn_inequivalentsites(istruc)
                   endif

                   idatapoint=idatapoint+1
                   exit
                endif
             enddo
          endif

          !print *,'About to search for forces' ! DEBUG
          !Forces
          if (weightsFr(istruc).gt.0d0) then
             !print *,'Forces:' ! DEBUG
             do ! now scan forward again to find Forces
                read(1,'(a80)') line
                if ((line(41:46).eq.'Forces').or.(line(29:34).eq.'Forces').or.(line(35:40)=='Forces').or.(line(47:52)=='Forces')) then        !(line(41:46).eq.'Forces') then
                   exit
                endif
             enddo
             !print *,'...found forces' ! DEBUG
             read(1,*)
             read(1,*)
             read(1,*)
             read(1,*)
             read(1,*)
             do iat=1,gn_forces(istruc)
                read(1,'(a106)') lineLong
                read(lineLong,*) snglchar,snglchar,snglchar,truedata(idatapointFr:idatapointFr+2)
                !if (iat.eq.1) then
                !   print *,truedata(idatapointFr:idatapointFr+2)
                !endif
                truedata(idatapointFr:idatapointFr+2)=truedata(idatapointFr:idatapointFr+2)!*hartreebohrovereVangstrom
                idatapointFr=idatapointFr+3
                idatapoint=idatapoint+3
             enddo
             do iat=1,gn_forces(istruc)
                optforce(iat,istruc)=.true.
             enddo
          endif

          !Stresses
          if (weightsSt(istruc).gt.0d0) then
             do
                read(1,'(a80)',IOSTAT=IOstatus) line
                if (IOstatus.lt.0) then
                   print *,'ERROR: Stress-tensor fitting specified for ',istrucInFile(istruc),'th ionic configuration '
                   print *,'of ',targetfiles(istruc),' but no stress data found, STOPPING.'
                   stop
                endif
                if ((line(20:32).eq.'Stress Tensor').or.(line(26:38).eq.'Stress Tensor')) then        !(line(41:46).eq.'Forces') then
                   exit
                endif
             enddo
             !print *,'found stresses...' ! DEBUG
             read(1,*)
             read(1,*)
             read(1,*)
             read(1,*)
             read(1,*)
             read(1,*) snglchar,snglchar,truedata(idatapointSt),string4,string5
             read(1,*) snglchar,snglchar,truedata(idatapointSt+3),truedata(idatapointSt+1),string4
             read(1,*) snglchar,snglchar,truedata(idatapointSt+4),truedata(idatapointSt+5),truedata(idatapointSt+2)
             !print *,'  ',truedata(idatapointSt:idatapointSt+5) ! DEBUG
             do idata=0,5
                truedata(idatapointSt+idata) = -truedata(idatapointSt+idata) * 10d0 * kB2eVperAng3 ! CASTEP has stresses in GPa (=10 kB), while VASP is in kB. For VASP we * by kB2eVperAng3, so here instead by 10*kB2eVperAng3. Note we have a minus because CASTEP has an opposite sign convention to MEAMfit (and to VASP)
             enddo
             if (excludeCoulomb.eqv..true.) then
                print *,'update readtargetdata for coulomb contribution for CASTEP'
                stop
             endif
             idatapoint=idatapoint+6
          endif
       endif

       targetfile_prev=trim(targetfiles(istruc))
       istrucInFile_prev=istrucInFile(istruc)
     
       if (excludeCoulomb.eqv..true.) then
          deallocate(coulombForce)
       endif
 
    enddo
    close(1)

    !print *,'Completed data read-in from DFT input files' ! DEBUG
    !stop

    !Define 'ndatapointsTrue' containing the ACTUAL number of
    !fitting points, used to print out the nubmer of fitting
    !points per parameter
    ndatapointsTrue=0
    do istruc=1,nstruct
        if (weightsEn(istruc).ne.0d0) then
            ndatapointsTrue=ndatapointsTrue+1
        endif
        if (weightsFr(istruc).gt.0d0) then
           do iat=1,gn_forces(istruc)
              if ( optforce(iat,istruc).eqv..true. ) then
                 ndatapointsTrue=ndatapointsTrue+3
              endif
           enddo
        endif
        if (weightsSt(istruc).gt.0d0) then
            ndatapointsTrue=ndatapointsTrue+6
        endif
    enddo

    if (excludeCoulomb.eqv..true.) then
       close(3)
       if (excludeCoulombMode==0) then
          print *,'coulombObservablesFull file created, STOPPING. Ready to run in parallel'
          createdCoulomb=.true.
          return
       endif
    endif

end subroutine readtargetdata
