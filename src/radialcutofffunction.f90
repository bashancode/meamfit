subroutine radialcutofffunction(q,hc)

    !------------------------------------------------------------c
    !
    !     Calculates the radial cutoff function, hc, used for
    !     radial density function. hc=1 if q<0; hc decreases
    !     from 1 to 0 over the interval 0<q<1; hc=0 if q>1.
    !
    !     Called by:     radialdensityfunction
    !     Calls:         -
    !     Arguments:     q
    !     Returns:       hc
    !     Files read:    -
    !     Files written: -
    !
    !     Marcel Sluiter, Feb 9 2006
    !
    !     Copyright (c) 2018, STFC
    !
    !------------------------------------------------------------c

    implicit none

    real*8 q,hc

    hc=1d0
    if(q.ge.1d0) then
        hc=0d0
    else if(q.gt.0d0) then
        hc=1d0-q*q*q*(6d0*q*q-15d0*q+10d0)
    endif

end subroutine radialcutofffunction
