subroutine broyden(iter,nbro,nstor,mixparam, &
        work, &
        x,f)
    ! driver program for Broyden, solves f(x)=0
    ! give a trial x and f(x), returns new guess for x
    ! needs to be called repeatedly until ||f|| is sufficiently small
    ! Marcel Sluiter, April 26 2000, Nov 28 2001

    ! iter  = current iteration number (first call, iter=1)
    ! nbro  = dimensionality of problem
    ! nstor = number of iterations that are stored
    ! mixparam = mixing parameter for first step. [0:1], usually about 0.1
    ! work     = work area: real(8) dimension(nbro+2*nstor*(1+nstor+nbro))
    !            DO NOT overwrite work!
    ! x        = input: guess for solving f(x)=0
    !            output: new guess
    ! f        = input: function to be set to zero, evaluated at x
    !            output: x(new_guess) - x(old_guess), change in x
    !
    !     Copyright (c) 2018, STFC

    implicit none
    integer iter,nbro,nstor,n,m
    real(8) x(nbro),f(nbro),mixparam,work(nbro+2*nstor*(1+nstor+nbro))
    intent(in) iter,nbro,nstor,mixparam

    n=nstor**2
    m=1+2*n+nbro+2*nstor
    call broyden_bns(iter,mixparam,nbro,nstor, &
        work(1),work(1+n),work(1+2*n),work(1+2*n+nbro), &
        work(1+2*n+nbro+nstor),work(m),work(m+nbro*nstor), &
        x,f)
end subroutine broyden
