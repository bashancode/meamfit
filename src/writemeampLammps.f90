 subroutine writemeamplammps

    !---------------------------------------------------------------c
    !
    !     Writes MEAM parameters from the 'p' array into ouput
    !     channel 2 (should be defined w.r.t a filename prior to
    !     call)
    !
    !     Called by:     optimizeparameters
    !     Calls:         -
    !     Arguments:     maxspecies,lmax,workparameterfile,p
    !     Returns:       -
    !     Files read:    -
    !     Files written: workparameterfile
    !
    !     Written by Prashanth Srinivasan
    !
    !     Copyright STFC 2018
    !
    !---------------------------------------------------------------c

    use m_filenames
    use m_atomproperties
    use m_generalinfo
    use m_optimization
    use m_meamparameters

    implicit none

    integer i,j,k,paracounter,paracounter2,rando

    !First check that non of the values are NaN
    do i=1,2*m3+(5+lmax)*m1+34*m2+12*lm1*m2
        if ((p(i).le.0d0).or.(p(i).ge.0d0)) then
            !ok...
        else
            print *,'NaN detected -not going to write', &
                'work_meam_parameters',p(i)
            !stop
        endif
    enddo
    write(2,*) '# RFMEAM - Embedding function :', element(speciestoZ(1:maxspecies))
    paracounter=2*m3+lmax*m1
    do i=1,maxspecies
        do j=1,maxspecies
                paracounter=paracounter+12*lm1
        enddo
    enddo
    !write(2,*) 'ef ' , element(speciestoZ(1:maxspecies))  

    write(2,"(A)", advance='no') ' ef'
    do i=1,maxspecies
        if (i.eq.maxspecies) then
                write(2,"(A20)") element(speciestoZ(i))
        else
                write(2,"(A20)",advance='no') element(speciestoZ(i))
        endif
    enddo

        
    
    write(2,"(A)", advance='no') ' ef.E0'
    do i=1,maxspecies
        if (i.eq.maxspecies) then
                write(2,"(F25.16)") -p(paracounter+m1)
        else
                write(2,"(F25.16)",advance='no') -p(paracounter+i)
        endif
    enddo
    write(2,"(A)", advance='no') ' ef.E1'
    do i=1,maxspecies
        if (i.eq.maxspecies) then
                write(2,"(F25.16)") p(paracounter+2*m1)
        else
                write(2,"(F25.16)",advance='no') p(paracounter+m1+i)
        endif
    enddo
    write(2,"(A)", advance='no') ' ef.E2'
    do i=1,maxspecies
        if (i.eq.maxspecies) then
                write(2,"(F25.16)") p(paracounter+3*m1)
        else
                write(2,"(F25.16)",advance='no') p(paracounter+2*m1+i)
        endif
    enddo
!    write(2,*) 'ef.E0 ' , -p(paracounter+1:paracounter+m1)
!    write(2,*) 'ef.E1 ' , p(paracounter+m1+1:paracounter+2*m1)
!    write(2,*) 'ef.E2 ' , p(paracounter+2*m1+1:paracounter+3*m1)
    write(2,*) '#'
    write(2,*) '# RFMEAM - Background electron density :', element(speciestoZ(1:maxspecies))
    !write(2,*) 'bed ' , element(speciestoZ(1:maxspecies))  
    write(2,"(A)", advance='no') ' bed'
    do i=1,maxspecies
        if (i.eq.maxspecies) then
                write(2,"(A20)") element(speciestoZ(i))
        else
                write(2,"(A20)",advance='no') element(speciestoZ(i))
        endif
    enddo
    !Multiplicative factors below are to accomodate for fact that my legendre
    !polynomials are not properly normalized (a la Baskes original paper, PRB
    !46 2727) unlike Prashanth's LAMMPS MEAM implementation which uses the 
    !properly normalized form of Thijse ( Mod. Sim. Mater. Sci. Eng. 19 015003)
   
    write(2,"(A)", advance='no') ' bed.t1'
    do i=1,maxspecies
        if (i.eq.maxspecies) then
                write(2,"(F25.16)") p(2*m3+maxspecies)
        else
                write(2,"(F25.16)",advance='no') p(2*m3+i)
        endif
    enddo
    write(2,"(A)", advance='no') ' bed.t2'
    do i=1,maxspecies
        if (i.eq.maxspecies) then
                write(2,"(F25.16)") p(2*m3+2*maxspecies)*(2d0/3d0)
        else
                write(2,"(F25.16)",advance='no') p(2*m3+maxspecies+i)*(2d0/3d0)
        endif
    enddo
    write(2,"(A)", advance='no') ' bed.t3'
    do i=1,maxspecies
        if (i.eq.maxspecies) then
                write(2,"(F25.16)") p(2*m3+3*maxspecies)*(2d0/5d0)
        else
                write(2,"(F25.16)",advance='no') p(2*m3+2*maxspecies+i)*(2d0/5d0)
        endif
    enddo
    paracounter=2*m3+lmax*m1
    write(2,"(A)", advance='no') ' bed.alpha10'
    do i=1,maxspecies
        if (i.eq.maxspecies) then
                write(2,"(F25.16)") p(paracounter+1+12*lm1*(maxspecies-1))
        else
                write(2,"(F25.16)",advance='no') p(paracounter+1+(i-1)*12*lm1)
        endif
    enddo    
    write(2,"(A)", advance='no') ' bed.alpha11'
    do i=1,maxspecies
        if (i.eq.maxspecies) then
                write(2,"(F25.16)") p(paracounter+1+12+12*lm1*(maxspecies-1))
        else
                write(2,"(F25.16)",advance='no') p(paracounter+1+12+(i-1)*12*lm1)
        endif
    enddo
    write(2,"(A)", advance='no') ' bed.alpha12'
    do i=1,maxspecies
        if (i.eq.maxspecies) then
                write(2,"(F25.16)") p(paracounter+1+24+12*lm1*(maxspecies-1))
        else
                write(2,"(F25.16)",advance='no') p(paracounter+1+24+(i-1)*12*lm1)
        endif
    enddo    
    write(2,"(A)", advance='no') ' bed.alpha13'
    do i=1,maxspecies
        if (i.eq.maxspecies) then
                write(2,"(F25.16)") p(paracounter+1+36+12*lm1*(maxspecies-1))
        else
                write(2,"(F25.16)",advance='no') p(paracounter+1+36+(i-1)*12*lm1)
        endif
    enddo
    write(2,"(A)", advance='no') ' bed.alpha20'
    do i=1,maxspecies
        if (i.eq.maxspecies) then
                write(2,"(F25.16)") p(paracounter+3+12*lm1*(maxspecies-1))
        else
                write(2,"(F25.16)",advance='no') p(paracounter+3+(i-1)*12*lm1)
        endif
    enddo
    write(2,"(A)", advance='no') ' bed.alpha21'
    do i=1,maxspecies
        if (i.eq.maxspecies) then
                write(2,"(F25.16)") p(paracounter+3+12+12*lm1*(maxspecies-1))
        else
                write(2,"(F25.16)",advance='no') p(paracounter+3+12+(i-1)*12*lm1)
        endif
    enddo
    write(2,"(A)", advance='no') ' bed.alpha22'
    do i=1,maxspecies
        if (i.eq.maxspecies) then
                write(2,"(F25.16)") p(paracounter+3+24+12*lm1*(maxspecies-1))
        else
                write(2,"(F25.16)",advance='no') p(paracounter+3+24+(i-1)*12*lm1)
        endif
    enddo
    write(2,"(A)", advance='no') ' bed.alpha23'
    do i=1,maxspecies
        if (i.eq.maxspecies) then
                write(2,"(F25.16)") p(paracounter+3+36+12*lm1*(maxspecies-1))
        else
                write(2,"(F25.16)",advance='no') p(paracounter+3+36+(i-1)*12*lm1)
        endif
    enddo
    write(2,"(A)", advance='no') ' bed.alpha30'
    do i=1,maxspecies
        if (i.eq.maxspecies) then
                write(2,"(F25.16)") p(paracounter+5+12*lm1*(maxspecies-1))
        else    
                write(2,"(F25.16)",advance='no') p(paracounter+5+(i-1)*12*lm1)
        endif
    enddo
    write(2,"(A)", advance='no') ' bed.alpha31'
    do i=1,maxspecies
        if (i.eq.maxspecies) then
                write(2,"(F25.16)") p(paracounter+5+12+12*lm1*(maxspecies-1))
        else
                write(2,"(F25.16)",advance='no') p(paracounter+5+12+(i-1)*12*lm1)
        endif
    enddo
    write(2,"(A)", advance='no') ' bed.alpha32'
    do i=1,maxspecies
        if (i.eq.maxspecies) then
                write(2,"(F25.16)") p(paracounter+5+24+12*lm1*(maxspecies-1))
        else    
                write(2,"(F25.16)",advance='no') p(paracounter+5+24+(i-1)*12*lm1)
        endif
    enddo
    write(2,"(A)", advance='no') ' bed.alpha33'
    do i=1,maxspecies
        if (i.eq.maxspecies) then
                write(2,"(F25.16)") p(paracounter+5+36+12*lm1*(maxspecies-1))
        else
                write(2,"(F25.16)",advance='no') p(paracounter+5+36+(i-1)*12*lm1)
        endif
    enddo
    write(2,"(A)", advance='no') ' bed.alpha40'
    do i=1,maxspecies
        if (i.eq.maxspecies) then
                write(2,"(F25.16)") p(paracounter+7+12*lm1*(maxspecies-1))
        else
                write(2,"(F25.16)",advance='no') p(paracounter+7+(i-1)*12*lm1)
        endif
    enddo
    write(2,"(A)", advance='no') ' bed.alpha41'
    do i=1,maxspecies
        if (i.eq.maxspecies) then
                write(2,"(F25.16)") p(paracounter+7+12+12*lm1*(maxspecies-1))
        else
                write(2,"(F25.16)",advance='no') p(paracounter+7+12+(i-1)*12*lm1)
        endif
    enddo
    write(2,"(A)", advance='no') ' bed.alpha42'
    do i=1,maxspecies
        if (i.eq.maxspecies) then
                write(2,"(F25.16)") p(paracounter+7+24+12*lm1*(maxspecies-1))
        else
                write(2,"(F25.16)",advance='no') p(paracounter+7+24+(i-1)*12*lm1)
        endif
    enddo
    write(2,"(A)", advance='no') ' bed.alpha43'
    do i=1,maxspecies
        if (i.eq.maxspecies) then
                write(2,"(F25.16)") p(paracounter+7+36+12*lm1*(maxspecies-1))
        else
                write(2,"(F25.16)",advance='no') p(paracounter+7+36+(i-1)*12*lm1)
        endif
    enddo
    write(2,"(A)", advance='no') ' bed.alpha50'
    do i=1,maxspecies
        if (i.eq.maxspecies) then
                write(2,"(F25.16)") p(paracounter+9+12*lm1*(maxspecies-1))
        else
                write(2,"(F25.16)",advance='no') p(paracounter+9+(i-1)*12*lm1)
        endif
    enddo
    write(2,"(A)", advance='no') ' bed.alpha51'
    do i=1,maxspecies
        if (i.eq.maxspecies) then
                write(2,"(F25.16)") p(paracounter+9+12+12*lm1*(maxspecies-1))
        else
                write(2,"(F25.16)",advance='no') p(paracounter+9+12+(i-1)*12*lm1)
        endif
    enddo
    write(2,"(A)", advance='no') ' bed.alpha52'
    do i=1,maxspecies
        if (i.eq.maxspecies) then
                write(2,"(F25.16)") p(paracounter+9+24+12*lm1*(maxspecies-1))
        else
                write(2,"(F25.16)",advance='no') p(paracounter+9+24+(i-1)*12*lm1)
        endif
    enddo
    write(2,"(A)", advance='no') ' bed.alpha53'
    do i=1,maxspecies
        if (i.eq.maxspecies) then
                write(2,"(F25.16)") p(paracounter+9+36+12*lm1*(maxspecies-1))
        else
                write(2,"(F25.16)",advance='no') p(paracounter+9+36+(i-1)*12*lm1)
        endif
    enddo
    write(2,"(A)", advance='no') ' bed.alpha60'
    do i=1,maxspecies
        if (i.eq.maxspecies) then
                write(2,"(F25.16)") p(paracounter+11+12+12*lm1*(maxspecies-1))
        else
                write(2,"(F25.16)",advance='no') p(paracounter+11+12+(i-1)*12*lm1)
        endif
    enddo
    write(2,"(A)", advance='no') ' bed.alpha61'
    do i=1,maxspecies
        if (i.eq.maxspecies) then
                write(2,"(F25.16)") p(paracounter+11+12+12*lm1*(maxspecies-1))
        else
                write(2,"(F25.16)",advance='no') p(paracounter+11+12+(i-1)*12*lm1)
        endif
    enddo
    write(2,"(A)", advance='no') ' bed.alpha62'
    do i=1,maxspecies
        if (i.eq.maxspecies) then
                write(2,"(F25.16)") p(paracounter+11+24+12*lm1*(maxspecies-1))
        else
                write(2,"(F25.16)",advance='no') p(paracounter+11+24+(i-1)*12*lm1)
        endif
    enddo
    write(2,"(A)", advance='no') ' bed.alpha63'
    do i=1,maxspecies
        if (i.eq.maxspecies) then
                write(2,"(F25.16)") p(paracounter+11+36+12*lm1*(maxspecies-1))
        else
                write(2,"(F25.16)",advance='no') p(paracounter+11+36+(i-1)*12*lm1)
        endif
    enddo
    write(2,"(A)", advance='no') ' bed.r10'
    do i=1,maxspecies
        if (i.eq.maxspecies) then
                write(2,"(F25.16)") p(paracounter+2+12*lm1*(maxspecies-1))
        else
                write(2,"(F25.16)",advance='no') p(paracounter+2+(i-1)*12*lm1)
        endif
    enddo
    write(2,"(A)", advance='no') ' bed.r11'
    do i=1,maxspecies
        if (i.eq.maxspecies) then
                write(2,"(F25.16)") p(paracounter+2+12+12*lm1*(maxspecies-1))
        else
                write(2,"(F25.16)",advance='no') p(paracounter+2+12+(i-1)*12*lm1)
        endif
    enddo
    write(2,"(A)", advance='no') ' bed.r12'
    do i=1,maxspecies
        if (i.eq.maxspecies) then
                write(2,"(F25.16)") p(paracounter+2+24+12*lm1*(maxspecies-1))
        else
                write(2,"(F25.16)",advance='no') p(paracounter+2+24+(i-1)*12*lm1)
        endif
    enddo
    write(2,"(A)", advance='no') ' bed.r13'
    do i=1,maxspecies
        if (i.eq.maxspecies) then
                write(2,"(F25.16)") p(paracounter+2+36+12*lm1*(maxspecies-1))
        else
                write(2,"(F25.16)",advance='no') p(paracounter+2+36+(i-1)*12*lm1)
        endif
    enddo
    write(2,"(A)", advance='no') ' bed.r20'
    do i=1,maxspecies
        if (i.eq.maxspecies) then
                write(2,"(F25.16)") p(paracounter+4+12*lm1*(maxspecies-1))
        else
                write(2,"(F25.16)",advance='no') p(paracounter+4+(i-1)*12*lm1)
        endif
    enddo
    write(2,"(A)", advance='no') ' bed.r21'
    do i=1,maxspecies
        if (i.eq.maxspecies) then
                write(2,"(F25.16)") p(paracounter+4+12+12*lm1*(maxspecies-1))
        else
                write(2,"(F25.16)",advance='no') p(paracounter+4+12+(i-1)*12*lm1)
        endif
    enddo
    write(2,"(A)", advance='no') ' bed.r22'
    do i=1,maxspecies
        if (i.eq.maxspecies) then
                write(2,"(F25.16)") p(paracounter+4+24+12*lm1*(maxspecies-1))
        else
                write(2,"(F25.16)",advance='no') p(paracounter+4+24+(i-1)*12*lm1)
        endif
    enddo
    write(2,"(A)", advance='no') ' bed.r23'
    do i=1,maxspecies
        if (i.eq.maxspecies) then
                write(2,"(F25.16)") p(paracounter+4+36+12*lm1*(maxspecies-1))
        else
                write(2,"(F25.16)",advance='no') p(paracounter+4+36+(i-1)*12*lm1)
        endif
    enddo
    write(2,"(A)", advance='no') ' bed.r30'
    do i=1,maxspecies
        if (i.eq.maxspecies) then
                write(2,"(F25.16)") p(paracounter+6+12*lm1*(maxspecies-1))
        else
                write(2,"(F25.16)",advance='no') p(paracounter+6+(i-1)*12*lm1)
        endif
    enddo
    write(2,"(A)", advance='no') ' bed.r31'
    do i=1,maxspecies
        if (i.eq.maxspecies) then
                write(2,"(F25.16)") p(paracounter+6+12+12*lm1*(maxspecies-1))
        else
                write(2,"(F25.16)",advance='no') p(paracounter+6+12+(i-1)*12*lm1)
        endif
    enddo
    write(2,"(A)", advance='no') ' bed.r32'
    do i=1,maxspecies
        if (i.eq.maxspecies) then
                write(2,"(F25.16)") p(paracounter+6+24+12*lm1*(maxspecies-1))
        else
                write(2,"(F25.16)",advance='no') p(paracounter+6+24+(i-1)*12*lm1)
        endif
    enddo
    write(2,"(A)", advance='no') ' bed.r33'
    do i=1,maxspecies
        if (i.eq.maxspecies) then
                write(2,"(F25.16)") p(paracounter+6+36+12*lm1*(maxspecies-1))
        else
                write(2,"(F25.16)",advance='no') p(paracounter+6+36+(i-1)*12*lm1)
        endif
    enddo
    write(2,"(A)", advance='no') ' bed.r40'
    do i=1,maxspecies
        if (i.eq.maxspecies) then
                write(2,"(F25.16)") p(paracounter+8+12*lm1*(maxspecies-1))
        else
                write(2,"(F25.16)",advance='no') p(paracounter+8+(i-1)*12*lm1)
        endif
    enddo
    write(2,"(A)", advance='no') ' bed.r41'
    do i=1,maxspecies
        if (i.eq.maxspecies) then
                write(2,"(F25.16)") p(paracounter+8+12+12*lm1*(maxspecies-1))
        else
                write(2,"(F25.16)",advance='no') p(paracounter+8+12+(i-1)*12*lm1)
        endif
    enddo
    write(2,"(A)", advance='no') ' bed.r42'
    do i=1,maxspecies
        if (i.eq.maxspecies) then
                write(2,"(F25.16)") p(paracounter+8+24+12*lm1*(maxspecies-1))
        else
                write(2,"(F25.16)",advance='no') p(paracounter+8+24+(i-1)*12*lm1)
        endif
    enddo
    write(2,"(A)", advance='no') ' bed.r43'
    do i=1,maxspecies
        if (i.eq.maxspecies) then
                write(2,"(F25.16)") p(paracounter+8+36+12*lm1*(maxspecies-1))
        else
                write(2,"(F25.16)",advance='no') p(paracounter+8+36+(i-1)*12*lm1)
        endif
    enddo
    write(2,"(A)", advance='no') ' bed.r50'
    do i=1,maxspecies
        if (i.eq.maxspecies) then
                write(2,"(F25.16)") p(paracounter+10+12*lm1*(maxspecies-1))
        else
                write(2,"(F25.16)",advance='no') p(paracounter+10+(i-1)*12*lm1)
        endif
    enddo
    write(2,"(A)", advance='no') ' bed.r51'
    do i=1,maxspecies
        if (i.eq.maxspecies) then
                write(2,"(F25.16)") p(paracounter+10+12+12*lm1*(maxspecies-1))
        else
                write(2,"(F25.16)",advance='no') p(paracounter+10+12+(i-1)*12*lm1)
        endif
    enddo
    write(2,"(A)", advance='no') ' bed.r52'
    do i=1,maxspecies
        if (i.eq.maxspecies) then
                write(2,"(F25.16)") p(paracounter+10+24+12*lm1*(maxspecies-1))
        else
                write(2,"(F25.16)",advance='no') p(paracounter+10+24+(i-1)*12*lm1)
        endif
    enddo
    write(2,"(A)", advance='no') ' bed.r53'
    do i=1,maxspecies
        if (i.eq.maxspecies) then
                write(2,"(F25.16)") p(paracounter+10+36+12*lm1*(maxspecies-1))
        else
                write(2,"(F25.16)",advance='no') p(paracounter+10+36+(i-1)*12*lm1)
        endif
    enddo
    write(2,"(A)", advance='no') ' bed.r60'
    do i=1,maxspecies
        if (i.eq.maxspecies) then
                write(2,"(F25.16)") p(paracounter+12+12+12*lm1*(maxspecies-1))
        else
                write(2,"(F25.16)",advance='no') p(paracounter+12+12+(i-1)*12*lm1)
        endif
    enddo
    write(2,"(A)", advance='no') ' bed.r61'
    do i=1,maxspecies
        if (i.eq.maxspecies) then
                write(2,"(F25.16)") p(paracounter+12+12+12*lm1*(maxspecies-1))
        else
                write(2,"(F25.16)",advance='no') p(paracounter+12+12+(i-1)*12*lm1)
        endif
    enddo
    write(2,"(A)", advance='no') ' bed.r62'
    do i=1,maxspecies
        if (i.eq.maxspecies) then
                write(2,"(F25.16)") p(paracounter+12+24+12*lm1*(maxspecies-1))
        else
                write(2,"(F25.16)",advance='no') p(paracounter+12+24+(i-1)*12*lm1)
        endif
    enddo
    write(2,"(A)", advance='no') ' bed.r63'
    do i=1,maxspecies
        if (i.eq.maxspecies) then
                write(2,"(F25.16)") p(paracounter+12+36+12*lm1*(maxspecies-1))
        else
               write(2,"(F25.16)",advance='no') p(paracounter+12+36+(i-1)*12*lm1)
        endif
    enddo
    do i=1,maxspecies
        do j=1,maxspecies
                paracounter=paracounter+12*lm1
        enddo
    enddo    
    paracounter=paracounter+4*m1
    paracounter2=0
    do i=1,maxspecies
        paracounter2=paracounter2+i
    enddo
    write(2,*) '#'
    write(2,*) '# RFMEAM - Pair potential'
    write(2,"(A)", advance='no') ' pp'
    do i=1,maxspecies
        do j=1,maxspecies
                if ((i.eq.maxspecies).and.(j.eq.maxspecies)) then
                        write(2,"(A20)",advance='no') element(speciestoZ(i))
                        write(2,"(A)",advance='no') '-'
                        write(2,"(A)") element(speciestoZ(j))
                else if (j.ge.i) then
                        write(2,"(A20)",advance='no') element(speciestoZ(i))
                        write(2,"(A)",advance='no') '-'
                        write(2,"(A)",advance='no') element(speciestoZ(j))
                endif
        enddo
    enddo    
    do i=1,16
        if (i.lt.10) then
                write(2,"(A,I1)",advance='no') ' pp.b',i
        else
                write(2,"(A,I2)",advance='no') ' pp.b',i
        endif
        rando=0
        do j=1,maxspecies
                do k=1,maxspecies
                        rando=rando+1
                        if ((k.ge.j).and.(j.eq.maxspecies)) then
                                write(2,"(F25.16)") p(paracounter+(2*i-1)+32*(rando-1))
                        else if (k.ge.j) then
                                write(2,"(F25.16)",advance='no') p(paracounter+(2*i-1)+32*(rando-1))
                        endif
                enddo
        enddo
    enddo       
    paracounter=paracounter+1   
    do i=1,16
        if (i.lt.10) then
                write(2,"(A,I1)",advance='no') ' pp.s',i
        else
                write(2,"(A,I2)",advance='no') ' pp.s',i
        endif
        rando=0
        do j=1,maxspecies
                do k=1,maxspecies
                        rando=rando+1
                        if ((k.ge.j).and.(j.eq.maxspecies)) then
                                write(2,"(F25.16)") p(paracounter+(2*i-1)+32*(rando-1))
                        else if (k.ge.j) then
                                write(2,"(F25.16)",advance='no') p(paracounter+(2*i-1)+32*(rando-1))
                        endif
                enddo
        enddo
    enddo

paracounter=paracounter-1

    write(2,*) '#'

    write(2,"(A)", advance='no') ' cutoffMax'
    write(2,"(F25.16)") p_rmax
    write(2,*) '#'
end subroutine writemeamplammps


