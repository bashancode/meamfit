subroutine rmforcenoise(en1,en2,dist,force,forcenew)

    !----------------------------------------------------------------------
    !
    !     Determine to how many s.f. the force is accurate to (assuming
    !     force calculation using finite difference method: force =
    !     - ( en2 - en1 ) / dist), and then truncate the remaining s.f.'s
    !
    !     Andrew Duff
    !
    !     Copyright (c) 2018, STFC
    !
    !----------------------------------------------------------------------

    use m_generalinfo

    implicit none

    integer orderen,orderfrc,frcprc,numdptomv,nint,tmp,orderendiff
    real(8) en1,en2,dist,force,forcenew

        ! write(*,'(A18,F30.20)') 'original energy: ',en1
        ! write(*,'(A18,F30.20)') 'displaced energy:',en2
        ! write(*,'(A6,F30.20,A8,F30.20)') 'diff=',en2-en1,', dist=',dist
    orderendiff=floor(log10(abs(en2-en1))) !log10 gets the order,
    !floor rounds down
          ! write(*,*) '...of order: O(',orderendiff,')'
    orderen=floor(log10(abs(en1)))
          !write(*,*) 'energy of order: O(',orderen,')'
          !write(*,*) 'therefore difference in energy occurs at ', &
          !       -orderendiff+orderen+1,'th s.f. of energy'
          !write(*,*) 'nsigdig for all calcs=',nsigdig
    if (-orderendiff+orderen+1.gt.nsigdig) then
         !       write(*,*) 'diff in en thus occurs greater than available', &
         !              ' precision, thus settings force=0'
        forcenew=0d0
    else
        frcprc=nsigdig+orderendiff-orderen
         !       write(*,*) 'diff, and thus force, therefore only precise to ', &
         !              frcprc
        !the following moves all s.f. to left of d.p., converts to
        !integer, then moves the digits back to their original
        !positions
        !(thus settings all those digits > no. s.f. to zero)
        orderfrc=floor(log10(abs(force)))
        numdptomv=frcprc-orderfrc-1
        !         write(61,*) 'moving digits in force left ',numdptomv,' digits'
        !         forcenew=floor(force*dble(10.d0**numdptomv))/
        !     +            dble(10.d0**numdptomv)
        tmp=nint(force*dble(10.d0**numdptomv))
        forcenew=dble(tmp)/dble(10.d0**numdptomv)
    endif
    ! write(*,'(A18,F30.20)') 'original force:  ',force
    ! write(*,'(A18,F30.20)') 'original force:  ',forcenew
    ! stop
end subroutine rmforcenoise
