subroutine readBoundsForRandParas

    !--------------------------------------------------------------c
    !
    !     Reads in the limits which are to be used to initialize
    !     the random starting values of the MEAM parameters from
    !     the 'dataforMEAMparagen' file.
    !
    !     Called by:
    !     Calls:
    !     Arguments: maxspecies,lmax
    !     Returns:
    !     Files read:
    !     Files written:
    !
    !     Andy Duff
    !
    !     Copyright (c) 2018, STFC
    !
    !--------------------------------------------------------------c

    use m_meamparameters
    use m_filenames
    use m_atomproperties
    use m_generalinfo
    use m_optimization

    implicit none

    integer i,j,ii,ll,ispc,jspc,spc_cnt
    character*80 string
    !Variables just used for reading in start_meam_parameters file:
    integer nfreepTmp

    allocate( meamtau_minorder(1:lmax,maxspecies), &
        meamtau_maxorder(1:lmax,maxspecies), &
        meamrhodecay_minradius(0:lmax,maxspecies,maxspecies), &
        meamrhodecay_maxradius(0:lmax,maxspecies,maxspecies), &
        meamrhodecay_minorder(1:6,0:lmax,maxspecies,maxspecies), &
        meamrhodecay_maxorder(1:6,0:lmax,maxspecies,maxspecies), &
        meame0_minorder(maxspecies), meame0_maxorder(maxspecies), &
        meamrho0_minorder(maxspecies), meamrho0_maxorder(maxspecies), &
        meamemb3_minorder(maxspecies), meamemb3_maxorder(maxspecies), &
        meamemb4_minorder(maxspecies), meamemb4_maxorder(maxspecies), &
        pairpotparameter_minradius(maxspecies,maxspecies), &
        pairpotparameter_maxradius(maxspecies,maxspecies), &
        pairpotparameter_minorder(1:8,maxspecies,maxspecies), &
        pairpotparameter_maxorder(1:8,maxspecies,maxspecies), &
        minordervaluepairpot(1:16), &
        maxordervaluepairpot(1:16), &
        nfreeppairpot(maxspecies,maxspecies), &
        enconst_minorder(maxspecies), enconst_maxorder(maxspecies) )

    !Set the maxradius variables to zero (then only those 'in use' will be used
    !in the 'checkParas' subroutine).
    meamrhodecay_maxradius=0d0
    pairpotparameter_maxradius=0d0

    open(unit=1,file=trim(settingsfile))

    print *,'looking for randparasmanual...'
    do
       read(1,*) string
       if (string.eq."RANDPARASMANUAL") then
          exit
       endif
    enddo
    print *,'   found'

    !meamtau
    do ll=1,lmax
        do ispc=1,maxspecies
            if (freep(2*m3+ispc+(ll-1)*m1).ge.2) then
                read(1,*) meamtau_minorder(ll,ispc)
                read(1,*) meamtau_maxorder(ll,ispc)
                print *,'l=',ll,', ispc=',ispc,', meamtau_minorder=',meamtau_minorder(ll,ispc),', meamtau_maxorder=',meamtau_maxorder(ll,ispc)
            endif
        enddo
    enddo

    !electron density functions
    spc_cnt=0
    do ispc=1,maxspecies
        do jspc=1,maxspecies
            if ((ispc.eq.1).or.(thiaccptindepndt.eqv..false.)) then
                do ll=0,lmax
                    !Check if we need to generate random paramaters for
                    !thi(ispc,jspc,ll)
                    nfreepTmp=0
                    do i=1,12
                        if (freep(2*m3+lmax*m1+12*lm1*spc_cnt+12*ll+i).ge.2) then
                            nfreepTmp=nfreepTmp+1
                        endif
                    enddo
                    if (nfreepTmp.gt.0) then
                       print *,'rho(',ispc,',',jspc,'):'
                        if (typethi(ispc,jspc).eq.1) then !Cubic form
                            !Read in parameters from 'dataforMEAMparagen'
                            !necessary
                            !to generate these parameters
                            read(1,*) meamrhodecay_maxradius(ll,ispc,jspc)
                            read(1,*) meamrhodecay_minradius(ll,ispc,jspc)
                            print *,'rho maxrad=',meamrhodecay_maxradius(ll,ispc,jspc)
                            print *,'rho minrad=',meamrhodecay_minradius(ll,ispc,jspc)

                            !For each coefficient which needs to be generated,
                            !read in
                            !min and max orders (10^..)
                            ii=1
                            do i=1,6
                                if (freep(2*m3+lmax*m1+12*lm1*spc_cnt+12*ll+2*i-1).ge.2) &
                                    then
                                    read(1,*) meamrhodecay_minorder(ii,ll,ispc,jspc)
                                    read(1,*) meamrhodecay_maxorder(ii,ll,ispc,jspc)
                                    print *,'rho minorder=',meamrhodecay_minorder(ii,ll,ispc,jspc)
                                    print *,'rho maxorder=',meamrhodecay_maxorder(ii,ll,ispc,jspc)
                                    ii=ii+1
                                endif
                            enddo
                        endif
                    endif
                enddo
            endif
            spc_cnt=spc_cnt+1
        enddo
    enddo

    !embedding functions
    read(1,*) embfuncRandType
    do ispc=1,maxspecies
        do i=1,4
            if (freep(2*m3+lmax*m1+12*lm1*m2+ispc+(i-1)*m1).ge.2) then
                if (i.eq.1) then
                    read(1,*) meame0_minorder(ispc)
                    read(1,*) meame0_maxorder(ispc)
                   print *,'meame0 minO=',meame0_minorder(ispc)
                   print *,'meame0 maxO=',meame0_maxorder(ispc)
                elseif (i.eq.2) then
                    read(1,*) meamrho0_minorder(ispc)
                    read(1,*) meamrho0_maxorder(ispc)
                   print *,'meamrho0 minO=',meamrho0_minorder(ispc)
                   print *,'meamrho0 maxO=',meamrho0_maxorder(ispc)
                elseif (i.eq.3) then
                    read(1,*) meamemb3_minorder(ispc)
                    read(1,*) meamemb3_maxorder(ispc)
                   print *,'meamemb3 minO=',meamemb3_minorder(ispc)
                   print *,'meamemb3 maxO=',meamemb3_maxorder(ispc)
                elseif (i.eq.4) then
                    read(1,*) meamemb4_minorder(ispc)
                    read(1,*) meamemb4_maxorder(ispc)
                   print *,'meamemb4 minO=',meamemb4_minorder(ispc)
                   print *,'meamemb4 maxO=',meamemb4_maxorder(ispc)
                endif
            endif
        enddo
    enddo

    !pair-potentials
    spc_cnt=0
    do ispc=1,maxspecies
        do jspc=1,maxspecies
            if (jspc.ge.ispc) then
                !Check if we need to generate random paramaters for
                !pairpot(isp,jspc)
                nfreeppairpot(ispc,jspc)=0
                do i=1,32
                    if (freep(2*m3+(4+lmax)*m1+12*lm1*m2+i+32*spc_cnt) &
                        .ge.2) then
                        nfreeppairpot(ispc,jspc)=nfreeppairpot(ispc,jspc)+1
                    endif
                enddo
                if (nfreeppairpot(ispc,jspc).gt.0) then
                    if (typepairpot(ispc,jspc).eq.2) then
                        !Read in parameters from 'dataforMEAMparagen' necessary
                        !to
                        !generate these parameters
                        read(1,*) pairpotparameter_maxradius(ispc,jspc)
                        read(1,*) pairpotparameter_minradius(ispc,jspc)
                        print *,'pairpot maxrad=',pairpotparameter_maxradius(ispc,jspc)
                        print *,'pairpot minrad=',pairpotparameter_minradius(ispc,jspc)

                        !For each coefficient which needs to be generated, read
                        !in min and max orders (10^..)
                        ii=1
                        do i=1,16
                            if (freep(2*m3+(4+lmax)*m1+12*lm1*m2+2*i-1+32*spc_cnt) &
                                .ge.2) then
                                read(1,*) pairpotparameter_minorder(ii,ispc,jspc)
                                read(1,*) pairpotparameter_maxorder(ii,ispc,jspc)
                                print *,'   minorder=',pairpotparameter_minorder(ii,ispc,jspc)
                                print *,'   maxorder=',pairpotparameter_maxorder(ii,ispc,jspc)
                                ii=ii+1
                            endif
                        enddo
                    else
                        print *,'typepairpot=',typepairpot(ispc,jspc), &
                            'not supported,stopping'
                        stop
                    endif
                endif
            endif
            spc_cnt=spc_cnt+1
        enddo
    enddo

    !enconst
    do ispc=1,maxspecies
        if (freep(m4+2*m2+ispc).ge.2) then
            read(1,*) enconst_minorder(ispc)
            read(1,*) enconst_maxorder(ispc) 
        endif
    enddo

    close(1)

end subroutine readBoundsForRandParas
