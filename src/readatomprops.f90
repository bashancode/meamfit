subroutine readAtomPropsFromVasprun

    !----------------------------------------------------------------------c
    !
    !     From the start position <modeling> in vasprun (read in through
    !     input channel 1) extract the following: nspecies, natoms, z, 
    !     nat, zz, and tr.
    !
    !     Called by:     
    !     Calls:         
    !     Arguments:    
    !     Returns:    
    !     Files read:   
    !     Files written:
    !
    !     Andrew Duff, 2014
    !
    !     Copyright (c) 2018, STFC
    !
    !----------------------------------------------------------------------c

    use m_poscar

    implicit none

    integer wordLength,i,j,isp,it1,marker,ios
    character*2 chemSym,chemSymPrev
    character*80 strng1,strng2,strng3, &
            strng1orig,strng2orig,strng3orig
    character*80 line
    character*80, allocatable:: words(:)

    !natoms is found under a line such as:   <atoms>      64</atoms>
    do
      read(1,'(a80)') line
      if (line(1:9).eq.'  <atoms>') then
         read(line,*) strng1,strng2
         call keepNumber(strng2)
         strng2=trim(strng2)
         !wordLength=len_trim(strng2)
         !strng2=strng2(1:wordLength-1)
         read(strng2,*) natoms
!         print *,'natoms=',natoms !21_10
         exit
      endif
    enddo

    !nspecies is found under a line such as:  <types>       2</types>
    do
      read(1,'(a80)') line
      if (line(1:9).eq.'  <types>') then
         read(line,*) strng1,strng2
         call keepNumber(strng2)
         strng2=trim(strng2)
         !wordLength=len_trim(strng2)
         !strng2=strng2(1:wordLength-1)
         read(strng2,*) nspecies
!         print *,'nspecies=',nspecies !21_10
         exit
      endif
    enddo
 
    if (.not.allocated(z)) then
       !allocate(nat(nspecies))
       allocate(z(nspecies))
       allocate(zz(natoms))
    endif
 
    do i=1,5
       read(1,*)
    enddo

    !Determine atomic numbers of species from this vasprun file: z(nspecies) and 
    !assign each atom the appropriate atomic number: zz(natoms)
    !atom types are of the form:     <rc><c>Zr</c><c>   1</c></rc>
    isp=1
    do i=1,natoms
       read(1,*) strng1
       chemSym(1:2)=strng1(8:9)
       if (i.eq.1) then
          call elementToZ(chemSym,z(isp))
       elseif (chemSym.ne.chemSymPrev) then
          isp=isp+1 !BUG: does this mean the code cannot read in unordered chemical species?
          call elementToZ(chemSym,z(isp))
       endif
       zz(i)=z(isp)
       chemSymPrev=chemSym
!       print *,'zz(',i,')=',zz(i)      !21_10
    enddo
!    do isp=1,nspecies               !21_10
!       print *,'z(',isp,')=',z(isp) !21_10
!    enddo                           !21_10

    !   !Old code: assumes 'blocks' of species
    !      allocate(words(nspecies))
 
    !      read(1,*) strng1
    !      words(1)=strng1(8:9)
    !      !print *,'first chem symbol=',words(1)
    !      isp=1
    !      marker=1
    !      do i=2,natoms
    !         read(1,*) strng1
    !         !print *,'read strng1=',strng1
    !         chemSym(1:2)=strng1(8:9)
    !         !print *,'... atom ',i,' chem symbol=',chemSym(1:2)
    !         if (chemSym.ne.words(isp)) then
    !            nat(isp)=i-marker
    !            marker=i
    !            isp=isp+1
    !            words(isp)=chemSym
    !         endif
    !      enddo
    !      nat(isp)=natoms-marker+1
    !      !print *,'nat(1:',nspecies,')=',nat(1:nspecies)
 
    !      !print *,'chemsymbs:',words(1:nspecies)
    !      call elementToZ(words)
    !      !print *,'Z(1:',nspecies,')=',Z(1:nspecies)
    !      !print *,'species per atom (zz):'
    !      it1=0
    !      do i=1,nspecies
    !          do j=1,nat(i)
    !              it1=it1+1
    !              zz(it1)=z(i)
    !              !print *,'zz(',it1,')=',zz(it1),'=z(',i,')=',z(i)
    !          enddo
    !      enddo
    !   print *
    !   print *,'Old code:'
    !   print *,'z=',z,', zz=',zz
    !   stop
 
    !Previously used to read in the basis and volume at the start of the vasprun
    !file. However, now I read them in at each 'positions', to allow for cell
    !relaxation calculations.
    !   do
    !     read(1,'(a80)') line
    !     if ((line(1:16).eq.'   <varray name=').and.(line(18:22).eq.'basis')) then
    !        !print *,'found <varray name= basis >'
    !        read(1,*) strng1,tr(1,1),tr(2,1),strng2
    !        !print *,'read line 1'
    !        call keepNumber(strng2)
    !        strng2=trim(strng2)
    !        !wordLength=len_trim(strng2)
    !        !strng2=strng2(1:wordLength-1)
    !        read(strng2,*) tr(3,1)
    !        read(1,*) strng1,tr(1,2),tr(2,2),strng2
    !        !print *,'read line 2'
    !        call keepNumber(strng2)
    !        strng2=trim(strng2)
    !        !wordLength=len_trim(strng2)
    !        !strng2=strng2(1:wordLength-1)
    !        read(strng2,*) tr(3,2)
    !        read(1,*) strng1,tr(1,3),tr(2,3),strng2
    !        !print *,'read line 3'
    !        call keepNumber(strng2)
    !        strng2=trim(strng2)
    !        !wordLength=len_trim(strng2)
    !        !strng2=strng2(1:wordLength-1)
    !        read(strng2,*) tr(3,3)
    !        !print *,'read line 4'
    !        
    !        !Also read in volume
    !        read(1,*)
    !        read(1,*) strng1,strng2,strng3
    !        strng3orig=strng3
    !        call keepNumber(strng3)
    !        strng3=trim(strng3)
    !        read(strng3,*,IOSTAT=ios) vol
    !        if (ios.ne.0) then
    !           print *,'ERROR: error reading volume from vasprun file'
    !           print *,'(string read from file=',strng3orig,'). STOPPING.'
    !           stop
    !        endif
    !        !print *,'read volume, =',vol

    !        exit
    !     endif
    !   enddo
    !print *,'finished read sub'

end subroutine readAtomPropsFromVasprun


subroutine readAtomPropsFromCastep_geom

    !----------------------------------------------------------------------c
    !
    !     By reading the .geom files we extract the following: 
    !     nspecies, natoms, z, nat, zz, and tr.
    !
    !     Note, we used to read in .castep files, so the code from this
    !     is retained, but commented out.
    !
    !     Called by:     
    !     Calls:         
    !     Arguments:    
    !     Returns:    
    !     Files read:   
    !     Files written:
    !
    !     Andrew Duff, 2018
    !
    !----------------------------------------------------------------------c

    use m_poscar

    implicit none

    logical foundSpecies
    integer wordLength,i,j,isp,it1,marker
    character*2 chemSym,chemSymPrev
    character*80 strng1
    character*106 lineLong
    character*80 line
    character*80, allocatable:: words(:)

    !print *,'Entered: readAtomPropsFromCastep'

    !Identify start of position block
    do
       read(1,'(a106)') lineLong
       !print *,'lineLong=',lineLong
       if (lineLong(102:106)=='<-- R') then
          exit
       endif
    enddo
    backspace(1)
    i=1
    isp=1 
    !Determine all the unique chemical elements and keep track of the number of
    !each species
    do
       read(1,'(a106)') lineLong
       !print *,'   lineLong=',lineLong
       if (lineLong(102:106)/='<-- R') then
          exit
       endif
       chemSym(1:2)=lineLong(2:3)
       !print *,'chemSym(1:2)=',chemSym(1:2)
       if ((i.gt.1).and.(chemSym.ne.chemSymPrev)) then
          isp=isp+1
       endif
       chemSymPrev=chemSym
       i=i+1
    enddo
    !print *,'   exited'
    natoms=i-1
    nspecies=isp

    if (.not.allocated(z)) then
       !allocate(nat(nspecies))
       allocate(z(nspecies))
       allocate(zz(natoms))
    endif

    !Now re-read and setup the z and zz arrays
    do i=1,natoms+1
       backspace(1)
    enddo

    isp=1
    do i=1,natoms
       read(1,'(a106)') lineLong
       chemSym(1:2)=lineLong(2:3)
       !print *,'chemSym(1:2)=',chemSym(1:2)
       if (i.eq.1) then
          call elementToZ(chemSym,z(isp))
          !print *,'z(',isp,')=',z(isp)
       elseif (chemSym.ne.chemSymPrev) then
          isp=isp+1
          call elementToZ(chemSym,z(isp))
       endif
       zz(i)=z(isp)
       chemSymPrev=chemSym
    enddo


    !  Old .castep code:
    !
    !  !natoms is found under a line such as:  Total number of ions in cell =    4
    !  do
    !     read(1,'(a80)') line
    !     if (line(26:53).eq.'Total number of ions in cell') then
    !        exit
    !     endif
    !  enddo
    !  strng1=trim(line(56:65))
    !  read(strng1,*) natoms
    !  print *,'natoms=',natoms

    !  !nspecies is on the next line...
    !  read(1,'(a80)') line
    !  strng1=trim(line(56:65))
    !  read(strng1,*) nspecies
    !  print *,'nspecies=',nspecies
    !  !stop

    !  if (.not.allocated(z)) then
    !     !allocate(nat(nspecies))
    !     allocate(z(nspecies))
    !     allocate(zz(natoms))
    !  endif

    !  !stop
 
    !  !read in the element labels per atom. These are of the form:
    !  !   x Al      1
    !  !   x Al      2
    !  !   x Ni      3 ...etc

    !  !Recode the following as in the previous subroutine
    !  allocate(words(nspecies))
 
    !  do i=1,6
    !     read(1,'(a80)') strng1
    !  enddo
    !  isp=1
    !  do i=1,natoms
    !     read(1,'(a80)') strng1
    !     chemSym(1:2)=strng1(16:17)
    !     if (i.eq.1) then
    !        call elementToZ(chemSym,z(isp))
    !     elseif (chemSym.ne.chemSymPrev) then
    !        isp=isp+1
    !        call elementToZ(chemSym,z(isp))
    !     endif
    !     zz(i)=z(isp)
    !     chemSymPrev=chemSym
    !  enddo

end subroutine readAtomPropsFromCastep_geom


subroutine readAtomPropsFromCastep_castep

    !----------------------------------------------------------------------c
    !
    !     By reading the .castep files we extract the following: 
    !     nspecies, natoms, z, nat, zz, and tr.
    !
    !     Called by:     
    !     Calls:         
    !     Arguments:    
    !     Returns:    
    !     Files read:   
    !     Files written:
    !
    !     Andrew Duff, 2018
    !
    !----------------------------------------------------------------------c

    use m_poscar
    use m_atomproperties

    implicit none

    logical foundSpecies
    integer wordLength,i,j,isp,it1,marker
    character*2 chemSym,chemSymPrev
    character*80 strng1
    character*106 lineLong
    character*80 line
    character*80, allocatable:: words(:)

    !  Old .castep code:
    
    !natoms is found under a line such as:  Total number of ions in cell =    4
    do
       read(1,'(a80)') line
       if (line(26:53).eq.'Total number of ions in cell') then
          exit
       endif
    enddo
    strng1=trim(line(56:65))
    read(strng1,*) natoms
    !print *,'natoms=',natoms

    !nspecies is on the next line...
    read(1,'(a80)') line
    strng1=trim(line(56:65))
    read(strng1,*) nspecies
    !print *,'nspecies=',nspecies
    !stop

    if (.not.allocated(z)) then
       !allocate(nat(nspecies))
       allocate(z(nspecies))
       allocate(zz(natoms))
    endif

    !stop
 
    !read in the element labels per atom. These are of the form:
    !   x Al      1
    !   x Al      2
    !   x Ni      3 ...etc

    !Recode the following as in the previous subroutine
    allocate(words(nspecies))
 
    do i=1,6
       read(1,'(a80)') strng1
    enddo
    isp=1
    do i=1,natoms
       read(1,'(a80)') strng1
       chemSym(1:2)=strng1(16:17)
       if (i.eq.1) then
          call elementToZ(chemSym,z(isp))
       elseif (chemSym.ne.chemSymPrev) then
          isp=isp+1
          call elementToZ(chemSym,z(isp))
       endif
       zz(i)=z(isp)
       chemSymPrev=chemSym
    enddo

    deallocate(words)

end subroutine readAtomPropsFromCastep_castep



subroutine readAtomPropsFromQuantumEspresso

    !----------------------------------------------------------------------c
    !
    !     Extract: nspecies, natoms, z, and zz.
    !
    !     Read in atom properties from the .out file.
    !
    !     Notes: for now ensure that formatting is, e.g., 
    !     "   nat= 128", I.e., two spaces, followed by string, followed by
    !     space, followed by number. Eventually the parser used in the
    !     settings file can be used to make this more user-friendly.
    !
    !     Called by:     
    !     Calls:         
    !     Arguments:    
    !     Returns:    
    !     Files read:   
    !     Files written:
    !
    !     Andrew Duff, Ian Shuttleworth, 2020
    !
    !----------------------------------------------------------------------c

    use m_poscar

    implicit none

    logical foundSpecies
    integer wordLength,i,j,isp,it1,marker
    character*2 chemSym,chemSymPrev
    character*6 strng6
    character*7 strng7
    character*80 strng1
    character*106 lineLong
    character*80 line
    character*80, allocatable:: words(:)

    !natoms
    do
       read(2,'(a106)') lineLong
       !print *,'lineLong(3:5)=',lineLong(3:5)
       if (lineLong(3:5)=='nat') then
          exit
       endif
    enddo
    backspace(2)
    read(2,'(A,I5)') strng6,natoms !previously I3
!    read(2,'(A,I)') strng6,natoms
    print *,strng6,natoms
    stop
    !print *,'natoms=',natoms

    !nspecies
    rewind(1)
    do
       read(2,'(a106)') lineLong
       !print *,'lineLong=',lineLong
       if (lineLong(3:6)=='ntyp') then
          exit
       endif
    enddo
    backspace(2)
    read(2,'(A,I3)') strng7,nspecies
    !print *,'nspecies=',nspecies

    if (.not.allocated(z)) then
       !allocate(nat(nspecies))
       allocate(z(nspecies))
       allocate(zz(natoms))
    endif

    !Z(nspecies)
    rewind(1)
    do
       read(2,'(a106)') lineLong
       !print *,'lineLong=',lineLong
       if (lineLong(1:14)=='ATOMIC_SPECIES') then
          exit
       endif
    enddo
    do isp=1,nspecies
       read(2,'(a106)') lineLong
       chemSym(1:2)=lineLong(1:2) !3:4
       call elementToZ(chemSym,z(isp))
    enddo

    !zz(nspecies)
    rewind(1)
    do
       read(2,'(a106)') lineLong
       !print *,'lineLong=',lineLong
       if (lineLong(1:16)=='ATOMIC_POSITIONS') then
          exit
       endif
    enddo
    i=1
    do i=1,natoms
       read(2,'(a106)') lineLong
       chemSym(1:2)=lineLong(2:3)
       !print *,'chemSym=',chemSym(1:2)
       call elementToZ(chemSym,zz(i))
    enddo

end subroutine readAtomPropsFromQuantumEspresso


subroutine keepNumber(strng)

!   Keeps numbers, d's, e's, g's and '-' signs in the char*80 strng. 
!   For keeping numerical components of strings (d for double precision, etc)

    implicit none

    integer i
    character*80 strng

    !print *,'starting string=',strng

    do i=1,80
       if ((strng(i:i).eq.'0').or. &
           (strng(i:i).eq.'1').or. &
           (strng(i:i).eq.'2').or. &
           (strng(i:i).eq.'3').or. &
           (strng(i:i).eq.'4').or. &
           (strng(i:i).eq.'5').or. &
           (strng(i:i).eq.'6').or. &
           (strng(i:i).eq.'7').or. &
           (strng(i:i).eq.'8').or. &
           (strng(i:i).eq.'9').or. &
           (strng(i:i).eq.'-').or. &
           (strng(i:i).eq.'d').or. &
           (strng(i:i).eq.'D').or. &
           (strng(i:i).eq.'e').or. &
           (strng(i:i).eq.'E').or. &
           (strng(i:i).eq.'g').or. &
           (strng(i:i).eq.'G').or. &
           (strng(i:i).eq.'.')) then
          !OK
       else
          strng(i:i)=' '
       endif
    enddo

    !print *,'finishing string=',strng
    !stop

end subroutine KeepNumber
