
      subroutine finalOutput

      !----------------------------------------------------------------------c
      !
      !      Print out and save final output relating to the potential
      !      currently stored in the module m_meamparameters. These
      !      include the fitted vs target energies, atomic forces and
      !      stress tensor components.
      !
      !      Andrew Duff 2017
      !
      !     Copyright (c) 2018, STFC
      !
      !----------------------------------------------------------------------c

      use m_datapoints
      use m_filenames
      use m_optimization
      use m_generalinfo
      use m_atomproperties
      use m_plotfiles
      use m_geometry
      use m_meamparameters
      use m_objectiveFunction
      use m_observables

      implicit none

      character*80 filename
      character*20 string1,string2,string3
      integer i,j,isp,k,ip,n,idatapoint,iat,hrs,mins,secs
      real(8) force(3)
      call objectiveFunction(.true.) !True, for calculation of largest
                !rho_i and for decomposing energy into pair-pot and
                !embed-func
      if (verbosity.gt.0) then
         print *
         print *,'---------------------------------------------------'
         if (cutoffPen) then
            print *,'Objective function, without penalty=',FnoPen
            print *,'(En|Frc|Str contributions: ',Fen,Ffrc,Fstr,')'
            print *,'(unscaled:',FnoScl,')'
            print *,'Objective function including penalty=',F
            print *,'(cutoff penalty:',FcutoffPen,')'
         else
            print *,'Objective function=',F, &
                ' (penalty function disactivated)'
            print *,'(unscaled:',FnoScl,')'
         endif
         print *,'---------------------------------------------------'
         print *
         print *,'rms error on energies=',funcen
         print *,'rms error on forces=',funcforc
         print *,'rms error on stresses=',funcstr
      endif

      if (verbosity.gt.0) call writemeamdatapoints(6,.false.)

      !call writederivedquantities(6)
      open(61,file='fitted_quantities.out')
      write(61,*) 'Variances of energies, forces and stress tensor components:'
      write(61,*) varEn,varFrc,varStr
      write(61,*) 'rms error on energies=',funcen
      write(61,*) 'rms error on forces=',funcforc
      write(61,*) 'rms error on stresses=',funcstr
      write(61,*)
      call writemeamdatapoints(61,.true.)
      close(61)

      110     format (F6.2,A,F6.2,A,F6.2,A,F6.2,A,F6.2,A,F6.2, &
          A,F6.2,A,F6.2,A,F6.2,A,F6.2,A,F6.2,A,F6.2, &
          A,F6.2,A,F6.2,A,F6.2,A,F6.2,A,F6.2,A,F6.2, &
          A,F6.2,A,F6.2,A,F6.2,A,F6.2,A,F6.2,A,F6.2, &
          A,F6.2,A,F6.2,A,F6.2,A,F6.2,A,F6.2,A,F6.2, &
          A,F6.2,A,F6.2,A,F6.2,A,F6.2,A,F6.2,A,F6.2,A,F6.2)
      close(51)

      if (verbosity.gt.0) then
         print *
         print *,'----------------------'
         print *,'Optimization completed'
         call displaytime(6,hrs,mins,secs)
         print *,'----------------------'
      endif

      if (writePot.eqv..true.) then

         if (lmax.eq.0) then
            !Open file for output of potential in LAMMPS format
            filename=""
            do isp=1,maxspecies
                write(string1,'(A2)') element(speciestoZ(isp))
                filename=trim(filename)//trim(string1)
            enddo
            string2='.eam.alloy'
            filename=trim(filename)//trim(string2)
            open(58,file=trim(adjustl(filename)))
            if (dlpolyOut) then
               open(70,file='TABEAM')
               open(71,file='FIELD')
            endif
         endif
                         
         !Output files (inc. LAMMPS, Camelon and potential plots
         call plotfunctions(.true.)

         if (lmax.eq.0) then
            close(58)
            if (dlpolyOut) then
               close(70)
               close(71)
            endif
         endif

         if (lmax.eq.3) then
            !LAMMPS MEAM potentials for use with Prashanth's altered
            !LAMMPS
            filename=""
            do isp=1,maxspecies
                write(string1,'(A2)') element(speciestoZ(isp))
                filename=trim(filename)//trim(string1)
            enddo
            string2='library.rfmeam.'
            filename=trim(string2)//trim(filename)
            open(2,file=trim(adjustl(filename)))
            call writemeamplammps
            close(2)
         endif

      endif

      if (verbosity.gt.1) then
         print *,'maximum background density encountered:',maxlargestrho
         print *,'(recorded per structure in largestrho.dat)'
      endif
      print *

      end subroutine finalOutput

