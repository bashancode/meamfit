subroutine getMEAM_EnergyForce(NIONS,NTYP,NITYP,elementNames,POSION,A,energyMEAM,forcesMEAM)

    !----------------------------------------------------------------------c
    !
    !  Subroutine to calculate the energy and atomic forces for a given
    !  configuration of ions.
    !
    !  This code is adapted from the main code in 'MEAMfit.90', and was
    !  designed originally for the purposes of calculating MEAM energies
    !  and forces from within VASP, within the TU-TILD approach.
    !
    !  Andrew Duff, Blazej Grabowski 2017
    !
    !     Copyright (c) 2018, STFC
    !
    !----------------------------------------------------------------------c

    use m_poscar
    use m_optimization
    use m_filenames
    use m_geometry
    use m_datapoints
    use m_generalinfo
    use m_meamparameters
    use m_atomproperties
    use m_observables
    use m_atomproperties

    implicit none

    logical maxOK,ok
    integer i,iseed,nattempts,istruc
    real(8) tmp

    !---- initiailize VASP variables and use to fill m_poscar ----
    integer NIONS, & !Number of atoms
            NTYP, &  !Number of species
            iatom,j
    integer NITYP(NTYP), & !Number of atoms of each species
            atomicNumbers(NTYP)     !Atomic number of each species
    CHARACTER (LEN=2) :: elementNames(NTYP)
    real(8) energyMEAM,aux1(3),aux2(3)
    real(8) A(3,3)
    real(8) POSION(1:3,NIONS),forcesMEAM(1:3,NIONS),ff(3)
    !Variables for analytic derivatives of meam energy. Not necessary to compute
    !here, but need to put in the call to meamenergy

    ok = .FALSE.
    do i=1,NTYP
      do j=1,size(element)
        if (elementNames(i).eq.element(j)) then
          atomicNumbers(i)=j
          ok=.TRUE.
        endif
      enddo
      if (.NOT.ok) then
        write (6,*) "ERROR: element ",elementNames(i)," not found in list"
        stop
      endif
    enddo

    natoms=NIONS
    maxspecies=NTYP
    nspecies=NTYP
    if (allocated(z)) deallocate(z)
    !if (allocated(nat)) deallocate(nat)
    allocate (z(maxspecies))!,nat(maxspecies))
    z=atomicNumbers
    !Set up zz array using NITYPE and atomicNumbers
    if (allocated(zz)) deallocate(zz)
    allocate (zz(natoms))
    iatom=1
    do i=1,maxspecies
       !nat(i)=NITYP(i)   <--commented this and the above out as I no longer use
       !nat. NEed to update this subroutine if I want to use it again.
       do j=1,NITYP(i)
          zz(iatom)=atomicNumbers(i)
          iatom=iatom+1
       enddo
    enddo
 !  print *,'atomicNumbers=',atomicNumbers,' NITYP=',NITYP
 !  print *,'zz=',zz
 !  stop
    tr=A
    if (allocated(coordinates)) deallocate(coordinates)
    allocate(coordinates(3,natoms))
    coordinates=POSION
    !Convert direct coordinates to cartesian
    do i=1,natoms
        aux1 = coordinates(1:3,i)
        aux2 = matmul(tr,aux1)
        coordinates(1:3,i) = aux2
    enddo
    if (allocated(coords_old)) deallocate(coords_old)
    allocate(coords_old(3,natoms))
    natoms_old=natoms
    do i=1,natoms
        coords_old(1:3,i)=coordinates(1:3,i)
    enddo
    !-------------------------------------------------------------

    call getTime(tStart)

    if (verbosity.gt.1) print *,'MEAMfit: calculating energies and atomic forces'

    !---- set up filenames; and set up for an energy calculation
    !followed ----
    !     by an atomic force calculation
    settingsfile='settings'
    nTargetFiles=2
    nstruct=2

    if (allocated(strucnames)) deallocate(strucnames)
    if (allocated(computeForces)) deallocate(computeForces)
    if (allocated(rlxstruc)) deallocate(rlxstruc)
    if (allocated(freeenergy)) deallocate(freeenergy)
    if (allocated(istrucInFile)) deallocate(istrucInFile)
    if (allocated(weightsEn)) deallocate(weightsEn)
    if (allocated(weightsFr)) deallocate(weightsFr)
    if (allocated(weightsSt)) deallocate(weightsSt)
    allocate( strucnames(nstruct),computeForces(nstruct),rlxstruc(nstruct),freeenergy(nstruct),istrucInFile(nstruct), &
              weightsEn(nstruct),weightsFr(nstruct),weightsSt(nstruct) )

    strucnames=""
    weightsEn=0d0
    weightsFr=0d0
    weightsSt=0d0
    weightsEn(1)=1d0     !Not used for the optfunc, but necessary for forces to be computed in Fnew
    weightsFr(2)=1d0
    forcefit=.true.
    computeForces(1)=.false.
    computeForces(2)=.true.
    freeenergy(1)=.true.
    freeenergy(2)=.false.
    istrucInFile(1)=1
    istrucInFile(2)=1
    rlxstruc(1)=0
    rlxstruc(2)=0
    !-------------------------------------------------------------------------

    call checkInputFilesExist !Check if 'fitdbse' and 'settings' files exist
    if (readpotfile.eqv..true.) then
        call extractlmax
    endif
    if (settingsfileexist.eqv..false.) then
       call setupdefaultlmax
    endif

    call readsettings(iseed) !read settings from settings file
    if (settingsfileexist.eqv..true.) then
       call setuprandomseed(iseed)
    endif
    !---- Initial meam-parameter set-up ----
    call initializemeam       !Initialize initial meam parameters

    if ((readParasOnebyone.eqv..false.).and.(readpotfile)) then
       !If a potential file is supplied by user, amend freep so that
       !non-zero parameters
       !in this file are optimized (unless PARASTOOPT is specified in
       !the settings file)
       call amendFreep
    endif
    call getRmax !Find the maximum interatomic separation that needs to be considered.

    !---- Set-up structures and fitting database ----
    call initializestruc_fromarrays(0) !Read in atomic configurations from vasprun files
    optforce=.true.
    if (verbosity.gt.1) print *,'outside initializestruc sub'
    !Determine the number of datapoints to read in from the outcar
    !files. This will depend on whether we are fitting to energies or
    !forces for each of the outcar files.

    !The following was originally in a 'readtargetdata' call, but in the
    !following we extract just the ndatapoints set up and array
    !initialization
    ndatapoints=0
    do istruc=1,nstruct
        if (weightsEn(istruc).ne.0d0) then
            ndatapoints=ndatapoints+1
        endif
        if (weightsFr(istruc).gt.0d0) then
            ndatapoints=ndatapoints+3*gn_forces(istruc)
        endif
    enddo
    !Allocate fitting arrays.
    if (allocated(truedata)) deallocate(truedata)
    if (allocated(fitdata)) deallocate(fitdata)
    if (allocated(summeam_paireStr)) deallocate(summeam_paireStr)
    if (allocated(summeamfStr)) deallocate(summeamfStr)
    if (allocated(bestfitdata)) deallocate(bestfitdata)
    if (allocated(smallestsepnStr)) deallocate(smallestsepnStr)
    if (allocated(smlsepnperspcStr)) deallocate(smlsepnperspcStr)
    if (allocated(lrgsepnperspcStr)) deallocate(lrgsepnperspcStr)
    allocate(truedata(ndatapoints),fitdata(ndatapoints), &
        bestfitdata(ndatapoints),summeam_paireStr(ndatapoints), &
        summeamfStr(ndatapoints),smallestsepnStr(ndatapoints), &
        smlsepnperspcStr(15,ndatapoints), &
        lrgsepnperspcStr(15,ndatapoints) )

    if (maxspecies.gt.5) then
       print *,'ERROR: Please increase smlsepnperspcStr and lrgsepnperspcStr arrays, STOPPING.'
       stop
    endif

    call calcSds       !Determine s.d's of energies and forces to rescale optfunc
    if (verbosity.gt.1) print *,'setting up nn tables'
    call setupnntables !Set up atom separation tables, also for forces (must
                       !come after 'readtargetdata' as this determines
                       !which forces are to be optimized)
    if (verbosity.gt.1) print *,'finding smallest sepn'
    call findsmallestsepn !Determine smallest and largest interatomic separations
    if ((randgenparas.eqv..false.).and.(readpotfile.eqv..false.)) then
       print *,'sepnHistogram file produced. No potential filename supplied and'
       print *,'-noopt/-no flag at command-line, STOPPING.'
       stop
    endif
    !------------------------------------------------
    if (verbosity.gt.1) print *,'checking pot'
    call checkPotType
    call allocateArrays
    !Note: before I introduced the call immediately above, Blazej found it
    !necessary to add the following lines to fix a bug:
    if (verbosity.gt.1) print *,'initializing noptp'
    if (.NOT.allocated(optmeamtau)) call initialize_noptp
    !If there is an issue with the code when running getMEAM_EnergyForce, 
    !please check these lines carefully

    !Print out energy and atomic forces and then stop (nsteps=1 makes
    !sure of this)
    istr=1
    call meamenergy
    energyMEAM=energy
    istr=2
    call meamforce
    forcesMEAM=forces

end subroutine getMEAM_EnergyForce
