      SUBROUTINE symnum(strng,type) 
      ! Subroutine to identify the type of "strng", numeric or symbolic 
      ! type returned is "INTG" for a string that contains a integer type 
      ! number (no decimal and no exponent marker); "NUME" if "strng"
      ! contains 
      ! a valid number real number; or "SYMB" otherwise. 
      ! valid characters in a number are: numbers from 0 to 9 
      ! the "+" and "-" symbols 
      ! decimal marker "." 
      ! "E" and "e" (exponent markers) 
      ! this routine expects input strings containing no leading blanks 
      ! Note: the presence of a double quote ('"') at the beginning of the 
      ! string is expected to be used to signal a string that would 
      ! otherwise be identified as being a scalar 
      !
      ! Vincent G (provided free on Yahoo.answers)
      
      implicit none

      CHARACTER*80 strng 
      CHARACTER*4 type 
      INTEGER lste,iper,i,jblnk
      
      iper=0 
      lste=0 
      jblnk=INDEX(strng,' ')-1 
      IF(jblnk.LT.0) jblnk=80 
      ! get first blank position: this is the depth of the input string 
      
      type='INTG' 
      ! assume this is an integer number 
      DO 10 i=1,jblnk 
      ! plain number detected 
      IF(strng(i:i).GE.'0' .AND. strng(i:i).LE.'9') GOTO 10 
      ! sign detection, must be before any number or right after an exponent
      ! marker 
      IF(strng(i:i).EQ.'+'.OR.strng(i:i).EQ.'-') THEN 
      ! sign after the exponent mark or sign in the first position 
      IF(lste.EQ.(i-1)) GOTO 10 
      ! else, not a number 
      GOTO 20 
      ! the letter "E" is present, may be text or maybe the exponent marker 
      ELSEIF(strng(i:i).EQ.'E'.OR.strng(i:i).EQ.'e') THEN 
      type='NUME' 
      ! downgrade to real type number 
      IF(lste.EQ.0) THEN 
      lste=i 
      GOTO 10 
      ELSE 
      ! there was another "E" before 
      GOTO 20 
      ENDIF 
      ! the period symbol is valid, but only once 
      ELSEIF(strng(i:i).EQ.'.') THEN 
      type='NUME' 
      ! downgrade to real type number 
      IF(iper.NE.0) GOTO 20 
      iper=i 
      ELSE 
      ! any other character is invalid for a number and is considered a
      ! separator 
      GOTO 20 
      ENDIF 
      10 CONTINUE 
      RETURN 
      20 type='SYMB' 
      RETURN 
      END 
