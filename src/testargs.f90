subroutine testargs

    !-------------------------------------------------------------------c
    !
    !     Check if arguments have been supplied from command line.
    !
    !     Called by:     program MEAMfit
    !     Calls:         -
    !     Returns:       contjob, noOpt, readpotfile
    !
    !     Andrew Duff 2014
    !
    !     Copyright (c) 2018, STFC
    !
    !-------------------------------------------------------------------c

    use m_generalinfo
    use m_optimization
    use m_meamparameters
    use m_filenames

    implicit none

    integer i,narg,num
    character*4 type
    character*20 name,name2
    character*80 filename

    !defaults:
    verbosity=2
    readpotfile=.false.
    contjob=.false.

    narg=command_argument_count()

    if (narg.gt.0) then
       i=1
       do
          call get_command_argument(i,name)
          select case(adjustl(name))
          case("-help","-h")
             i=i+1
             call get_command_argument(i,name2)
             if (adjustl(name2).eq."command-line") then
                print *
                print *,'Command-line options:'
                print *,'  -potfilein/-i filename   Read in potential from file: filename. Used either'
                print *,'                       with -onehot/-o (see below) to check fitted data, or'
                print *,'                       to provide starting point for further optimization.'
                print *,'  -noopt/-no           One iteration; no random para-gen (use in combination'
                print *,'                       with -potfilein/-i filename to check fitted data for a'
                print *,'                       given potential.'
             elseif (adjustl(name2).eq."settings-options") then
                print *,'Settings-file commands:'
                print *,'  Necessary to set:'
                print *,'  OPTPARAREADIN=1/2    The input method for specifying which parameters are'
                print *,'                       to be optimized. 1: select the parameters one-by-one'
                print *,'                       using command PARASTOOPT. 2: use keywords'
                print *,'                       (NTERMS_DENS, NTERMS_PP, NTERMS_EMB, etc) to specify'
                print *,'                       parameters.'
                print *,'  Optional:'
                print *,'  POTFILEIN=filename   Read in potential from file: filename.'
                print *,'  STARTPARAS=1/2       1: no random generation of potential parameters'
                print *,'                       (overrides all other commands relating to random'
                print *,'                       generation of potential parameters; this option requires'
                print *,'                       POTFILEIN). 2: enable random generation of potential'
                print *,'                       parameters (with the particular parameters to optimized'
                print *,'                       determined by other commands in settings files)'
                print *,'  NOOPT=TRUE           No optimization'
             else
                print *
                print *,'Options:'
                print *,'  -help command-line   Show possible command-line options.'
                print *,'  -help settings-options  Show possible variables for inclusion in settings'
                print *,'                       file.'
             endif
             stop
          case("-noopt","-no")
             noOpt=.true.
         !case("-ncutoff","-nc")
         !   i=i+1
         !   call get_command_argument(i,name)
         !   call symnum(name,type)
         !   if (type.ne."INTG") then
         !      print *,'Warning, no integer after -ncutoff/-nc, STOPPING.'
         !      stop
         !   endif
         !   read(name, '(i10)' ) nCutoffOverride
          case("-potfilein","-i")
             i=i+1
             call get_command_argument(i,startparameterfile)
             call symnum(startparameterfile,type)
             if (type.ne."SYMB") then
                print *,'Warning, expected filename after -potin/-i, STOPPING.'
                stop
             endif
             readpotfile=.true.
             !print *,'file=',startparameterfile
             !stop
          case("-cont")
             contjob=.true.
          case("-verbosity","-verb")
             i=i+1
             call get_command_argument(i,name2)
             call symnum(name2,type)
             if (type.ne."INTG") then
                print *,'Warning, expected integer from 0-1 after -verbosity/-verb, STOPPING.'
                stop
             endif
             read(name2,'(I1)') verbosity
          case default
             print *,'Command-line option un-known; stopping.'
             stop
          end select
          if (i.eq.narg) then
             exit
          endif
          i=i+1
       enddo
    endif

end subroutine testargs
