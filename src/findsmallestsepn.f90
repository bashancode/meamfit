subroutine findsmallestsepn

    !     Calculate the smallest and largest seperations (less than p_rmax)
    !     as a function of pairs of species. Write a file containing the 
    !     smallest interatomic separation as a function of file number and 
    !     species, and also store for later output in the 'fitted_quantities'
    !     file. This data is useful when choosing fitting database and/or 
    !     understanding why some energies/forces are not well reproduced by 
    !     potential.
    !
    !     Andrew Duff, 2014-2016
    !
    !     Copyright (c) 2018, STFC

    use m_geometry
    use m_atomproperties
    use m_generalinfo
    use m_optimization
    use m_meamparameters

    implicit none

    integer i,j,jj,nni,isp,jsp,cnt, &
        isp_forwrite,jsp_forwrite,ibin,isp_test,jsp_test,cumulSepns, &
        numsepnsTot,cutoff,isp2,jsp2,ip,maxspecies2
    integer, parameter :: nbin = 100 ! Num bins used to store atom sepns
    integer storeids(maxspecies,maxspecies,nstruct,2), &
        numsepns(maxspecies,maxspecies)
    integer, allocatable:: numsepnsinbin(:,:,:),numsepns_linarray(:)
    real(8) dsepn,sepn,tmp,piPrefactor,tofileTmp
    real(8) smallestsepn,largestsepn,smlsepnperspc(maxspecies,maxspecies), &
        lrgsepnperspc(maxspecies,maxspecies)
    real(8), allocatable:: tofileSml(:),tofileLrg(:),tofile(:,:),tofile2(:)

    maxspecies2=maxspecies*(maxspecies+1)/2
    allocate(numsepns_linarray(maxspecies2),tofileSml(maxspecies2), &
        tofileLrg(maxspecies2),tofile(nbin,maxspecies2),tofile2(maxspecies2))
    if (allocated(lrgsepnperspc_overall)) deallocate(lrgsepnperspc_overall)
    if (allocated(smlsepnperspc_overall)) deallocate(smlsepnperspc_overall)
    if (allocated(sdSepn)) deallocate(sdSepn,avgSepn,nSepn)

    allocate(numsepnsinbin(nbin,maxspecies,maxspecies))
    allocate(smlsepnperspc_overall(maxspecies,maxspecies), &
             lrgsepnperspc_overall(maxspecies,maxspecies))
    allocate(sdSepn(maxspecies,maxspecies), &
             avgSepn(maxspecies,maxspecies), &
             nSepn(maxspecies,maxspecies))

    !Record largest and smallest seperations for each structure (and also the
    !same, for each species combinations). Also record the largest and smallest
    !seperations over all structures.
    open(60,file='smallestsepnvsstruc.dat')
    write(60,*) '# structure-id | smallest sepn'

    !Set-up initial values of smallest and largest overall (across all
    !structures) seperations to ensure, e.g., zero is not wrongly returned as
    !the smallest seperation.
    smlsepn_overall=1000d0
    lrgsepn_overall=0d0
    do isp=1,maxspecies
        do jsp=1,maxspecies
            smlsepnperspc_overall(isp,jsp)=1000d0
            lrgsepnperspc_overall(isp,jsp)=0d0
        enddo
    enddo
    numsepns=0d0

    do istr=1,nstruct
        !Set up initial values as above but for use 'per structure'
        smallestsepn=1000d0
        largestsepn=0d0
        do isp=1,maxspecies
            do jsp=1,maxspecies
                smlsepnperspc(isp,jsp)=1000d0
                lrgsepnperspc(isp,jsp)=0d0
            enddo
        enddo
        do i=1,gn_inequivalentsites(istr)
            isp=gspecies(i,istr)
            nni=gn_neighbors(i,istr)
            do jj=1,nni
                j=gneighborlist(jj,i,istr)
                jsp=gspecies(j,istr)
                smallestsepn=min(smallestsepn,diststr(jj,i,0,0,istr))
                largestsepn=max(largestsepn,diststr(jj,i,0,0,istr))
                smlsepn_overall=min(smlsepn_overall, &
                    diststr(jj,i,0,0,istr))
                !print *,'istr=',istr,' i=',i,' j=',j,', smlsepn_overall=',smlsepn_overall
                lrgsepn_overall=max(lrgsepn_overall, &
                    diststr(jj,i,0,0,istr))
                !Ensure we only store separations between different species
                !in the array smlsepnperspc(:,:) for the case where the
                !first index is smaller than the second.
                if (isp.gt.jsp) then
                    isp_forwrite=jsp
                    jsp_forwrite=isp
                else
                    isp_forwrite=isp
                    jsp_forwrite=jsp
                endif
                numsepns(isp_forwrite,jsp_forwrite)= &
                     numsepns(isp_forwrite,jsp_forwrite)+1
                if (diststr(jj,i,0,0,istr).lt. &
                    smlsepnperspc(isp_forwrite,jsp_forwrite)) then
                    smlsepnperspc(isp_forwrite,jsp_forwrite)= &
                        diststr(jj,i,0,0,istr)
                    storeids(isp_forwrite,jsp_forwrite,istr,1)=i
                    storeids(isp_forwrite,jsp_forwrite,istr,2)=j
                endif
                if ((diststr(jj,i,0,0,istr).gt. &
                    lrgsepnperspc(isp_forwrite,jsp_forwrite)) .and. &
                    (diststr(jj,i,0,0,istr).lt. &
                     p_rmax)) then
                    lrgsepnperspc(isp_forwrite,jsp_forwrite)= &
                        diststr(jj,i,0,0,istr)
                endif
                if (smallestsepn.eq.0d0) then
                    print *,'error, smallestsepn=0 in structure ',istr,', atom ',i,', nearest neighbour ',jj
                    print *,'diststr(',jj,',',i,',0,0,',istr,')=',diststr(jj,i,0,0,istr),', STOPPING.'
                    stop
                endif
            enddo
        enddo
        !Check if the smallest or largest seperations found for this structure
        !are the smallest or largest overall across all structures
        cnt=0
        do isp=1,maxspecies
            do jsp=1,maxspecies
                if (isp.le.jsp) then
                    cnt=cnt+1
                    if (cnt.gt.maxspecies2) then
                        !print *,'increase array size in findsmallestsepn'
                        print *,'improperdefinition of maxspecies2 in findsmalllestsepn'
                        stop
                    endif
                    tofileSml(cnt)=smlsepnperspc(isp,jsp)
                    tofileLrg(cnt)=lrgsepnperspc(isp,jsp)
                    smlsepnperspc_overall(isp,jsp)=min( &
                           smlsepnperspc_overall(isp,jsp), &
                           smlsepnperspc(isp,jsp) )
                    lrgsepnperspc_overall(isp,jsp)=max( &
                           lrgsepnperspc_overall(isp,jsp), &
                           lrgsepnperspc(isp,jsp) )
                endif
            enddo
        enddo
        smallestsepnStr(istr)=smallestsepn
        write(60,'(I4,A1,F10.5)',advance="no") istr,' ', &
            smallestsepn
        do i=1,cnt
            smlsepnperspcStr(i,istr)=tofileSml(i)
            lrgsepnperspcStr(i,istr)=tofileLrg(i)
            write(60,'(A1,F10.5)',advance="no") ' ',tofileSml(i)
        enddo
        smlsepnperspcStr_numEntries=cnt
        write(60,*)
    enddo

    !Check if any interatomic separations are absent from the fitting set. Then,
    !if the corresponding pair-wise terms are being optimized, report an error.
    do isp=1,maxspecies
       do jsp=1,maxspecies
          if (isp.le.jsp) then
             if (lrgsepnperspc_overall(isp,jsp).eq.0d0) then
                !No isp-jsp pairs in fitting set - check if any potential
                !parameters are redundant.
                !  !Check electron densities:
                !  isp2=1
                !  jsp2=1
                !  do ip=2*m3+lmax*m1+1,2*m3+lmax*m1+12*lm1*m2,12*lm1
                !     !Check if we are at relevant portion of array
                !     if ((isp2.eq.isp).and.(jsp2.eq.jsp)) then
                !        !Test if any of the parameters are to be optimized
                !        if ( ANY(freep(ip:ip+12*lm1-1).gt.0) ) then
                !           print *
                !           write(*,'(A48,I1,A5,I1,A23)') 'ERROR: No interatomic separations between atoms ',isp,' and ',jsp, &
                !             ' located in fitting set'
                !           write(*,'(A51,I1,A1,I1,A12)') 'yet you have selected to optimize electron density(',isp,',',jsp, &
                !             '), STOPPING.'
                !           stop
                !        endif
                !     endif
                !     if (jsp2.lt.maxspecies) then
                !        jsp2=jsp2+1
                !     elseif (isp2.lt.maxspecies) then
                !        jsp2=1
                !        isp2=isp2+1
                !     endif
                !  enddo
                !Check pair-potentials:
                isp2=1
                jsp2=1
                do ip=2*m3+(4+lmax)*m1+12*lm1*m2+1,m4,32
                   !Check if we are at relevant portion of array
                   if ((isp2.eq.isp).and.(jsp2.eq.jsp)) then
                      !Test if any of the parameters are to be optimized
                      if ( ANY(freep(ip:ip+31).gt.0) ) then
                         print *
                         write(*,'(A48,I1,A5,I1,A23)') 'ERROR: No interatomic separations between atoms ',isp,' and ',jsp, &
                           ' located in fitting set'
                         write(*,'(A48,I1,A1,I1,A12)') 'yet you have selected to optimize pairpotential(',isp,',',jsp, &
                           '), STOPPING.'
                         stop
                      endif
                   endif
                   if (jsp2.lt.maxspecies) then
                      jsp2=jsp2+1
                   elseif (isp2.lt.maxspecies) then
                      jsp2=1
                      isp2=isp2+1
                   endif
                enddo
             endif
          endif
       enddo
    enddo

    !Correct for double counting of the number of separations in the above, and
    !also setup 'tofile' variables to help writing data to files
    cnt=0
    do isp=1,maxspecies
       do jsp=1,maxspecies
          if (isp.le.jsp) then
             cnt=cnt+1
             numsepns(isp,jsp)=numsepns(isp,jsp)/2
             numsepns_linarray(cnt)=numsepns(isp,jsp)
          endif
       enddo
    enddo

    write(60,*) 'smallest sepns across all files:'
    do isp=1,maxspecies
        do jsp=1,maxspecies
            if (isp.le.jsp) then
               write(60,*) isp,jsp,':',smlsepnperspc_overall(isp,jsp)
            endif
        enddo
    enddo
    write(60,*) 'overall :',smlsepn_overall
    write(60,*) 
    write(60,*) 'largest sepns across all files:'
    do isp=1,maxspecies
        do jsp=1,maxspecies
            if (isp.le.jsp) then
                write(60,*) isp,jsp,':',lrgsepnperspc_overall(isp,jsp)
            endif
        enddo
    enddo
    write(60,*) 'overall :',lrgsepn_overall
    close(60)

    !For each of the seperations provided in the previous file, record the id
    !numbers (in the order read in from the vasprun.xml/POSCAR files) for the
    !atoms corresponding to these seperations.
    open(60,file='idsforsmallestsepns')
    write(60,*)
    write(60,*) '# ids of atoms (i,j) for above smallest sepns'
    write(60,*) '(for species, e.g. (1,1); (1,2); (2,2))'
    do istr=1,nstruct
        write(60,'(I4,A2)',advance="no") istr,': '
        do isp=1,maxspecies
            do jsp=1,maxspecies
                if (isp.le.jsp) then
                    write(60,'(A1,I4,A1,I4)',advance="no") ' ', &
                        storeids(isp,jsp,istr,1),',',storeids(isp,jsp,istr,2)
                endif
            enddo
        enddo
        write(60,*)
    enddo
    close(60)

    !Also produce histogram showing distribution of separations for different
    !pairs of species
    dsepn=(lrgsepn_overall-smlsepn_overall)/dble(nbin)
    do ibin=1,nbin
       sepn=smlsepn_overall+dble(ibin-1)*dsepn
       cnt=0
       do isp=1,maxspecies
          do jsp=1,maxspecies
             if (isp.le.jsp) then
                cnt=cnt+1
                tofile(ibin,cnt)=0
                numsepnsinbin(ibin,isp,jsp)=0
                do istr=1,nstruct
                   tofileTmp=0d0
                   !print *,'gn_forces(',istr,')=',gn_forces(istr),', gn_inequivalentsites(',istr,')=',gn_inequivalentsites(istr)
                   !stop
                   do i=1,gn_forces(istr)
                       isp_test=gspecies(i,istr)
                       nni=gn_neighbors(i,istr)
                       do jj=1,nni
                           j=gneighborlist(jj,i,istr)
                           jsp_test=gspecies(j,istr)
                           !Ensure we only store separations between different species
                           !in the array numsepnsinbin(:,:,:) for the case where the
                           !second index is smaller than the third.
                           ! if (isp_test.gt.jsp_test) then
                           !     isp_forwrite=jsp_test
                           !     jsp_forwrite=isp_test
                           ! else
                               isp_forwrite=isp_test
                               jsp_forwrite=jsp_test
                           ! endif
                           if ((isp_forwrite.eq.isp).and.(jsp_forwrite.eq.jsp)) then
                              if ((diststr(jj,i,0,0,istr).ge.sepn).and. &
                                  (diststr(jj,i,0,0,istr).lt.sepn+dsepn)) then
                                 numsepnsinbin(ibin,isp_forwrite,jsp_forwrite)=numsepnsinbin(ibin,isp_forwrite,jsp_forwrite)+1
                                 tofileTmp=tofileTmp+1
                              endif
                           endif
                       enddo
                   enddo
                   tofile(ibin,cnt) = tofile(ibin,cnt) + tofileTmp * g_vol(istr) 
                   !print *,'g_vol(',istr,')=',g_vol(istr)
                   !stop
                enddo
                tofile(ibin,cnt) = tofile(ibin,cnt) / nstruct
                !numsepnsinbin(ibin,isp,jsp)=numsepnsinbin(ibin,isp,jsp)/2 !Correct for double counting
             endif
          enddo
       enddo
       numsepnsTot=0d0
       do i=1,cnt
          numsepnsTot=numsepnsTot+ dble(numsepns_linarray(i))
       enddo
       !print *,'numsepnsTot=',numsepnsTot

    enddo

    tofile2=0
    cnt=0
    do isp=1,maxspecies
       do jsp=1,maxspecies
          if (isp.le.jsp) then
             cnt=cnt+1
             tofile2(cnt)=nAtomsPerSpecies(isp)*nAtomsPerSpecies(jsp)
          endif
       enddo
    enddo

    open(60,file='sepnHistogram.out')
    write(60,*) '# sepn   no. sepns within range, per species (e.g. (1,1); (1,2); (2,2))'
    piPrefactor=4d0/3d0*3.14159265359d0
    do ibin=1,nbin
       sepn=smlsepn_overall+dble(ibin-1)*dsepn
       write(60,'(F10.5,A1)',advance="no") sepn,' '
       do i=1,cnt
           !print *,'sepn=',sepn,': tofile(',ibin,',',i,')=',tofile(ibin,i),', tofile2(',i,')=',tofile2(i),', 4/3pi(sepn+dsepn)**3 - (sepn**3)=',piPrefactor*((sepn+dsepn)**3 - (sepn**3))
           
           ! here we use form used by lammps and vasppy. namely (Zr and Ba as
           ! examples):
           ! g(r_ibin)= hist_all * V / ( 4/3pi(r_{ibin+1}^3-r_ibin^3) * n_Zr atoms * n_Ba atoms  ) 
           ! (n_Zr atoms = # of Zr atoms participating in histogram)
           ! V=volume of cell
           ! (also remember to average over no. configs)

           write(60,'(A1,F10.5)',advance="no") ' ',dble(tofile(ibin,i)) / & ! 'tofile' = hist_all * V, in the above equation
                                   ( tofile2(i) * piPrefactor*( (sepn+dsepn)**3 - (sepn**3) ) ) ! 'tofile2^2' = n_Zr*n_Ba in the above
       enddo
       write(60,*)

    enddo
    close(60)

    !Determine average seperations between species
    avgSepn=0
    nSepn=0
    do isp=1,maxspecies
       do jsp=1,maxspecies
          if (isp.le.jsp) then
             do istr=1,nstruct
                do i=1,gn_inequivalentsites(istr)
                   isp_test=gspecies(i,istr)
                   nni=gn_neighbors(i,istr)
                   do jj=1,nni
                      j=gneighborlist(jj,i,istr)
                      jsp_test=gspecies(j,istr)
                      if ((isp_test.eq.isp).and.(jsp_test.eq.jsp)) then
                         avgSepn(isp,jsp)=avgSepn(isp,jsp)+diststr(jj,i,0,0,istr)
                         nSepn(isp,jsp)=nSepn(isp,jsp)+1
                      endif
                   enddo
                enddo
             enddo
             avgSepn(isp,jsp)=avgSepn(isp,jsp)/nSepn(isp,jsp)
          endif
       enddo
    enddo

    sdSepn=0
    do isp=1,maxspecies
       do jsp=1,maxspecies
          if (isp.le.jsp) then
             do istr=1,nstruct
                do i=1,gn_inequivalentsites(istr)
                   isp_test=gspecies(i,istr)
                   nni=gn_neighbors(i,istr)
                   do jj=1,nni
                      j=gneighborlist(jj,i,istr)
                      jsp_test=gspecies(j,istr)
                      if ((isp_test.eq.isp).and.(jsp_test.eq.jsp)) then
                         sdSepn(isp,jsp)=sdSepn(isp,jsp)+ &
                   (diststr(jj,i,0,0,istr)-avgSepn(isp,jsp))**2
                      endif
                   enddo
                enddo
             enddo
             sdSepn(isp,jsp)=sqrt(sdSepn(isp,jsp)/nSepn(isp,jsp))
             !print *,'For isp=',isp,', jsp=',jsp,', average sepn=',avgSepn(isp,jsp), &
             !    ', s.d.=',sdSepn(isp,jsp)
          endif
       enddo
    enddo

    !Determine cutoff radii to use in the first stage of optimization if holdcutoffs=.true.
    if (holdcutoffs.and.(nsteps.gt.1)) then
       allocate(cutoffDens(6,maxspecies,maxspecies),cutoffPairpot(16,maxspecies,maxspecies))
       !ncutoffDens
       if (thiaccptindepndt.eqv..true.) then
          do isp=1,maxspecies
             numsepnsTot=0
             do jsp=1,maxspecies
                numsepnsTot=numsepnsTot+numsepns(MIN(isp,jsp),MAX(isp,jsp))
             enddo
             cutoff=1
             cumulSepns=0
             do ibin=1,nbin
                sepn=smlsepn_overall+dble(ibin)*dsepn !sepn here defined as
                           !upper limit of each bin rather than lower limit
                tmp=0d0
                do jsp=1,maxspecies
                   cumulSepns=cumulSepns+numsepnsinbin(ibin,MIN(isp,jsp),MAX(isp,jsp))
                   !cumulSepns=cumulSepns+numsepnsinbin(ibin,1,2)
                   tmp=tmp+numsepnsinbin(ibin,MIN(isp,jsp),MAX(isp,jsp))
                enddo
                write(50+isp,*) sepn,tmp/(dble(numsepnsTot)*dble(dsepn))
                if ( (cumulSepns.ge.cutoff*numsepnsTot/ncutoffDens(1,isp)) .and. & 
                       (cutoff.lt.ncutoffDens(1,isp)) ) then
                   !Assign the last cutoff outside the loop, because for
                   !some reason numsepnsTot and cumulSepns (after summation)
                   !differ slightly
                   cutoffDens(cutoff,1,isp)=sepn
                   cutoff=cutoff+1
                endif
             enddo
             cutoffDens(cutoff,1,isp)=lrgsepn_overall
           enddo
        else
           print *,'Please extend code in: findsmallestsepn, STOPPING'
           stop
        endif

        !pairpot:
        do isp=1,maxspecies
           do jsp=1,maxspecies
              if (jsp.ge.isp) then
                 cutoff=1
                 cumulSepns=0
                 do ibin=1,nbin
                    sepn=smlsepn_overall+dble(ibin)*dsepn
                    cumulSepns=cumulSepns+numsepnsinbin(ibin,isp,jsp)
                    if ( (cumulSepns.ge.cutoff*numsepns(isp,jsp)/ncutoffPairpot(isp,jsp))  &
                           .and.(cutoff.lt.ncutoffPairpot(isp,jsp)) ) then
                       cutoffPairpot(cutoff,isp,jsp)=sepn
                       cutoff=cutoff+1
                    endif
                 enddo
                 cutoffPairpot(cutoff,isp,jsp)=lrgsepn_overall
              endif
           enddo
        enddo

    endif

    deallocate(numsepnsinbin,numsepns_linarray,tofileSml,tofileLrg,tofile)

end subroutine findsmallestsepn
