

module m_filenames

    !     Copyright (c) 2018, STFC

    !Contains the number of poscar files to be read in. Also contains
    !the names of the following files: the poscar and outcar files;
    !the files containing the input, output and workspace MEAM
    !parameters; a file which can be used to store the atomic
    !positions of one of the poscar files in xyz format; a settings
    !file; a file containing a list of all of the poscar files; a
    !file containing a list of all of the outcar files.
    logical dlpolyOut,potFuncOut,outputLAMMPSposns,createdFitdbse,createdSettings,createdCoulomb
    integer nTargetFiles,nLammpsFiles,outputVsTime,nextHourRec
    character*80, allocatable:: targetfiles(:),strucnames(:)
    integer, allocatable:: DFTdata(:) ! 0=vasprun; 1=castep; 2=quantum espresso
    character*80 startparameterfile,crystalstrucfile,settingsfile, &
        fitdbse,lookuptablefile
    character*80 lammpsPotIn(10)

end module m_filenames
