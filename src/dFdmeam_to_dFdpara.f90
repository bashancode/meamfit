subroutine dFdmeam_to_dFdpara

    !---------------------------------------------------------------c
    !
    !     Copies the derivatives of the objective function, F, w.r.t
    !     the sensibly-named MEAM variables (cmin, etc), onto the 
    !     dF_dpara() array. This is a precursor to setting up the
    !     dF_dpopt() array for sending to the CG optimizer.
    !
    !     Caled by:     program MEAMfit,initializemeam,meamenergy
    !
    !     Andrew Duff
    !
    !     Copyright (c) 2018, STFC
    !
    !---------------------------------------------------------------c

    use m_filenames
    use m_meamparameters
    use m_optimization
    use m_atomproperties
    use m_generalinfo
    use m_objectiveFunction

    implicit none

    integer i,j,k,l,m,isp,jsp,ip

    dF_dpara=0d0

    isp=1
    l=1
    do i=2*m3+1,2*m3+lmax*m1
       dF_dpara(i)=dF_dmeamtau(l,isp)
       !print *,'dF_dpara(',i,')=',dF_dpara(i),', dF_dmeamtau(',l,',',isp,')=',dF_dmeamtau(l,isp)
       if (isp.lt.maxspecies) then
          isp=isp+1
       else
          isp=1
          l=l+1
       endif
    enddo
    !print *,'in dFdmeam_to_dFdpara:'
    !print *
    !print *,'dF_dmeamtau=',dF_dmeamtau
    !print *,'dF_dpara(2*m3+1,2*m3+lm1*m1)=',dF_dpara(2*m3+1:2*m3+lm1*m1)

    ip=1
    l=0
    isp=1
    jsp=1
    do i=2*m3+lmax*m1+1,2*m3+lmax*m1+12*lm1*m2
        if (isp.eq.1) then
           dF_dpara(i)=dF_draddens(ip,l,jsp)
        else
           dF_dpara(i)=0d0
        endif
        !print *,'conv to p(',i,')=',p(i)
        if (ip.lt.12) then
            ip=ip+1
        elseif (l.lt.lmax) then
            ip=1
            l=l+1
        elseif (jsp.lt.maxspecies) then
            ip=1
            l=0
            jsp=jsp+1
        elseif (isp.lt.maxspecies) then
            ip=1
            l=0
            isp=isp+1
            jsp=1
        endif
    enddo
    dF_dpara(2*m3+lmax*m1+12*lm1*m2+1: &
        2*m3+m1+lmax*m1+12*lm1*m2)=dF_dmeame0(1:maxspecies)
    dF_dpara(2*m3+m1+lmax*m1+12*lm1*m2+1: &
        2*m3+2*m1+lmax*m1+12*lm1*m2)=dF_dmeamrho0(1:maxspecies)
    dF_dpara(2*m3+2*m1+lmax*m1+12*lm1*m2+1: &
        2*m3+3*m1+lmax*m1+12*lm1*m2)=dF_dmeamemb3(1:maxspecies)
    dF_dpara(2*m3+3*m1+lmax*m1+12*lm1*m2+1: &
        2*m3+4*m1+lmax*m1+12*lm1*m2)=dF_dmeamemb4(1:maxspecies)

    ip=1
    if (maxspecies.eq.1) then
        do i=2*m3+(4+lmax)*m1+12*lm1*m2+1,2*m3+(4+lmax)*m1+12*lm1*m2+32
            dF_dpara(i)=dF_dpairpot(ip,1,1)
            ip=ip+1
        enddo
    else
        isp=1
        jsp=1
        do i=2*m3+(4+lmax)*m1+12*lm1*m2+1,m4
            dF_dpara(i)=dF_dpairpot(ip,isp,jsp)
            if (ip.lt.32) then
                ip=ip+1
            elseif (jsp.lt.maxspecies) then
                ip=1
                jsp=jsp+1
            elseif (isp.lt.maxspecies) then
                ip=1
                jsp=1
                isp=isp+1
            endif
        enddo
    endif

    if (opt_enconst) then
       dF_dpara(m4+2*m2+1:m4+2*m2+m1)=dF_denconst
    endif

    !print *,'dF_dpara=',dF_dpara
    !stop
end subroutine dFdmeam_to_dFdpara
