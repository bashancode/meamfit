subroutine setupstage2freep

    !--------------------------------------------------------------c
    !
    !     Change freep so that all parameters optimize but are not
    !     randomly generated (freep values of 1)
    !
    !     Called by: MEAMfit
    !     Calls: -
    !     Returns: freep
    !     Files read: -
    !     Files written: -
    !
    !     Andy Duff, Oct 2016.
    !
    !     Copyright (c) 2018, STFC
    !
    !--------------------------------------------------------------c

    use m_meamparameters
    use m_filenames
    use m_atomproperties
    use m_generalinfo
    use m_optimization

    implicit none

    !logical sorted
    !integer iseed
    integer i,j,ll,isp,jsp,spc_cnt

    !First reinstate the cutoffs:

    !---- electron density functions ----
    spc_cnt=0
    do isp=1,maxspecies
        do jsp=1,maxspecies
            if ((isp.eq.1).or.(thiaccptindepndt.eqv..false.)) then
                do ll=0,lmax
                    do i=2,12,2
                        freep(2*m3+lmax*m1+12*lm1*spc_cnt+12*ll+i)= &
                            freep_backup(2*m3+lmax*m1+12*lm1*spc_cnt+12*ll+i)
                    enddo
                enddo
            endif
            spc_cnt=spc_cnt+1
        enddo
    enddo
    !------------------------------------

    !---- pair-potentials ----
    spc_cnt=0
    do isp=1,maxspecies
        do jsp=1,maxspecies
            if (jsp.ge.isp) then
                if (nfreeppairpot(isp,jsp).gt.0) then
                    if (typepairpot(isp,jsp).eq.2) then
                        do i=2,16,2
                            freep(2*m3+(4+lmax)*m1+12*lm1*m2+2*i+32*spc_cnt)= &
                                freep_backup(2*m3+(4+lmax)*m1+12*lm1*m2+2*i+32*spc_cnt)
                        enddo
                    endif
                endif
            endif
            spc_cnt=spc_cnt+1
        enddo
    enddo
    !--------------------------------------------------------

    !Now change so that all parameters optimize but are not randomly generated
    do i=1,np
       if (freep(i).gt.0) then
          freep(i)=1
       endif
    enddo

    deallocate(freep_backup)

end subroutine setupstage2freep
