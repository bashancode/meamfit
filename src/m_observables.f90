

module m_observables

    !Copyright (c) 2018, STFC

    !Energies and their derivatives w.r.t potential parameters
    real(8) energy,summeam_paire,summeamf

    !Derivatives w.r.t meam parameters
    real(8), allocatable:: dsummeamf_dmeamtau(:,:),dsummeamf_dpara(:,:,:,:), &
        dsummeamf_dmeame0(:),dsummeamf_dmeamrho0(:),dsummeamf_dmeamemb3(:), &
        dsummeamf_dmeamemb4(:),dsummeam_paire_dpara(:,:,:),dsummeam_paire_dpara_dummy(:,:,:)
    !Interaction energy derivatives
    real(8), allocatable:: dintEn_fit_dmeamtau(:,:),dintEn_fit_draddens(:,:,:), &
        dintEn_fit_dmeame0(:),dintEn_fit_dmeamrho0(:),dintEn_fit_dmeamemb3(:), &
        dintEn_fit_dmeamemb4(:),dintEn_fit_dpairpot(:,:,:)
    !Also store values for reference configurations
    real(8), allocatable:: dsummeamf_dmeamtau_ref(:,:,:),dsummeamf_dpara_ref(:,:,:,:,:), &
        dsummeamf_dmeame0_ref(:,:),dsummeamf_dmeamrho0_ref(:,:),dsummeamf_dmeamemb3_ref(:,:), &
        dsummeamf_dmeamemb4_ref(:,:),dsummeam_paire_dpara_ref(:,:,:,:)

    !Forces and their derivatives w.r.t potential parameters
    real(8), allocatable:: forces(:,:),dforce_dpairpot(:,:,:,:,:),dforce_draddens(:,:,:,:,:), &
        dforce_dmeamtau(:,:,:,:),dforce_dmeame0(:,:,:),dforce_dmeamrho0(:,:,:), &
        dforce_dmeamemb3(:,:,:),dforce_dmeamemb4(:,:,:)

    !Stress tensor and its derivatives w.r.t potential parameters
    real(8) stressTensor(9)
    real(8), allocatable:: dstressTensor_dpairpot(:,:,:,:),dstressTensor_draddens(:,:,:,:), &
        dstressTensor_dmeamtau(:,:,:),dstressTensor_dmeame0(:,:),dstressTensor_dmeamrho0(:,:), &
        dstressTensor_dmeamemb3(:,:), dstressTensor_dmeamemb4(:,:)

end module m_observables
