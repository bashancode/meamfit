subroutine embeddingfunction

    !-------------------------------------------------------------c
    !
    !     Calculates the embedding-function, F(rho_i) (=meam_f(i)
    !     in the code), for each of the inequivalent sites for
    !     of the 'istr'th structure. Also computes the derivatives
    !     of F(rho_i) w.r.t. potential parameters
    !
    !     Called by:     meamenergy
    !     Calls:         -
    !     Returns:       meam_f
    !     Files read:    -
    !     Files written: -
    !
    !     Andrew Duff, Marcel Sluiter
    !
    !     Copyright (c) 2018, STFC
    !
    !-------------------------------------------------------------c

    use m_generalinfo
    use m_atomproperties
    use m_geometry
    use m_meamparameters
    use m_electrondensity
    use m_optimization
    use m_observables

    implicit none

    integer iat,isp,jsp,ll,iiCoeff,iiCutoff,ip
    real(8) sqrt_rhoi,rhoi_sqr,rhoi_cube,rhoi_quart,dmeamf_drhoi

    !print *,'in embeddingfunction'
    !  if (allocated(dmeamf_dpara)) deallocate(dmeamf_dpara, &
    !           dmeamf_dmeame0,dmeamf_dmeamrho0,dmeamf_dmeamemb3,dmeamf_dmeamemb4)
    !  allocate( dmeamf_dpara(12,0:lmax,1:maxspecies,gn_inequivalentsites(istr)), &
    !            dmeamf_dmeame0(gn_inequivalentsites(istr)), &
    !            dmeamf_dmeamrho0(gn_inequivalentsites(istr)), &
    !            dmeamf_dmeamemb3(gn_inequivalentsites(istr)), &
    !            dmeamf_dmeamemb4(gn_inequivalentsites(istr)) )

    summeamf=0d0

    if (embfunctype.eq.1) then

       if (lmax.eq.0) then

          dmeamf_dpara=0d0

          do iat=1,gn_inequivalentsites(istr)
              !print *,'iat=',iat
              isp=gspecies(iat,istr)
              if (rho_i(iat).lt.0d0) then
                 print *,'negative density!!!!'
                 print *,'rho_i(',iat,')=',rho_i(iat)
                 stop
              endif
              !print *,'rho_i(iat)=',rho_i(iat)
              sqrt_rhoi=sqrt(rho_i(iat))
              rhoi_sqr=(rho_i(iat)**2)
              rhoi_cube=rhoi_sqr*rho_i(iat)
              rhoi_quart=rhoi_sqr**2
              meam_f(iat)= -meame0(isp)*sqrt_rhoi &
                  + meamrho0(isp)*rhoi_sqr &
                  + meamemb3(isp)*rhoi_cube &
                  + meamemb4(isp)*rhoi_quart
              summeamf=summeamf+meam_f(iat)
              !print *,isp,' ',iat,' ',rho_i(iat),' ',meam_f(iat)
              !Calculate analytic derivatives of meam_f(i)
              !Derivatives w.r.t meame0, etc:
              dmeamf_dmeame0(iat)=-sqrt_rhoi
              dmeamf_dmeamrho0(iat)=rhoi_sqr
              dmeamf_dmeamemb3(iat)=rhoi_cube
              dmeamf_dmeamemb4(iat)=rhoi_quart

              if (rho_i(iat).ne.0d0) then
                 dmeamf_drhoi= - 0.5d0*meame0(isp)/sqrt_rhoi &
                          + 2d0*meamrho0(isp)*rho_i(iat) &
                          + 3d0*meamemb3(isp)*rhoi_sqr &
                          + 4d0*meamemb4(isp)*rhoi_cube
              else
                 !If rho_i=0, drhoi_dpara will also = 0 below,
                 !so the dmeamf_dpara should = 0. However if
                 !we evaluate the singularity above (/sqrt_rhoi),
                 !dmeamf_dpara will wrongly be set to NaN.
                 dmeamf_drhoi=0d0
              endif
              !Below, isp is used to obtain the parameter indices for the
              !pairwise density 
              !functions, so should =1 unless using acceptor-dependant densities
              isp=1
              if (thiaccptindepndt.eqv..false.) isp=gspecies(iat,istr)
              do jsp=1,maxspecies
                 !Derivatives w.r.t the pairwise density parameters:
                 do iiCoeff=1,noptdensityCoeff(0,isp,jsp)
                    ip = ioptdensityCoeff(iiCoeff,0,isp,jsp)
                    dmeamf_dpara(ip,0,jsp,iat)= &
                          dmeamf_drhoi * drhoi_dpara(ip,0,jsp,iat)
                 enddo
                 do iiCutoff=1,noptdensityCutoff(0,isp,jsp)
                    ip = ioptdensityCutoff(iiCutoff,0,isp,jsp)
                    dmeamf_dpara(ip,0,jsp,iat)= &
                          dmeamf_drhoi * drhoi_dpara(ip,0,jsp,iat)
                 enddo
              enddo

          enddo

       else

          !  if (allocated(dmeamf_dmeamtau)) deallocate(dmeamf_dmeamtau)
          !  allocate( dmeamf_dmeamtau(1:lmax,1:maxspecies,gn_inequivalentsites(istr)) )

          dmeamf_dpara=0d0

          do iat=1,gn_inequivalentsites(istr)
              isp=gspecies(iat,istr)
              if (rho_i(iat).lt.0d0) then
                 print *,'negative density!!!!'
                 stop
              endif
              sqrt_rhoi=sqrt(rho_i(iat))
              rhoi_sqr=(rho_i(iat)**2)
              rhoi_cube=rhoi_sqr*rho_i(iat)
              rhoi_quart=rhoi_sqr**2
              meam_f(iat)= -meame0(isp)*sqrt_rhoi &
                  + meamrho0(isp)*rhoi_sqr &
                  + meamemb3(isp)*rhoi_cube &
                  + meamemb4(isp)*rhoi_quart
              !print *,'meam_f(',iat,')=',meam_f(iat),' rho_i(',iat,')=',rho_i(iat)
              summeamf=summeamf+meam_f(iat)

              !Calculate analytic derivatives of meam_f(i)
              !Derivatives w.r.t meame0, etc:
              dmeamf_dmeame0(iat)=-sqrt_rhoi
              dmeamf_dmeamrho0(iat)=rhoi_sqr
              dmeamf_dmeamemb3(iat)=rhoi_cube
              dmeamf_dmeamemb4(iat)=rhoi_quart

              if (rho_i(iat).ne.0d0) then
                 dmeamf_drhoi= - 0.5d0*meame0(isp)/sqrt_rhoi &
                          + 2d0*meamrho0(isp)*rho_i(iat) &
                          + 3d0*meamemb3(isp)*rhoi_sqr &
                          + 4d0*meamemb4(isp)*rhoi_cube
              else
                 !If rho_i=0, drhoi_dmeamtau and drhoi_dpara will also = 0 below,
                 !so the dmeamf_dmeamtau and dmeamf_dpara should = 0. However if
                 !we evaluate the singularity above (/sqrt_rhoi), dmeamf_dmeamtau
                 !and dmeamf_dpara will wrongly be set to NaN.
                 dmeamf_drhoi=0d0
              endif
              !Below, isp is used to obtain the parameter indices for the pairwise density 
              !functions, so should =1 unless using acceptor-dependant densities
              isp=1
              if (thiaccptindepndt.eqv..false.) isp=gspecies(iat,istr)
              do jsp=1,maxspecies
                 do ll=1,lmax
                     !Derivatives w.r.t meam_tau:
                     dmeamf_dmeamtau(ll,jsp,iat)= &
                              dmeamf_drhoi * drhoi_dmeamtau(ll,jsp,iat)
                 enddo
                 do ll=0,lmax
                     !Derivatives w.r.t the pairwise density parameters:
                     do iiCoeff=1,noptdensityCoeff(ll,isp,jsp)
                        ip = ioptdensityCoeff(iiCoeff,ll,isp,jsp)
                        dmeamf_dpara(ip,ll,jsp,iat)= &
                              dmeamf_drhoi * drhoi_dpara(ip,ll,jsp,iat)
                     enddo
                     do iiCutoff=1,noptdensityCutoff(ll,isp,jsp)
                        ip = ioptdensityCutoff(iiCutoff,ll,isp,jsp)
                        dmeamf_dpara(ip,ll,jsp,iat)= &
                              dmeamf_drhoi * drhoi_dpara(ip,ll,jsp,iat)
                     enddo
                 enddo
              enddo

          enddo

       endif

    elseif (embfunctype.eq.2) then

        !The Hepburn embedding function

        !Included during early stage of code development - can re-instate later
        !if there is demand but for now not sufficiently tested to include.
        print *,'ERROR: embedding function type unsupported, stopping.'
        stop

        do iat=1,gn_inequivalentsites(istr)

            isp=gspecies(iat,istr)
            if (isp.eq.1) then

                meam_f(iat)=-meame0(isp)*sqrt(rho_i(iat)) &
                    + meamrho0(isp)*(rho_i(iat)**2) &
                    + meamemb3(isp)*(rho_i(iat)**4)
            elseif (isp.eq.2) then

                if (rho_i(iat).le.0d0) then
                    meam_f(iat)=0d0
                elseif ((rho_i(iat).gt.0d0).and. &
                        (rho_i(iat).le.0.001d0)) then
                    meam_f(iat)= -meame0(2)* ( &
                        3d0*((rho_i(iat)/0.001d0)**2)- &
                        2d0*((rho_i(iat)/0.001d0)**3) )
                elseif ((rho_i(iat).gt.0.001d0).and. &
                        (rho_i(iat).le.0.5)) then
                    meam_f(iat)= -meame0(2)
                elseif ((rho_i(iat).gt.0.5d0).and. &
                        (rho_i(iat).le.1d0)) then
                    meam_f(iat)= 5d0*(-meamrho0(2)) - meame0(2) &
                        - 12d0*(-meamrho0(2))*(rho_i(iat)/0.5d0) &
                        + 9d0*(-meamrho0(2))*( (rho_i(iat)/0.5d0)**2 ) &
                        - 2d0*(-meamrho0(2))*( (rho_i(iat)/0.5d0)**3 )
                elseif (rho_i(iat).gt.1d0) then
                    meam_f(iat)=-meame0(2)-meamrho0(2)
                endif
            else
                print *,'isp.ne.1 or 2 in emb en routine'
                stop
            endif

        enddo

    elseif( embfunctype.eq.3) then

        !Alternative embedding function formalism from Baskes

        !Included during early stage of code development - can re-instate later
        !if there is demand but for now not sufficiently tested to include.
        print *,'ERROR: embedding function type unsupported, stopping.'
        stop

        do iat=1,gn_inequivalentsites(istr)

            isp=gspecies(iat,istr)
            if (isp.eq.1) then

                meam_f(iat)=meame0(isp)*(rho_i(iat)/meamrho0(isp))* &
                    log(rho_i(iat)/meamrho0(isp))

            endif

        enddo

    endif

    summeamf=summeamf/gn_inequivalentsites(istr)
    !print *,'gn_inequivalentsites(',istr,')=',gn_inequivalentsites(istr),' in embeddingfunction'    


end subroutine embeddingfunction


subroutine embeddingfunctionSpline

    !-------------------------------------------------------------c
    !
    !     Calculates the embedding-function, F(rho_i) (=meam_f(i)
    !     in the code), for each of the inequivalent sites for
    !     of the 'istr'th structure. Also computes the derivatives
    !     of F(rho_i) w.r.t. potential parameters
    !
    !     Called by:     meamenergy
    !     Calls:         -
    !     Returns:       meam_f
    !     Files read:    -
    !     Files written: -
    !
    !     Andrew Duff, Marcel Sluiter
    !
    !     Copyright (c) 2018, STFC
    !
    !-------------------------------------------------------------c

    use m_generalinfo
    use m_atomproperties
    use m_geometry
    use m_meamparameters
    use m_electrondensity
    use m_optimization
    use m_observables

    implicit none

    integer iat,isp,jsp,ll,iiCoeff,iiCutoff,ip,rhoIndex
    real(8) sqrt_rhoi,rhoi_sqr,rhoi_cube,rhoi_quart,dmeamf_drhoi

    summeamf=0d0

    dmeamf_dpara=0d0
    dmeamf_dmeame0=0d0
    dmeamf_dmeamrho0=0d0
    dmeamf_dmeamemb3=0d0
    dmeamf_dmeamemb4=0d0
    dmeamf_dpara=0d0

    do iat=1,gn_inequivalentsites(istr)

       isp=gspecies(iat,istr)
       if (rho_i(iat).lt.0d0) then
          print *,'negative density!!!!'
          print *,'rho_i(',iat,')=',rho_i(iat)
          stop
       endif

       if (useSpline1PartFns(isp).eqv..true.) then
          rhoIndex=1+FLOOR(rho_i(iat)/drhoLmps(isp))
          call interpolateSpline(drhoLmps(isp),embfuncSpline(1:nRhoMaxSpline,isp),embfuncSplineSecDer(1:nRhoMaxSpline,isp),nRhoMaxSpline,rhoIndex,rho_i(iat),meam_f(iat))
!          print *,'For rho_i(',iat,')=',rho_i(iat),', meam_f(',iat,')=',meam_f(iat),' (numerically)'

       else

          if (lmax.eq.0) then

             sqrt_rhoi=sqrt(rho_i(iat))
             rhoi_sqr=(rho_i(iat)**2)
             rhoi_cube=rhoi_sqr*rho_i(iat)
             rhoi_quart=rhoi_sqr**2
             meam_f(iat)= -meame0(isp)*sqrt_rhoi &
                 + meamrho0(isp)*rhoi_sqr &
                 + meamemb3(isp)*rhoi_cube &
                 + meamemb4(isp)*rhoi_quart
             summeamf=summeamf+meam_f(iat)

             !Calculate analytic derivatives of meam_f(i)
             !Derivatives w.r.t meame0, etc:
             dmeamf_dmeame0(iat)=-sqrt_rhoi
             dmeamf_dmeamrho0(iat)=rhoi_sqr
             dmeamf_dmeamemb3(iat)=rhoi_cube
             dmeamf_dmeamemb4(iat)=rhoi_quart

             if (rho_i(iat).ne.0d0) then
                dmeamf_drhoi= - 0.5d0*meame0(isp)/sqrt_rhoi &
                         + 2d0*meamrho0(isp)*rho_i(iat) &
                         + 3d0*meamemb3(isp)*rhoi_sqr &
                         + 4d0*meamemb4(isp)*rhoi_cube
             else
                !If rho_i=0, drhoi_dpara will also = 0 below,
                !so the dmeamf_dpara should = 0. However if
                !we evaluate the singularity above (/sqrt_rhoi),
                !dmeamf_dpara will wrongly be set to NaN.
                dmeamf_drhoi=0d0
             endif
             !Below, isp is used to obtain the parameter indices for the
             !pairwise density 
             !functions, so should =1 unless using acceptor-dependant densities
             isp=1
             if (thiaccptindepndt.eqv..false.) isp=gspecies(iat,istr)
             do jsp=1,maxspecies
                !Derivatives w.r.t the pairwise density parameters:
                do iiCoeff=1,noptdensityCoeff(0,isp,jsp)
                   ip = ioptdensityCoeff(iiCoeff,0,isp,jsp)
                   dmeamf_dpara(ip,0,jsp,iat)= &
                         dmeamf_drhoi * drhoi_dpara(ip,0,jsp,iat)
                enddo
                do iiCutoff=1,noptdensityCutoff(0,isp,jsp)
                   ip = ioptdensityCutoff(iiCutoff,0,isp,jsp)
                   dmeamf_dpara(ip,0,jsp,iat)= &
                         dmeamf_drhoi * drhoi_dpara(ip,0,jsp,iat)
                enddo
             enddo

          else

             sqrt_rhoi=sqrt(rho_i(iat))
             rhoi_sqr=(rho_i(iat)**2)
             rhoi_cube=rhoi_sqr*rho_i(iat)
             rhoi_quart=rhoi_sqr**2
             meam_f(iat)= -meame0(isp)*sqrt_rhoi &
                 + meamrho0(isp)*rhoi_sqr &
                 + meamemb3(isp)*rhoi_cube &
                 + meamemb4(isp)*rhoi_quart
             !print *,'meam_f(',iat,')=',meam_f(iat),'
             !rho_i(',iat,')=',rho_i(iat)
             summeamf=summeamf+meam_f(iat)

             !Calculate analytic derivatives of meam_f(i)
             !Derivatives w.r.t meame0, etc:
             dmeamf_dmeame0(iat)=-sqrt_rhoi
             dmeamf_dmeamrho0(iat)=rhoi_sqr
             dmeamf_dmeamemb3(iat)=rhoi_cube
             dmeamf_dmeamemb4(iat)=rhoi_quart

             if (rho_i(iat).ne.0d0) then
                dmeamf_drhoi= - 0.5d0*meame0(isp)/sqrt_rhoi &
                         + 2d0*meamrho0(isp)*rho_i(iat) &
                         + 3d0*meamemb3(isp)*rhoi_sqr &
                         + 4d0*meamemb4(isp)*rhoi_cube
             else
                !If rho_i=0, drhoi_dmeamtau and drhoi_dpara will also = 0
                !below,
                !so the dmeamf_dmeamtau and dmeamf_dpara should = 0. However if
                !we evaluate the singularity above (/sqrt_rhoi),
                !dmeamf_dmeamtau
                !and dmeamf_dpara will wrongly be set to NaN.
                dmeamf_drhoi=0d0
             endif
             !Below, isp is used to obtain the parameter indices for the
             !pairwise density 
             !functions, so should =1 unless using acceptor-dependant densities
             isp=1
             if (thiaccptindepndt.eqv..false.) isp=gspecies(iat,istr)
             do jsp=1,maxspecies
                do ll=1,lmax
                    !Derivatives w.r.t meam_tau:
                    dmeamf_dmeamtau(ll,jsp,iat)= &
                             dmeamf_drhoi * drhoi_dmeamtau(ll,jsp,iat)
                enddo
                do ll=0,lmax
                    !Derivatives w.r.t the pairwise density parameters:
                    do iiCoeff=1,noptdensityCoeff(ll,isp,jsp)
                       ip = ioptdensityCoeff(iiCoeff,ll,isp,jsp)
                       dmeamf_dpara(ip,ll,jsp,iat)= &
                             dmeamf_drhoi * drhoi_dpara(ip,ll,jsp,iat)
                    enddo
                    do iiCutoff=1,noptdensityCutoff(ll,isp,jsp)
                       ip = ioptdensityCutoff(iiCutoff,ll,isp,jsp)
                       dmeamf_dpara(ip,ll,jsp,iat)= &
                             dmeamf_drhoi * drhoi_dpara(ip,ll,jsp,iat)
                    enddo
                enddo
             enddo

          endif

          !print *,'For rho_i(',iat,')=',rho_i(iat),', meam_f(',iat,')=',meam_f(iat),' (analitically)'
          !stop

       endif 
       summeamf=summeamf+meam_f(iat)

    enddo
    
end subroutine embeddingfunctionSpline



subroutine embeddingfunctionForce

    !-------------------------------------------------------------c
    !
    !     Calculates the embedding-function, F(rho_i) (=meam_f(i)
    !     in the code), for each of the inequivalent sites for
    !     of the 'istr'th structure. Also computes the derivatives
    !     of F(rho_i) w.r.t. potential parameters
    !
    !     Called by:     meamenergy
    !     Calls:         -
    !     Returns:       meam_f
    !     Files read:    -
    !     Files written: -
    !
    !     Andrew Duff, Marcel Sluiter
    !
    !     Copyright (c) 2018, STFC
    !
    !-------------------------------------------------------------c

    use m_generalinfo
    use m_atomproperties
    use m_geometry
    use m_meamparameters
    use m_electrondensity
    use m_optimization
    use m_observables

    implicit none

    logical errorOnce
    integer iat,jj,alph,isp,jsp,ll,iiCoeff,iiCutoff,ip
    real(8) sqrt_rhoi,rhoi_sqr,rhoi_cube,rhoi_quart,d2meamf_drhoi_dmeame0, &
         d2meamf_drhoi_dmeamrho0,d2meamf_drhoi_dmeamemb3,d2meamf_drhoi_dmeamemb4
    real(8) dmeamf_drhoi(gn_inequivalentsites(istr)),d2meamf_drhoi2(gn_inequivalentsites(istr))

    !   if (allocated(dmeamf_dpara)) deallocate(dmeamf_dpara,dmeamf_dmeame0,dmeamf_dmeamrho0, &
    !            dmeamf_dmeamemb3,dmeamf_dmeamemb4)
    !   if (allocated(dmeamf_dxyz)) deallocate(dmeamf_dxyz,d2meamf_dxyz_dpara, &
    !            d2meamf_dxyz_dmeame0,d2meamf_dxyz_dmeamrho0,d2meamf_dxyz_dmeamemb3,d2meamf_dxyz_dmeamemb4)

    !   allocate( dmeamf_dxyz(1:3,0:maxneighbors,gn_inequivalentsites(istr)), &
    !             dmeamf_dpara(12,0:lmax,1:maxspecies,gn_inequivalentsites(istr)), &
    !             dmeamf_dmeame0(gn_inequivalentsites(istr)), &
    !             dmeamf_dmeamrho0(gn_inequivalentsites(istr)), &
    !             dmeamf_dmeamemb3(gn_inequivalentsites(istr)), &
    !             dmeamf_dmeamemb4(gn_inequivalentsites(istr)), &
    !             d2meamf_dxyz_dpara(3,0:maxneighbors,12,0:lmax,maxspecies,gn_inequivalentsites(istr)), &
    !             d2meamf_dxyz_dmeame0(3,0:maxneighbors,gn_inequivalentsites(istr)), &
    !             d2meamf_dxyz_dmeamrho0(3,0:maxneighbors,gn_inequivalentsites(istr)), & 
    !             d2meamf_dxyz_dmeamemb3(3,0:maxneighbors,gn_inequivalentsites(istr)), & 
    !             d2meamf_dxyz_dmeamemb4(3,0:maxneighbors,gn_inequivalentsites(istr)) )

    dsummeamf_dpara=0d0
    dsummeamf_dmeamtau=0d0
    dsummeamf_dmeame0=0d0
    dsummeamf_dmeamrho0=0d0
    dsummeamf_dmeamemb3=0d0
    dsummeamf_dmeamemb4=0d0
    summeamf=0d0

    if (embfunctype.eq.1) then

       dmeamf_dxyz=0d0 
       d2meamf_dxyz_dmeame0=0d0
       d2meamf_dxyz_dmeamrho0=0d0
       d2meamf_dxyz_dmeamemb3=0d0
       d2meamf_dxyz_dmeamemb4=0d0
       dmeamf_dpara=0d0
       d2meamf_dxyz_dpara=0d0

       !First compute summeamf and derivatives w.r.t potential paras, along with
       !the atomic forces contributions and potential para derivatives thereof
       !that can be defined within the original supercell [gn_forces(istr) atoms]
       do iat=1,gn_forces(istr)
           !print *,iat,'/',gn_forces(istr)
           isp=gspecies(iat,istr)
           !print *,'rho_i(',iat,')=',rho_i(iat),', species=',gspecies(iat,istr),' position=',gxyz(1:3,iat,istr)
           sqrt_rhoi=sqrt(rho_i(iat))
           rhoi_sqr=(rho_i(iat)**2)
           rhoi_cube=rhoi_sqr*rho_i(iat)
           rhoi_quart=rhoi_sqr**2

           meam_f(iat)= -meame0(isp)*sqrt_rhoi &
               + meamrho0(isp)*rhoi_sqr &
               + meamemb3(isp)*rhoi_cube &
               + meamemb4(isp)*rhoi_quart
!              print *,'meam_f(',iat,')=',meam_f(iat),' rho_i(',iat,')=',rho_i(iat)
           if (rho_i(iat).ne.0d0) then
              dmeamf_drhoi(iat)= - 0.5d0*meame0(isp)/sqrt_rhoi &
                       + 2d0*meamrho0(isp)*rho_i(iat) &
                       + 3d0*meamemb3(isp)*rhoi_sqr &
                       + 4d0*meamemb4(isp)*rhoi_cube
              d2meamf_drhoi2(iat)= 0.25d0*meame0(isp)/(rho_i(iat)**(3d0/2d0)) &
                         + 2d0*meamrho0(isp) &
                         + 6d0*meamemb3(isp)*rho_i(iat) &
                         + 12d0*meamemb4(isp)*rhoi_sqr
              d2meamf_drhoi_dmeame0= -0.5d0/sqrt_rhoi
              d2meamf_drhoi_dmeamrho0= 2d0*rho_i(iat)
              d2meamf_drhoi_dmeamemb3= 3d0*rhoi_sqr
              d2meamf_drhoi_dmeamemb4= 4d0*rhoi_cube

           else
              !If rho_i=0, drhoi_dmeamtau and drhoi_dpara will also = 0 below,
              !so the dmeamf_dmeamtau and dmeamf_dpara should = 0. However if
              !we evaluate the singularity above (/sqrt_rhoi), dmeamf_dmeamtau
              !and dmeamf_dpara will wrongly be set to NaN.
              dmeamf_drhoi(iat)=0d0
              d2meamf_drhoi2(iat)= 0d0
              d2meamf_drhoi_dmeame0= 0d0
              d2meamf_drhoi_dmeamrho0= 0d0
              d2meamf_drhoi_dmeamemb3= 0d0
              d2meamf_drhoi_dmeamemb4= 0d0
           endif

           !Calculate derivatives w.r.t coordinates
           if (rho_i(iat).ne.0d0) then
              do jj=0,gn_neighbors(iat,istr)
                 do alph=1,3
                    dmeamf_dxyz(alph,jj,iat)= &
                       dmeamf_drhoi(iat) * drhoi_dxyz(alph,jj,iat)

                    d2meamf_dxyz_dmeame0(alph,jj,iat)= &
                       d2meamf_drhoi_dmeame0 * drhoi_dxyz(alph,jj,iat)
                    d2meamf_dxyz_dmeamrho0(alph,jj,iat)= &
                       d2meamf_drhoi_dmeamrho0 * drhoi_dxyz(alph,jj,iat)
                    d2meamf_dxyz_dmeamemb3(alph,jj,iat)= &
                       d2meamf_drhoi_dmeamemb3 * drhoi_dxyz(alph,jj,iat)
                    d2meamf_dxyz_dmeamemb4(alph,jj,iat)= &
                       d2meamf_drhoi_dmeamemb4 * drhoi_dxyz(alph,jj,iat)
                 enddo
              enddo

              !Calculate derivatives of dmeamf_dxyz w.r.t parameters
              !Below, isp is used to obtain the parameter indices for the
              !pairwise density 
              !functions, so should =1 unless using acceptor-dependant densities
              isp=1     
              if (thiaccptindepndt.eqv..false.) isp=gspecies(iat,istr)
              do jsp=1,maxspecies
                 do ll=0,lmax
                     !Derivatives w.r.t the pairwise density parameters:
                     do iiCoeff=1,noptdensityCoeff(ll,isp,jsp)
                        ip = ioptdensityCoeff(iiCoeff,ll,isp,jsp)
                        do jj=0,gn_neighbors(iat,istr)
                           do alph=1,3
                              d2meamf_dxyz_dpara(alph,jj,ip,ll,jsp,iat)= &
                              d2meamf_drhoi2(iat) * drhoi_dpara(ip,ll,jsp,iat) * drhoi_dxyz(alph,jj,iat) + &
                              dmeamf_drhoi(iat) * d2rhoi_dxyz_dpara(alph,jj,ip,ll,jsp,iat)
                           enddo
                        enddo
                     enddo
                     do iiCutoff=1,noptdensityCutoff(ll,isp,jsp)
                        ip = ioptdensityCutoff(iiCutoff,ll,isp,jsp)
                        do jj=0,gn_neighbors(iat,istr)
                           do alph=1,3
                              d2meamf_dxyz_dpara(alph,jj,ip,ll,jsp,iat)= &
                              d2meamf_drhoi2(iat) * drhoi_dpara(ip,ll,jsp,iat) * drhoi_dxyz(alph,jj,iat) + &
                              dmeamf_drhoi(iat) * d2rhoi_dxyz_dpara(alph,jj,ip,ll,jsp,iat)
                           enddo
                        enddo
                     enddo
                 enddo
              enddo

           endif

           !Calculate analytic derivatives of meam_f(at)
           !Derivatives w.r.t meame0, etc:
           dmeamf_dmeame0(iat)=-sqrt_rhoi
           dmeamf_dmeamrho0(iat)=rhoi_sqr
           dmeamf_dmeamemb3(iat)=rhoi_cube
           dmeamf_dmeamemb4(iat)=rhoi_quart

           !Below, isp is used to obtain the parameter indices for the
           !pairwise density 
           !functions, so should =1 unless using acceptor-dependant densities
           isp=1
           if (thiaccptindepndt.eqv..false.) isp=gspecies(iat,istr)
           do jsp=1,maxspecies
              do ll=0,lmax
                  !Derivatives w.r.t the pairwise density parameters:
                  do iiCoeff=1,noptdensityCoeff(ll,isp,jsp)
                     ip = ioptdensityCoeff(iiCoeff,ll,isp,jsp)
                     dmeamf_dpara(ip,ll,jsp,iat)= &
                           dmeamf_drhoi(iat) * drhoi_dpara(ip,ll,jsp,iat)
                  enddo
                  do iiCutoff=1,noptdensityCutoff(ll,isp,jsp)
                     ip = ioptdensityCutoff(iiCutoff,ll,isp,jsp)
                     dmeamf_dpara(ip,ll,jsp,iat)= &
                           dmeamf_drhoi(iat) * drhoi_dpara(ip,ll,jsp,iat)

                  enddo
              enddo
           enddo

           !Update quantities to be summed over original unit cell
           summeamf=summeamf+meam_f(iat)
           !Caution: dsummeamf_dpara has jsp,isp rather than
           !isp,jsp in meamrhodecay [meamrhodecay(np,l,isp,jsp)]
           isp=1
           if (thiaccptindepndt.eqv..false.) isp=gspecies(iat,istr)
           ! dsummeamf_dpara(1:12,0:lmax,1:maxspecies,isp) = &
           !       dsummeamf_dpara(1:12,0:lmax,1:maxspecies,isp) + &
           !       dmeamf_dpara(1:12,0:lmax,1:maxspecies,i)
           !We swap the order of the last two indices here. For calculation
           !efficiency purposes, 'i' was the last index. Now, for consistency
           !with meamrhodecay, we use the order: isp, jsp.
           dsummeamf_dpara(1:12,0:lmax,isp,1:maxspecies) = &
                 dsummeamf_dpara(1:12,0:lmax,isp,1:maxspecies) + &
                 dmeamf_dpara(1:12,0:lmax,1:maxspecies,iat)
           isp=gspecies(iat,istr)
           dsummeamf_dmeame0(isp)=dsummeamf_dmeame0(isp)+dmeamf_dmeame0(iat)
           dsummeamf_dmeamrho0(isp)=dsummeamf_dmeamrho0(isp)+dmeamf_dmeamrho0(iat)
           dsummeamf_dmeamemb3(isp)=dsummeamf_dmeamemb3(isp)+dmeamf_dmeamemb3(iat)
           dsummeamf_dmeamemb4(isp)=dsummeamf_dmeamemb4(isp)+dmeamf_dmeamemb4(iat)

       enddo

       do iat=gn_forces(istr)+1,gn_inequivalentsites(istr)

           isp=gspecies(iat,istr)

           sqrt_rhoi=sqrt(rho_i(iat))
           rhoi_sqr=(rho_i(iat)**2)
           rhoi_cube=rhoi_sqr*rho_i(iat)
           rhoi_quart=rhoi_sqr**2

           !MEAM energies not needed for total energy, but retain for use in
           !numerical derivatives for debug purposes (remove in final version)
           meam_f(iat)= -meame0(isp)*sqrt_rhoi &
               + meamrho0(isp)*rhoi_sqr &
               + meamemb3(isp)*rhoi_cube &
               + meamemb4(isp)*rhoi_quart

           if (rho_i(iat).ne.0d0) then
              dmeamf_drhoi(iat)= - 0.5d0*meame0(isp)/sqrt_rhoi &
                       + 2d0*meamrho0(isp)*rho_i(iat) &
                       + 3d0*meamemb3(isp)*rhoi_sqr &
                       + 4d0*meamemb4(isp)*rhoi_cube
              d2meamf_drhoi2(iat)= 0.25d0*meame0(isp)/(rho_i(iat)**(3d0/2d0)) &
                         + 2d0*meamrho0(isp) &
                         + 6d0*meamemb3(isp)*rho_i(iat) &
                         + 12d0*meamemb4(isp)*rhoi_sqr
              d2meamf_drhoi_dmeame0= -0.5d0/sqrt_rhoi
              d2meamf_drhoi_dmeamrho0= 2d0*rho_i(iat)
              d2meamf_drhoi_dmeamemb3= 3d0*rhoi_sqr
              d2meamf_drhoi_dmeamemb4= 4d0*rhoi_cube

           else
              !If rho_i=0, drhoi_dmeamtau and drhoi_dpara will also = 0 below,
              !so the dmeamf_dmeamtau and dmeamf_dpara should = 0. However if
              !we evaluate the singularity above (/sqrt_rhoi), dmeamf_dmeamtau
              !and dmeamf_dpara will wrongly be set to NaN.
              dmeamf_drhoi(iat)=0d0
              d2meamf_drhoi2(iat)= 0d0
              d2meamf_drhoi_dmeame0= 0d0
              d2meamf_drhoi_dmeamrho0= 0d0
              d2meamf_drhoi_dmeamemb3= 0d0
              d2meamf_drhoi_dmeamemb4= 0d0
           endif

           !Calculate derivatives w.r.t coordinates
           if (rho_i(iat).ne.0d0) then
              do jj=0,gn_neighbors(iat,istr)
                 do alph=1,3
                    dmeamf_dxyz(alph,jj,iat)= &
                       dmeamf_drhoi(iat) * drhoi_dxyz(alph,jj,iat)
                    d2meamf_dxyz_dmeame0(alph,jj,iat)= &
                       d2meamf_drhoi_dmeame0 * drhoi_dxyz(alph,jj,iat)
                    d2meamf_dxyz_dmeamrho0(alph,jj,iat)= &
                       d2meamf_drhoi_dmeamrho0 * drhoi_dxyz(alph,jj,iat)
                    d2meamf_dxyz_dmeamemb3(alph,jj,iat)= &
                       d2meamf_drhoi_dmeamemb3 * drhoi_dxyz(alph,jj,iat)
                    d2meamf_dxyz_dmeamemb4(alph,jj,iat)= &
                       d2meamf_drhoi_dmeamemb4 * drhoi_dxyz(alph,jj,iat)
                 enddo
              enddo

              !Calculate derivatives of dmeamf_dxyz w.r.t parameters
              !Below, isp is used to obtain the parameter indices for the pairwise density 
              !functions, so should =1 unless using acceptor-dependant densities
              isp=1     
              if (thiaccptindepndt.eqv..false.) isp=gspecies(iat,istr)
              do jsp=1,maxspecies
                 do ll=0,lmax
                     !Derivatives w.r.t the pairwise density parameters:
                     do iiCoeff=1,noptdensityCoeff(ll,isp,jsp)
                        ip = ioptdensityCoeff(iiCoeff,ll,isp,jsp)
                        do jj=0,gn_neighbors(iat,istr)
                           do alph=1,3
                              d2meamf_dxyz_dpara(alph,jj,ip,ll,jsp,iat)= &
                              d2meamf_drhoi2(iat) * drhoi_dpara(ip,ll,jsp,iat) * drhoi_dxyz(alph,jj,iat) + &
                              dmeamf_drhoi(iat) * d2rhoi_dxyz_dpara(alph,jj,ip,ll,jsp,iat)
                           enddo
                        enddo
                     enddo
                     do iiCutoff=1,noptdensityCutoff(ll,isp,jsp)
                        ip = ioptdensityCutoff(iiCutoff,ll,isp,jsp)
                        do jj=0,gn_neighbors(iat,istr)
                           do alph=1,3
                              d2meamf_dxyz_dpara(alph,jj,ip,ll,jsp,iat)= &
                              d2meamf_drhoi2(iat) * drhoi_dpara(ip,ll,jsp,iat) * drhoi_dxyz(alph,jj,iat) + &
                              dmeamf_drhoi(iat) * d2rhoi_dxyz_dpara(alph,jj,ip,ll,jsp,iat)
                           enddo
                        enddo
                     enddo
                 enddo
              enddo

           endif

           !The derivatives w.r.t parameters are not needed outside of the
           !original cell, however retain commented out code in case they are
           !required for numerical derivatives.
           !
           !  !Calculate analytic derivatives of meam_f(at)
           !  !Derivatives w.r.t meame0, etc:
           !  dmeamf_dmeame0(iat)=-sqrt_rhoi
           !  dmeamf_dmeamrho0(iat)=rhoi_sqr
           !  dmeamf_dmeamemb3(iat)=rhoi_cube
           !  dmeamf_dmeamemb4(iat)=rhoi_quart
           !
           !  !Below, isp is used to obtain the parameter indices for the
           !  !pairwise density 
           !  !functions, so should =1 unless using acceptor-dependant densities
           !  isp=1
           !  if (thiaccptindepndt.eqv..false.) isp=gspecies(iat,istr)
           !  do jsp=1,maxspecies
           !     do ll=0,lmax
           !         !Derivatives w.r.t the pairwise density parameters:
           !         do iiCoeff=1,noptdensityCoeff(ll,isp,jsp)
           !            ip = ioptdensityCoeff(iiCoeff,ll,isp,jsp)
           !            dmeamf_dpara(ip,ll,jsp,iat)= &
           !                  dmeamf_drhoi(iat) * drhoi_dpara(ip,ll,jsp,iat)
           !         enddo
           !         do iiCutoff=1,noptdensityCutoff(ll,isp,jsp)
           !            ip = ioptdensityCutoff(iiCutoff,ll,isp,jsp)
           !            dmeamf_dpara(ip,ll,jsp,iat)= &
           !                  dmeamf_drhoi(iat) * drhoi_dpara(ip,ll,jsp,iat)
           !
           !         enddo
           !     enddo
           !  enddo

       enddo

       if (lmax.gt.0) then

          !  if (allocated(dmeamf_dmeamtau)) deallocate(dmeamf_dmeamtau)
          !  if (allocated(d2meamf_dxyz_dmeamtau)) deallocate(d2meamf_dxyz_dmeamtau)
          !  allocate(dmeamf_dmeamtau(1:lmax,1:maxspecies,gn_inequivalentsites(istr)), &
          !      d2meamf_dxyz_dmeamtau(3,0:maxneighbors,1:lmax,maxspecies,gn_inequivalentsites(istr)) )

          errorOnce=.false.!Set to true for extra debugging info
          do iat=1,gn_forces(istr)
             if (rho_i(iat).ne.0d0) then
                do jsp=1,maxspecies
                   do ll=1,lmax
                      dmeamf_dmeamtau(ll,jsp,iat)= &
                                dmeamf_drhoi(iat) * drhoi_dmeamtau(ll,jsp,iat)
                      do jj=0,gn_neighbors(iat,istr)
                         do alph=1,3
                            d2meamf_dxyz_dmeamtau(alph,jj,ll,jsp,iat)= &
                                d2meamf_drhoi2(iat) * drhoi_dmeamtau(ll,jsp,iat) * drhoi_dxyz(alph,jj,iat) + &
                                dmeamf_drhoi(iat) * d2rhoi_dxyz_dmeamtau(alph,jj,ll,jsp,iat)
                            if (d2meamf_dxyz_dmeamtau(alph,jj,ll,jsp,iat)/=d2meamf_dxyz_dmeamtau(alph,jj,ll,jsp,iat)) then
                               if ((debug.eqv..true.).and.(errorOnce.eqv..false.)) then
                                  print *,'ERROR, d2meamf_dxyz_dmeamtau(',alph,jj,ll,jsp,iat,')=',d2meamf_dxyz_dmeamtau(alph,jj,ll,jsp,iat)
                                  errorOnce=.true.
                                  print *,'d2meamf_drhoi2(',iat,')=',d2meamf_drhoi2(iat)
                                  print *,'drhoi_dmeamtau(',ll,jsp,iat,')=',drhoi_dmeamtau(ll,jsp,iat)
                                  print *,'drhoi_dxyz(',alph,jj,iat,')=',drhoi_dxyz(alph,jj,iat)
                                  print *,'dmeamf_drhoi(',iat,')=',dmeamf_drhoi(iat)
                                  print *,'d2rhoi_dxyz_dmeamtau(',alph,jj,ll,jsp,iat,')=',d2rhoi_dxyz_dmeamtau(alph,jj,ll,jsp,iat)
                               endif
                            endif
                         enddo
                      enddo
                   enddo
                enddo
             else
                dmeamf_dmeamtau(1:lmax,1:maxspecies,iat)=0d0
                d2meamf_dxyz_dmeamtau(1:3,0:gn_neighbors(iat,istr),1:lmax,1:maxspecies,iat)=0d0
             endif
             !Update quantities to be summed over original unit cell
             dsummeamf_dmeamtau(1:lmax,1:maxspecies)= &
                   dsummeamf_dmeamtau(1:lmax,1:maxspecies) + &
                   dmeamf_dmeamtau(1:lmax,1:maxspecies,iat)
          enddo

          do iat=gn_forces(istr)+1,gn_inequivalentsites(istr)
             if (rho_i(iat).ne.0d0) then
                do jsp=1,maxspecies
                   do ll=1,lmax
                      !Following not needed but keep commented code in case
                      !required for numerical derivatives:
                      ! dmeamf_dmeamtau(ll,jsp,iat)= &
                      !           dmeamf_drhoi(iat) * drhoi_dmeamtau(ll,jsp,iat)
                      do jj=0,gn_neighbors(iat,istr)
                         do alph=1,3
                            d2meamf_dxyz_dmeamtau(alph,jj,ll,jsp,iat)= &
                                d2meamf_drhoi2(iat) * drhoi_dmeamtau(ll,jsp,iat) * drhoi_dxyz(alph,jj,iat) + &
                                dmeamf_drhoi(iat) * d2rhoi_dxyz_dmeamtau(alph,jj,ll,jsp,iat)
                         enddo
                      enddo
                   enddo
                enddo
             else
                !Following not needed but keep commented code in case 
                !required for numerical derivatives:
                ! dmeamf_dmeamtau(1:lmax,1:maxspecies,iat)=0d0
                d2meamf_dxyz_dmeamtau(1:3,0:gn_neighbors(iat,istr),1:lmax,1:maxspecies,iat)=0d0
             endif
          enddo

       endif

    elseif (embfunctype.eq.2) then

        !The Hepburn embedding function

        !Included during early stage of code development - can re-instate later
        !if there is demand but for now not sufficiently tested to include.
        print *,'ERROR: embedding function type unsupported, stopping.'
        stop

        do iat=1,gn_inequivalentsites(istr)

            isp=gspecies(iat,istr)
            if (isp.eq.1) then

                meam_f(iat)=-meame0(isp)*sqrt(rho_i(iat)) &
                    + meamrho0(isp)*(rho_i(iat)**2) &
                    + meamemb3(isp)*(rho_i(iat)**4)
            elseif (isp.eq.2) then

                if (rho_i(iat).le.0d0) then
                    meam_f(iat)=0d0
                elseif ((rho_i(iat).gt.0d0).and. &
                        (rho_i(iat).le.0.001d0)) then
                    meam_f(iat)= -meame0(2)* ( &
                        3d0*((rho_i(iat)/0.001d0)**2)- &
                        2d0*((rho_i(iat)/0.001d0)**3) )
                elseif ((rho_i(iat).gt.0.001d0).and. &
                        (rho_i(iat).le.0.5)) then
                    meam_f(iat)= -meame0(2)
                elseif ((rho_i(iat).gt.0.5d0).and. &
                        (rho_i(iat).le.1d0)) then
                    meam_f(iat)= 5d0*(-meamrho0(2)) - meame0(2) &
                        - 12d0*(-meamrho0(2))*(rho_i(iat)/0.5d0) &
                        + 9d0*(-meamrho0(2))*( (rho_i(iat)/0.5d0)**2 ) &
                        - 2d0*(-meamrho0(2))*( (rho_i(iat)/0.5d0)**3 )
                elseif (rho_i(iat).gt.1d0) then
                    meam_f(iat)=-meame0(2)-meamrho0(2)
                endif
            else
                print *,'isp.ne.1 or 2 in emb en routine'
                stop
            endif

        enddo

    elseif( embfunctype.eq.3) then

        !Alternative embedding function formalism from Baskes

        !Included during early stage of code development - can re-instate later
        !if there is demand but for now not sufficiently tested to include.
        print *,'ERROR: embedding function type unsupported, stopping.'
        stop

        do iat=1,gn_inequivalentsites(istr)

            isp=gspecies(iat,istr)
            if (isp.eq.1) then

                meam_f(iat)=meame0(isp)*(rho_i(iat)/meamrho0(isp))* &
                    log(rho_i(iat)/meamrho0(isp))

            endif

        enddo

    endif

    summeamf=summeamf/gn_forces(istr)
    dsummeamf_dpara=dsummeamf_dpara/gn_forces(istr)
    dsummeamf_dmeamtau=dsummeamf_dmeamtau/gn_forces(istr)
    dsummeamf_dmeame0=dsummeamf_dmeame0/gn_forces(istr)
    dsummeamf_dmeamrho0=dsummeamf_dmeamrho0/gn_forces(istr)
    dsummeamf_dmeamemb3=dsummeamf_dmeamemb3/gn_forces(istr)
    dsummeamf_dmeamemb4=dsummeamf_dmeamemb4/gn_forces(istr)
    !print *,'gn_forces(',istr,')=',gn_forces(istr),' in embeddingfunctionForce'    

    !stop
end subroutine embeddingfunctionForce



subroutine embeddingfunctionForceSpline

    !-------------------------------------------------------------c
    !
    !     Calculates the embedding-function, F(rho_i) (=meam_f(i)
    !     in the code), for each of the inequivalent sites for
    !     of the 'istr'th structure. Also computes the derivatives
    !     of F(rho_i) w.r.t. potential parameters
    !
    !     Called by:     meamenergy
    !     Calls:         -
    !     Returns:       meam_f
    !     Files read:    -
    !     Files written: -
    !
    !     Andrew Duff, Marcel Sluiter
    !
    !     Copyright (c) 2018, STFC
    !
    !-------------------------------------------------------------c

    use m_generalinfo
    use m_atomproperties
    use m_geometry
    use m_meamparameters
    use m_electrondensity
    use m_optimization
    use m_observables

    implicit none

    logical errorOnce
    integer iat,jj,alph,isp,jsp,ll,iiCoeff,iiCutoff,ip,rhoIndex
    real(8) sqrt_rhoi,rhoi_sqr,rhoi_cube,rhoi_quart,d2meamf_drhoi_dmeame0, &
         d2meamf_drhoi_dmeamrho0,d2meamf_drhoi_dmeamemb3,d2meamf_drhoi_dmeamemb4
    real(8) dmeamf_drhoi(gn_inequivalentsites(istr)),d2meamf_drhoi2(gn_inequivalentsites(istr))

    dsummeamf_dpara=0d0
    dsummeamf_dmeamtau=0d0
    dsummeamf_dmeame0=0d0
    dsummeamf_dmeamrho0=0d0
    dsummeamf_dmeamemb3=0d0
    dsummeamf_dmeamemb4=0d0
    summeamf=0d0

    dmeamf_dxyz=0d0 
    d2meamf_dxyz_dmeame0=0d0
    d2meamf_dxyz_dmeamrho0=0d0
    d2meamf_dxyz_dmeamemb3=0d0
    d2meamf_dxyz_dmeamemb4=0d0
    dmeamf_dpara=0d0
    d2meamf_dxyz_dpara=0d0

    !First compute summeamf and derivatives w.r.t potential paras, along with
    !the atomic forces contributions and potential para derivatives thereof
    !that can be defined within the original supercell [gn_forces(istr)
    !atoms]
    do iat=1,gn_forces(istr)

        isp=gspecies(iat,istr)

        if (useSpline1PartFns(isp).eqv..true.) then

           rhoIndex=1+FLOOR(rho_i(iat)/drhoLmps(isp))
           call interpolateSplineDeriv(drhoLmps(isp),embfuncSpline(1:nRhoMaxSpline,isp),embfuncSplineSecDer(1:nRhoMaxSpline,isp),nRhoMaxSpline,rhoIndex,rho_i(iat),meam_f(iat),dmeamf_drhoi(iat))

           !print *,'numeric, meam_f(',iat,')=',meam_f(iat),', dmeamf_drhoi(',iat,')=',dmeamf_drhoi(iat)

           !Calculate derivatives w.r.t coordinates
           if (rho_i(iat).ne.0d0) then
              do jj=0,gn_neighbors(iat,istr)
                 do alph=1,3
                    dmeamf_dxyz(alph,jj,iat)= &
                       dmeamf_drhoi(iat) * drhoi_dxyz(alph,jj,iat)

                    !print *,'numeric, dmeamf_dxyz(',alph,jj,iat,')=',dmeamf_dxyz(alph,jj,iat)
                 enddo
              enddo
    
           endif
    
           !Update quantities to be summed over original unit cell
           summeamf=summeamf+meam_f(iat)

        else

           sqrt_rhoi=sqrt(rho_i(iat))
           rhoi_sqr=(rho_i(iat)**2)
           rhoi_cube=rhoi_sqr*rho_i(iat)
           rhoi_quart=rhoi_sqr**2

           meam_f(iat)= -meame0(isp)*sqrt_rhoi &
               + meamrho0(isp)*rhoi_sqr &
               + meamemb3(isp)*rhoi_cube &
               + meamemb4(isp)*rhoi_quart

           if (rho_i(iat).ne.0d0) then
              dmeamf_drhoi(iat)= - 0.5d0*meame0(isp)/sqrt_rhoi &
                       + 2d0*meamrho0(isp)*rho_i(iat) &
                       + 3d0*meamemb3(isp)*rhoi_sqr &
                       + 4d0*meamemb4(isp)*rhoi_cube
              d2meamf_drhoi2(iat)= 0.25d0*meame0(isp)/(rho_i(iat)**(3d0/2d0)) &
                         + 2d0*meamrho0(isp) &
                         + 6d0*meamemb3(isp)*rho_i(iat) &
                         + 12d0*meamemb4(isp)*rhoi_sqr
              d2meamf_drhoi_dmeame0= -0.5d0/sqrt_rhoi
              d2meamf_drhoi_dmeamrho0= 2d0*rho_i(iat)
              d2meamf_drhoi_dmeamemb3= 3d0*rhoi_sqr
              d2meamf_drhoi_dmeamemb4= 4d0*rhoi_cube

           else
              !If rho_i=0, drhoi_dmeamtau and drhoi_dpara will also = 0 below,
              !so the dmeamf_dmeamtau and dmeamf_dpara should = 0. However if
              !we evaluate the singularity above (/sqrt_rhoi), dmeamf_dmeamtau
              !and dmeamf_dpara will wrongly be set to NaN.
              dmeamf_drhoi(iat)=0d0
              d2meamf_drhoi2(iat)= 0d0
              d2meamf_drhoi_dmeame0= 0d0
              d2meamf_drhoi_dmeamrho0= 0d0
              d2meamf_drhoi_dmeamemb3= 0d0
              d2meamf_drhoi_dmeamemb4= 0d0
           endif

           !Calculate derivatives w.r.t coordinates
           if (rho_i(iat).ne.0d0) then
              do jj=0,gn_neighbors(iat,istr)
                 do alph=1,3
                    dmeamf_dxyz(alph,jj,iat)= &
                       dmeamf_drhoi(iat) * drhoi_dxyz(alph,jj,iat)
                    d2meamf_dxyz_dmeame0(alph,jj,iat)= &
                       d2meamf_drhoi_dmeame0 * drhoi_dxyz(alph,jj,iat)
                    d2meamf_dxyz_dmeamrho0(alph,jj,iat)= &
                       d2meamf_drhoi_dmeamrho0 * drhoi_dxyz(alph,jj,iat)
                    d2meamf_dxyz_dmeamemb3(alph,jj,iat)= &
                       d2meamf_drhoi_dmeamemb3 * drhoi_dxyz(alph,jj,iat)
                    d2meamf_dxyz_dmeamemb4(alph,jj,iat)= &
                       d2meamf_drhoi_dmeamemb4 * drhoi_dxyz(alph,jj,iat)
                 enddo
              enddo

              !Calculate derivatives of dmeamf_dxyz w.r.t parameters
              !Below, isp is used to obtain the parameter indices for the
              !pairwise density 
              !functions, so should =1 unless using acceptor-dependant densities
              isp=1
              if (thiaccptindepndt.eqv..false.) isp=gspecies(iat,istr)
              do jsp=1,maxspecies
                 do ll=0,lmax
                     !Derivatives w.r.t the pairwise density parameters:
                     do iiCoeff=1,noptdensityCoeff(ll,isp,jsp)
                        ip = ioptdensityCoeff(iiCoeff,ll,isp,jsp)
                        do jj=0,gn_neighbors(iat,istr)
                           do alph=1,3
                              d2meamf_dxyz_dpara(alph,jj,ip,ll,jsp,iat)= &
                              d2meamf_drhoi2(iat) * drhoi_dpara(ip,ll,jsp,iat) * drhoi_dxyz(alph,jj,iat) + &
                              dmeamf_drhoi(iat) * d2rhoi_dxyz_dpara(alph,jj,ip,ll,jsp,iat)
                           enddo
                        enddo
                     enddo
                     do iiCutoff=1,noptdensityCutoff(ll,isp,jsp)
                        ip = ioptdensityCutoff(iiCutoff,ll,isp,jsp)
                        do jj=0,gn_neighbors(iat,istr)
                           do alph=1,3
                              d2meamf_dxyz_dpara(alph,jj,ip,ll,jsp,iat)= &
                              d2meamf_drhoi2(iat) * drhoi_dpara(ip,ll,jsp,iat) * drhoi_dxyz(alph,jj,iat) + &
                              dmeamf_drhoi(iat) * d2rhoi_dxyz_dpara(alph,jj,ip,ll,jsp,iat)
                           enddo
                        enddo
                     enddo
                 enddo
              enddo

           endif

           !Calculate analytic derivatives of meam_f(at)
           !Derivatives w.r.t meame0, etc:
           dmeamf_dmeame0(iat)=-sqrt_rhoi
           dmeamf_dmeamrho0(iat)=rhoi_sqr
           dmeamf_dmeamemb3(iat)=rhoi_cube
           dmeamf_dmeamemb4(iat)=rhoi_quart

           !Below, isp is used to obtain the parameter indices for the
           !pairwise density 
           !functions, so should =1 unless using acceptor-dependant densities
           isp=1
           if (thiaccptindepndt.eqv..false.) isp=gspecies(iat,istr)
           do jsp=1,maxspecies
              do ll=0,lmax
                  !Derivatives w.r.t the pairwise density parameters:
                  do iiCoeff=1,noptdensityCoeff(ll,isp,jsp)
                     ip = ioptdensityCoeff(iiCoeff,ll,isp,jsp)
                     dmeamf_dpara(ip,ll,jsp,iat)= &
                           dmeamf_drhoi(iat) * drhoi_dpara(ip,ll,jsp,iat)
                  enddo
                  do iiCutoff=1,noptdensityCutoff(ll,isp,jsp)
                     ip = ioptdensityCutoff(iiCutoff,ll,isp,jsp)
                     dmeamf_dpara(ip,ll,jsp,iat)= &
                           dmeamf_drhoi(iat) * drhoi_dpara(ip,ll,jsp,iat)

                  enddo
              enddo
           enddo

           !Update quantities to be summed over original unit cell
           summeamf=summeamf+meam_f(iat)
           !Caution: dsummeamf_dpara has jsp,isp rather than
           !isp,jsp in meamrhodecay [meamrhodecay(np,l,isp,jsp)]
           isp=1
           if (thiaccptindepndt.eqv..false.) isp=gspecies(iat,istr)
           ! dsummeamf_dpara(1:12,0:lmax,1:maxspecies,isp) = &
           !       dsummeamf_dpara(1:12,0:lmax,1:maxspecies,isp) + &
           !       dmeamf_dpara(1:12,0:lmax,1:maxspecies,i)
           !We swap the order of the last two indices here. For calculation
           !efficiency purposes, 'i' was the last index. Now, for consistency
           !with meamrhodecay, we use the order: isp, jsp.
           dsummeamf_dpara(1:12,0:lmax,isp,1:maxspecies) = &
                 dsummeamf_dpara(1:12,0:lmax,isp,1:maxspecies) + &
                 dmeamf_dpara(1:12,0:lmax,1:maxspecies,iat)
           isp=gspecies(iat,istr)
           dsummeamf_dmeame0(isp)=dsummeamf_dmeame0(isp)+dmeamf_dmeame0(iat)
           dsummeamf_dmeamrho0(isp)=dsummeamf_dmeamrho0(isp)+dmeamf_dmeamrho0(iat)
           dsummeamf_dmeamemb3(isp)=dsummeamf_dmeamemb3(isp)+dmeamf_dmeamemb3(iat)
           dsummeamf_dmeamemb4(isp)=dsummeamf_dmeamemb4(isp)+dmeamf_dmeamemb4(iat)

           !print *,'numeric, meam_f(',iat,')=',meam_f(iat),', dmeamf_drhoi(',iat,')=',dmeamf_drhoi(iat)
           !stop

       endif

       !stop

    enddo

    do iat=gn_forces(istr)+1,gn_inequivalentsites(istr)

       isp=gspecies(iat,istr)

       if (useSpline1PartFns(isp).eqv..true.) then

           rhoIndex=1+FLOOR(rho_i(iat)/drhoLmps(isp))
           call interpolateSplineDeriv(drhoLmps(isp),embfuncSpline(1:nRhoMaxSpline,isp),embfuncSplineSecDer(1:nRhoMaxSpline,isp),nRhoMaxSpline,rhoIndex,rho_i(iat),meam_f(iat),dmeamf_drhoi(iat))

           !Calculate derivatives w.r.t coordinates
           if (rho_i(iat).ne.0d0) then
              do jj=0,gn_neighbors(iat,istr)
                 do alph=1,3
                    dmeamf_dxyz(alph,jj,iat)= &
                       dmeamf_drhoi(iat) * drhoi_dxyz(alph,jj,iat)
                 enddo
              enddo
           endif

       else

           sqrt_rhoi=sqrt(rho_i(iat))
           rhoi_sqr=(rho_i(iat)**2)
           rhoi_cube=rhoi_sqr*rho_i(iat)
           rhoi_quart=rhoi_sqr**2

           !MEAM energies not needed for total energy, but retain for use in
           !numerical derivatives for debug purposes (remove in final version)
           meam_f(iat)= -meame0(isp)*sqrt_rhoi &
               + meamrho0(isp)*rhoi_sqr &
               + meamemb3(isp)*rhoi_cube &
               + meamemb4(isp)*rhoi_quart

           if (rho_i(iat).ne.0d0) then
              dmeamf_drhoi(iat)= - 0.5d0*meame0(isp)/sqrt_rhoi &
                       + 2d0*meamrho0(isp)*rho_i(iat) &
                       + 3d0*meamemb3(isp)*rhoi_sqr &
                       + 4d0*meamemb4(isp)*rhoi_cube
              d2meamf_drhoi2(iat)= 0.25d0*meame0(isp)/(rho_i(iat)**(3d0/2d0)) &
                         + 2d0*meamrho0(isp) &
                         + 6d0*meamemb3(isp)*rho_i(iat) &
                         + 12d0*meamemb4(isp)*rhoi_sqr
              d2meamf_drhoi_dmeame0= -0.5d0/sqrt_rhoi
              d2meamf_drhoi_dmeamrho0= 2d0*rho_i(iat)
              d2meamf_drhoi_dmeamemb3= 3d0*rhoi_sqr
              d2meamf_drhoi_dmeamemb4= 4d0*rhoi_cube
           else
              !If rho_i=0, drhoi_dmeamtau and drhoi_dpara will also = 0 below,
              !so the dmeamf_dmeamtau and dmeamf_dpara should = 0. However if
              !we evaluate the singularity above (/sqrt_rhoi), dmeamf_dmeamtau
              !and dmeamf_dpara will wrongly be set to NaN.
              dmeamf_drhoi(iat)=0d0
              d2meamf_drhoi2(iat)= 0d0
              d2meamf_drhoi_dmeame0= 0d0
              d2meamf_drhoi_dmeamrho0= 0d0
              d2meamf_drhoi_dmeamemb3= 0d0
              d2meamf_drhoi_dmeamemb4= 0d0
           endif

           !Calculate derivatives w.r.t coordinates
           if (rho_i(iat).ne.0d0) then
              do jj=0,gn_neighbors(iat,istr)
                 do alph=1,3
                    dmeamf_dxyz(alph,jj,iat)= &
                       dmeamf_drhoi(iat) * drhoi_dxyz(alph,jj,iat)
                    d2meamf_dxyz_dmeame0(alph,jj,iat)= &
                       d2meamf_drhoi_dmeame0 * drhoi_dxyz(alph,jj,iat)
                    d2meamf_dxyz_dmeamrho0(alph,jj,iat)= &
                       d2meamf_drhoi_dmeamrho0 * drhoi_dxyz(alph,jj,iat)
                    d2meamf_dxyz_dmeamemb3(alph,jj,iat)= &
                       d2meamf_drhoi_dmeamemb3 * drhoi_dxyz(alph,jj,iat)
                    d2meamf_dxyz_dmeamemb4(alph,jj,iat)= &
                       d2meamf_drhoi_dmeamemb4 * drhoi_dxyz(alph,jj,iat)
                 enddo
              enddo

              !Calculate derivatives of dmeamf_dxyz w.r.t parameters
              !Below, isp is used to obtain the parameter indices for the
              !pairwise density 
              !functions, so should =1 unless using acceptor-dependant densities
              isp=1
              if (thiaccptindepndt.eqv..false.) isp=gspecies(iat,istr)
              do jsp=1,maxspecies
                 do ll=0,lmax
                     !Derivatives w.r.t the pairwise density parameters:
                     do iiCoeff=1,noptdensityCoeff(ll,isp,jsp)
                        ip = ioptdensityCoeff(iiCoeff,ll,isp,jsp)
                        do jj=0,gn_neighbors(iat,istr)
                           do alph=1,3
                              d2meamf_dxyz_dpara(alph,jj,ip,ll,jsp,iat)= &
                              d2meamf_drhoi2(iat) * drhoi_dpara(ip,ll,jsp,iat) * drhoi_dxyz(alph,jj,iat) + &
                              dmeamf_drhoi(iat) * d2rhoi_dxyz_dpara(alph,jj,ip,ll,jsp,iat)
                           enddo
                        enddo
                     enddo
                     do iiCutoff=1,noptdensityCutoff(ll,isp,jsp)
                        ip = ioptdensityCutoff(iiCutoff,ll,isp,jsp)
                        do jj=0,gn_neighbors(iat,istr)
                           do alph=1,3
                              d2meamf_dxyz_dpara(alph,jj,ip,ll,jsp,iat)= &
                              d2meamf_drhoi2(iat) * drhoi_dpara(ip,ll,jsp,iat) * drhoi_dxyz(alph,jj,iat) + &
                              dmeamf_drhoi(iat) * d2rhoi_dxyz_dpara(alph,jj,ip,ll,jsp,iat)
                           enddo
                        enddo
                     enddo
                 enddo
              enddo

           endif


       endif

    enddo


end subroutine embeddingfunctionForceSpline





    ! Code fragment to use if I code in look-up tables:
    !
    !      if (lookuptables.eqv..true.) then
    !
    !         print *,'using lookuptables for emb en calc'
    !
    !         do i=1,gn_inequivalentsites(istr)
    !            isp=gspecies(i,istr)
    !
    !            if (isp.eq.1) then
    !
    !              call splint(rho_embFe,embFe,secder_embFe,
    !     +              emb_Fe_arrsz,emb_Fe_arrsz,
    !     +              rho_i(i),meam_f(i)) !***need to put index in before
    !                                        !rij
    !
    !            elseif (isp.eq.2) then
    !
    !              call splint(rho_embC,embC,secder_embC,
    !     +              emb_C_arrsz,emb_C_arrsz,
    !     +              rho_i(i),meam_f(i)) !***need to put index in before
    !                                        !rij
    !
    !            else
    !               print *,'stop in emb en routine'
    !               stop
    !            endif
    !         enddo
    !
    !      else
