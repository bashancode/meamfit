subroutine setuprandomseed(iseed)

    !-------------------------------------------------------------------c
    !
    !     Initializes the random seed, either based on the current
    !     time or a user supplied seed from the settings file.
    !
    !     Andrew Duff 2015
    !
    !     Copyright (c) 2018, STFC
    !
    !-------------------------------------------------------------------c
  

    implicit none

    integer, allocatable :: seed(:)
    integer :: nSeedArr !,seed(12) !Changed from 8   
    character*8 charac1
    character*10 charac2
    character*5 charac3
    integer values(8),iseed,i
    real(8) tmp

    call random_seed(size=nSeedArr) !This returns the minimum size that the seed array must take. Apparently this changes with new
    !updates of gfortran.
    allocate(seed(nSeedArr))
    seed=0
    !print *,'DEBUG info: random_seed(size=...) returned size ',nSeedArr

    if (iseed.eq.0) then
       !Initialize random seed using data and time
       call date_and_time(charac1, charac2, charac3, values)
       do i=1,MIN(8,nSeedArr)
          seed(i)=values(9-i)*values(i)
       enddo
    else
       !Initialize random seed using integer provided in settings file
       do i=1,nSeedArr
          seed(i)=iseed*dble(i)
       enddo
    endif
   
   call random_seed(put=seed)

   !test
   !call RANDOM_NUMBER(tmp)
   !print *,tmp
   !call RANDOM_NUMBER(tmp)
   !print *,tmp
   !call RANDOM_NUMBER(tmp)
   !print *,tmp
   !call RANDOM_NUMBER(tmp)
   !print *,tmp
   !call RANDOM_NUMBER(tmp)
   !print *,tmp
   !stop
   !!:::::::

end subroutine setuprandomseed
