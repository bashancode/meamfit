subroutine interpolateSpline(dx,YA,Y2A,N_ARR,index,X,Y)


    !----------------------------------------------------------------c
    !     From Numerical Recipes                                     c
    !     DESCRIPTION: given arrays YA(1:N_ARR) on x grid:           c
    !                  0, dx, 2dx, with Y2A(1:N_ARR) as              c
    !                  calculated by 'spline', the subroutine        c
    !                  calculates the value of the spline at X and   c
    !                  returns it as Y                               c
    !----------------------------------------------------------------c


    implicit none

    integer N_ARR,KLO,KHI,K,index
    real(8) YA(N_ARR),Y2A(N_ARR),X,Y,H,A,B,dx

    KLO=index
    KHI=index+1

    H=dx
    A=((KHI-1)*dx-X)/H    !(XA(KHI)-X)/H
    B=(X-(KLO-1)*dx)/H !(X-XA(KLO))/H
    !print *,'A=',A,' B=',B,' Y2A(',KLO,')=',Y2A(KLO),' Y2A(',KHI,')=',Y2A(KHI)
    !print *,'X=',X,' KLO=',KLO,' dx=',dx,' H=',H
    Y=A*YA(KLO)+B*YA(KHI)+((A**3-A)*Y2A(KLO)+(B**3-B)*Y2A(KHI))*(H**2) &
        /6.d0

end subroutine interpolateSpline



subroutine interpolateSplineDeriv(dx,YA,Y2A,N_ARR,index,X,Y,DYDX)


    !----------------------------------------------------------------c
    !     From Numerical Recipes                                     c
    !     DESCRIPTION: given arrays YA(1:N_ARR) on x grid:           c
    !                  0, dx, 2dx, with Y2A(1:N_ARR) as              c
    !                  calculated by 'spline', the subroutine        c
    !                  calculates the value of the spline at X and   c
    !                  returns it as Y                               c
    !----------------------------------------------------------------c


    implicit none

    integer N_ARR,KLO,KHI,K,index
    real(8) YA(N_ARR),Y2A(N_ARR),X,Y,H,A,B,dx,DYDX,DADX,DBDX

    KLO=index
    KHI=index+1

    H=dx
    A=((KHI-1)*dx-X)/H    !(XA(KHI)-X)/H
    DADX=-1d0/H
    B=(X-(KLO-1)*dx)/H !(X-XA(KLO))/H
    DBDX=1d0/H
    !print *,'A=',A,' B=',B,' Y2A(',KLO,')=',Y2A(KLO),' Y2A(',KHI,')=',Y2A(KHI)
    !print *,'X=',X,' KLO=',KLO,' dx=',dx,' H=',H
    Y=A*YA(KLO)+B*YA(KHI)+((A**3-A)*Y2A(KLO)+(B**3-B)*Y2A(KHI))*(H**2) &
        /6.d0
    DYDX=DADX*YA(KLO)+DBDX*YA(KHI)+((3d0*(A**2)-1)*DADX*Y2A(KLO)+(3d0*(B**2)-1)*DBDX*Y2A(KHI))*(H**2)/6d0

end subroutine interpolateSplineDeriv




subroutine splint(XA,YA,Y2A,N,N_ARR,index,X,Y)


    !----------------------------------------------------------------c
    !     From Numerical Recipes                                     c
    !     DESCRIPTION: given arrays YA(1:N) and XA(1:N) which        c
    !                  tabulate a function and Y2A(1:N) as           c
    !                  calculated by 'spline', the subroutine        c
    !                  calculates the value of the spline at X and   c
    !                  returns it as Y                               c
    !----------------------------------------------------------------c


    implicit none

    integer N,N_ARR,KLO,KHI,K,index
    real(8) XA(N_ARR),YA(N_ARR),Y2A(N_ARR),X,Y,H,A,B

    !       KLO=1
    !       KHI=N
    !  1    if (KHI-KLO.gt.1) then
    !          K=(KHI+KLO)/2
    !          if (XA(K).gt.X) then
    !             KHI=K
    !          else
    !             KLO=K
    !          endif
    !          goto 1
    !       endif
    !      print *,'inside subr: KLO=',KLO,', KHI=',KHI
    !
    KLO=index
    KHI=index+1
    !      print *,'before subr: KLO=',KLO,', KHI=',KHI
    !c      stop

    H=XA(KHI)-XA(KLO)
    A=(XA(KHI)-X)/H
    B=(X-XA(KLO))/H
    Y=A*YA(KLO)+B*YA(KHI)+((A**3-A)*Y2A(KLO)+(B**3-B)*Y2A(KHI))*(H**2) &
        /6.d0

end subroutine splint
