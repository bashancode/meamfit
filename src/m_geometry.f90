

module m_geometry

    !     Copyright (c) 2018, STFC

    !List of coordinates and neighbors
    integer nstruct,maxnnatoms,istr,smlsepnperspcStr_numEntries, &
            maxneighbors,maxsites
    real(8) rij,rij2,rij3,smlsepn_overall,lrgsepn_overall,strWghtParTot
    integer, allocatable:: gn_inequivalentsites(:), & !struct
        gspecies(:,:),          & !nnatom, struct
        gn_neighbors(:,:),      & !site, struct
        gneighborlist(:,:,:),   & !neighborsite,site,struct,
        gn_forces(:)
    real(8), allocatable:: gxyz(:,:,:),g_vol(:), & !g_vol is the volume per target DFT file
        diststr(:,:,:,:,:), &
        dist2str(:,:,:,:,:),dist3str(:,:,:,:,:), &
        dxstr(:,:,:,:,:),dystr(:,:,:,:,:), &
        dzstr(:,:,:,:,:), & !xyz,nnatom,struct
        gxyz_backup(:,:,:),smallestsepnStr(:), &
        smlsepnperspcStr(:,:), &
        lrgsepnperspcStr(:,:), &
        smlsepnperspc_overall(:,:), &
        lrgsepnperspc_overall(:,:), &
        sdSepn(:,:), &
        avgSepn(:,:), &
        nSepn(:,:)
    ! we also store some geometry related properties for use in determining
    ! which structures are computed by which nodes:
    real(8), allocatable:: strWghtPar(:) !this contains natoms^3 and is used in determining structures to allocate to each node
    integer, allocatable:: start_i_stored(:),stop_i_stored(:) !the first and last structure to be computed by each node

contains
    real(8) function distance(i,j)
        integer i,j
        distance=sqrt( (gxyz(1,i,istr)-gxyz(1,j,istr))**2+ &
            (gxyz(2,i,istr)-gxyz(2,j,istr))**2+ &
            (gxyz(3,i,istr)-gxyz(3,j,istr))**2 )
    end function distance
    real(8) function distance2(i,j)
        integer i,j
        distance2=(gxyz(1,i,istr)-gxyz(1,j,istr))**2+ &
            (gxyz(2,i,istr)-gxyz(2,j,istr))**2+ &
            (gxyz(3,i,istr)-gxyz(3,j,istr))**2
    end function distance2
end module m_geometry
