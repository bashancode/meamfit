subroutine setupfilenames

    !-------------------------------------------------------------c
    !
    !     Assigns filenames to the filename variables. Also sets
    !     up the array 'strucnames', which contains abbreviated
    !     forms of the vasprun.xml/POSCAR filenames, specifically, 
    !     only the parts of the filenames pertinant to defining the
    !     structure. This is explained by way of example. Say we
    !     have a filename vasprun_Coct_pot_relaxed.xml, then the 
    !     part of this filename which goes into the relevant
    !     'strucnames' array element is 'Coct'. The vasprun_ is
    !     always removed, and if either 'pot', 'EAM' or 'MEAM'
    !     are identified, then these (and everything to the right
    !     of them) are also removed.
    !
    !     Called by:     program MEAMfit
    !     Calls:         createfitdbse, readFitdbseLine,
    !                 checkifnumber, getnumconfigs 
    !     Returns:       nTargetFiles,nstruct,startparameterfile,
    !                 crystalstrucfile,settingsfile,fitdbse, 
    !                 forcefit,targetfiles,strucnames,
    !                 weights,rlxstruc,
    !                 computeForces_backup,freeenergy,istrucInFile,
    !                 DFTdata
    !     Files read:    fitdbse
    !
    !     Andy Duff, Feb 2008
    !
    !     Copyright (c) 2018, STFC
    !
    !-------------------------------------------------------------c

    use m_generalinfo
    use m_filenames
    use m_geometry
    use m_datapoints
    use m_optimization

    implicit none

    logical num,freeenergy_tocopy,exist,&
            computeForces_tocopy
    integer i,j,ifile,istruc,nlimits,stringLen,DFTdata_tocopy
    integer limits(10),step(10)
    character*7 stringStrt,stringEnd
    character*80 line
    character*80 tmp,tmp2,string,string2,string3,string4,concatString_Num
    character*80 targetfiles_tocopy
    real(8) weightsEn_tocopy,weightsFr_tocopy,weightsSt_tocopy,double,double2,double3

    crystalstrucfile='crystal_struc.xyz'
    fitdbse='fitdbse'
    lookuptablefile='lookuptablefile'

    !Check if there is a fitdbse file
    inquire(file=trim(fitdbse),exist=exist)
    if (exist.eqv..false.) then
       call createfitdbse
       createdFitdbse=.true.
       return
    endif
 
    !Read in list of files containing target fitting data
    open(unit=1,file=trim(fitdbse),status='old')
    read(1,*) nTargetFiles
    if (nTargetFiles.lt.1) then
        print *,'ERROR: no. of files in fitdbse must be greater than'
        print *,'or equal to 1, STOPPING.'
        stop
    endif
    !Scan through fitdbse once to see how many ionic structures we have to fit
    !to (this will not necessarily equal the number of VASP files)
    nstruct=0
    do ifile=1,nTargetFiles
 
        read(1,'(A80)') line
        call readFitdbseLine(line,tmp,string,string4,double,double2,double3)
        call checkifnumber(string,num)
        if (num.eqv..true.) then
           call getnumconfigs(string,limits,nlimits,step)
           do i=1,nlimits,2
              do j=limits(i),limits(i+1),step(i)
                 nstruct=nstruct+1
              enddo
           enddo
        else
           nstruct=nstruct+1
        endif
    enddo

    rewind(1)
    read(1,*)
    allocate(targetfiles(nstruct), &
        strucnames(nstruct),computeForces(nstruct), &
        weightsEn(nstruct),weightsFr(nstruct),weightsSt(nstruct), &
        rlxstruc(nstruct),computeForces_backup(nstruct), &
        freeenergy(nstruct),istrucInFile(nstruct), &
        DFTdata(nstruct),refenergy(nstruct))
    refenergy=0
    refenergy(1)=1 !The first configuration is always taken as a reference

    if (verbosity.gt.1) then
       print *
       print *,'Initializing Fitting database'
       print *,'-----------------------------'
       print *,'Files                             | Configs  | Energy to fit     | Weights (En/Fo/St)'
    endif

    rlxstruc=0
    energyfit=.false.
    forcefit=.false.
    istruc=1

    do ifile=1,nTargetFiles
  
       !Read fitdbse file line-by-line
       read(1,'(A80)') line
       call readFitdbseLine(line,targetfiles(istruc),string,string4, &
                            weightsEn(istruc),weightsFr(istruc),weightsSt(istruc))

       if (verbosity.gt.1) write (*,'(A1,A35,A1,A10,A1,A14,A1,F12.5,A1,F12.5,A1,F12.5)') &
               ' ',targetfiles(istruc),' ',string,' ', &
               string4,' ',weightsEn(istruc),' ',weightsFr(istruc),' ',weightsSt(istruc)

       !Check type of input file (VASP or CASTEP)
       call findDFTfileType(targetfiles(istruc),DFTdata(istruc))


       if (weightsEn(istruc).gt.0d0) then
          energyfit=.true.
       endif

       if ((weightsFr(istruc).gt.0d0).or.(weightsSt(istruc).gt.0d0)) then
          computeForces(istruc)=.true.
          forcefit=.true.
       else
          computeForces(istruc)=.false.
       endif

       !Third column: currently specifies the energy type to fit to (E0=total
       !energy or Fr=free energy). Later: extend this column so that it can take
       !multiple 'keyword' inputs, eg: Fr;rl. This would fit to free energies
       !and also relax the structure prior to evaluating the energy. Another
       !keyword: rf, take as reference the energy of this structure.
       if (string4(1:2).eq.'fr'.or.string4(1:2).eq.'Fr'.or. &
           string4(1:2).eq.'fR'.or.string4(1:2).eq.'FR') then
           freeenergy(istruc)=.true.
       elseif (string4(1:2).eq.'e0'.or.string4(1:2).eq.'E0') then
           freeenergy(istruc)=.false.
       else
           print *,'ERROR: Incorrect entry in 3rd column of fitdbse file.'
           print *,'Expecting E0 or FR. Stopping'
           stop
       endif
       !Code to check if an energy is to be used as a reference energy for
       !subsequent energies, to include later:
       stringLen=LEN(trim(string4))
       !print *,'cheking if reference is to be used (',stringLen,')'
       if (stringLen.ge.5) then
          do i=4,stringLen-1
             !print *,i,'/',stringLen-1,': ',string4(i:i+1)
             if ( (string4(i:i+1).eq.'rf') .or. &
                  (string4(i:i+1).eq.'Rf') .or. &
                  (string4(i:i+1).eq.'RF') ) then
                if (stringLen.eq.5) then
                   refenergy(istruc)=1
                else
                   read(string4(i+2:i+2),*) refenergy(istruc)
                endif
                if (energyfit.eqv..false.) then
                   print *,'ERROR: you cannot use a reference with non-zero weight on the energy.'
                   print *,'Please adjust your fitdbse, STOPPING.'
                   stop
                endif
                if (verbosity.gt.1) then
                   print *,'refenergy(',istruc,')=',refenergy(istruc)
                endif
             endif
          enddo
       endif
       !   !Code for structure relaxation to include later:
       !   if (string(1:1).eq.'R'.or.string(1:1).eq.'r') then
       !       rlxstruc(istruc)=1 !Relax structure using potential
       !   endif

       !If a range of ionic configurations is specified, copy relevant arrays       
       call getnumconfigs(string,limits,nlimits,step)
       DFTdata_tocopy=DFTdata(istruc)
       targetfiles_tocopy=targetfiles(istruc)
       computeForces_tocopy=computeForces(istruc)
       freeenergy_tocopy=freeenergy(istruc)
       weightsEn_tocopy=weightsEn(istruc)
       weightsFr_tocopy=weightsFr(istruc)
       weightsSt_tocopy=weightsSt(istruc)
       !print *,'file=',ifile,' nlimits=',nlimits
       !print *,'limits(1)=',limits(1),'limits(2)=',limits(2)
       !print *,'weightsFr_tocopy=',weightsFr_tocopy
       !print *,'computeForces_tocopy=',computeForces_tocopy
       do i=1,nlimits,2
          do j=limits(i),limits(i+1),step(i)
             DFTdata(istruc)=DFTdata_tocopy
             targetfiles(istruc)=targetfiles_tocopy
             rlxstruc(istruc)=0
             computeForces(istruc)=computeForces_tocopy
             freeenergy(istruc)=freeenergy_tocopy
             weightsEn(istruc)=weightsEn_tocopy
             weightsFr(istruc)=weightsFr_tocopy
             weightsSt(istruc)=weightsSt_tocopy
             istrucInFile(istruc)=j
             strucnames(istruc)=concatString_Num(targetfiles_tocopy,istrucInFile(istruc))
             istruc=istruc+1
          enddo
       enddo

     enddo

     close(unit=1)

     if (verbosity.gt.1) then
        write(*,'(A13,I5,A19)') ' (fitting to ',istruc-1,' atomic structures)'
        print *
     endif

end subroutine setupfilenames


subroutine createfitdbse

    !----------------------------------------------------------------------c
    !
    !     Checks for vasprun.xml files in directory (later add in OUTCAR
    !     file check) and makes a template fitdbse file from them.
    !
    !     Called by: setupfilenames    
    !     Calls: system
    !     Files read: - 
    !     Files written: fitdbse
    !
    !     Andrew Duff, 2014
    !
    !----------------------------------------------------------------------c

    use m_meamparameters
    use m_generalinfo
    use m_atomproperties
    use m_optimization
    use m_filenames
    use m_mpi_info

    implicit none

    logical scfMD
    integer, parameter:: nfileLim=10000
    integer, allocatable:: nconf(:)
    integer ifile,IOstatus,nfile,iconf,nottoinclude,sizeFile,statusFile,CASTEPgeomFileReadin,DFTdataTmp
    character*80, allocatable:: files(:)
    character*80 line,newstrng
    character*106 lineLong

    allocate(files(nfileLim),nconf(nfileLim))

    !Check for filenames starting with 'vasprun' or '.geom' (castep) in the directory, and
    !temporarily store them in 'checkfiles.tmp'

    if (verbosity.gt.1) then
       print *,'fitdbse file does not exist; attempting to create one.'
       print *,'Scanning directory with ls to find vasprun.xml,.geom (castep), and .out_QE or .out_PW (quantum espresso) files'
    endif
    call system("ls vasprun*.xml *.geom *.out_QE *.out_PW *.castep > checkfiles.tmp 2> /dev/null")
    inquire(file="checkfiles.tmp",size=sizeFile)
    if (sizeFile.eq.0) then
       if (verbosity.gt.1) then
          print *,'Cannot find input DFT files (vasprun#.xml, #.geom, #.out_QE, #.out_PW  accepted,'
          print *,'where # is any alphanumeric sequence). Stopping.'
       endif
       stop
    else
       if (verbosity.gt.1) print *,'Files identified'
    endif

    !Determine the number of DFT input files to read in
    open(50,file='checkfiles.tmp')
    ifile=1
    do
       read(50,*,IOSTAT=IOstatus) files(ifile)
       if (IOstatus.lt.0) then
          exit
       endif
       if (ifile.gt.nfileLim) then
          print *,'Too many input DFT files, please adjust code. Stopping.'
          stop
       endif
       ifile=ifile+1
    enddo
    nfile=ifile-1
    close(50)
    !CALL system("rm checkfiles.tmp")

    !Determine the number of atomic configurations per file (and record the
    !number that have no configurations: nottoinclude)
    nottoinclude=0
    CASTEPgeomFileReadin=0 !Keep track of the type of CASTEP file read in so we 
    !can throw an error if both .geom and .castep types are attempted to be read in
    do ifile=1,nfile
       !print *,'checking file:',trim(files(ifile))
       call findDFTfileType(files(ifile),DFTdataTmp)

       !Do not allow both .geom and .castep files to be read in at the same time
       if ( ((DFTdataTmp.eq.1).and.(CASTEPgeomFileReadin.eq.3)) .or. ((DFTdataTmp.eq.3).and.(CASTEPgeomFileReadin.eq.1)) )  then
          print *,'ERROR: you are attempting to read in both .geom and .castep files.'
          print *,'Please choose one or the other. Stopping.'
          print *,'(Advice: For CASTEP MD, do not copy .md files to .geom files and'
          print *,'try and read them in as the energies, forces and stress-tensors '
          print *,'there include contributions from the thermostat, while those in '
          print *,'.castep are just from the BO-surface.)'
          stop
       endif
       if (DFTdataTmp.eq.1) then
          CASTEPgeomFileReadin=1
       elseif (DFTdataTmp.eq.3) then
          CASTEPgeomFileReadin=3
       endif

       open(50,file=trim(files(ifile)),IOSTAT=IOstatus)
       iconf=0
       if (DFTdataTmp.eq.0) then
          do
            read(50,'(a80)',IOSTAT=IOstatus) line
            if (IOstatus.lt.0) then
               exit
            endif
            newstrng=line(17:22)
            if ( newstrng(1:6).eq.'forces') then
               iconf=iconf+1
            endif
          enddo
       elseif (DFTdataTmp.eq.1) then
          do
            read(50,'(a106)',IOSTAT=IOstatus) lineLong
            if (IOstatus.lt.0) then
               exit
            endif
            newstrng=lineLong(102:106)
            !print *,'newstrng=',newstrng
            if ( newstrng(1:5).eq.'<-- E') then
               iconf=iconf+1
            endif
          enddo
       elseif (DFTdataTmp.eq.2) then
          do
            read(50,'(a106)',IOSTAT=IOstatus) lineLong
            if (IOstatus.lt.0) then
               exit
            endif
            if (lineLong(1:28)=='                total energy') then
               iconf=iconf+1
            endif
          enddo
       elseif (DFTdataTmp.eq.3) then
          if (verbosity.gt.1) print *,'looking for Forces..........' ! We use *** Forces *** to identify configs in CASTEP. 'Final energy, E' is insufficient because it can occur multiple times for a given config (e.g. energy cutoff test), and 'Unit Cell' is no good because sometimes (in continuation jobs) Unit Cell occurs with no accompanying observables calculated. *** Forces *** can be used to determine a consistent set of configurations, energies, forces and stresses
          do
            read(50,'(a106)',IOSTAT=IOstatus) lineLong
            if (IOstatus.lt.0) then
               exit
            endif
            !print *,lineLong(1:17)
            if ((lineLong(41:46)=='Forces').or.(lineLong(29:34)=='Forces').or.(lineLong(35:40)=='Forces').or.(lineLong(47:52)=='Forces'))  then ! last search here is for where singleEnergy task is used in castep
               iconf=iconf+1
               !print *,'new iconf=',iconf
            endif
          enddo
       elseif (DFTdataTmp.eq.4) then !PWscf
          scfMD=.false.
          do
            read(50,'(a106)',IOSTAT=IOstatus) lineLong
            if (IOstatus.lt.0) then
               exit
            endif
            !For MD, positions are reported once at the start of the file
            if (lineLong(40:48)=='positions') then
               iconf=iconf+1
            endif
            !... and then ATOMIC_POSITIONS gets reported for each moved set of
            !positions. This includes at the end of the simulation where atoms
            !are moved but no energies, forces or stress tensor are computed.
            !Except for the last one, these are the positions and associated
            !observables we read in. Note, that this means we omit the first set
            !of positions (which have a different formatting).
            if (lineLong(1:16)=='ATOMIC_POSITIONS') then
               iconf=iconf+1
               scfMD=.true.
            endif
          enddo
          if (scfMD.eqv..true.) then
             !The last set of ATOMIC_POSITIONS has no observables attached to it:
             iconf=iconf-1
          endif

       endif
       close(50)
       if (verbosity.gt.1) write(*,'(A5,I5,A7,A35,A5,I5,A8)') 'file=',ifile,', name=',files(ifile),' has ',iconf,' configs'
       if (iconf.eq.0) then
          nottoinclude=nottoinclude+1
       endif
       nconf(ifile)=iconf
    enddo

    !Write the vasprun.xml files to 'fitdbse'
    open(50,file=trim(fitdbse))
    write(50,'(I5,A53)') nfile-nottoinclude,' # Files | Configs to fit | Quantity to fit | Weights'
    do ifile=1,nfile
       !print *,'ifile=',ifile,', nconf=',nconf(ifile)
       if (nconf(ifile).eq.0) then
          !don't include
       elseif (nconf(ifile).lt.10) then
          write(50,'(A50,A3,I1,A18)') files(ifile),' 1-',nconf(ifile),'   Fr 1 0.1 0.001'
       elseif(nconf(ifile).lt.100) then
          write(50,'(A50,A3,I2,A18)') files(ifile),' 1-',nconf(ifile),'   Fr 1 0.1 0.001'
       elseif(nconf(ifile).lt.1000) then
          write(50,'(A50,A3,I3,A18)') files(ifile),' 1-',nconf(ifile),'   Fr 1 0.1 0.001'
       elseif(nconf(ifile).lt.10000) then
          write(50,'(A50,A3,I4,A18)') files(ifile),' 1-',nconf(ifile),'   Fr 1 0.1 0.001'
       endif
    enddo
    close(50)

    !print *

    if (verbosity.gt.1) print *,'Finished writing fitdbse file, stopping.'
!    call mpi_finalize(ierr)
!    call p_finalize()

end subroutine createfitdbse


function concatString_Num(string,num)

     implicit none

     integer num
     character*80 string,concatString_Num,string2

     if (num.lt.10) then
        write(string2,'(I1)') num
     elseif (num.lt.100) then
        write(string2,'(I2)') num
     elseif (num.lt.1000) then
        write(string2,'(I3)') num
     elseif (num.lt.10000) then
        write(string2,'(I4)') num
     endif

     concatString_Num=trim(string)//"_"//trim(string2)

end function concatString_Num

subroutine findDFTfileType(targetFile,DFTdata)

       implicit none

       integer DFTdata
       integer stringLen
       character*4 stringEnd2
       character*7 stringStrt,stringEnd,stringEnd3
       character*80 targetFile

       !Check type of input file (VASP or CASTEP)
       stringLen=LEN(trim(targetFile))
       stringStrt=""
       stringEnd=""
       if (stringLen.ge.7) then
          stringStrt=targetFile(1:7)
          stringEnd=targetFile(stringLen-4:stringLen)
          stringEnd3=targetFile(stringLen-6:stringLen)
       endif
       if (stringLen.ge.5) then
          stringEnd2=targetFile(stringLen-3:stringLen)
       endif
       if (stringStrt.eq."vasprun") then
          DFTdata=0
       elseif (stringEnd.eq.".geom") then
          !CASTEP .geom files
          DFTdata=1
       elseif (stringEnd3.eq.".out_QE") then ! Output files of QE should be
                          !renamed with _QE at the end, to avoid interpreting
                          !e.g. sepnHistogram.out as a QE file. May need to make
                          !changes elsewhere in the code.
          !Quantum espresso
          DFTdata=2
       elseif (stringEnd3.eq.".castep") then
          !CASTEP .castep files
          DFTdata=3
       elseif (stringEnd3.eq.".out_PW") then
          !Quantum espresso PWscf
          DFTdata=4
       else
          print *,'ERROR: filenames in your fitdbse file must be either vasprun.xml (for'
          print *,'VASP), .geom or .castep files (for CASTEP), .out_QE, or .out_PW (Quantum Espresso) . STOPPING.'
          stop
       endif

end subroutine findDFTfileType


subroutine readFitdbseLine(line,string1,string2,string3,double1,double2,double3)

        implicit none

        logical spcPrev,num
        integer iprev,iword,i
        real(8) double1,double2,double3
        character*80 line
        character*80 str,string1,string2,string3

        spcPrev=.true. !If this is true, don't accept the space as delimiter
        iprev=1 !Marks the start of a string to be extracted
        iword=1
        double2=-1 !If double2 retains this negative value, we know a fifth word
                   !has not been read in

        do i=1,80

          if (line(i:i).eq." ") then

            if (spcPrev.eqv..false.) then
               if (iword.eq.1) then
                  string1=trim(line(iprev:i))
               elseif (iword.eq.2) then
                  string2=trim(line(iprev:i))
               elseif (iword.eq.3) then
                  string3=trim(line(iprev:i))
               elseif (iword.eq.4) then
                  str=trim(line(iprev:i))
                  read( str, * ) double1
               elseif (iword.eq.5) then
                  str=trim(line(iprev:i))
                  read( str, * ) double2
               elseif (iword.eq.6) then
                  str=trim(line(iprev:i))
                  read( str, * ) double3
               endif
               spcPrev=.true.
               iword=iword+1
               !if (iword.eq.5) then
               !   exit
               !endif
            endif
            spcPrev=.true.

          else
             if (spcPrev.eqv..true.) then
                iprev=i
             endif
             spcPrev=.false.
          endif
        enddo

        !Check number of words
        if (iword-1.ne.6) then
           write(*,'(A57,I1)') 'ERROR: expected 6 words in fitdbse file, instead found ',iword-1
           print *,'(NOTE: you must use 80 characters or less per line), STOPPING.'
           stop
        endif
        !Check that string2 starts with a number (string2 indexs the ionic config from the DFT input file to fit to. This
        !can be a string (e.g, 1 or 1,2 or 1-5 or 1-5,6-10) however the first character at least must be a number.
        call checkifnumber(string2(1:1),num)
        if (num.eqv..false.) then
           write(*,'(A26,A1,A37)') 'ERROR: Unexpected letter: ',string2(1:1), &
                           ', in 2nd column of fitdbse. STOPPING.'
           stop
        endif

end subroutine readFitdbseLine


subroutine getnumconfigs(string,limits,nlimits,step)

    !-------------------------------------------------------------------c
    !
    !     Extract from 'string' the ranges of ionic configurations which 
    !     are to be used in the optimization, and then assign the limits
    !     of these ranges to 'limits', with the number of ranges
    !     assigned to 'nlimits'. Also check for 's', I.e., specification
    !     of a 'step' to be taken between adjacent configurations.
    !     Return these in 'step'.
    !
    !     Called by:     setupfilenames
    !     Calls:         checkifnumber
    !     Returns:       limits, nlimits, step
    !     Files read:    -
    !     Files written: -
    !
    !     Andrew Duff 2014
    !
    !     Copyright (c) 2018, STFC
    !
    !-------------------------------------------------------------------c

    use m_optimization

    implicit none

    logical num,awaiting_upper,awaiting_step
    integer i,ilower,istruc,nlimits
    integer limits(10),step(10)
    character*1 char
    character*80 string,tmpstring

    i=1
    nlimits=0
    ilower=1
    awaiting_upper=.false.
    awaiting_step=.false.
    step=1 !Steps between adjancent configs set to 1 by default

    !Step through 'string' sequentially, checking if each character is a number
    !of a letter.
    do
       char=string(i:i)
       call checkifnumber(char,num)
       if (num.eqv..false.) then
          if (awaiting_step) then
             !If an 's' has previously been found, then the value of the step
             !has now been found
             awaiting_step=.false.
             tmpstring=trim(string(ilower:i-1))
             read(tmpstring,'(I10)') step(nlimits-1)
          else
             !...otherwise the presence of a character indicates that a limit
             !has been found (a lower or upper limit on configuration number)
             nlimits=nlimits+1
             if (nlimits.gt.10) then
                print *,'ERROR: more than 10 limits read in from fitdbse,'
                print *,'STOPPING'
                stop
             endif
             tmpstring=trim(string(ilower:i-1))
             read(tmpstring,'(I10)') limits(nlimits)
             if (char.eq.'-') then
                !We have found the lower limit...
                awaiting_upper=.true.
             elseif ((char.eq.';').or.(char.eq.' ').or.(char.eq.'s')) then
                if (awaiting_upper.eqv..false.) then
                   !We have found a single configuration
                   nlimits=nlimits+1
                   limits(nlimits)=limits(nlimits-1)
                   if (char.eq.'s') then
                      !Can't specify a step if we only have one configuration!
                      print *,'ERROR: step specified for single config in fitdbse,'
                      print *,'STOPPING.'
                      stop
                   endif
                else
                   !We have found the upper limit
                   awaiting_upper=.false.
                   if (char.eq.'s') then
                      awaiting_step=.true.
                   endif
                endif
             elseif (char.eq.',') then
                print *,'ERROR: , detected in fitdbse file, please use ; instead, STOPPING'
                stop
             endif
          endif
          !Store the location for the start of the next string of numbers:
          ilower=i+1
       endif
       if (char.eq." ") then
          exit
       endif
       i=i+1
    enddo

end subroutine getnumconfigs

