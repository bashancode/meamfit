subroutine resetxyz

    !     Copyright (c) 2018, STFC

    !Reset xyz files

    use m_filenames
    use m_geometry

    character*80 poscarheader,poscarspecies
    integer i,j
    real(8) latticeconst,poscarcell(9)

    print *,'check that the following are the same as originals'
    do i=1,nstruct
        open(unit=1,file=trim(targetfiles(i)))
        read(1,'(a)') poscarheader
        read(1,*) latticeconst
        read(1,*) poscarcell
        read(1,'(a)') poscarspecies
        close(1)
        rewind(unit=1)
        write(1,'(a)') trim(poscarheader)
        write(1,*) latticeconst
        write(1,'(3f12.6)') poscarcell
        write(1,'(a)') trim(poscarspecies)
        write(1,'(a)') 'Cartesian'
        do j=1,gn_forces(i) !or gn_inequivalentsites(i) ?
            write(1,*) gxyz_backup(1:3,j,i)/latticeconst
        enddo
        close(1)
    enddo
    print *,'stopping after resetxyzx...'
    stop
end subroutine resetxyz
