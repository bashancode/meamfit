subroutine checkInputFilesExist

    !--------------------------------------------------------------c
    !
    !     Check to see if a settings file is present. Also, if the
    !     user has specified for a potential file to be read in,
    !     check it exists.
    !
    !     Called by:     program MEAMfit
    !     Returns:       settingsfileexist
    !
    !     Andrew Duff 2014
    !
    !     Copyright (c) 2018, STFC
    !
    !--------------------------------------------------------------c

    use m_filenames
    use m_generalinfo

    implicit none

    logical exist
    integer iLmp

    !If a potential parameter file has been specified to be read-in, make sure
    !it exists.
    if (readpotfile.eqv..true.) then
       inquire(file=trim(startparameterfile),exist=exist)
       if (exist.eqv..false.) then
          print *,'ERROR: specified read in potential parameters file ', &
                 trim(startparameterfile),' but file does not exist, stopping.'
          stop
       endif
    endif

end subroutine checkInputFilesExist
