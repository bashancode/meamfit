subroutine getmaxspecies

    !--------------------------------------------------------------c
    !
    !     Read in the set in the vasprun.xml files, or
    !     alternatively POSCAR files, and determine
    !     the total number of species used in them.
    !     UPDATE: POSCAR support removed (relevant lines retained
    !     however so that it can be re-instated later if needed)
    !
    !     Called by:     program MEAMfit
    !     Calls:         readtargetstruc
    !     Returns:       maxspecies 
    !
    !     Andrew Duff 2014
    !
    !     Copyright (c) 2018, STFC
    !
    !--------------------------------------------------------------c

    use m_generalinfo
    use m_atomproperties
    use m_filenames
    use m_poscar
    use m_optimization
    use m_geometry
    use m_meamparameters

    implicit none
    integer istruc,j,istrucInFile_prev
    character *80 targetfile_prev

    z2species=0
    maxspecies=0
    do istruc=1,nstruct !loop through each atomic configuration
        !print *,'istruc=',istruc,'/',nstruct
        if ((istruc.gt.1).and.(trim(targetfiles(istruc)).eq.targetfile_prev).and. &
              (istrucInFile(istruc).eq.istrucInFile_prev+1)) then
           !print *,'reading from left of position (istrucInFile(',istruc,')=',istrucInFile(istruc),&
           !         ', istrucInFile_prev+1=',istrucInFile_prev+1,')'
           !Do not need to read all the way from the start of the file
           call readtargetstruc( trim(targetfiles(istruc)),istrucInFile(istruc),DFTdata(istruc),.true. )
        else 
           !print *,'reading from start (istrucInFile(',istruc,')=',istrucInFile(istruc), &
           !          ', istrucInFile_prev+1=',istrucInFile_prev+1,')'
           !Need to read from start of the file
           call readtargetstruc( trim(targetfiles(istruc)),istrucInFile(istruc),DFTdata(istruc),.false. )
        endif
        !Find all atomic species
        !print *,'finding all atomic species, nspecies='
        !print *,nspecies
        do j=1,nspecies
            !print *,'j=',j,' z(j)=',z(j)
            if(z2species(z(j)).eq.0) then
                !print *,'z2species(z(j))=',z2species(z(j))
                maxspecies=maxspecies+1
                if (maxspecies.gt.10) then
                   print *,'ERROR: maxspecies>10. Please re-size speciestoZ'
                   print *,'array and then re-run.'
                   stop
                endif
                z2species(z(j))=maxspecies
                speciestoZ(maxspecies)=mod(z(j),112) !z>112 are duplicate elements (eg 113=h), but speciestoz
                                                         !should always give the 'true' z
                !print *,'speciestoZ(maxspecies)   =    ',speciestoZ(maxspecies)
            endif
        enddo
        targetfile_prev=trim(targetfiles(istruc))
        istrucInFile_prev=istrucInFile(istruc)
    enddo
    !print *,'end sub getmaxspecies'

end subroutine getmaxspecies





subroutine initializestruc(filenmr)

    !--------------------------------------------------------------c
    !
    !     Read in the set of vasprun.xml or .md files, process 
    !     them, and store geometrical info in the module 
    !     'm_geometry'. If filenmr=0 then all files are processed, 
    !     otherwise only vasprun.xml/.md file number 'filenmr' 
    !     is treated.
    !
    !     For filenmr=0, vasprun.xml/.md files are read three 
    !     times, whereas for filenmr>0 only two passes are used 
    !     (and for only a single vasprun.xml/.md file). The 
    !     first pass (only for filenmr=0) determines the total 
    !     number of species across all vasprun.xml/.md files, 
    !     maxspecies, and constructs the array z2species, for 
    !     which the Zth element contains the order in which the 
    !     atomic number Z first appeared in the vasprun.xml/
    !     .md files (and this order is henceforth refered to 
    !     as the 'species'). E.g., for a vasprun.xml file
    !     containing two species, C (Z=12) and Fe (Z=26). C is 
    !     first to appear and so we have z2species(12)=1, and 
    !     next is Fe, so that z2species(26)=2. In this way, the 
    !     z2species array maps the atomic number z to the species 
    !     number. In this example maxspecies=2.
    !
    !     The next two passes are used for both filenmr=0 and for
    !     filenmr>0, but in the latter only a single vasprun.xml/
    !     .md file is processed. In the second pass, we read in 
    !     the vasprun.xml/.md files again and extend the 
    !     lattice where neccesary (for cases where force 
    !     optimisation is specified) and determine the largest
    !     values of n_inequivalentsites, etc (so that we can
    !     define array sizes). Then we can read in for the third
    !     time and fill the gn_neighbors arrays, etc.
    !
    !     Notes for programmer:
    !     When 'filenmr' is non-zero, only the neighborhood of the 
    !     specified structure number is determined. However, the 
    !     arrays are still re-allocated and given sizes spanning 
    !     the full range of structure files, with array elements
    !     elements corresponding to files .ne. filenmr set to 
    !     zero. This turned out to be the tidiest way of
    !     implementing the 'filenmr' switch.
    !
    !     Written by Andrew Duff.
    !
    !     Called by:     program MEAMfit
    !     Calls:         readtargetstruc,extendlattice,neighborhood
    !     Returns:       maxnnatoms,maxspecies,z2species,
    !                 gn_inequivalentsites,gspecies,gn_neighbors,
    !                 gneighborlist,gxyz
    !     Files read:    targetfiles
    !     Files written: crystalstrucfile
    !
    !--------------------------------------------------------------c

    use m_generalinfo
    use m_filenames
    use m_poscar
    use m_neighborlist
    use m_geometry
    use m_atomproperties
    use m_datapoints
    use m_meamparameters
    use m_optimization
    use m_mpi_info

    implicit none
    logical file_exists
    integer istruc,j,ns,nshell, &
        iaux(1),filenmr,istrucInFile_prev,err,iproc,iat
    real(8) strWghtParCumal
    character*5 string5
    character*80 targetfile_prev,filename

    if (verbosity.gt.1) then
       print *,'Preparing to initialize structures...'
    endif

    if (verbosity.gt.1.and.(outputLAMMPSposns.or.excludeCoulomb)) then
       print *,'Printing LAMMPS structure files...'
    endif

    if (filenmr.eq.0) then

        !First pass:
        !print *,'first pass:'
        z2species=0
        maxspecies=0
        do istruc=1,nstruct
            !print *,'istruc=',istruc,'/',nstruct
            if ((istruc.gt.1).and.(trim(targetfiles(istruc)).eq.targetfile_prev).and. &
              (istrucInFile(istruc).eq.istrucInFile_prev+1)) then
               call readtargetstruc( trim(targetfiles(istruc)),istrucInFile(istruc),DFTdata(istruc),.true. )
            else
               call readtargetstruc( trim(targetfiles(istruc)),istrucInFile(istruc),DFTdata(istruc),.false. )
            endif
            !Find all atomic species
            do j=1,nspecies
                if(z2species(z(j)).eq.0) then
                    maxspecies=maxspecies+1
                    if (maxspecies.gt.10) then
                       print *,'ERROR: maxspecies>10. Please re-size speciestoZ'
                       print *,'array and then re-run.'
                       stop
                    endif
                    z2species(z(j))=maxspecies
                    speciestoZ(maxspecies)=mod(z(j),112) !z>112 are duplicate elements (eg 113=h), but speciestoz
                                                         !should always give the 'true' z
                endif
            enddo
            targetfile_prev=trim(targetfiles(istruc))
            istrucInFile_prev=istrucInFile(istruc)

            if (istruc.eq.1) then
                !Count up number of each species
                nAtomsPerSpecies=0
                do iat=1,natoms
                   do j=1,nspecies
                      if (zz(iat).eq.speciesToZ(j)) then
                         nAtomsPerSpecies(j)=nAtomsPerSpecies(j)+1
                      endif
                   enddo
                enddo
                !print *,'nAtomsPerSpecies=',nAtomsPerSpecies
                !stop
            endif

        enddo

        !Second pass:
        if (verbosity.gt.1) print *,'second pass:'
        maxsites=0
        maxneighbors=0
        maxnnatoms=0
        do istruc=1,nstruct
           !print *,istruc,'/',nstruct
           !print *,'targetfiles=',targetfiles(istruc),', prev=',targetfile_prev
           !print *,'istrucInFile(istruc)=',istrucInFile(istruc),' istrucInFile_prev+1=',istrucInFile_prev+1
            if ((istruc.gt.1).and.(trim(targetfiles(istruc)).eq.targetfile_prev).and. &
              (istrucInFile(istruc).eq.istrucInFile_prev+1)) then
               if (computeForces(istruc)) then
                  !call readtargetstruc( trim(targetfiles(istruc)),istrucInFile(istruc),DFTdata(istruc),.true. )
                  !TEMPORARILY COMMENTED Out BECAUSE NATOMS AND ZZ ARE CHANGED!!!
                  call readtargetstruc( trim(targetfiles(istruc)),istrucInFile(istruc),DFTdata(istruc),.false.)
               else
                   call readtargetstruc( trim(targetfiles(istruc)),istrucInFile(istruc),DFTdata(istruc),.true.)
               endif
            else
               call readtargetstruc( trim(targetfiles(istruc)),istrucInFile(istruc),DFTdata(istruc),.false. )
            endif
            n_inequivalentsites=natoms

            nshell=0
            if (computeForces(istruc)) then
                !Normally we extend the lattice if there are forces to fit, so we
                !can apply finite difference on energies. (Although this ought to be
                !removed now, since we have analytic forces, or?) However if we are
                !computing Coulomb forces to set up the coulombObservablesFull file,
                !then we do not want to do this
                inquire(file="coulombObservablesFull", EXIST=file_exists)
                if ( (excludeCoulomb.eqv..true.).and.(file_exists.eqv..false.) ) then
                    if (verbosity.gt.1) then
                       print *,'Positions being saved so that LAMMPS can be called to compute' 
                       print *,'Coulomb forces, therefore NOT extending lattice.'
                    endif
                    norigatoms=natoms
                else
                     !Need to make atoms in adjacent cells to the central cell
                     !inequivalent sites, so that when the force calculation
                     !takes place, these atoms are included in the energy
                     !calculation.
                     call extendlattice
                endif
            endif
            !Find all neighbors

            if (outputLAMMPSposns.or.excludeCoulomb) then
                !Plot output positions files for LAMMPS
                filename="positions.eam_"
                if (istruc.lt.10) then
                   write(string5,'(I1)') istruc
                elseif (istruc.lt.100) then
                   write(string5,'(I2)') istruc
                elseif (istruc.lt.1000) then
                   write(string5,'(I3)') istruc
                elseif (istruc.lt.10000) then
                   write(string5,'(I4)') istruc
                endif
                filename=trim(filename)//trim(string5)
                open(unit=50,file=trim(adjustl(filename)),status='unknown')
            endif
            call neighborhood(computeForces(istruc))
            if (outputLAMMPSposns.or.excludeCoulomb) then
                close(50)
                if (outputLAMMPSposns.and.(istruc.eq.nstruct)) then
                        print *,'Finished produced LAMMPS position files'
                        stop
                endif
            endif

            maxsites=max(maxsites,n_inequivalentsites)
            iaux=maxval(n_neighbors)
            maxneighbors=max(maxneighbors,iaux(1))
            !print *,'istruc=',istruc,' maxval(n_neighbors)=',maxval(n_neighbors),' current maxneighbors=',maxneighbors
            maxnnatoms=max(maxnnatoms,nnatoms)
            targetfile_prev=trim(targetfiles(istruc))
            istrucInFile_prev=istrucInFile(istruc)
        enddo

    elseif (filenmr.gt.0) then

        !Second pass:
        call readtargetstruc( trim(targetfiles(filenmr)),istrucInFile(filenmr),DFTdata(filenmr),.false. )
        n_inequivalentsites=natoms
        !         if (filenmr.eq.23) then
        !            print *,'natoms (for file nmr 23)=',natoms
        !         endif
        nshell=0
        if (computeForces(filenmr)) then
            !Need to make atoms in adjacent cells to the central cell
            !inequivalent sites, so that when the force calculation
            !takes place, these atoms are included in the energy
            !calculation.
            if ( (excludeCoulomb.eqv..true.).and.(file_exists.eqv..false.) ) then
               !no extend lattice
               norigatoms=natoms
            else
               call extendlattice
            endif
        endif
        !Find all neighbors
        call neighborhood(computeForces(filenmr))
        maxsites=n_inequivalentsites
        maxneighbors=maxval(n_neighbors)
        maxnnatoms=nnatoms
        !         if (filenmr.eq.23) then
        !            print *,'natoms still=',natoms
        !         endif

    endif

    !(Re)allocate relevant arrays (note: we allocate arrays
    !of size nstruct, even if filenmr>0, I.e. if we just need
    !the neighborhood of one structure. This is to avoid having to
    !rewrite other subroutines, which assume such a form for these
    !arrays).
    if (allocated(gn_inequivalentsites)) &
        deallocate(gn_inequivalentsites)
    if (allocated(gspecies)) deallocate(gspecies)
    if (allocated(gn_neighbors)) deallocate(gn_neighbors)
    if (allocated(gneighborlist)) deallocate(gneighborlist)
    if (allocated(gxyz)) deallocate(gxyz)
    if (allocated(gn_forces)) deallocate(gn_forces)
    if (allocated(g_vol)) deallocate(g_vol)
    if (allocated(optforce)) deallocate(optforce)
    if (allocated(diststr)) deallocate(diststr)
    if (allocated(dist2str)) deallocate(dist2str)
    if (allocated(dist3str)) deallocate(dist3str)
    if (allocated(dxstr)) deallocate(dxstr)
    if (allocated(dystr)) deallocate(dystr)
    if (allocated(dzstr)) deallocate(dzstr)
    if (allocated(strWghtPar)) deallocate(strWghtPar)
    allocate( gn_inequivalentsites(nstruct), &
        gspecies(maxnnatoms,nstruct), &
        optforce(maxsites,nstruct), &
        gn_neighbors(maxsites,nstruct), &
        gneighborlist(maxneighbors,maxsites,nstruct), &
        gxyz(3,maxnnatoms,nstruct), &
        gn_forces(nstruct), &
        g_vol(nstruct), &
        strWghtPar(nstruct) ) ! structure weight for parallel running
    gn_forces=0 !so we can do a MAXVAL on it later, and not get garbage for structures which aren't to be force optimized
    allocate( diststr(maxneighbors,maxsites,0:0,0:0,nstruct), &
        dist2str(maxneighbors,maxsites,0:0,0:0,nstruct), &
        dist3str(maxneighbors,maxsites,0:0,0:0,nstruct), &
        dxstr(maxneighbors,maxsites,0:0,0:0,nstruct), &
        dystr(maxneighbors,maxsites,0:0,0:0,nstruct), &
        dzstr(maxneighbors,maxsites,0:0,0:0,nstruct),stat=err)
    if (.not.allocated(gxyz_backup)) then
        allocate(gxyz_backup(3,maxnnatoms,nstruct))
        !This should just be allocated once and then left alone
    else
        print *,'GXYZ_BACKUP ALREADY ALLOCATED - NOT REALLOCATING'
    endif

    !Third pass:
    !print *,'third pass:'
    if (filenmr.eq.0) then

        strWghtParTot=0d0
        do istruc=1,nstruct
            !print *,'istruc=',istruc
            if ((istruc.gt.1).and.(trim(targetfiles(istruc)).eq.targetfile_prev).and. &
              (istrucInFile(istruc).eq.istrucInFile_prev+1)) then
               if (computeForces(istruc)) then
                  !call readtargetstruc( trim(targetfiles(istruc)),istrucInFile(istruc),DFTdata(istruc),.true. )
                  !TEMPORARILY COMMENTED Out BECAUSE NATOMS AND ZZ ARE CHANGED!!!
                  call readtargetstruc( trim(targetfiles(istruc)),istrucInFile(istruc),DFTdata(istruc),.false.)
               else
                  call readtargetstruc( trim(targetfiles(istruc)),istrucInFile(istruc),DFTdata(istruc),.true.)
               endif
            else
               call readtargetstruc( trim(targetfiles(istruc)),istrucInFile(istruc),DFTdata(istruc),.false. )
            endif
            !Record volume of cell
            g_vol(istruc)=vol
            !print *,'g_vol(',istruc,')=',g_vol(istruc)

            !  ! assuming cubic scaling with system size, here we define weights (and total weight) to be used in assigning structures to each node
            !  strWghtPar(istruc)=natoms !natoms**2
            !  strWghtParTot=strWghtParTot+strWghtPar(istruc)

            n_inequivalentsites=natoms
            nshell=0
            if (computeForces(istruc)) then
                !Need to make atoms in adjacent cells to the central cell
                !inequivalent sites, so that when the force calculation
                !takes place, these atoms are included in the energy
                !calculation.
                if ( (excludeCoulomb.eqv..true.).and.(file_exists.eqv..false.) ) then
                   !no extend lattice
                   norigatoms=natoms
                else
                   call extendlattice
                endif
            endif
            call neighborhood(computeForces(istruc))
            ns=n_inequivalentsites
            gn_inequivalentsites(istruc)=ns
            !            print *,'istr=',i,', gn_inequivalentsites=',
            !     +          gn_inequivalentsites(istruc)
            if (computeForces(istruc)) then
                gn_forces(istruc)=norigatoms
                if (gn_forces(istruc)==0) then
                   print *,'ERROR: gn_forces(',istruc,')=0. This should not happen and will cause a segmentation faults when we divide energies by numbers of atoms, STOPPING'
                   stop
                endif
            endif
            gn_neighbors(1:ns,istruc)=n_neighbors(1:ns)
            do j=1,ns
                gneighborlist(1:n_neighbors(j),j,istruc)= &
                    neighborlist(1:n_neighbors(j),j)
            enddo
            do j=1,nnatoms
                !print *,'j=',j,' species(j)=',species(j)
                gxyz(1:3,j,istruc)=xyz(1:3,j)!ERROR: here the second index is
                !the nearest neighbor number
                gspecies(j,istruc)=species(j)
            enddo
            !stop
            targetfile_prev=trim(targetfiles(istruc))
            istrucInFile_prev=istrucInFile(istruc)
        enddo

        !  ! using the weights per structure, determine which structures to be run
        !  ! by which process
        !  allocate(start_i_stored(0:nprocs-1),stop_i_stored(0:nprocs-1))
        !  strWghtParCumal=0d0
        !  iproc=0
        !  start_i_stored(0)=1
        !  do istruc=1,nstruct
        !     strWghtParCumal=strWghtParCumal+strWghtPar(istruc)
        !     print *,'istruc=',istruc,' strWghtParCumal=',strWghtParCumal,' strWghtPar(istruc)=',strWghtPar(istruc)
        !     print *,'strWghtParCumal/strWghtParTot=',strWghtParCumal/strWghtParTot,' real(iproc+1)/real(nprocs)=',real(iproc+1)/real(nprocs)
        !     if ((strWghtParCumal/strWghtParTot).ge.(real(iproc+1)/real(nprocs))) then
        !        stop_i_stored(iproc)=istruc
        !        print *,'for iproc=',iproc,': start_i_stored(',iproc,')=',start_i_stored(iproc),', stop_i_stored(',iproc,')=',stop_i_stored(iproc)
        !        iproc=iproc+1
        !        if (iproc.eq.nprocs) then
        !           exit
        !        endif
        !        start_i_stored(iproc)=istruc+1
        !     endif
        !  enddo
        !  stop_i_stored(nprocs)=nstruct
        !  !print *,'stopping'
        !  !stop

    else
        call readtargetstruc( trim(targetfiles(filenmr)),istrucInFile(filenmr),DFTdata(filenmr),.false. )
        n_inequivalentsites=natoms
        !         if (filenmr.eq.23) then
        !            print *,'n_inequiv... still=',n_inequivalentsites
        !         endif

        nshell=0
        if (computeForces(filenmr)) then
            !Need to make atoms in adjacent cells to the central cell
            !inequivalent sites, so that when the force calculation takes place,
            !these atoms are included in the energy calculation.
            if ( (excludeCoulomb.eqv..true.).and.(file_exists.eqv..false.) ) then
               !no extend lattice
               norigatoms=natoms
            else
               call extendlattice
            endif
        endif
        
        call neighborhood(computeForces(filenmr))
        ns=n_inequivalentsites
        gn_inequivalentsites(filenmr)=ns
        !         if (filenmr.eq.23) then
        !            print *,'gn_inequiv(23)=',gn_inequivalentsites(23),ns
        !         endif
        if (computeForces(filenmr)) then
            gn_forces(filenmr)=norigatoms
        endif
        gn_neighbors(1:ns,filenmr)=n_neighbors(1:ns)
        do j=1,ns
            gneighborlist(1:n_neighbors(j),j,filenmr)= &
                neighborlist(1:n_neighbors(j),j)
        enddo
        do j=1,nnatoms
            gxyz(1:3,j,filenmr)=xyz(1:3,j)!ERROR: here the second index
            !is the nearest neighbor number
            gspecies(j,filenmr)=species(j)
        enddo
    endif
    if (verbosity.gt.1) print *,'stopping afer third pass'

    close(1)
    if(allocated(z)) deallocate(z,coordinates)
    if(allocated(xyz)) deallocate(xyz)
    if(allocated(species)) deallocate(species)
    if(allocated(n_neighbors)) deallocate(n_neighbors)
    if(allocated(neighborlist)) deallocate(neighborlist)

    if (verbosity.gt.1) print *,'Completed structure initialization'

end subroutine initializestruc
