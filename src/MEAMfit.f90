!
! MEAMfit v2
! ----------
!
! Software for optimizing ‘reference-free modified embedded atom method’
! (RF-MEAM) force-fields for material science applications. Reads
! directly with the output of the density functional theory VASP package
! or CASTEP and produces force-fields that can be directly exploited in the 
! LAMMPS software.
!
! Authors: Andrew I. Duff, Marcel H. F. Sluiter.
! Contact: andrew.duff@stfc.ac.uk
!
! Copyright 2018 Science and Technology Facilities Council (UK)
!
! MEAMfit2 is open-source, licensed under BSD 3 clause license. See LICENSE.txt file for full terms of license.
!
! Citation
! --------
!
! When using MEAMfit v2 in your publications please cite:
!
! “MEAMfit: A reference-free modified embedded atom method (RF-MEAM)
! energy and force-fitting code”, A. I. Duff, et al, Comp. Phys. Comm.
! 196 439-445 (2015)
!


program MEAMfit

   use m_optimization
   use m_filenames
   use m_geometry
   use m_datapoints
   use m_generalinfo
   use m_meamparameters
   use m_mpi_info

   implicit none

   logical maxOK,exist
   integer ip,j,k,iseed,nattempts,nfreep
   real(8) tmp

   ! Initializes MPI
   call p_setup

 !  !               !REMOVE WHEN FINISHED WITH SUB TEST
 !  !---- initiailize VASP variables and use to fill m_poscar ----
 !  integer NIONS, & !Number of atoms
 !          NTYP     !Number of species
 !  integer, allocatable :: NITYP(:), & !Number of atoms of each species
 !                          ZVAL(:)     !Atomic number of each species
 !  real(8) energyMEAM
 !  real(8) A(3,3)
 !  real(8), allocatable :: POSION(:,:),forcesMEAM(:,:)

 !  NIONS=64
 !  NTYP=2
 !  allocate (NITYP(NTYP),ZVAL(NTYP))
 !  NITYP(1)=32
 !  NITYP(2)=32
 !  ZVAL(1)=40
 !  ZVAL(2)=6
 !  A(1:3,1)=(/ 9.7d0 , 0d0 , 0d0 /)
 !  A(1:3,2)=(/ 0d0 , 9.7d0 , 0d0 /)
 !  A(1:3,3)=(/ 0d0 , 0d0 , 9.7d0 /)
 !  allocate (POSION(1:3,NIONS),forcesMEAM(1:3,NIONS))
 !  POSION(1:3,1)= (/ 0.9914058086d0 ,  0.9927124569d0 , 0.0200012364d0 /)
 !  POSION(1:3,2)= (/ 0.4851006396d0 ,  0.0119480359d0 , 0.0458121566d0 /)
 !  POSION(1:3,3)= (/ 0.0024075572d0 ,  0.5276553250d0 , 0.9977621441d0 /)
 !  POSION(1:3,4)= (/ 0.5025444401d0 ,  0.4785721563d0 , 0.0314887940d0 /)
 !  POSION(1:3,5)= (/ 0.9890654853d0 ,  0.9803129828d0 , 0.4927542367d0 /)
 !  POSION(1:3,6)= (/ 0.4806264837d0 ,  0.0005011282d0 , 0.4905084708d0 /)
 !  POSION(1:3,7)= (/ 0.0203618625d0 ,  0.4677552157d0 , 0.4866564736d0 /)
 !  POSION(1:3,8)= (/ 0.4875929003d0 ,  0.5128524786d0 , 0.4887314329d0 /)
 !  POSION(1:3,9)= (/ 0.2233204513d0 ,  0.0056408991d0 , 0.2747639027d0 /)
 !  POSION(1:3,10)= (/ 0.7342052552d0,  0.9750745551d0 , 0.2311941734d0 /)
 !  POSION(1:3,11)= (/ 0.2391246794d0,  0.5124956501d0 , 0.2286471284d0 /)
 !  POSION(1:3,12)= (/ 0.7681643412d0,  0.5221628321d0 , 0.2806304354d0 /)
 !  POSION(1:3,13)= (/ 0.2674174017d0,  0.9922928288d0 , 0.7532630915d0 /)
 !  POSION(1:3,14)= (/ 0.7576955117d0,  0.0141970015d0 , 0.7417460317d0 /)
 !  POSION(1:3,15)= (/ 0.2858126570d0,  0.5016169685d0 , 0.7933225479d0 /)
 !  POSION(1:3,16)= (/ 0.7511153995d0,  0.5140877326d0 , 0.7116205653d0 /)
 !  POSION(1:3,17)= (/ 0.2253338491d0,  0.2681416118d0 , 0.9969455688d0 /)
 !  POSION(1:3,18)= (/ 0.7482662609d0,  0.2528374942d0 , 0.9983919528d0 /)
 !  POSION(1:3,19)= (/ 0.2459901698d0,  0.7633213409d0 , 0.9781536295d0 /)
 !  POSION(1:3,20)= (/ 0.7378866316d0,  0.7235561149d0 , 0.9865290136d0 /)
 !  POSION(1:3,21)= (/ 0.2571093889d0,  0.2520087296d0 , 0.4968551168d0 /)
 !  POSION(1:3,22)= (/ 0.7572371002d0,  0.2411457373d0 , 0.4917188618d0 /)
 !  POSION(1:3,23)= (/ 0.2363229515d0,  0.7335037884d0 , 0.5224186477d0 /)
 !  POSION(1:3,24)= (/ 0.7648659628d0,  0.7756512050d0 , 0.4986902731d0 /)
 !  POSION(1:3,25)= (/ 0.9965924277d0,  0.2467025598d0 , 0.2296577593d0 /)
 !  POSION(1:3,26)= (/ 0.4729388230d0,  0.2428337218d0 , 0.2662285652d0 /)
 !  POSION(1:3,27)= (/ 0.9991341576d0,  0.7591013892d0 , 0.2456571164d0 /)
 !  POSION(1:3,28)= (/ 0.4881253783d0,  0.7521381518d0 , 0.2377774419d0 /)
 !  POSION(1:3,29)= (/ 0.0139816964d0,  0.2665018405d0 , 0.7584828345d0 /)
 !  POSION(1:3,30)= (/ 0.5426041097d0,  0.2424165668d0 , 0.7262343514d0 /)
 !  POSION(1:3,31)= (/ 0.0089746481d0,  0.7409726744d0 , 0.7641758060d0 /)
 !  POSION(1:3,32)= (/ 0.5133923801d0,  0.7506424029d0 , 0.7248177611d0 /)
 !  POSION(1:3,33)= (/ 0.2416735586d0,  0.0338014718d0 , 0.9973187462d0 /)
 !  POSION(1:3,34)= (/ 0.7322721582d0,  0.0075072578d0 , 0.9714572935d0 /)
 !  POSION(1:3,35)= (/ 0.2363432441d0,  0.5104394604d0 , 0.9938401201d0 /)
 !  POSION(1:3,36)= (/ 0.7133602467d0,  0.5046027339d0 , 0.9736621228d0 /)
 !  POSION(1:3,37)= (/ 0.2639205926d0,  0.9746877434d0 , 0.5152050576d0 /)
 !  POSION(1:3,38)= (/ 0.7500926313d0,  0.9902267270d0 , 0.4874349981d0 /)
 !  POSION(1:3,39)= (/ 0.2720736217d0,  0.4752335259d0 , 0.5122759080d0 /)
 !  POSION(1:3,40)= (/ 0.7909116020d0,  0.5016408370d0 , 0.4956977254d0 /)
 !  POSION(1:3,41)= (/ 0.9925262256d0,  0.9811127357d0 , 0.2623146808d0 /)
 !  POSION(1:3,42)= (/ 0.4817206702d0,  0.9475057308d0 , 0.2705874680d0 /)
 !  POSION(1:3,43)= (/ 0.0125317358d0,  0.4895175697d0 , 0.2304526551d0 /)
 !  POSION(1:3,44)= (/ 0.4936009657d0,  0.4699950923d0 , 0.2494810219d0 /)
 !  POSION(1:3,45)= (/ 0.9583453106d0,  0.9710597055d0 , 0.7568043077d0 /)
 !  POSION(1:3,46)= (/ 0.5136067544d0,  0.9875247196d0 , 0.7608244172d0 /)
 !  POSION(1:3,47)= (/ 0.0121122691d0,  0.4922957801d0 , 0.7396452177d0 /)
 !  POSION(1:3,48)= (/ 0.5313273047d0,  0.4831546428d0 , 0.7200211499d0 /)
 !  POSION(1:3,49)= (/ 0.0011538798d0,  0.2188114854d0 , 0.9960235529d0 /)
 !  POSION(1:3,50)= (/ 0.5274148202d0,  0.2720163224d0 , 0.0453407265d0 /)
 !  POSION(1:3,51)= (/ 0.0241580997d0,  0.7596635750d0 , 0.9782630914d0 /)
 !  POSION(1:3,52)= (/ 0.5127230799d0,  0.7489860967d0 , 0.0153119202d0 /)
 !  POSION(1:3,53)= (/ 0.0110223605d0,  0.2375068546d0 , 0.4790676744d0 /)
 !  POSION(1:3,54)= (/ 0.4862925383d0,  0.2499572644d0 , 0.4848988759d0 /)
 !  POSION(1:3,55)= (/ 0.0172716858d0,  0.7385553258d0 , 0.5150065256d0 /)
 !  POSION(1:3,56)= (/ 0.4869642347d0,  0.7637838912d0 , 0.4981230478d0 /)
 !  POSION(1:3,57)= (/ 0.2101792301d0,  0.2579031241d0 , 0.2717514243d0 /)
 !  POSION(1:3,58)= (/ 0.7700932359d0,  0.2331746416d0 , 0.2781328482d0 /)
 !  POSION(1:3,59)= (/ 0.2205976317d0,  0.7661477625d0 , 0.2713188890d0 /)
 !  POSION(1:3,60)= (/ 0.7430277257d0,  0.7410706843d0 , 0.2709333394d0 /)
 !  POSION(1:3,61)= (/ 0.2668766859d0,  0.2530828713d0 , 0.7929159459d0 /)
 !  POSION(1:3,62)= (/ 0.7608333891d0,  0.2865453346d0 , 0.7594620122d0 /)
 !  POSION(1:3,63)= (/ 0.2833717597d0,  0.7540054059d0 , 0.7359041089d0 /)
 !  POSION(1:3,64)= (/ 0.7217267809d0,  0.7363024028d0 , 0.7340364686d0 /)
 !
 !  call getMEAM_EnergyForce(NIONS,NTYP,NITYP,ZVAL,POSION,A,energyMEAM,forcesMEAM)
 !  stop
 !  !

   call getTime(tStart)
   call testargs !Check for command-line arguments

   !Check if settings file exists (other filenames are defined and their existence checked later)
   settingsfile='settings'
   inquire(file=trim(settingsfile),exist=exist)
   if (exist.eqv..false.) then
      settingsfileexist=.false.
   else
      settingsfileexist=.true.
   endif
   call readVerbosityInSettings !Pre-check the settings file for verbosity

   ! MPI: make sure only the main process prints this
   if (procid.eq.0) then
     if (verbosity.gt.1) then
       print *,'  __  __ _____    _    __  __  __ _ _   ____   '
       print *,' |  \/  | ____|  / \  |  \/  |/ _(_) |_|___ \  '
       print *,' | |\/| |  _|   / _ \ | |\/| | |_| | __| __) | '
       print *,' | |  | | |___ / ___ \| |  | |  _| | |_ / __/  '
       print *,' |_|  |_|_____/_/   \_\_|  |_|_| |_|\__|_____| '
       print *,'                                               '
       print *,'         ------- Version 2.00 -------          '
       print *
       print *,'             andrew.duff@stfc.ac.uk            '
       print *,'              Copyright 2018 STFC              '
       print *,'                                               '
       print *,'                   Authors:                    '
       print *,'                Andrew I. Duff                 '
       print *,'             Marcel H. F. Sluiter              '
       print *,'                                               '
       print *,'            Contributing authors:              '
       print *,'      Prashanth Srinivasan, Thomas Mellan,     '
       print *,'         Blazej Grabowski, Timo Verlaan,       '
       print *,'   Matthijs van Eijsden, Thijs van der Hoeven  '
       print *,'                                               '
       print *,'     Please cite the following article if      '
       print *,'    you use this code in your publication:     '
       print *
       print *,'      "MEAMfit: A reference-free modified      '
       print *,'     embedded atom method (RF-MEAM) energy     '
       print *,'            and force-fitting code"            '
       print *,'     A. I. Duff, et al, Comp. Phys. Comm.      '
       print *,'              196 439-445 (2015)               '
       print *
     end if
   endif

   createdFitdbse=.false.
   createdSettings=.false.
   call setupfilenames  !Define all filenames
   if (createdFitdbse.eqv..true.) then
      ! This has been added for the python implementation. The 'stop' in setupfilenames doesn't allow the subroutine to end
      ! normally, so this is a workaround. Similarly further down
      stop
   endif
   call checkInputFilesExist !Check if 'settings' file and, if applicable, potential files exist
   call getmaxspecies   !Determine 'maxspecies' from the vasprun files (the total number of species
                 !to be used). This needs doing in advance of readsettings for setting up arrays.
   if (readpotfile.eqv..true.) then
       call extractlmax
   endif
   if (settingsfileexist.eqv..false.) then
      call setupdefaultlmax
   endif
   if (settingsfileexist.eqv..true..and.procid.eq.0) then
     if (verbosity.gt.1) then
        print *
        print *,'General initialization'
        print *,'----------------------'
     end if
   endif

   call readsettings(iseed) !read settings from settings file
   if (settingsfileexist.eqv..true.) then
      call setuprandomseed(iseed)
   endif
   if (createdSettings.eqv..true.) then
      stop
   endif
   !---- Initial meam-parameter set-up ----
   call initializemeam       !Initialize initial meam parameters

   if (nLammpsFiles.gt.0) then
      !Read in LAMMPS potentials and set-up splines
      call initializeLammpsPot
   endif

   if ((readParasOnebyone.eqv..false.).and.(readpotfile).and.(noOpt.eqv..false.)) then
      !If a potential file is supplied by user, amend freep so that non-zero parameters
      !in this file are optimized (unless PARASTOOPT is specified in the settings file)
      call amendFreep
   endif

   call getRmax !Find the maximum interatomic separation that needs to be considered.

 ! !Prepare splines
 ! narrpairpotXX=1000!1000
 ! narrthiXX=1000
 ! allocate(r_pairpotXXarr(narrpairpotXX), &
 !     pairpotXXarr(narrpairpotXX), &
 !     secderpairpotXX(narrpairpotXX), &
 !     r_thiXXarr(narrthiXX), &
 !     thiXXarr(narrthiXX), &
 !     secderthiXX(narrthiXX))
 ! do i=1,narrpairpotXX
 !     r_pairpotXXarr(i) = &
 !         ( (dble(i)/dble(narrpairpotXX)) )**2 * &
 !         p_rmax
 ! enddo
 ! do i=1,narrthiXX
 !     r_thiXXarr(i) = &
 !         ( (dble(i)/dble(narrthiXX)) )**2 * &
 !         p_rmax
 ! enddo
 ! !--------------------------------------

   if (optimizestruc) then
       !Optimize positons of ions, keeping the MEAM parameters fixed.
       !Note: currently unsupported - to be fully instated later
       call optimizestructure(.false.) !Index: optimize all structures?
       stop
   endif

   !---- Set-up structures and fitting database ----
!  if (analyzeVib.eqv..false.) then
!     !We do not need atom positions (or calculated properties using the
!     !potential) if we are in analyzeVib mode.
      if ((procid.eq.0).and.(verbosity.gt.0)) then
        print *,'calling initializestruc'
      endif
      call initializestruc(0) !Read in atomic configurations from vasprun files.
      !Note that, depending on flags, LAMMPS input structure files may also be
      !generated here.
      
      call backupxyz !Store original positions so we can reset them after each optimization
      !cycle.
!   endif

   if (nstruct.lt.nprocs) then
       print*,'Error: more processes than nstructs, program is terminating.'
       stop
   endif

   if ((procid.eq.0).and.(verbosity.gt.0)) then
     print *,'reading target data...'
   endif
   createdCoulomb=.false.
   call readtargetdata  !Read energies, forces and stress tensor components from vasprun files
   if (createdCoulomb.eqv..true.) then
      stop
   endif
   if (analyzeVib.eqv..true.) then
      !If requested, analyze DFT input MD data and predict an appropriate
      !timestep for subsequent DFT calculations
      call analyzeVibrations
      print *
      print *,'Stopping.'
      stop
   endif
   if (verbosity.gt.2) print *,'calculating standard deviations of observables...'
   call calcSds       !Determine s.d's of energies and forces to rescale optfunc
   if (verbosity.gt.2) print *,'setting up nn tables...'
   call setupnntables !Set up atom separation tables, also for forces (must
                      !come after 'readtargetdata' as this determines which
                      !forces are to be optimized)
   if (verbosity.gt.2) print *,'finding smallest separation...'
   call findsmallestsepn !Determine smallest and largest interatomic separations
   if ((procid.eq.0).and.(nsteps.eq.1).and.(readpotfile.eqv..false.).and.(nLammpsFiles.eq.0).and.(analyzeVib.eqv..false.)) then
      print *,'No optimization specified, and no potential filenames supplied:'
      print *,'Producing sepnHistrogram file and STOPPING.'
      stop
   endif
   !------------------------------------------------

   if ((settingsfileexist.eqv..false.).or. &
       (writeExPot.eqv..true.)) then
      !Create template file for 'potparas_best' if requested
      call createMeamParasTemplate
   endif
   !Check which parameters are to be optimized, and initiate nopt variables
   !accordingly
   !print *,'check which parameters are to be optimized'
   call signRadFuncs

   !NOTE: FROM here on, the worker process and the main process are no longer executing the same code!!!
   !Here the workerthreads will split off from the main thread
   !The worker threads will go into an endeless do loop calling the objective function
   !In the objective function they will only execute the main optimation loop if they get assigned by the main thread

   if (.not.procid.eq.0) then
       ! MPI: The Workerinit subroutine allocates the necessary arrays for the workers to get started in the objectivefunction
       call p_workerThread
   endif

   ! MPI: The main thread (proqid=0) continues sequentually

   if (randgenparas) then
      !Parameters to be randomly initialized: Read in bounds for initialization
      if (readparasfromsettings) then
         call readBoundsForRandParas
         print *,"Read in limits on random parameters from settings file"
      else
         call defaultBoundsForRandParas
         print *,"Limits on random parameters set to default values"
      endif
      print *
      print *,"New potential parameters to be generated:"
   endif
   if (verbosity.gt.0) call displayBoundsForVarParas

   if (printoptparas.eqv..true.) then
      !If requested, write out sample PARASTOOPT and stop
      call writeoptparasSub
      print *
      print *,'Stopping.'
      stop
   endif

   call checkPotType
   if (nsteps.gt.1) then
      call checkParas !Check bounds for random parameter initialization as well
                      !as the parameters themselves if supplied by user
      !Determine the number of parameters to be optimized
      nfreep=0
      do ip=1,np
         if ((freep(ip).eq.1).or.(freep(ip).eq.2)) then
            nfreep=nfreep+1
         endif
      enddo
   endif

   if (contjob.eqv..false.) then
      n_optfunc=1
      allocate(bestoptfuncs(noptfuncstore),timeoptfuncHr(noptfuncstore), &
          timeoptfuncMin(noptfuncstore))
   else
      n_optfunc=noptfuncstore+1
   endif

   call allocateArrays

   if (nsteps.eq.1) then

      if (verbosity.gt.0) then
         print *
         print *,'Calculation of properties (no optimization)'
         print *,'-------------------------------------------'
         print *
      endif
      call initialize_noptp
      if (verbosity.gt.0) print *,'final out:'
      call finalOutput
      stop

   else

      !Display fitting data
      print *
      print *,'Fitting summary'
      print *,'---------------'
      print *
      write(*,'(A12,I5,A58)') ' Fitting to ',ndatapointsTrue,' data-points (energies and force/stress tensor components)'
      write(*,'(A7,I5,A12)') ' using ',nfreep,' parameters.'
      fitPntsPerPara=dble(ndatapointsTrue)/dble(nfreep)
      if (fitPntsPerPara.gt.minFitPntsPerPara) then
         write(*,'(F10.5,A28,I5,A1)') fitPntsPerPara,' fit points per parameter (>',minFitPntsPerPara,')'
      else
         write(*,'(F10.5,A28,I5,A1)') fitPntsPerPara,' fit points per parameter (<',minFitPntsPerPara,')'
         write(*,'(A20,I5,A65)') ' WARNING: Less than ',minFitPntsPerPara,' fitting datapoints per potential parameter - likely to overfit!'
      endif
      print *
      print *,'Beginning optimization'
      print *,'----------------------'
      print *
      deltaOlimit=1d0 !Criterion for stopping local optimization
      call optimizeParameters(nfreep) !Main call: Optimize the parameters
      print *
      if (optComplete) then
         print *
         print *,'Optimization completed'
         print *,'----------------------'
         print *
         call finalOutput
      else
         print *
         print *,'Optimization failed'
         print *,'-------------------'
         print *
      endif

   endif

   call p_finalize

end program MEAMfit
