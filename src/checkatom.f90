subroutine checkatom(xyztry,inlist,atomnumber)

    !--------------------------------------------------------------c
    !
    !     Takes xyztry and finds out if it is equal to xyz(1:3,j) 
    !     of any of the inequivalent atoms. If so, inlist is set to 
    !     .true. and the atomnumber of the atom is returned
    !
    !     Called by:     neighborhood
    !     Calls:         -
    !     Returns:       inlist, atomnumber
    !     Files read:    -
    !     Files written: -
    !
    !     Andrew Duff, 2010
    !
    !     Copyright (c) 2018, STFC
    !
    !--------------------------------------------------------------c

    use m_poscar

    implicit none

    integer atomnumber,i
    real(8) xyztry(3)
    logical inlist

    inlist=.false.
    do i=1,natoms
        !Check components of xyztry against those of inequivalent atoms
        if ( (coordinates(1,i).eq.xyztry(1)).and. &
            (coordinates(2,i).eq.xyztry(2)).and. &
            (coordinates(3,i).eq.xyztry(3)) ) then
            atomnumber=i
            inlist=.true.
            exit
        endif
    enddo

end subroutine checkatom
