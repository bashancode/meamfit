subroutine calculateshells(nshell,nnatoms_est,rmax)

    !----------------------------------------------------------------------c
    !
    !     Calculates how many shells, 'nshell', of unit cells are required
    !     in order to include all atoms which are within a distance rmax
    !     from at least one of the atoms in the
    !     central unit cell. The (exact) number of such atoms
    !     atoms is calculated as 'nnatoms_est'.
    !     The shells are defined so that, for example, the first shell of
    !     unit cells forms a cubic shell in direct coordinate space about
    !     the central unit cell. I.e., the unit cells included in the
    !     first shell are those at (in direct coordinates): (-1,-1,-1),
    !     (0,-1,-1), (1,-1,-1), (-1,0,-1), (0,0,-1), (1,0,-1), (-1,1,-1),
    !     (0,1,-1), (1,1,-1) <- bottom face; (-1,-1,0), (0,-1,0),
    !     (1,-1,0), (-1,0,0), (1,0,0), (-1,1,0), (0,1,0), (1,1,0) -< mid-
    !     section; (-1,-1,1), (0,-1,1), (1,-1,1), (-1,0,1), (0,0,1),
    !     (1,0,1), (-1,1,1) -<top face.
    !
    !     Called by:     neighborhood
    !     Calls:         map_icoords_to_its
    !     Returns:       nshell,nnatoms_est
    !     Files read:    -
    !     Files written: -
    !
    !     Andy Duff, Dec 2007
    !
    !     Copyright (c) 2018, STFC
    !
    !----------------------------------------------------------------------c

    use m_generalinfo
    use m_poscar
    use m_neighborlist

    implicit none
    logical no_nn_found,nn_found
    logical, allocatable:: atom_tested(:,:,:,:) !Use this array to
    !record which atoms have been tested to see if they
    !are nn's. This is so that we don't triple count
    !the number of nn's in the cells of adjoining
    !faces. The first 3 indicies are for the unit cell,
    !the fourth is for the atom in the unit cell.
    integer j,k,it1,it2,it3,iface,icoord1,icoord2,nshell,nnatoms_est
    integer, parameter:: unitcell_dim=10 !The maximum number of unit
    !cells expected out from the origin along any
    !lattice direction.
    real(8) rmax2,xyztry(3),dxyz(3),dd,rmax

    allocate(atom_tested(-unitcell_dim:unitcell_dim, &
        -unitcell_dim:unitcell_dim,-unitcell_dim:unitcell_dim, &
        natoms))

    atom_tested=.false.
    rmax2=rmax**2
    nshell=1
    nnatoms_est=n_inequivalentsites
    !   do j=1,natoms
    !       print *,'coords(1:3,',j,')=',coordinates(1:3,j)
    !   enddo
    !   print *,'tr(1:3,1  2   3)=',tr(1:3,1),tr(1:3,2),tr(1:3,3)
    !   print *,'rmax2=',rmax2
    !   stop
    do                        !Loop over 'shells' of unit cells around
        no_nn_found=.true.     !the central unit cell
        do iface=1,6           !Loop over the faces of the cube (a cube
            !in direct coordinate space)

            do icoord1=-nshell,nshell !Loop over the points on the
                do icoord2=-nshell,nshell !cube face

                    call map_icoords_to_its(it1,it2,it3,iface,nshell, &
                        icoord1,icoord2) !Convert icoord values into
                    !it values.
                    ! print *,'it#=',it1,it2,it3
                    do j=1,natoms
                          !print *,'j=',j,'/',natoms
                        xyztry=coordinates(1:3,j)+tr(1:3,1)*dble(it1)+ &
                            tr(1:3,2)*dble(it2)+tr(1:3,3)*dble(it3)
                        !if ((it1.eq.0).and.(it2.eq.1).and.(it3.eq.0).and.(j.eq.2)) then
                        !   print *,'coordinates(1:3,',j,')=',coordinates(1:3,j)
                        !   print *,'xyztry=',xyztry
                        !endif

                        !Check to see if this coordinate is a nearest
                        !neighbor of any of the atoms in the central unit
                        !cell
                        nn_found=.false.
                        do k=1,n_inequivalentsites
                            dxyz=xyztry-coordinates(1:3,k)
                            dd=dxyz(1)**2+dxyz(2)**2+dxyz(3)**2
                            !if (k.eq.1) then
                            !   print *,'coordinates(1:3,',k,')=',coordinates(1:3,k),' dxyz=',dxyz,' dd=',dd,' rmax2=',rmax2
                            !endif
                            !stop
                            if (dd.le.rmax2) then
                               !   if (nshell.eq.20) then
                               !           print *,'xyztry=',xyztry,' coordinates=',coordinates(1:3,k)
                               !           print *,'   (it#=',it1,it2,it3,', tr(1:3,1)=',tr(1:3,1),', tr(1:3,2)=',tr(1:3,2),', tr(1:3,3)=',tr(1:3,3),')'
                               !   endif

                                nn_found=.true.
                                no_nn_found=.false.
                                exit
                            endif

                        enddo

                        !The same unit cells are included more than once
                        !on the separate faces. Take this into account in
                        !the estimate for the number of nn's:
                        if ((nn_found.eqv..true.).and. &
                            (atom_tested(it1,it2,it3,j).eqv..false.)) &
                            then
                            nnatoms_est=nnatoms_est+1
                        endif
                        atom_tested(it1,it2,it3,j)=.true.

                    enddo
                    !
                enddo
            enddo
        enddo

        if (no_nn_found) then  !No nn's found the present shell,
            exit                !Therefore we have located all nn's of
        endif                  !the central unit cell
        nshell=nshell+1
    enddo

    nshell=nshell-1
    deallocate(atom_tested)

end subroutine calculateshells
