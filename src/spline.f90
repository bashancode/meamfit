subroutine fitSpline(dx,Y,n,n_arr,y2)

    !-----------------------------------------------------------------
    !     Adapted by Andrew I Duff from Numerical Recipes
    !     DESCRIPTION: fits a cubic spline to a supplied function
    !                  y(x).
    !     INPUT: A function y(x) stored in the array
    !            Y(1:n_arr) defined at x=0, dx, 2*dx, ..., (n-1)*dx
    !            (I.e., not all parts of array are used)
    !            Points from 1 to
    !            N are included. d2Y/dx2 set to zero at x=0 and
    !            x=(n-1)*dx
    !     OUTPUT: Subroutine calculates d^2Y/dx^2 at all x points
    !             and returns it in the array Y2(1:N)
    !     INSTRUCTIONS: run this subroutine once to calculate Y2,
    !                   then 'splint' can be run to calculate the
    !                   value of the interpolated Y(x) at any x. Or
    !                   'spline_integrate' can be run to integrate
    !                   the spline.
    !                   to fit a natural spline (d^2Y/dx^2=0 at
    !                   x=0 ), set YP1 or YPN to > 10^30
    !-----------------------------------------------------------------

    implicit none
    integer i,n,nmax,n_arr
    real *8 YP1,YPN,dx,Y(n_arr),y2(n_arr)
    parameter (nmax=120000)!3000
    INTEGER k
    REAL *8 p,qn,sig,un,u(nmax)
    !      print *,'n=',n,', nmax=',nmax
    if (n.gt.nmax) then
        print *,'nmax in spline needs increasing. Stopping.'
        stop
    endif
    y2(1)=0.d0
    u(1)=0.d0
    do i=2,n-1             !This is the decomposition loop of the
        sig=0.5d0 !(X(i)-X(i-1))/(X(i+1)-X(i-1)) !tridiagonal algorithm. y2
        p=sig*y2(i-1)+2.d0  !and u are used fo temporary storage of
        y2(i)=(sig-1.d0)/p  !the decomposed factors.
        u(i)=(6.d0*((Y(i+1)-Y(i))/dx-(Y(i)-Y(i-1)) &
            /dx)/(2*dx)-sig*u(i-1))/p
    enddo
    qn=0.d0
    un=0.d0
    y2(n)=(un-qn*u(n-1))/(qn*y2(n-1)+1.d0)
    do k=n-1,1,-1          !This is the backsubstitution loop of the
        y2(k)=y2(k)*y2(k+1)+u(k) !tridiagonal algorithm.
    enddo

end subroutine fitSpline




subroutine spline(X,Y,n,YP1,YPN,y2)


    !-----------------------------------------------------------------
    !     From Numerical Recipes
    !     DESCRIPTION: fits a cubic spline to a supplied function
    !                  y(X).
    !     INPUT: A function y(X) stored in the array
    !            y(1:N_ARR) defined at points X(1:n).
    !            Points from 1 to
    !            N are included. dY/dX must also be specified at
    !            X(1) and X(N) and are passed to the subroutine as
    !            YP1 and YPN.
    !     OUTPUT: Subroutine calculates d^2Y/dx^2 at all x points
    !             and returns it in the array Y2(1:N)
    !     INSTRUCTIONS: run this subroutine once to calculate Y2,
    !                   then 'splint' can be run to calculate the
    !                   value of the interpolated Y(X) at any X. Or
    !                   'spline_integrate' can be run to integrate
    !                   the spline.
    !                   to fit a natural spline (d^2Y/dx^2=0 at
    !                   X(1) ), set YP1 or YPN to > 10^30
    !-----------------------------------------------------------------

    implicit none
    integer i,n,nmax
    real *8 YP1,YPN,X(n),Y(n),y2(n)
    parameter (nmax=120000)!3000
    INTEGER k
    REAL *8 p,qn,sig,un,u(nmax)
    !      print *,'n=',n,', nmax=',nmax
    if (n.gt.nmax) then
        print *,'nmax in spline needs increasing. Stopping.'
        stop
    endif
    !      if (YP1.gt..99e30) then    !The lower boundary condition is set
    !                                 !either to be
    y2(1)=0.d0
    u(1)=0.d0
    !      else                      !or else to have a specified first
    !         y2(1)=-0.5d0           !derivative.
    !         u(1)=(3.d0/(X(2)-X(1)))*((Y(2)-Y(1))/(X(2)-X(1))-YP1)
    !      endif
    do i=2,n-1             !This is the decomposition loop of the
        sig=(X(i)-X(i-1))/(X(i+1)-X(i-1)) !tridiagonal algorithm. y2
        p=sig*y2(i-1)+2.d0  !and u are used fo temporary storage of
        y2(i)=(sig-1.d0)/p  !the decomposed factors.
        u(i)=(6.d0*((Y(i+1)-Y(i))/(X(i+1)-X(i))-(Y(i)-Y(i-1)) &
            /(X(i)-X(i-1)))/(X(i+1)-X(i-1))-sig*u(i-1))/p
    enddo
    !      if (YPN.gt..99e30) then    !The upper boundary condition is set
    qn=0.d0                  !either to be
    un=0.d0
    !      else                      !or else to have a specified first
    !         qn=0.5d0               !derivative.
    !         un=(3.d0/(X(n)-X(n-1)))*(YPN-(Y(n)-Y(n-1))/
    !     +        (X(n)-X(n-1)))
    !      endif
    y2(n)=(un-qn*u(n-1))/(qn*y2(n-1)+1.d0)
    do k=n-1,1,-1          !This is the backsubstitution loop of the
        y2(k)=y2(k)*y2(k+1)+u(k) !tridiagonal algorithm.
    enddo

end subroutine spline
