subroutine raddens(rij,isp,jsp,density,ddensity_dpara)

!   Computes density contributions, 'density(0:lmax)', and density derivative,
!   'ddensity_dpara(0:lmax)' from atom j (species 
!   jsp) to atom i (species isp). Normally will be called with isp=1, unless 
!   acceptor-dependant densities are being used (acceptor-dependant densities 
!   currently deprecated).
!
!   Andy Duff, 2014-2017
!
!     Copyright (c) 2018, STFC

    use m_electrondensity
    use m_meamparameters
    use m_optimization

    implicit none

    integer isp,jsp,ip,o,iiCoeff,iiCutoff
    real(8) rij,density(0:lmax),ddensity_dpara(12,0:lmax)

    do o=0,lmax !loop over ang. mom.
        density(o) = 0d0
        do ip=2,12,2
            if (rij.le.meamrhodecay(ip,o,isp,jsp)) then
                density(o) = density(o) + &
                    meamrhodecay(ip-1,o,isp,jsp) * &
                    ((meamrhodecay(ip,o,isp,jsp) - rij) **3)
            endif
        enddo
        !print *,'density(',o,')=',density(o)
    enddo
    !stop

    !Calculate derivative of density w.r.t parameters
    !Coefficients:
    do o=0,lmax !loop over ang. mom.
       do iiCoeff=1,noptdensityCoeff(o,isp,jsp)
          ip = ioptdensityCoeff(iiCoeff,o,isp,jsp)
          if (rij.le.meamrhodecay(ip+1,o,isp,jsp)) then
             ddensity_dpara(ip,o) = (meamrhodecay(ip+1,o,isp,jsp) - rij) **3
          else
             ddensity_dpara(ip,o) = 0d0
          endif
       enddo
    enddo

    !Cutoffs:
    do o=0,lmax !loop over ang. mom.
       do iiCutoff=1,noptdensityCutoff(o,isp,jsp)
          ip = ioptdensityCutoff(iiCutoff,o,isp,jsp)
          if (rij.le.meamrhodecay(ip,o,isp,jsp)) then
             ddensity_dpara(ip,o) = 3d0 * meamrhodecay(ip-1,o,isp,jsp) * &
                      ((meamrhodecay(ip,o,isp,jsp) - rij) **2)
          else
             ddensity_dpara(ip,o) = 0d0
          endif
       enddo
    enddo

end subroutine raddens


subroutine raddensSpline(rij,isp,jsp,density,ddensity_dpara)

    !------------------------------------------------------------------c
    !
    !     Calculates the 'pairpot', for two atoms
    !     of species, 'species1' and 'species2' which are a
    !     distance, 'distance', apart from one another.
    !
    !     Called by:     meamenergy
    !     Calls:         -
    !     Arguments:     species1,species2,distance,pairpotparameter
    !     Returns:       pairpot
    !     Files read:    -
    !     Files written: -
    !
    !     Marcel Sluiter, Feb 9 2006
    !
    !     Copyright (c) 2018, STFC
    !
    !------------------------------------------------------------------c

    use m_electrondensity
    use m_meamparameters
    use m_optimization

    implicit none

    integer isp,jsp,ip,o,iiCoeff,iiCutoff,rIndex
    real(8) rij,density(0:lmax),ddensity_dpara(12,0:lmax)

    rIndex=1+FLOOR(rij/drLmps(jsp,jsp))
    call interpolateSpline(drLmps(jsp,jsp),densSpline(1:nRhoMaxSpline,jsp),densSplineSecDer(1:nRhoMaxSpline,jsp),nRMaxSpline,rIndex,rij,density(0))
    if (lmax.gt.0) then
       density(1:lmax)=0d0
    endif
    ddensity_dpara=0d0

end subroutine raddensSpline



subroutine raddensForce(rij,distComp,isp,jsp,density,ddensity_dxyz,ddensity_dpara,d2density_dxyz_dpara)

!   Computes density derivative contributions, 'ddensity_dxyz(1:3,0:lmax)', and derivatives,
!   'ddensity_dxyz_dpara(1:3,0:lmax)' w.r.t potential parameters from atom j (species 
!   jsp) to atom i (species isp). Normally will be called with isp=1, unless 
!   acceptor-dependant densities are being used (acceptor-dependant densities 
!   currently deprecated).
!
!   Andy Duff, 2014-2017
!
!     Copyright (c) 2018, STFC

    use m_electrondensity
    use m_meamparameters
    use m_optimization

    implicit none

    integer isp,jsp,ip,o,iiCoeff,iiCutoff,alph
    real(8) rij,density(0:lmax),ddensity_dxyz(1:3,0:lmax),ddensity_dpara(12,0:lmax),d2density_dxyz_dpara(1:3,1:12,0:lmax), &
          ddensFact,distComp(1:3),distCompOver_rij(1:3)

    ddensity_dxyz=0d0
    do o=0,lmax !loop over ang. mom.
        density(o) = 0d0
        do ip=2,12,2
            if (rij.le.meamrhodecay(ip,o,isp,jsp)) then
              ddensFact=meamrhodecay(ip-1,o,isp,jsp) * &
                    ((meamrhodecay(ip,o,isp,jsp) - rij) **2)
                density(o) = density(o) + &
                     ddensFact*(meamrhodecay(ip,o,isp,jsp) - rij)
                do alph=1,3
                   distCompOver_rij(alph)=distComp(alph)/rij
                   !Note: the following derivative is w.r.t atomic coordinates
                   !on site i (cancellation of 2 minus signs)
                   ddensity_dxyz(alph,o) = ddensity_dxyz(alph,o) + &
                       3d0*ddensFact*distCompOver_rij(alph)
                enddo
            endif
        enddo
    enddo

    !Calculate derivative of density w.r.t parameters
    !Coefficients:
    do o=0,lmax !loop over ang. mom.
       do iiCoeff=1,noptdensityCoeff(o,isp,jsp)
          ip = ioptdensityCoeff(iiCoeff,o,isp,jsp)
          if (rij.le.meamrhodecay(ip+1,o,isp,jsp)) then
             ddensity_dpara(ip,o) = (meamrhodecay(ip+1,o,isp,jsp) - rij) **3
          else
             ddensity_dpara(ip,o) = 0d0
          endif
       enddo
    enddo

    !Cutoffs:
    do o=0,lmax !loop over ang. mom.
       do iiCutoff=1,noptdensityCutoff(o,isp,jsp)
          ip = ioptdensityCutoff(iiCutoff,o,isp,jsp)
          if (rij.le.meamrhodecay(ip,o,isp,jsp)) then
             ddensity_dpara(ip,o) = 3d0 * meamrhodecay(ip-1,o,isp,jsp) * &
                      ((meamrhodecay(ip,o,isp,jsp) - rij) **2)
          else
             ddensity_dpara(ip,o) = 0d0
          endif
       enddo
    enddo

    !Calculate derivative of density w.r.t parameters
    !Coefficients:
    do o=0,lmax !loop over ang. mom.
       do iiCoeff=1,noptdensityCoeff(o,isp,jsp)
          ip = ioptdensityCoeff(iiCoeff,o,isp,jsp)
          if (rij.le.meamrhodecay(ip+1,o,isp,jsp)) then
             ddensFact=3d0*(meamrhodecay(ip+1,o,isp,jsp) - rij) **2
             do alph=1,3
                d2density_dxyz_dpara(alph,ip,o) = ddensFact* &
                     distCompOver_rij(alph)
             enddo
          else
             d2density_dxyz_dpara(1:3,ip,o) = 0d0
          endif
       enddo
    enddo
    !Cutoffs:
    do o=0,lmax !loop over ang. mom.
       do iiCutoff=1,noptdensityCutoff(o,isp,jsp)
          ip = ioptdensityCutoff(iiCutoff,o,isp,jsp)
          if (rij.le.meamrhodecay(ip,o,isp,jsp)) then
             ddensFact=6d0*meamrhodecay(ip-1,o,isp,jsp)*(meamrhodecay(ip,o,isp,jsp) - rij)
             do alph=1,3
                d2density_dxyz_dpara(alph,ip,o) = ddensFact*distCompOver_rij(alph)
             enddo
          else
             d2density_dxyz_dpara(1:3,ip,o) = 0d0
          endif
       enddo
    enddo

end subroutine raddensForce


subroutine raddensForceSpline(rij,distComp,isp,jsp,density,ddensity_dxyz,ddensity_dpara,d2density_dxyz_dpara)

!   Computes density derivative contributions, 'ddensity_dxyz(1:3,0:lmax)', and
!   derivatives,
!   'ddensity_dxyz_dpara(1:3,0:lmax)' w.r.t potential parameters from atom j
!   (species 
!   jsp) to atom i (species isp). Normally will be called with isp=1, unless 
!   acceptor-dependant densities are being used (acceptor-dependant densities 
!   currently deprecated).
!
!   Andy Duff, 2014-2017
!
!     Copyright (c) 2018, STFC

    use m_electrondensity
    use m_meamparameters
    use m_optimization

    implicit none

    integer isp,jsp,ip,o,iiCoeff,iiCutoff,alph,rIndex
    real(8) rij,density(0:lmax),ddensity_dr(0:lmax),ddensity_dxyz(1:3,0:lmax),ddensity_dpara(12,0:lmax),d2density_dxyz_dpara(1:3,1:12,0:lmax), &
          ddensFact,distComp(1:3),distCompOver_rij(1:3)

    rIndex=1+FLOOR(rij/drLmps(jsp,jsp))
    call interpolateSplineDeriv(drLmps(jsp,jsp),densSpline(1:nRhoMaxSpline,jsp),densSplineSecDer(1:nRhoMaxSpline,jsp),nRMaxSpline,rIndex,rij,density(0),ddensity_dr(0))
    if (lmax.gt.0) then
       density(1:lmax)=0d0
       ddensity_dr(1:lmax)=0d0
       ddensity_dxyz(1:3,1:lmax)=0d0
    endif
    ddensity_dpara=0d0
    do alph=1,3
       distCompOver_rij(alph)=distComp(alph)/rij
       ddensity_dxyz(alph,0) = -ddensity_dr(0)* distCompOver_rij(alph)
    enddo
    d2density_dxyz_dpara=0d0


end subroutine raddensForceSpline

