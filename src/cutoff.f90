real(8) function cutoff(r,rmin,rmax)

    !     If rmin<r<rmax, cutoff=1, otherwise cutoff=0
    !
    !     Andrew Duff, 2012
    !
    !     Copyright (c) 2018, STFC

    real(8) r,rmin,rmax

    if ((r.gt.rmin).and.(r.lt.rmax)) then
        cutoff=1d0
    else
        cutoff=0d0
    endif

end function cutoff
