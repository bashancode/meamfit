subroutine defaultBoundsForRandParas

    !--------------------------------------------------------------c
    !
    !     Initializes default bounds for random generation of
    !     potential parameters.
    !
    !     Called by: MEAMfit
    !     Calls:
    !     Returns:
    !     Files read:
    !     Files written:
    !
    !     Andy Duff, 2014
    !
    !     Copyright (c) 2018, STFC
    !
    !--------------------------------------------------------------c

    use m_meamparameters
    use m_filenames
    use m_atomproperties
    use m_generalinfo
    use m_optimization
    use m_geometry

    implicit none

    integer i,ii,ll,isp,jsp,spc_cnt,l

    !Variables used for reading in start_meam_parameters file:
    integer nfreepThi

    allocate( meamtau_minorder(1:lmax,maxspecies), &
        meamtau_maxorder(1:lmax,maxspecies), &
        meamrhodecay_minradius(0:lmax,maxspecies,maxspecies), &
        meamrhodecay_maxradius(0:lmax,maxspecies,maxspecies), &
        meamrhodecay_minorder(1:6,0:lmax,maxspecies,maxspecies), &
        meamrhodecay_maxorder(1:6,0:lmax,maxspecies,maxspecies), &
        meame0_minorder(maxspecies), meame0_maxorder(maxspecies), &
        meamrho0_minorder(maxspecies), meamrho0_maxorder(maxspecies), &
        meamemb3_minorder(maxspecies), meamemb3_maxorder(maxspecies), &
        meamemb4_minorder(maxspecies), meamemb4_maxorder(maxspecies), &
        pairpotparameter_minradius(maxspecies,maxspecies), &
        pairpotparameter_maxradius(maxspecies,maxspecies), &
        pairpotparameter_minorder(1:8,maxspecies,maxspecies), &
        pairpotparameter_maxorder(1:8,maxspecies,maxspecies), &
        minordervaluepairpot(1:16), &
        maxordervaluepairpot(1:16), &
        nfreeppairpot(maxspecies,maxspecies), &
        enconst_minorder(maxspecies), enconst_maxorder(maxspecies) )

    !Determine max and min values for cutoffs based on separation distribution
    !from fitting database
    meamrhodecay_maxradius=0d0
    pairpotparameter_maxradius=0d0
    do isp=1,maxspecies
       !First, max values
       meamrhodecay_maxradius(0,1,isp)=0d0
       do jsp=isp,maxspecies
          meamrhodecay_maxradius(0,1,isp)=max(meamrhodecay_maxradius(0,1,isp),lrgsepnperspc_overall(isp,jsp))
       enddo
       do jsp=1,isp
          meamrhodecay_maxradius(0,1,isp)=max(meamrhodecay_maxradius(0,1,isp),lrgsepnperspc_overall(jsp,isp))
       enddo
       !Now, min values
       meamrhodecay_minradius(0,1,isp)=10000d0
       do jsp=isp,maxspecies
          meamrhodecay_minradius(0,1,isp)=min(meamrhodecay_minradius(0,1,isp),smlsepnperspc_overall(isp,jsp))
       enddo
       do jsp=1,isp
          meamrhodecay_minradius(0,1,isp)=min(meamrhodecay_minradius(0,1,isp),smlsepnperspc_overall(jsp,isp))
       enddo
       !These min values can't be below 'cutoffMin' (specified either by user or
       !given a default value - neccessary to stop cutoffs enchroaching on
       !Biersack-Ziegler low separation limit.
       if (meamrhodecay_minradius(0,1,isp).lt.cutoffMin) then
          meamrhodecay_minradius(0,1,isp)=cutoffMin
       endif

    enddo
    do l=1,lmax
       do isp=1,maxspecies
          meamrhodecay_minradius(l,1,isp)=meamrhodecay_minradius(0,1,isp)
          meamrhodecay_maxradius(l,1,isp)=meamrhodecay_maxradius(0,1,isp)
       enddo
    enddo
    do isp=1,maxspecies
        do jsp=1,maxspecies
            if (jsp.ge.isp) then
                pairpotparameter_minradius(isp,jsp)=max(smlsepnperspc_overall(isp,jsp), &
                                                          cutoffMin)
                pairpotparameter_maxradius(isp,jsp)=lrgsepnperspc_overall(isp,jsp)
            endif
        enddo
    enddo

    !meamtau
    if (lmax.gt.0) then
       do ll=1,lmax
           do isp=1,maxspecies
               if (freep(2*m3+isp+(ll-1)*m1).ge.2) then
                   meamtau_minorder(ll,isp)=meamtau_minorder_default
                   meamtau_maxorder(ll,isp)=meamtau_maxorder_default
               endif
           enddo
       enddo
    endif

    !electron density functions
    spc_cnt=0
    do isp=1,maxspecies
        do jsp=1,maxspecies
            if ((isp.eq.1).or.(thiaccptindepndt.eqv..false.)) then
                do ll=0,lmax
                    !Check if we need to generate random paramaters for
                    !thi(isp,jsp,ll)
                    nfreepThi=0
                    do i=1,12
                        if (freep(2*m3+lmax*m1+12*lm1*spc_cnt+12*ll+i).ge.2) then
                            nfreepThi=nfreepThi+1
                        endif
                    enddo
                    if (nfreepThi.gt.0) then
                        if (typethi(isp,jsp).eq.1) then !Cubic form

                            !For each coefficient which needs to be generated,
                            !read in min and max orders (10^..)
                            ii=1
                            do i=1,6
                                if (freep(2*m3+lmax*m1+12*lm1*spc_cnt+12*ll+2*i-1).ge.2) &
                                    then
                                    meamrhodecay_minorder(ii,ll,isp,jsp)=meamrhodecay_minorder_default
                                    meamrhodecay_maxorder(ii,ll,isp,jsp)=meamrhodecay_maxorder_default
                                    ii=ii+1
                                endif
                            enddo
                        else
                           print *,'ERROR, typethi(',isp,',',jsp,') =',typethi(isp,jsp),' /= 1'
                           stop
                        endif
                    endif
                enddo
            endif
            spc_cnt=spc_cnt+1
        enddo
    enddo

    !embedding functions
    embfuncRandType=2 !Logarithmic by default
    do isp=1,maxspecies
        do i=1,4
            if (freep(2*m3+lmax*m1+12*lm1*m2+isp+(i-1)*m1).ge.2) then
                if (i.eq.1) then
                    meame0_minorder(isp)=meame0_minorder_default
                    meame0_maxorder(isp)=meame0_maxorder_default
                elseif (i.eq.2) then
                    meamrho0_minorder(isp)=meamrho0_minorder_default
                    meamrho0_maxorder(isp)=meamrho0_maxorder_default
                elseif (i.eq.3) then
                    meamemb3_minorder(isp)=meamemb3_minorder_default
                    meamemb3_maxorder(isp)=meamemb3_maxorder_default
                elseif (i.eq.4) then
                    meamemb4_minorder(isp)=meamemb4_minorder_default
                    meamemb4_maxorder(isp)=meamemb4_maxorder_default
                endif
            endif
        enddo
    enddo

    !pair-potentials
    spc_cnt=0
    do isp=1,maxspecies
        do jsp=1,maxspecies
            if (jsp.ge.isp) then
                !Check if we need to generate random paramaters for
                !pairpot(isp,jsp)
                nfreeppairpot(isp,jsp)=0
                do i=1,32
                    if (freep(2*m3+(4+lmax)*m1+12*lm1*m2+i+32*spc_cnt) &
                        .ge.2) then
                        nfreeppairpot(isp,jsp)=nfreeppairpot(isp,jsp)+1
                    endif
                enddo
                if (nfreeppairpot(isp,jsp).gt.0) then
                    if (typepairpot(isp,jsp).eq.2) then
                        !Setup default minimum and maximum orders for generating
                        !pair-potential coefficients
                        ii=1
                        do i=1,16
                            if (freep(2*m3+(4+lmax)*m1+12*lm1*m2+2*i-1+32*spc_cnt) &
                                .ge.2) then
                                pairpotparameter_minorder(ii,isp,jsp)=pairpotparameter_minorder_default
                                pairpotparameter_maxorder(ii,isp,jsp)=pairpotparameter_maxorder_default
                                ii=ii+1
                            endif
                        enddo
                    else
                        print *,'typepairpot=',typepairpot(isp,jsp), &
                            'not supported,stopping'
                        stop
                    endif
                endif
            endif
            spc_cnt=spc_cnt+1
        enddo
    enddo

    !enconst
    do isp=1,maxspecies
        if (freep(m4+2*m2+isp).ge.2) then
            enconst_minorder(isp)=enconst_minorder_default
            enconst_maxorder(isp)=enconst_maxorder_default
        endif
    enddo

end subroutine defaultBoundsForRandParas
