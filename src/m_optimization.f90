

module m_optimization

    !     For more detailed descriptions of these variables in many cases see
    !     'readsettings.f90'.
    !     Copyright (c) 2018, STFC

    !Fit points per parameter:
    real(8) fitPntsPerPara
    logical opt_enconst,debug !debug: display extra optimization data for debuging
    integer objFuncType,nGuesses_Rnd,nGuesses_GA
    integer, parameter:: MinFitPntsPerPara=15
    !optimization algorithm, .true.: quasi Newton; .false.: conjugate gradient
    !(CG)
    logical quasiNewton,numDiff
    logical excludeCoulomb !whether to exclude Coulomb contributions from
                           !observables in optimization
    logical analyticDerivs !Whether to use analytic derivatives
    !freep stores values 0, 1 or 2 for each parameter, to hold fixed, read-in
    !and optimize, randomly generated and optimize, respectively
    integer, allocatable:: freep(:),freep_backup(:)
    !from freep, record parameters in each part of potential to be optimized
    !'n' is the total number of each (isp:jsp), 'i' stores the index of each
    logical, allocatable:: optmeamtau(:,:)
    integer, allocatable:: noptpairpotCoeff(:,:),noptpairpotCutoff(:,:), &
            ioptpairpotCoeff(:,:,:),ioptpairpotCutoff(:,:,:), &
            noptdensityCoeff(:,:,:),noptdensityCutoff(:,:,:), &
            ioptdensityCoeff(:,:,:,:),ioptdensityCutoff(:,:,:,:)
    integer n_optfunc,noptfuncstore,embfuncRandType,stopTime
    integer np,nsteps         !number of meam parameters and number of
    !changes in the meam parameters allowed
    logical randgenparas      !Whether parameters are to be randomly generated
    logical adjustedStopTime  !Records whether we have adjusted stopTime internally yet
    integer nRandGen !Number of randomly generated parameters
    logical firstpraxiscall,optimizestruc,noOpt,opt_failed,cutoffopt, &
            energyfit,forcefit,printoptparas,readParasOnebyone,genAlgo,fixPotIn,useRef, &
            holdcutoffs,optcomplete,hardStop
    integer, allocatable:: ncutoffDens(:,:),ncutoffPairpot(:,:)
    real(8), allocatable:: cutoffDens(:,:,:),cutoffPairpot(:,:,:)
    logical, allocatable:: positivep(:)
    integer, allocatable:: istrucInFile(:)
    real(8), allocatable:: p(:),scaledstep(:),p_orig(:),p_saved(:,:),p_best(:)
    real(8), parameter:: poptStrt=10d0
    real(8), allocatable:: fitdata(:) !size: ndatapoints
    real(8), allocatable:: bestfitdata(:) !size: ndatapoints
    real(8) avgEn,avgFrc,avgStr,varEn,varFrc,varStr
    real(8), allocatable:: summeam_paireStr(:),summeamfStr(:)
    real(8), allocatable:: weightsEn(:),weightsFr(:),weightsSt(:) !Weights for energies, forces and stress-tensors
                                                                    !in the objective function, per structure
    !optimization function. size: nstruc
    integer, allocatable:: timeoptfuncHr(:),timeoptfuncMin(:)
    real(8), allocatable:: bestoptfuncs(:)
    real(8) funcforc,funcen,funcstr,maxoptfuncallowed, &
        nSMAvalue,nFX,optdiff,optAcc,optfunc_err
    integer maxfuncevals
    real(8) cutoffMin,cutoffMax,CutoffPenalty,CutoffPenCoeff,fracCutoffChng
    real(8), parameter:: cutoffMinLim=1.5d0
    real(8) deltaOlimit ! a limit on precision on local minimization before cutting out
    !real(8), allocatable:: pairpotStr(:,:,:)
    !integer splnNvals

    !Parameters used to randomly generate the initial MEAM parameters
    integer, allocatable:: meamtau_minorder(:,:), &
        meamtau_maxorder(:,:), &
        meamrhodecay_negvals(:,:,:), &
        meamrhodecay_minorder(:,:,:,:), &
        meamrhodecay_maxorder(:,:,:,:), &
        pairpotparameter_negvals(:,:), &
        pairpotparameter_minorder(:,:,:), &
        pairpotparameter_maxorder(:,:,:), &
        nfreeppairpot(:,:), &
        minordervaluepairpot(:),maxordervaluepairpot(:)
    real(8), allocatable:: meamrhodecay_minradius(:,:,:), &
        meamrhodecay_maxradius(:,:,:), &
        pairpotparameter_minradius(:,:), &
        pairpotparameter_maxradius(:,:), &
        meame0_minorder(:), meame0_maxorder(:), &
        enconst_minorder(:), enconst_maxorder(:), &
        meamrho0_minorder(:),meamrho0_maxorder(:), &
        meamemb3_minorder(:),meamemb3_maxorder(:), &
        meamemb4_minorder(:),meamemb4_maxorder(:)
    !Default values for setting up random parameters:
    integer, parameter:: meamtau_minorder_default = -1
    integer, parameter:: meamtau_maxorder_default = 1 !3
    integer, parameter:: meamrhodecay_negvals_default = 1 !2 for positive l=0 contr
    integer, parameter:: meamrhodecay_minorder_default = 0
    integer, parameter:: meamrhodecay_maxorder_default = 1 !8
    integer, parameter:: meame0_minorder_default = 0
    integer, parameter:: meame0_maxorder_default = 3 ! 1
    integer, parameter:: meamrho0_minorder_default = -5
    integer, parameter:: meamrho0_maxorder_default = -2
    integer, parameter:: meamemb3_minorder_default = -9
    integer, parameter:: meamemb3_maxorder_default = -6
    integer, parameter:: meamemb4_minorder_default = -1
    integer, parameter:: meamemb4_maxorder_default = 2
    integer, parameter:: pairpotparameter_negvals_default = 2 !1 for positive l=0 contr
    integer, parameter:: pairpotparameter_minorder_default = 0
    integer, parameter:: pairpotparameter_maxorder_default = 1
    integer, parameter:: enconst_minorder_default = 0
    integer, parameter:: enconst_maxorder_default = 1
    logical:: readparasfromsettings

    !Genetic algo
    logical forceMut
    real(8) probRand,probPot1

end module m_optimization

