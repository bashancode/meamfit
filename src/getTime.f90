      subroutine getTime(timeReturn)

      ! Return time in seconds (need to subtract the time at start of
      ! simulation to get run-time)
      !
      ! Marcel Sluiter
      !
      !     Copyright (c) 2018, STFC

      implicit none

      integer nl, timetaken(6),timeReturn

      character numb*6,line*80,date*8,time*10,zone*5

      call date_and_time(date,time,zone) 

      read(date(1:4),*)timetaken(1) !years
      read(date(5:6),*)timetaken(2) !months
      read(date(7:8),*)timetaken(3) !days
      read(time(1:2),*)timetaken(4) !hours
      read(time(3:4),*)timetaken(5) !minutes
      read(time(5:6),*)timetaken(6) !seconds

      timeReturn=(((timetaken(2)*30+timetaken(3))*24+timetaken(4))*60 &
                     +timetaken(5))*60+timetaken(6)

      end subroutine getTime
