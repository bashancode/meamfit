

module m_objectiveFunction

    !     Copyright (c) 2018, STFC

    logical negElecDens,negBackDens,gammaOutOfBounds,finiteF,cutoffOutOfBounds,cutoffPen
    logical negElecDensWarning !Warn the user (only on the first instance) if we are not evaluating the objective function due to there
                               !being a negative density
    logical scaleObyVar !Whether to divide Fen by variance of energies or not
    logical scaleOFrcbyVar !Whether to divide Fen by variance of energies or not
    !Full objective function
    real(8) F,FnoScl,Fprev,Fprev2
    real(8), allocatable:: dF_dmeamtau(:,:),dF_draddens(:,:,:)
    real(8), allocatable:: dF_dmeame0(:),dF_dmeamrho0(:),dF_dmeamemb3(:),dF_dmeamemb4(:)
    real(8), allocatable:: dF_dpairpot(:,:,:),dF_denconst(:)
    real(8), allocatable:: dF_dpara(:),dF_dpopt(:) !In terms of para and popt
    real(8), allocatable:: dF_dpoptStore(:,:) !For toms611
    !Objective function components
    real(8) Fen,Ffrc,Fstr,FnoPen
    real(8), allocatable:: dFen_dmeamtau(:,:)
    real(8), allocatable:: dFen_draddens(:,:,:)
    real(8), allocatable:: dFen_dmeame0(:),dFen_dmeamrho0(:),dFen_dmeamemb3(:),dFen_dmeamemb4(:)
    real(8), allocatable:: dFen_dpairpot(:,:,:)
    real(8), allocatable:: dFen_denconst(:)
    real(8), allocatable:: dFen2_dmeamtau(:,:)
    real(8), allocatable:: dFen2_draddens(:,:,:)
    real(8), allocatable:: dFen2_dmeame0(:),dFen2_dmeamrho0(:),dFen2_dmeamemb3(:),dFen2_dmeamemb4(:)
    real(8), allocatable:: dFen2_dpairpot(:,:,:)
    real(8), allocatable:: dFfrc_dmeamtau(:,:)
    real(8), allocatable:: dFfrc_draddens(:,:,:)
    real(8), allocatable:: dFfrc_dmeame0(:)
    real(8), allocatable:: dFfrc_dmeamrho0(:)
    real(8), allocatable:: dFfrc_dmeamemb3(:)
    real(8), allocatable:: dFfrc_dmeamemb4(:)
    real(8), allocatable:: dFfrc_dpairpot(:,:,:)
    real(8), allocatable:: dFstr_dmeamtau(:,:)
    real(8), allocatable:: dFstr_draddens(:,:,:)
    real(8), allocatable:: dFstr_dmeame0(:)
    real(8), allocatable:: dFstr_dmeamrho0(:)
    real(8), allocatable:: dFstr_dmeamemb3(:)
    real(8), allocatable:: dFstr_dmeamemb4(:)
    real(8), allocatable:: dFstr_dpairpot(:,:,:)
    !Parameters for penalties
    logical negElecDensAllow
    !logical lowDensPen
    real(8) FcutoffPen!,FlowDensPen
    !real(8), parameter:: lowDensPen_cutoffFrac=0.05d0
    !real(8), parameter:: lowDensPen_coeff=1d0
    real(8), allocatable:: dFcutoffPen_draddens(:,:,:),dFcutoffPen_dpairpot(:,:,:)
    !real(8), allocatable:: dFlowdensPen_draddens(:,:,:)
    real(8) cutoffPenDist !0.1d0
    real(8) cutoffPenPrefact !1000d0

end module m_objectiveFunction
