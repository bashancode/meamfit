subroutine readtargetstruc(filein,nconf,DFTdata,startprevposn)

    !-----------------------------------------------------------c
    !
    !     Reads the 'nconfig'th ionic configuration from a
    !     DFT data file.
    !     Converts atomic coordinates to direct coordinates if
    !     necessary and then makes sure all atoms are in the
    !     central unit cell. Finally atomic coordinates are
    !     converted to cartesian coordinates
    !
    !     Called by:     initializestruc
    !     Calls:         optionalarg,matinv,matmul
    !     Arguments:     filein
    !     Returns:       tr,nspecies,nat,natoms,z,zz,coordinates,
    !                    coords_old
    !     Files read:    filein
    !
    !     Andrew Duff, 2006-2016
    !
    !     Copyright (c) 2018, STFC
    !
    !-----------------------------------------------------------c

    use m_atomproperties
    use m_poscar
    use m_generalinfo

    implicit none

    logical num,startprevposn,file_exists,cartesian,fracCoordsFound
    logical, parameter:: force_atoms_into_cell=.false. !If true, the
    !atoms in the poscar file are translated
    !by the basis vectors so that they lie
    !within the unit cell defined by these
    !basis vectors
    integer istruc,i,j,it1,nconf,wordLength,ios,integernum,DFTdata,idata,IOstatus
    real(8) aux1(3),aux2(3),scale,aa,bb,cc,lattPara
    real(8), parameter:: bohroverangstrom=0.529177249d0
    character*(*) filein
    character*1 snglechar
    character*2 doublechar
    character*9 newstrng,newstrng2
    character*80 inputfile,strng1,strng2,strng3,strng3orig
    character*80 line,zspecies
    character*106 lineLong

    !print *,'startprevposn=',startprevposn,', nconf=',nconf

    !Are we reading in the next configuration in sequence? If not we need to
    !jump past all the previous configrurations in the file that we are not
    !interested in.
    if (startprevposn.eqv..false.) then

       close(1)
       open(unit=1,file=filein)
       !print *,'reading from scratch from file:',trim(filein)
       !print *,'nconf=',nconf,', DFTdata=',DFTdata

       if (DFTdata.eq.1) then

          !Reading CASTEP .geom file: either first time we've opened it, or else we've
          !jumped forward by more than one configuration since last time. First
          !read in atom properties (no. atoms, species, order of species, etc),
          !then we skip forward past the configs we don't want. NOTE: there is a
          !bit of wastage here- if we jump forward two configs but are on the
          !same file, we shouldn't need to count to the new config from scratch,
          !and furthermore shouldn't have to read in new atom properties again
          !for this file.

          !Get atom properties:
          if(allocated(z)) deallocate(z)
          if(allocated(zz)) deallocate(zz)
          call readAtomPropsFromCastep_geom
          !print *,'returned!'
          !print *,'nspecies=',nspecies
          !print *,'natoms=',natoms
          !print *,'z=',z
          !print *,'zz=',zz

          !Skip past unwanted configs:
          rewind(1)
          istruc=1
          do
             read(1,'(a106)') lineLong
             if (lineLong(102:106)=='<-- E') then
                 if (istruc.eq.nconf) then
                    exit
                 endif
                 istruc=istruc+1
             endif
          enddo

          !Old code:
          !   do
          !      read(1,'(a80)') line
          !      if (line(38:50).eq.'Cell Contents') then
          !         if(allocated(z)) deallocate(z)
          !         if(allocated(zz)) deallocate(zz)
          !         call readAtomPropsFromCastep
          !      endif
          !   enddo

       elseif (DFTdata.eq.0) then

          !print *,'DEBUG nconf=',nconf
          if (nconf.gt.1) then
          
             !print *,'Reading from vasprun file'
             !Skip past the steps we are not interested in.
             istruc=1
             do
               read(1,'(a80)') line
               if (line(1:10).eq.'<modeling>') then
                  !print *,'found string <modeling>'
                  !This occurs at the start of a new vasprun.xml; which contains
                  !the same initial set of positions twice
                  !if(allocated(nat)) deallocate(nat)
                  if(allocated(z)) deallocate(z)
                  if(allocated(zz)) deallocate(zz)
                  call readAtomPropsFromVasprun
                  !print *,'nspecies=',nspecies
                  !print *,'natoms=',natoms
                  !print *,'z=',z
                  !!print *,'nat=',nat
                  !print *,'zz=',zz
                  !print *,'tr=',tr
               endif
               newstrng=line(18:26)
               if ((newstrng(1:9).eq.'positions').or.(newstrng(1:9).eq.'cartesian')) then
                 !print *,'found positions, istruc=',istruc,', nconf=',nconf
                 if (istruc.eq.(nconf-1)) then
                    exit
                 endif
                 istruc=istruc+1
               endif
             enddo

          endif

       elseif ((DFTdata.eq.2).or.(DFTdata.eq.4)) then

          !Reading QE .out file: either first time we've opened it, or else we've
          !jumped forward by more than one configuration since last time. First
          !read in atom properties (no. atoms, species, order of species, etc),
          !then we skip forward past the configs we don't want. NOTE: there is a
          !bit of wastage here- if we jump forward two configs but are on the
          !same file, we shouldn't need to count to the new config from scratch,
          !and furthermore shouldn't have to read in new atom properties again
          !for this file.

          !Get atom properties:
          if(allocated(z)) deallocate(z)
          if(allocated(zz)) deallocate(zz)
          !For QE, input data is read from the .in file rather than the .out
          !file (the .out file doesn't have, e.g natoms, etc)
          inputfile=trim(filein(1:len(filein)-6))//"in"
          !print *,'Reading from ',inputfile
          open(unit=2,file=inputfile)
          inquire(file=inputfile,exist=file_exists)
          if (file_exists.eqv..false.) then
             print *,'ERROR: Cannot find file ',inputfile,' STOPPING.'
             stop
          endif
          call readAtomPropsFromQuantumEspresso
          !print *,'returned!'
          !print *,'nspecies=',nspecies
          !print *,'natoms=',natoms
          !print *,'z=',z
          !print *,'zz=',zz

          close(2)
          if (DFTdata.eq.4) then

            !Read 'lattice parameter', which multiplies the lattice vectors and
            !positions
            rewind(1)
            do
               read(1,'(a106)') lineLong
               if (lineLong(6:22)=='lattice parameter') then
                   strng3=linelong(34:46)
                   strng3=trim(strng3)
                   read(strng3,fmt=*) lattPara 
                   !print *,'lattPara, as read=',lattPara
                   lattPara=lattPara*bohroverangstrom
                   !print *,'In Angstrom = ',lattPara
                   exit
               endif
            enddo

            !Read lattice parameters
            rewind(1)
            do
               read(1,'(a106)') lineLong
               if (lineLong(6:17)=='crystal axes') then
                   exit
               endif
            enddo
            read(1,'(a106)') lineLong
            strng3=linelong(25:57)
            strng3=trim(strng3)
            read(strng3,fmt=*) tr(1:3,1)
            read(1,'(a106)') lineLong
            strng3=linelong(25:57)
            strng3=trim(strng3)
            read(strng3,fmt=*) tr(1:3,2)
            read(1,'(a106)') lineLong
            strng3=linelong(25:57)
            strng3=trim(strng3)
            read(strng3,fmt=*) tr(1:3,3)
            tr=tr*lattPara
            !print *,'tr=',tr
            !The trace is read only once at the start of the .out file
            !(although presumably if cell vectors are set to change this is not
            !the case). On subsequent calls tr will not change.

          endif
          !print *,'returned!'
          !print *,'nspecies=',nspecies
          !print *,'natoms=',natoms
          !print *,'z=',z
          !print *,'zz=',zz
          !stop

          !Skip past unwanted configs:
          if (DFTdata.eq.2) then !car parinello type QE output

             rewind(1)
             istruc=1
             do
                read(1,'(a106)') lineLong
                if (lineLong(1:18)=='   CELL_PARAMETERS') then
                    if (istruc.eq.nconf) then
                       exit
                    endif
                    istruc=istruc+1
                endif
             enddo

          elseif (DFTdata.eq.4) then !PWscf type QE data

             rewind(1)
             istruc=1
             do
                read(1,'(a106)') lineLong
                !First incidence of positions is different to subsequent
                if (istruc.eq.1) then
                   if (lineLong(40:48)=='positions') then
                       if (istruc.eq.nconf) then
                          exit
                       endif
                       istruc=istruc+1
                   endif
                else
                   if (lineLong(1:16)=='ATOMIC_POSITIONS') then
                       if (istruc.eq.nconf) then
                          exit
                       endif
                       istruc=istruc+1
                   endif
                endif
             enddo

           endif

       elseif (DFTdata.eq.3) then

          !Reading .castep file: either first time we've opened it, or else we've
          !jumped forward by more than one configuration since last time. First
          !read in atom properties (no. atoms, species, order of species, etc),
          !then we skip forward past the configs we don't want. NOTE: there is a
          !bit of wastage here- if we jump forward two configs but are on the
          !same file, we shouldn't need to count to the new config from scratch,
          !and furthermore shouldn't have to read in new atom properties again
          !for this file.

          !Get atom properties:
          if(allocated(z)) deallocate(z)
          if(allocated(zz)) deallocate(zz)
          call readAtomPropsFromCastep_castep

          ! print *,'returned!' ! DEBUG
          ! print *,'nspecies=',nspecies
          ! print *,'natoms=',natoms
          ! print *,'z=',z
          ! print *,'zz=',zz
          ! stop


          !Skip past unwanted configs:
          rewind(1)
          istruc=1
          do
             read(1,'(a106)') lineLong
             if ((lineLong(41:46)=='Forces').or.(lineLong(29:34)=='Forces').or.(lineLong(35:40)=='Forces').or.(lineLong(47:52)=='Forces')) then ! We search for Forces rather than directly for 'Unit Cell' because sometimes the latter appears without observables (energy, forces, stresses), e.g in continuation jobs. We only want sets of coordinates paired with observables.
                 if (istruc.eq.nconf) then
                    exit
                 endif
                 istruc=istruc+1
             endif
          enddo

          !Old code:
          !   do
          !      read(1,'(a80)') line
          !      if (line(38:50).eq.'Cell Contents') then
          !         if(allocated(z)) deallocate(z)
          !         if(allocated(zz)) deallocate(zz)
          !         call readAtomPropsFromCastep
          !      endif
          !   enddo

       endif

    endif

    !Read in from the target-file the following:
    !nspecies, natoms, z, nat, zz, tr, trinv and
    !coordinates (in direct coords)

    if (DFTdata.eq.1) then

       do
          read(1,'(a106)') lineLong
          if (lineLong(102:106)=='<-- h') then
             !Code to read in lattice parameters
             read(lineLong,*) tr(1:3,1)
             read(1,'(a106)') lineLong
             read(lineLong,*) tr(1:3,2)
             read(1,'(a106)') lineLong
             read(lineLong,*) tr(1:3,3)
             tr=tr*bohroverangstrom
          elseif (lineLong(102:106)=='<-- R') then
             !Can't just read in next line after '<-- h' because there may be
             !stress tensor entries (<-- S) before the positions
             if(allocated(coordinates)) deallocate(coordinates)
             allocate(coordinates(3,natoms))
             backspace(1)
             do i=1,natoms
                read(1,'(a106)') lineLong
                !print *,'lineLong=',lineLong
                !read(lineLong,*) newstrng,j,coordinates(1,i)
                !print *,'newstrng=',newstrng,' j=',j,' coordinates(1,i)=',coordinates(1,i)
                !stop
                !read(lineLong,'(A9,I1,3E24.4)') newstrng,j,coordinates(1:3,i)
                
!                coordinates(1,i)=dble(lineLong(19:47))
!                coordinates(2,i)=dble(lineLong(48:74))
!                coordinates(3,i)=dble(lineLong(75:101))
!
                read(lineLong,*) strng1,strng2,coordinates(1,i),coordinates(2,i),coordinates(3,i)
                !print *,'r(1:3,atom=',i,')=',coordinates(1:3,i)
                !stop
             enddo
             coordinates=coordinates*bohroverangstrom
             exit
             !stop

          endif
       enddo
 
    elseif (DFTdata.eq.0) then

          !print *,'looking for <modeling>'
          do
            read(1,'(a80)') line
            if (line(1:10).eq.'<modeling>') then
               !print *,'found <modeling>'
               !This occurs at the start of a new vasprun.xml; which contains
               !the same initial set of positions twice
               !2015 July: Andy added---
               !if(allocated(nat)) deallocate(nat)
               if(allocated(z)) deallocate(z)
               if(allocated(zz)) deallocate(zz)
               !---
               call readAtomPropsFromVasprun
               !print *,'returned!'
               !print *,'nspecies=',nspecies
               !print *,'natoms=',natoms
               !print *,'z=',z
               !!print *,'nat=',nat
               !print *,'zz=',zz
               !print *,'tr=',tr
            endif
            newstrng=line(18:26)
            ! If 'positions' in the vasprun is changed to 'cartesian', e.g. by
            ! the outcar2vasprun script, then positions read in are taken to be
            ! cartesian rather than-- as is standard for vasprun.xml --direct
            if ((newstrng(1:9).eq.'positions').or.(newstrng(1:9).eq.'cartesian'))  then
                 if (newstrng(1:9).eq.'positions') then
                    cartesian=.false.
                 else
                    cartesian=.true.
                 endif
                 exit
            endif
          enddo
          !print *,'left first loop'
          if(allocated(coordinates)) deallocate(coordinates)
          allocate(coordinates(3,natoms))
          !print *,'allocated arrays, preping to read coords'
          do i=1,natoms
             read(1,*) strng1,coordinates(1,i),coordinates(2,i),strng2
             call keepNumber(strng2)
             strng2=trim(strng2)
             !wordLength=len_trim(strng2)
             !strng2=strng2(1:wordLength-1)
             read(strng2,*) coordinates(3,i)
             !if (i.le.3) then
             !   print *,'i=',i,' coords=',coordinates(1:3,i)
             !endif
          enddo
          !stop
          !print *,'read coords'
!          print *,'coordinates: ',coordinates !21_10

          do i=1,12+natoms
             backspace(1)
          enddo
          !print *,'attempting to read in trace now:'
          read(1,*) strng1,tr(1,1),tr(2,1),strng2
          !print *,'read line 1'
          call keepNumber(strng2)
          strng2=trim(strng2)
          !wordLength=len_trim(strng2)
          !strng2=strng2(1:wordLength-1)
          read(strng2,*) tr(3,1)
          read(1,*) strng1,tr(1,2),tr(2,2),strng2
          !print *,'read line 2'
          call keepNumber(strng2)
          strng2=trim(strng2)
          !wordLength=len_trim(strng2)
          !strng2=strng2(1:wordLength-1)
          read(strng2,*) tr(3,2)
          read(1,*) strng1,tr(1,3),tr(2,3),strng2
          !print *,'read line 3'
          call keepNumber(strng2)
          strng2=trim(strng2)
          !wordLength=len_trim(strng2)
          !strng2=strng2(1:wordLength-1)
          read(strng2,*) tr(3,3)
          !print *,'read line 4'
!          print *,'trace: ',tr !21_10

          !Also read in volume
          read(1,*)
          read(1,*) strng1,strng2,strng3
          !print *,'strng1... = ',strng1,' ',strng2,' ',strng3
          strng3orig=strng3
          call keepNumber(strng3)
          strng3=trim(strng3)
          read(strng3,*,IOSTAT=ios) vol
          if (ios.ne.0) then
             print *,'ERROR: error reading volume from vasprun file'
             print *,'(string read from file=',strng3orig,'). STOPPING.'
             stop
          endif
          !print *,'tr=',tr,' vol=',vol
          !Now read back ahead of the positions (otherwise we will just read in
          !the same positions again next time)
          do i=1,7+natoms
             read(1,*)
          enddo
          !print *,'volume: ',vol !21_10

          if (cartesian.eqv..false.) then ! By default for a vasprun.xml file, the following should play out (since coordinates need converting from direct to cartesian
             if (force_atoms_into_cell) then
                 !Use modulo functions to get all coordinates in 0-1 range
                 do i=1,natoms
                     do j=1,3
                         coordinates(j,i)=modulo(coordinates(j,i),1d0) !modulo
                         !gives sign of 2nd argument
                     enddo
                 enddo
             endif
             !Convert direct coordinates to cartesian
             do i=1,natoms
                 aux1 = coordinates(1:3,i)
                 aux2 = matmul(tr,aux1)
                 coordinates(1:3,i) = aux2
             enddo
             !print *,'coordinates post conversion: ',coordinates !21_10
          endif

    elseif (DFTdata.eq.2) then

       if (startprevposn.eqv..true.) then
          !In this case we didn't run the 'skipping configurations' bit of code
          !above, so we need to makes sure we have located the next
          !'CELL_PARAMETERS'
          do
             read(1,'(a106)') lineLong
             if (lineLong(1:18)=='   CELL_PARAMETERS') then
                 exit
             endif
          enddo
       endif

       !Read in lattice parameters
       !read(1,'(a106)') lineLong
       !read(lineLong,*) tr(1:3,1)
       !read(1,'(a106)') lineLong
       !read(lineLong,*) tr(1:3,2)
       !read(1,'(a106)') lineLong
       !read(lineLong,*) tr(1:3,3)   
       read(1,*) tr(1:3,1)
       read(1,*) tr(1:3,2)
       read(1,*) tr(1:3,3)
       tr=tr*bohroverangstrom  !units in .out are A.U. (c.f. System Volume)
       !print *,'tr=',tr

       !Also read in volume
       read(1,*)
       read(1,*)
       read(1,*)
       read(1,*)
       read(1,*) strng1,strng2,strng3,newstrng,vol
       !print *,'vol=',vol*(bohroverangstrom**3)

       !Lattice parameters
       do
          read(1,'(a106)') lineLong
          if (lineLong(1:19)=='   ATOMIC_POSITIONS') then
             exit
          endif
       enddo
       if(allocated(coordinates)) deallocate(coordinates)
       allocate(coordinates(3,natoms))
       do i=1,natoms
          read(1,'(a106)') lineLong
          !Note, below I previously had A,3F, because ifort does not require widths (automatically assigns a default width based on
          !type). gfortran does however. Below is based on the formatting from the QE output; may need testing.
          read(lineLong,'(A,3F21.14)') newstrng,coordinates(1,i),coordinates(2,i),coordinates(3,i)
          print *,coordinates(1,i),coordinates(2,i),coordinates(3,i)
          print *,'ERROR: Check these numbers correspond to positions in the QE output (with correct formatting/precision), then'
          print *,'remove relevant code lines and recompile, STOPPING'
          stop
       enddo
       coordinates=coordinates*bohroverangstrom
       !print *,'coordinates=',coordinates
       !stop

    elseif (DFTdata.eq.4) then

       if (startprevposn.eqv..true.) then
          !In this case we didn't run the 'skipping configurations' bit of code
          !above, so we need to makes sure we have located the next
          !'CELL_PARAMETERS'
          do
             read(1,'(a106)') lineLong
             if (lineLong(1:16)=='ATOMIC_POSITIONS') then
                 exit
             endif
          enddo
       endif

       !Read positions
       if(allocated(coordinates)) deallocate(coordinates)
       allocate(coordinates(3,natoms))
       !print *,'positions:'
       if (nconf.eq.1) then
          !Formatting of first entry for PWscf .out files:
          !                  site n.     atom                  positions (alat units)
          !         1           Y   tau(   1) = (   0.6032860  -0.3060322  -0.0627439  )
          do i=1,natoms
             read(1,'(a106)') lineLong
             strng3=linelong(39:76)
             strng3=trim(strng3)
             read(strng3,fmt=*) coordinates(1,i),coordinates(2,i),coordinates(3,i)
             do idata=1,3
                coordinates(idata,i)=coordinates(idata,i)*lattPara
             enddo
          enddo
       elseif (nconf>1) then
          !Subsequent entries:
          !ATOMIC_POSITIONS (angstrom)
          !Y        3.478557835  -1.764516904  -0.363830576
          do i=1,natoms
             read(1,'(a106)') lineLong
             strng3=linelong(6:48)
             strng3=trim(strng3)
             read(strng3,fmt=*) coordinates(1,i),coordinates(2,i),coordinates(3,i)
             !print *,coordinates(1,i),coordinates(2,i),coordinates(3,i)
          enddo
       endif

    elseif (DFTdata.eq.3) then

       if (startprevposn.eqv..true.) then
          !In this case we didn't run the 'skipping configurations' bit of code
          !above, so we need to makes sure we have located the next
          !'Forces'
          do
             read(1,'(a106)') lineLong
             if ((lineLong(41:46)=='Forces').or.(lineLong(29:34)=='Forces').or.(lineLong(35:40)=='Forces').or.(lineLong(47:52)=='Forces')) then
                 ! print *,' located Forces, istruc=',istruc,' (STARTING from previous positon)' ! DEBUG
                 !do i=1,6
                 !   read(1,'(a106)') lineLong
                 !enddo
                 !print *,' line  :  ',lineLong 
                 exit
             endif
          enddo
       endif
      

       !Go backwards in file to locate lattice parameters
       !print *,' GOING BACKWARD TO FIND Real Lattice' ! DEBUG
       do
          backspace(1) !1 refers to the file unit (not number of lines...)
          backspace(1)
          read(1,'(a106)') lineLong
          !print *,lineLong
          if (lineLong(9:20)=='Real Lattice') then
              exit
          endif
       enddo
       read(1,*) tr(1:3,1)
       read(1,*) tr(1:3,2)
       read(1,*) tr(1:3,3)
       ! print *,'tr=',tr ! DEBUG

       !Read volume
       ! print *,' GOING FORWARD TO FIND Current cell volume' ! DEBUG
       do
          read(1,'(a106)') lineLong
          !print *,lineLong
          if (lineLong(17:35)=='Current cell volume') then
             strng3=lineLong(48:58)
             read(strng3,*,IOSTAT=ios) vol
             ! print *,'reading volume from 48:58, = ',vol ! DEBUG
             exit
          else if (lineLong(24:42)=='Current cell volume') then
             strng3=lineLong(55:65)
             read(strng3,*,IOSTAT=ios) vol
             ! print *,'reading volume from 55:65, = ',vol ! DEBUG
             exit
          endif
       enddo

       !Read coordinates
       do
          read(1,'(a106)',IOSTAT=IOstatus) lineLong
          if (IOstatus.lt.0) then
              fracCoordsFound=.false.
              exit
          endif
          if (lineLong(39:60)=='Fractional coordinates') then
              fracCoordsFound=.true.
              exit
          endif
       enddo

       if (fracCoordsFound.eqv..false.) then
          if (verbosity.gt.1) then
             print *,'Fractional coordinates not found in between strings: Real lattice, and Forces.'
          endif
          if (nconf>1) then
              print *,'ERROR: unexpected if nconf>1, STOPPING (see code for more comments)'
              ! To my knowledge fractional coords aren't found between the strings 'Real lattice' and 'Forces' only in the case of a geometry optimization for which atom positions are equivalent. In this case CASTEP doesn't move the atoms, and so doesn't print the positions out except at the start. Similarly forces are only printed out once, but this is at the end. In this case we have to search further back in the .castep file to find the positions.
              ! If nconf>1 that means there are multiple forces in the file (since that is how we label configurations in MEAMfit), implying that atoms are not symmetrically equivalent, and that positions should in this case be reported in each Real lattice-Forces block.
              ! (As an aside: using Forces to count configs is clearly not the best way if we want to count configs of a geom opt with symmetrically equivalent sites. At present in such a case only the last config is counted. Where sites are not symmetrically equivalent I am presuming forces+positions are output for each step, and in this case the algorithm will proceeed as usual).
              stop
          endif
          ! Search backwards and first check that we are doing geom opt
          if (verbosity.gt.1) then
             print *,'   ...going backward in file to find: LBFGS Geom...' ! DEBUG
          endif
          do
             backspace(1) !1 refers to the file unit (not number of lines...)
             backspace(1)
             read(1,'(a106)',IOSTAT=IOstatus) lineLong
             if (IOstatus.lt.0) then
                 print *,'ERROR: Tried searching for fractional coords earlier in the .castep file, but unable to confirm calculation was a geometry optimization (the only case where we allow this backward traversal of file), STOPPING'
                 stop
             endif
             if (lineLong(2:29)=='LBFGS: Geometry optimization') then
                 exit
             endif
          enddo

          ! Now search back for positions.
          do
             backspace(1)
             backspace(1)
             read(1,'(a106)',IOSTAT=IOstatus) lineLong
             if (IOstatus.lt.0) then
                 print *,' ERROR: Tried searching for fractional coords earlier in the .castep file, but unable to locate, STOPPING'
                 stop
             endif
             if (lineLong(39:60)=='Fractional coordinates') then
                 exit
             endif
          enddo

          ! (We do not need to worry about going backwards 'too many MD steps'
          ! because as mentioned above in this case we should only have nconf=1
          ! so will not be returning to read from this file again)
       endif

       read(1,*)
       read(1,*)
       if(allocated(coordinates)) deallocate(coordinates)
       allocate(coordinates(3,natoms))
       !print *,'tr=',tr ! DEBUG
       do i=1,natoms
          read(1,'(a106)') lineLong
          strng1=linelong(34:71)
          strng1=trim(strng1)
          read(strng1,fmt=*) coordinates(1,i),coordinates(2,i),coordinates(3,i)
          !print *,'nconf=',nconf,' coords=',coordinates(1:3,i) ! DEBUG
       enddo

       if (force_atoms_into_cell) then
           !Use modulo functions to get all coordinates in 0-1 range
           do i=1,natoms
               do j=1,3
                   coordinates(j,i)=modulo(coordinates(j,i),1d0) !modulo
                   !gives sign of 2nd argument
               enddo
           enddo
       endif
       !Convert direct coordinates to cartesian
       do i=1,natoms
           aux1 = coordinates(1:3,i)
           aux2 = matmul(tr,aux1)
           coordinates(1:3,i) = aux2
       enddo

       !Having gone back to retrieve the coordinates etc we now need to move
       !back (just ahead of) the 'Forces' we originally read up to. (Otherwise
       !the next iteration will just read in the same coords, etc again...)
       do
          read(1,'(a106)') lineLong
          if ((lineLong(41:46)=='Forces').or.(lineLong(29:34)=='Forces').or.(lineLong(35:40)=='Forces').or.(lineLong(47:52)=='Forces')) then
              !print *,' located Forces, istruc=',istruc,' (STARTING from previous positon)' ! DEBUG
              !do i=1,6
              !   read(1,'(a106)') lineLong
              !enddo
              !print *,' line  :  ',lineLong 
              exit
          endif
       enddo
       read(1,*) ! Now we are just ahead of the original 'Forces' line we used
                 ! to index the current set of positions, vectors and volume

    endif

    !Store the coordinates in 'coords_old' for the case where we are
    !calculating forces. We need to keep the old coordinates and no.
    !of atoms in unit cell handy for when we perform the second scan
    !across trial unit cells to establish which atoms satisfy
    !p_rmax1 < dist < p_rmax2
    if(allocated(coords_old)) deallocate(coords_old)
    allocate(coords_old(3,natoms))
    natoms_old=natoms
    do i=1,natoms
        coords_old(1:3,i)=coordinates(1:3,i)
    enddo
    !print *,'end subroutine' ! DEBUG

end subroutine readtargetstruc

subroutine checkifnumber(line,num)

    implicit none

    logical num
    integer i,length
    character*(*) line

    length=len(line)
    !print *,'line=',line,', length=',length
    if (length.eq.0) then
       print *,'ERROR: length=0 in checkifnumber, STOPPING.'
       stop
    endif
   
    num=.false.
    i=1
    do
       if (line(i:i).eq.' ') then
          if (i.eq.length) then
             exit
          endif
          i=i+1
       else
          if (line(i:i).eq.'1') then
             num=.true.
          elseif (line(i:i).eq.'2') then
             num=.true.
          elseif (line(i:i).eq.'3') then
             num=.true.
          elseif (line(i:i).eq.'4') then
             num=.true.
          elseif (line(i:i).eq.'5') then
             num=.true.
          elseif (line(i:i).eq.'6') then
             num=.true.
          elseif (line(i:i).eq.'7') then
             num=.true.
          elseif (line(i:i).eq.'8') then
             num=.true.
          elseif (line(i:i).eq.'9') then
             num=.true.
          elseif (line(i:i).eq.'0') then
             num=.true.
          endif
          exit
       endif
    enddo

endsubroutine checkifnumber


subroutine elementToZ(elemName,elemNum)

    use m_atomproperties
    use m_poscar
    use m_geometry

    implicit none

    logical elementFound
    integer i,j,elemNum
    character*2 elemName

    !do i=1,nspecies
        elementFound=.false.
        !Elements 113+ contain duplicates of the periodic table elements (lower
        !case, eg, h, he, etc). For the duplicates, z(i) is not the true atomic
        !number but is instead z+112, enabling the duplicates to be treated as
        !separate species for the purposes of the fitting. The true z is stored
        !in speciestoz(i) however, for the purposes of the r<0.9 pairpotential
        !and outputing the output potential files correctly
        do j=1,224
           if (elemName.eq.element(j)) then
              elementFound=.true.
              elemNum=j
              exit
           endif
        enddo
        if (elementFound.eqv..false.) then
           print *,'ERROR: element not recognized in vasprun file, STOPPING'
           print *,'(read in: ',elemName,')'
           stop
        endif
    !enddo

end subroutine elementToZ
