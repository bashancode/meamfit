subroutine broyden_bns(iter,mixparam,nbro,nstor, &
        q,qinv,f_old,aux,aux2,y,s, &
        x,f)
    ! multi-secant Broyden method to solve f(x)=0
    ! storage saving BNS algorithm
    ! first iteration is linear mixing with mixparam
    ! multi-secant condition gives singular matrix Q, therefore SVD is used
    ! algorithm is modification of Sawamura et al (Trans JIM 40, p1186,
    ! 1999)
    ! Marcel Sluiter, April 24 2000
    !
    !     Copyright (c) 2018, STFC

    implicit none
    integer iter,nbro,nstor,istor,istorp,istorn,i,j,idim
    real(8) x(nbro),f(nbro),mixparam,nrmy, &
        s(nbro,nstor),y(nbro,nstor),q(nstor,nstor),qinv(nstor,nstor), &
        f_old(nbro),aux(nstor),aux2(nstor)

    istorp=mod(iter-3,nstor)+1   !previous storage location
    istor =mod(iter-2,nstor)+1   !current storage location
    istorn=mod(iter-1,nstor)+1   !next storage location
    if(iter.ge.2) then
        y(1:nbro,istor) = f - f_old
        nrmy = sqrt(sum(y(1:nbro,istor)**2))
        y(1:nbro,istor) = y(1:nbro,istor) / nrmy
        s(1:nbro,istor) = s(1:nbro,istor) / nrmy
    endif
    f_old = f

    ! generate Q-matrix
    idim = min(iter-1,nstor)
    do j = 1, idim
        q(istor,j) = -dot_product(y(:,istor),y(:,j))
        q(j,istor) = q(istor,j)
    enddo
    if(idim.ge.2) then
        ! get q**(-1) using SVD for idim=>2
        call svdinv(q,idim,idim,nstor,nstor, &
            qinv)
    else if(idim.eq.1) then
        if(abs(q(1,1)).gt.1d-6) then
            qinv(1,1)=1d0/q(1,1)
        else
            qinv(1,1)=1d0
        endif
    endif

    ! compute x(n+1) = x(n) + b[I+y(n-1)(q(n-1)**-1)y(n-1)]f(n) +
    !                       + s(n-1)(q(n-1)**-1)y(n-1)f(n)
    ! where y(j) = f(j+1)-f(j), s(j) = x(j+1)-x(j)
    ! aux = y(n-1)*f(n)
    do i = 1, idim
        aux(i) = dot_product(y(1:nbro,i),f)
    enddo
    ! aux2 = (q(n-1)**-1)y(n-1)f(n) = (q(n-1)**-1) aux
    do i = 1, idim
        aux2(i) = dot_product(qinv(i,1:idim),aux(1:idim))
    enddo
    ! xnew temporarily stored in f
    ! xnew = f(n) + y(n-1)(q(n-1)**-1)y(n-1)f(n) = f(n) + y(n-1) aux2
    do i = 1, idim
        f(1:nbro) = f(1:nbro) + y(1:nbro,i)*aux2(i)
    enddo
    ! xnew = b * [f(n) + y(n-1)(q(n-1)**-1)y(n-1)f(n) ]
    f = mixparam * f
    ! xnew = b * [f(n) + y(n-1)(q(n-1)**-1)y(n-1)f(n) ] +
    !                    s(n-1)(q(n-1)**-1)y(n-1)f(n)
    do i = 1, idim
        f(1:nbro) = f(1:nbro) + s(1:nbro,i)*aux2(i)
    enddo
    ! x(n+1) = x(n) + b (I + y(n-1).....
    x = x + f
    s(1:nbro,istorn) = f
end subroutine broyden_bns
