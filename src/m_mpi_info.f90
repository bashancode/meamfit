! MvE
! This module contains necessary MPI variables: nprocs (number of processes),
! procid (id of current process) and ierr (error variable)
module m_mpi_info

    integer ierr, nprocs, procid

    ! Used to backup p array in the main process
    real(8), allocatable:: q(:)

    ! Used to divide nstructs over MPI procs and some loop counters
    integer start_i, stop_i, i_node, loop

    ! initializing send and receive array for the communication of single variables
    real(8), allocatable :: send_arr(:)
    real(8), allocatable :: recv_arr(:)

    ! initializing send and receive array for the communication of 1d arrays
    ! (all 1d arrays will be put into one large  1d array)
    real(8), allocatable :: recv_all_1darr(:)
    real(8), allocatable :: send_all_1darr(:)

    ! initializing send and receive array for the communication of 2d arrays
    ! (all 2d arrays will be put into one large  1d array)
    real(8), allocatable :: send_all_2darr(:)
    real(8), allocatable :: recv_all_2darr(:)

    !! initializing send and receive arrays for the communication of 3d arrays (seperate 1d send/recv arrays
    ! will be made for ..._draddens and ..._dpairpot where ... is dFen, dFen2, dFfrc or dFstr
    real(8), allocatable :: recv_draddens_3darr(:)
    real(8), allocatable :: send_draddens_3darr(:)
    real(8), allocatable :: recv_dpairpot_3darr(:)
    real(8), allocatable :: send_dpairpot_3darr(:)

    integer, allocatable :: send_noptdensitycoeff(:)
    integer, allocatable :: recv_noptdensitycoeff(:)
    
    real(8), allocatable :: recv_fitdata(:)
    
    logical p_finalOutput
    logical p_killWorkers


end module m_mpi_info
