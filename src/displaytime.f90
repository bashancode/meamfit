subroutine displaytime(unit,hrs,mins,secs)

    !-------------------------------------------------------------------c
    !
    !     Displays total time that the code has been running, and
    !     returns the 'hrs', 'mins' and 'secs'.
    !
    !     Andrew Duff 2017
    !
    !     Copyright (c) 2018, STFC
    !
    !-------------------------------------------------------------------c

    use m_generalinfo

    implicit none

    integer unit,hrs,mins,secs,tottime

    call getTime(tottime)
    tottime=tottime-tStart
    ! 
    ! call CPU_TIME(tottime)

    !Display different units for the total time depending on whether only
    !seconds, minutes or hours have already passed.
    hrs=0
    mins=0
    secs=0
    !print *,'tottime=',tottime,', tStart=',tStart
    if (tottime.lt.2) then
       write(unit,'(A18,I4,A2)') ' Total time taken ',INT(tottime*1000),'ms.'
    elseif (tottime.lt.60) then
       write(unit,'(A18,I2,A2)') ' Total time taken ',INT(tottime),'s.'
    elseif (tottime.lt.3600) then
       mins=INT(tottime/60)
       secs=MOD(tottime,60)
       write(unit,'(A18,I2,A2,I2,A2)') ' Total time taken ',INT(mins),'m ', &
                 INT(secs),'s.'
    elseif (tottime.ge.3600) then
       hrs=tottime/3600
       mins=MOD(tottime,3600)/60
       secs=MOD(mins,1)*60
       write(unit,'(A18,I5,A2,I2,A2,I2,A2)') ' Total time taken ',hrs,'h ', &
                 mins,'m ',secs,'s.'
    endif

end subroutine displaytime
