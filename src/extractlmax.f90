subroutine extractlmax

    !--------------------------------------------------------------c
    !
    !     Read in lmax from a user supplied potential file.
    !
    !     Called by:     program MEAMfit
    !     Returns:       lmax
    !     Files read:    startparameterfile
    !
    !     Andrew Duff 2014
    !
    !     Copyright (c) 2018, STFC
    !
    !--------------------------------------------------------------c

    use m_filenames
    use m_meamparameters
    use m_atomproperties

    implicit none

    open(unit=1,file=trim(startparameterfile),status='old')
    read(1,*) lmax
    close(1)

end subroutine extractlmax
