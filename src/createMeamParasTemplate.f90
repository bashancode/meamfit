subroutine createMeamParasTemplate

    !--------------------------------------------------------------c
    !
    !     Generates a file 'start_meam_parameters', if requested
    !     by user, which contains a potential in the MEAMfit format
    !     using default values specified in initializemeam and
    !     from an analysis of the separations from the POSCAR 
    !     files.
    !
    !     Called by:     MEAMfit
    !     Calls:         variables_to_p, writemeamp
    !     Returns:       -
    !     Files read:    -
    !     Files written: startparameterfile
    !
    !     Andrew Duff Aug 2014
    !
    !     Copyright (c) 2018, STFC
    !
    !--------------------------------------------------------------c

    use m_meamparameters
    use m_atomproperties
    use m_filenames
    use m_generalinfo
    use m_optimization
    use m_geometry

    implicit none

    integer isp,jsp,i,l,spcCnt,ip
    real(8) smlrad,lrgrad,cutoff

    !These parameters are used to define the 'standard' automaed cut-off generation.
    integer nCutoff     ! Number of cut-off parameters to use for
                                      ! each pair-wise function
    real(8), parameter :: offset = 0.2d0 ! cutoffs need to be made larger than a
                        !given separation in order to affect the enegy at that
                        !separation. This (somewhat arbitrary) constant reflects
                        !that. See below for its use.

    nCutoff=3
    !if (nCutoffOverride.gt.0) then
    !   nCutoff=nCutoffOverride
    !endif

    if (settingsfileexist.eqv..false.) then
       open(unit=1,file=trim(settingsfile),status="old",position="append", action='write')
       if (lmax.eq.0) then
          !cmin and cmax
          freep(1:m3)=0
          freep(1+m3:2*m3)=0
       endif
       !electron density parameters:
       freep(2*m3+lmax*m1+1:2*m3+lmax*m1+12*lm1*m2)=0
       spcCnt=0
       do isp=1,m1
          do jsp=1,m1
             if ((isp.eq.1).or.(thiaccptindepndt.eqv..false.)) then
                do l=0,lmax
                   do i=1,nCutoff
                      freep(2*m3+lmax*m1+12*(spcCnt*lm1+l)+2*i-1)=2
                      freep(2*m3+lmax*m1+12*(spcCnt*lm1+l)+2*i)=1
                   enddo
                enddo
             endif
             spcCnt=spcCnt+1
          enddo
       enddo
       !one parameter for each embedding function optimized
       freep(2*m3+lmax*m1+12*lm1*m2+1:2*m3+m1 &
           +lmax*m1+12*lm1*m2)=2
       freep(2*m3+m1+lmax*m1+12*lm1*m2+1:2*m3+ &
           2*m1+lmax*m1+12*lm1*m2)=0
       freep(2*m3+2*m1+lmax*m1+12*lm1*m2+1:2*m3+ &
           3*m1+lmax*m1+12*lm1*m2)=0
       freep(2*m3+3*m1+lmax*m1+12*lm1*m2+1:2*m3+ &
           4*m1+lmax*m1+12*lm1*m2)=0
       !pair-potential parameters:
       freep(2*m3+(4+lmax)*m1+12*lm1*m2+1:2*m3+(4+lmax)*m1+32*m2+12*lm1*m2)=0
       spcCnt=0
       do isp=1,m1
          do jsp=1,m1
             if (isp.le.jsp) then
                do i=1,nCutoff
                   freep(2*m3+(4+lmax)*m1+12*lm1*m2+32*spccnt+2*i-1)=2
                   freep(2*m3+(4+lmax)*m1+12*lm1*m2+32*spccnt+2*i)=1
                enddo
             endif
             spcCnt=spcCnt+1
          enddo
       enddo
       if (lmax.eq.0) then
          !rc and rs not optimized
          freep(m4+1:m4+m2)=0
          freep(m4+m2+1:m4+2*m2)=0
       endif
       !enconst randomly generated and optimized
       freep(m4+2*m2+1:m4+2*m2+m1)=2

       write(1,'(I1,A7)') lmax,' ! lmax'
       do i=1,m3
          write(1,'(I1,A1)',advance='no') freep(i),' '
       enddo
       write(1,*)
       do i=1,m3
          write(1,'(I1,A1)',advance='no') freep(i+m3),' '
       enddo
       write(1,*)
       do i=2*m3+1,2*m3+lmax*m1
          write(1,'(I1,A1)',advance='no') freep(i),' '
       enddo
       write(1,*)
       spcCnt=0
       do isp=1,m1
          do jsp=1,m1
             do l=0,lmax
                do i=1,12
                   write(1,'(I1,A1)',advance='no') &
             freep(2*m3+lmax*m1+12*(spcCnt*lm1+l)+i),' '
                enddo
                write(1,*)
             enddo
             spcCnt=spcCnt+1
          enddo
       enddo
       do i=2*m3+lmax*m1+12*lm1*m2+1,2*m3+m1 &
           +lmax*m1+12*lm1*m2
          write(1,'(I1,A1)',advance='no') freep(i),' '
       enddo
       write(1,*)
       do i=2*m3+m1+lmax*m1+12*lm1*m2+1, &
           2*m3+2*m1+lmax*m1+12*lm1*m2
          write(1,'(I1,A1)',advance='no') freep(i),' '
       enddo
       write(1,*)
       do i=2*m3+2*m1+lmax*m1+12*lm1*m2+1, &
           2*m3+3*m1+lmax*m1+12*lm1*m2
          write(1,'(I1,A1)',advance='no') freep(i),' '
       enddo
       write(1,*)
       do i=2*m3+3*m1+lmax*m1+12*lm1*m2+1, &
           2*m3+4*m1+lmax*m1+12*lm1*m2
          write(1,'(I1,A1)',advance='no') freep(i),' '
       enddo
       write(1,*)
       spccnt=0
       do isp=1,m1
          do jsp=1,m1
             do i=1,32
                write(1,'(I1,A1)',advance='no') freep(2*m3+(4+lmax)*m1+ &
                      12*lm1*m2+32*spccnt+i),' '
             enddo
             write(1,*)
             spccnt=spccnt+1
          enddo
       enddo
       !pairpotparameter(16,maxspecies,maxspecies)
       do i=m4+1,m4+m2
          write(1,'(I1,A1)',advance='no') freep(i),' '
       enddo
       write(1,*)
       do i=m4+m2+1,m4+2*m2
          write(1,'(I1,A1)',advance='no') freep(i),' '
       enddo
       write(1,*)
       do i=m4+2*m2+1,m4+2*m2+m1
          write(1,'(I1,A1)',advance='no') freep(i),' '
       enddo
       write(1,*)
       close(1)
       print *
       print *,'Finished writing settings file, stopping.'
       stop
    endif

    print *
    print *,'Setting up meam_parameters file'

    !Set up initial guess at cut-off radii, based on the distribution
    !of separations found from 'findsmallestsepn'
    !---- for the electron densities ----
    if ((thiaccptindepndt.eqv..false.)) then
     print *,' need to generalise code for thiaccptindepndt=false'
     stop
    endif
    do l=0,lmax
       do isp=1,maxspecies
          !First find the smallest separation, note that for the case of
          !electron densities we have to search between separations between
          !species isp and _all_ of the other species.
          smlrad=10000d0
          lrgrad=0d0
          do jsp=isp,maxspecies
             smlrad=min(smlrad,smlsepnperspc_overall(isp,jsp))
             lrgrad=max(lrgrad,lrgsepnperspc_overall(isp,jsp))
          enddo
          do jsp=1,isp
             smlrad=min(smlrad,smlsepnperspc_overall(jsp,isp))
             lrgrad=max(lrgrad,lrgsepnperspc_overall(isp,jsp))
          enddo
          smlrad=smlrad+offset
          lrgrad=min(lrgrad+offset,p_rmax)

          nCutoff=0
          do ip=2*m3+lmax*m1+2+12*l*m2+12*lm1*(isp-1),2*m3+lmax*m1+12+12*l*m2+12*lm1*(isp-1),2
             if (freep(ip).gt.0) then
                nCutoff=nCutoff+1
             endif
          enddo

          i=1
           print *,'for l=',l,', checking:',2*m3+lmax*m1+2+12*l+2*12*lm1*(isp-1),' to ', &
                2*m3+lmax*m1+12+12*l+2*12*lm1*(isp-1)
        ! do ip=2*m3+lmax*m1+2+12*l*m2+12*lm1*(isp-1),2*m3+lmax*m1+12+12*l*m2+12*lm1*(isp-1),2
          do ip=2*m3+lmax*m1+2+12*l+2*12*lm1*(isp-1),2*m3+lmax*m1+12+12*l+2*12*lm1*(isp-1),2
             if (freep(ip).gt.0) then
                cutoff=smlrad + dble(i-1)*(lrgrad-smlrad) / dble(nCutoff-1)
                meamrhodecay(ip-(2*m3+lmax*m1+12*l*m2+12*lm1*(isp-1)),l,1,isp)= cutoff
                if (l.eq.lmax) then
                print *,'Found freep(',ip,')=',freep(ip)
                print *,'setting cutoff=',cutoff,' for meamrhodeacy(', &
      ip-(2*m3+lmax*m1+12*l*m2+12*lm1*(isp-1)),',l=',l,',1,isp=',isp
                endif
                i=i+1
             endif
          enddo
       enddo
    enddo
    !stop
    !--------------------------------------


    !---- for the pair-potentials ----
    spcCnt=0
    do isp=1,maxspecies
        do jsp=1,maxspecies
            if (jsp.ge.isp) then
                smlrad=smlsepnperspc_overall(isp,jsp)+offset
                lrgrad=min(lrgsepnperspc_overall(isp,jsp)+offset,p_rmax)

                nCutoff=0
                do ip=2*m3+(4+lmax)*m1+12*lm1*m2+32*spcCnt+2,2*m3+(4+lmax)*m1+12*lm1*m2+32*(spcCnt+1),2
                   if (freep(ip).gt.0) then
                      nCutoff=nCutoff+1
                   endif
                enddo
                i=1
                do ip=2*m3+(4+lmax)*m1+12*lm1*m2+32*spcCnt+2,2*m3+(4+lmax)*m1+12*lm1*m2+32*(spcCnt+1),2
                   if (freep(ip).gt.0) then
                      cutoff=smlrad + dble(i-1)*(lrgrad-smlrad) / dble(nCutoff-1)
                      pairpotparameter(ip-(2*m3+(4+lmax)*m1+12*lm1*m2+32*spcCnt),isp,jsp)=cutoff
                      i=i+1
                   endif
                enddo
            endif
            spcCnt=spcCnt+1
        enddo
    enddo
    !---------------------------------

    call variables_to_p

    if (writeExPot) then 
       open(2,file=trim(startparameterfile))
       call writemeamp
       close(2)
       print *
       print *,'Written potential file ',trim(startparameterfile),', stopping.'
       stop
    endif

end subroutine createMeamParasTemplate
