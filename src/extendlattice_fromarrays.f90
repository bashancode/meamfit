subroutine extendlattice_fromarrays(coords_tmp,zz_tmp)

    !----------------------------------------------------------------------c
    !
    !     Extend the lattice (I.e. repeat the supercell using the
    !     lattice vectors) so that when an atom is displaced to calculate
    !     an atomic force, the same atoms in neighboring supercells are not
    !     also displaced.
    !
    !     Called by: initializestruc
    !     Calls: calculateshells
    !     Returns: -
    !     Files read: -
    !     Files written: -
    !
    !     Andy Duff, Jan 2008
    !
    !     Copyright (c) 2018, STFC
    !
    !----------------------------------------------------------------------c


    use m_generalinfo
    use m_datapoints
    use m_poscar
    use m_meamparameters
    use m_atomproperties

    implicit none
    logical inlist
    integer nshell,nnatoms_tot,i,it1,it2,it3,j,nnatoms
    real(8) rmax2,dd, &
        dxyz(3),xyztry1(3),xyztry2(3),xyztry(3)
    integer zz_tmp(natoms)
    real(8) coords_tmp(1:3,natoms)

    !Determine how many shells of unit cells need to be included
    !around the central unit cell in order that all atoms within
    !p_rmax of any atom in the central unit cell are included.
    !print *,'calling calculateshells...'
    call calculateshells(nshell,nnatoms_tot,p_rmax)

    !Set-up new coordinates
    deallocate(coordinates,zz)
    allocate(coordinates(3,nnatoms_tot),zz(nnatoms_tot))
    do i=1,natoms
        coordinates(1:3,i)=coords_tmp(1:3,i)
        zz(i)=zz_tmp(i)
    enddo

    !For each atom in the additional shells of unit cells determined by
    !'calculateshells', see which atoms are within p_max of one of the
    !inequivalent atoms (I.e. those in the 'central' cell) - and only retain
    !those atoms.
    rmax2=p_rmax**2
    nnatoms=natoms
    do i=1,natoms
        do it1=-nshell,nshell
            !print *,'tr(1:3,1)=',tr(1:3,1)
            xyztry1=coordinates(1:3,i)+tr(1:3,1)*dble(it1)
            do it2=-nshell,nshell
                xyztry2=xyztry1+tr(1:3,2)*dble(it2)
                do it3=-nshell,nshell
                    xyztry=xyztry2+tr(1:3,3)*dble(it3)
                    inlist=((it1.eq.0).and.(it2.eq.0).and.(it3.eq.0))
                    do j=1,natoms
                        !print *,'j=',j,'/',natoms
                        !print *,'it1=',it1,' it2=',it2,' it3=',it3
                        dxyz=xyztry-coordinates(1:3,j)
                        dd=dxyz(1)**2+dxyz(2)**2+dxyz(3)**2
                        if ((dd.le.rmax2).and.(.not.inlist)) then
                            nnatoms=nnatoms+1 !add to the list
                            coordinates(1:3,nnatoms)=xyztry(1:3)
                            zz(nnatoms)=zz(i)
                            !print *,'zz(',i,')=',zz(i),', z2species(zz(',i,'))=',z2species(zz(i))
                            !nat(z2species(zz(i)))=nat(z2species(zz(i)))+1
                            inlist=.true.
                        endif
                    enddo
                enddo
            enddo
        enddo
    enddo
    natoms=nnatoms

end subroutine
