subroutine analyzeVibrations

    !-------------------------------------------------------------------c
    !
    !    Analyze the period of oscillation of atoms from VASP
    !    MD calculations. Requires vasprun_#.xml files, where # are 
    !    the timestep values (POTIM values in the case of VASP). Will 
    !    determine number of ionic steps required for atoms of different 
    !    species to complete vibrations. 
    !    NOTE: throughout we use the term 'period' but we are actually
    !    counting half-periods.
    !
    !    Andy Duff
    !
    !-------------------------------------------------------------------c

    use m_datapoints
    use m_filenames
    use m_geometry
    use m_optimization
    use m_atomproperties

    implicit none
    integer ipotim,istr_perPotim,isp,potimIndex,idatapoint,iat,icomp,nperiods(10,maxspecies), &
            nforceComps(10,maxspecies)
    real(8), allocatable:: prevForce(:,:),prevForce2(:,:),prevInflect(:,:)
    real(8) potim,potimPrev,potimArr(10),periodAvrg(10,maxspecies),periodAvrgNum(10,maxspecies)

    !We will be scanning through vaspruns to detect 'periods' (where a force
    !component 'changes direction'). Store the 'previous force' that has been
    !read in for a given component and atom in 'prevForce'
    allocate(prevForce(3,maxval(gn_forces))) !remember to use gn_forces instead of
              !gn_inequivalentsites, since the latter is 'expanded' where non
              !zero wFr are given.
    !Also store the force previous to that. Three values of a force component
    !are needed to see if its direction of travel has changed.
    allocate(prevForce2(3,maxval(gn_forces))) 
    !The nperiods array stores the number of periods so far detected for each
    !species as the vasprun files are read.
    nperiods=0
    nforceComps=0
    allocate(prevInflect(3,maxval(gn_forces)))

    print *,'Reading in vibration data from DFT input files...'
    idatapoint=1
    potimPrev=0
    potimIndex=0
    periodAvrg=0d0
    periodAvrgNum=0d0
    istr_perPotim=1 !Keep track of the structure number within the current POTIM block
    do istr=1,nstruct

       !Check if we are reading DFT data for a new POTIM value
       potim=weightsFr(istr)
       if (potim.ne.potimPrev) then
          !print *,'new POTIM:',potim
          potimIndex=potimIndex+1
          istr_perPotim=1
          prevInflect=0
          if (potimIndex.gt.10) then
             print *,'ERROR: you cannot have more than 10 POTIM values, STOPPING.'
             stop
          endif
          potimArr(potimIndex)=potim
       endif
       potimPrev=potim

       do iat=1,gn_forces(istr)
          isp=gspecies(iat,istr)
          do icomp=1,3
             if (istr_perPotim.gt.2) then
                !Check if force component have flipped sign
                if ( (truedata(idatapoint)-prevForce(icomp,iat))*(prevForce(icomp,iat)-prevForce2(icomp,iat)).lt.0d0 ) then
                   nperiods(potimIndex,isp)=nperiods(potimIndex,isp)+1
!                  if ((iat.eq.1).and.(icomp.eq.1)) then
!                  print *,'force flip, istr|iat|icomp=',istr,iat,icomp,' force=',truedata(idatapoint)
!                  print *,'   (curr|prev|prev2 :',truedata(idatapoint),prevForce(icomp,iat),prevForce2(icomp,iat)
!                  endif
                  if (prevInflect(icomp,iat).eq.0) then
                     prevInflect(icomp,iat)=istr_perPotim
                  else
                     periodAvrg(potimIndex,isp)=periodAvrg(potimIndex,isp)+(istr_perPotim-prevInflect(icomp,iat))
                     periodAvrgNum(potimIndex,isp)=periodAvrgNum(potimIndex,isp)+1
                     prevInflect(icomp,iat)=istr_perPotim
                  endif
                endif
             endif
             if (istr_perPotim.gt.1) then
                prevForce2(icomp,iat)=prevForce(icomp,iat)
             endif
             prevForce(icomp,iat)=truedata(idatapoint)
             nforceComps(potimIndex,isp)=nforceComps(potimIndex,isp)+1
             idatapoint=idatapoint+1
          enddo
       enddo

       istr_perPotim=istr_perPotim+1
    enddo
          print *,'ipotim,isp,periodAvrg(ipotim,isp),periodAvrgNum(ipotim,isp):'
    do ipotim=1,potimIndex
       do isp=1,maxspecies
          !print *,ipotim,isp,periodAvrg(ipotim,isp),periodAvrgNum(ipotim,isp)
          periodAvrg(ipotim,isp)=periodAvrg(ipotim,isp)/periodAvrgNum(ipotim,isp)
       enddo
    enddo

    open(55,file='potimVsPeriod.dat')
    print *,'Species      |     Timestep     |       Half-period (no. iterations)'
    write(55,*) 'Species      |     Timestep     |       Half-period (no. iterations)'
    do isp=1,maxspecies
       do ipotim=1,potimIndex
!       print *,'...for potim=',potimArr(ipotim)
           print *,trim(element(speciestoZ(isp))),potimArr(ipotim),periodAvrg(ipotim,isp)
           write(55,*) trim(element(speciestoZ(isp))),potimArr(ipotim),periodAvrg(ipotim,isp)
!          print *,'nForceComps=',nForceComps(ipotim,isp),', nperiods=', &
!             nperiods(ipotim,isp),', avrg period=',periodAvrg(ipotim,isp), &
!             ' (alt calc=',nForceComps(ipotim,isp)/nperiods(ipotim,isp),')'
       enddo
    enddo
    close(55)

    deallocate(prevForce,prevForce2)

end subroutine analyzeVibrations

