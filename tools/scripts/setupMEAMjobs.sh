#!/bin/bash
echo
echo "MEAM directory set-up utility"
echo "-----------------------------"
echo

# First check if we are in the 'MEAM' directory
currentDir=`echo ${PWD##*/}`
if [[ $currentDir != *"MEAM"* ]]; then
   echo "ERROR: Please make and enter a MEAM directory and re-run script"
   exit
fi

# See if 'potparas_EAM' exists. This is the potential file to be used as the starting
# point of the MEAM fits. If it doesn't ask which of the potential files the user would
# like to use for this purpose.
if [ -f potparas_EAM ];
then
   echo "potparas_EAM file found - this will be copied to each of the MEAM directories and"
   echo "used as the starting point for these optimizations."
else
   echo "ERROR:No potparas_EAM file detected. potparas files present in previous directory:"
   echo
   ls ../potparas*
   echo
   echo "Please type in which one you would like to copy to potparas_EAM, to be used as the"
   echo "starting point for the MEAM optimizations (press ENTER for potparas_best1)."
   read potparasChoice
   if [ -z $potparasChoice ]; then
      potparasChoice="../potparas_best1"
   else
      echo
   fi
   if [ ! -f $potparasChoice ]; then
      echo "ERROR: You have selected a filename that does not exist, stoping"
      exit
   else
      echo "...OK, cp $potparasChoice potparas_EAM"
      cp $potparasChoice potparas_EAM
   fi
fi
echo

# First check that the relevant files that are to be copied exist
if [ ! -f settings ]; then
   echo "ERROR: settings file does not exist, stopping."
   exit
fi
if [ ! -f fitdbse ]; then
   echo "ERROR: fitdbse file does not exist, stopping."
   exit
fi

# Check that the settings file is not set up for EAM calculation
nEAM=`grep '= EAM' settings | wc -l`
if [ $nEAM -gt 0 ]; then
   echo "ERROR: Detected = EAM in settings file. Please make sure you have: TYPE=MEAM and re-run script, stopping."
   exit
fi
nEAM=`grep '=EAM' settings | wc -l`
if [ $nEAM -gt 0 ]; then
   echo "ERROR: Detected = EAM in settings file. Please make sure you have: TYPE=MEAM and re-run script, stopping."
   exit
fi

# Check if line POTFILEIN=potparas_EAM is in settings file
npotparas=`grep 'potparas_EAM' settings | wc -l`
if [ $npotparas -eq 0 ]; then
   echo "CAUTION: Cannot find poparas_EAM in your settings file. Expecting line: POTFILEIN=potparas_EAM. Press enter"
   echo "to continue or CTRL+C to exit."
   read keypress
fi

# Check if the EAM potential did a continuation job. If it did, remind user to take this out of the settings file.
nCONT=`grep 'CONT' settings | wc -l`
if [ $nCONT -gt 0 ]; then
   echo "ERROR: CONT detected in settings file. Please remove and re-run script, stopping."
   exit
fi

echo "Please enter number of directories to generate"
read num

echo
echo "Will copy: settings, fitdbse, potparas_EAM and ${nVasprunFiles} vasprun.xml files (as listed in the fitdbse file)"
echo "to each directory."
echo
echo "Last check:"
echo "  Do you have all of the necessary vasprun.xml files in this directory?"
echo "  Have you made necessary changes to the fitting-set? (an increase w.r.t EAM fitting-set is recommended)"
echo "(If yes, press enter, if not, enter CTRL+C)"
read keypress

for ((i=1; i<=$num; i++));
do
   mkdir $i;

   cp {settings,fitdbse,potparas_EAM,vasprun*} $i/

done

echo "Finished."
