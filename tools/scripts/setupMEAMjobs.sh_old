#!/bin/bash
echo
echo "MEAM directory set-up utility"
echo "-----------------------------"
echo

# First check that a 'MEAM' directory does not exist
if [ -d MEAM ]; then
   echo "ERROR: MEAM directory already exists. Please remove and re-run. Stopping."
   exit
fi

# See if 'potparas_EAM' exists. This is the potential file to be used as the starting
# point of the MEAM fits. If it doesn't ask which of the potential files the user would
# like to use for this purpose.
if [ -f potparas_EAM ];
then
   echo "potparas_EAM file found - this will be copied to each of the MEAM directories and"
   echo "used as the starting point for these optimizations."
else
   echo "No potparas_EAM file detected. potparas files present in directory:"
   echo
   ls potparas*
   echo
   echo "Please type in which one you would like to copy to potparas_EAM, to be used as the"
   echo "starting point for the MEAM optimizations (press ENTER for potparas_best1)."
   read potparasChoice
   if [ -z $potparasChoice ]; then
      potparasChoice="potparas_best1"
   else
      echo
   fi
   if [ ! -f $potparasChoice ]; then
      echo "ERROR: You have selected a filename that does not exist, stoping"
      exit
   else
      echo "...OK, cp $potparasChoice potparas_EAM"
      cp $potparasChoice potparas_EAM
   fi
fi

# First check that the relevant files that are to be copied exist
if [ ! -f settings ]; then
   echo "ERROR: settings file does not exist, stopping."
   exit
fi
if [ ! -f fitdbse ]; then
   echo "ERROR: fitdbse file does not exist, stopping."
   exit
fi
# ...this includes the vasprun.xml files listed in the fitdbse file:
numlines=`wc -l fitdbse | cut -f1 -d' '`
nVasprunFiles=$((numlines-1))
for ((i=2; i<=$numlines; i++));
do
   var=$(awk -v line="$i" -v field=1 'NR==line{print $field}' fitdbse);
   vasprunFilename[$i-1]="$var"
done

echo
echo "Please enter number of directories to generate"
read num

echo
echo "Will copy: settings, fitdbse, potparas_EAM and ${nVasprunFiles} vasprun.xml files (as listed"
echo "in the fitdbse file) to each directory, and will adjust settings files accordingly."

mkdir MEAM

# Adjust settings file (or rather, make a copy which we will delete) for MEAM optimization
cp settings settingsMEAM
# Find EAM and change it to MEAM. This will follow the TYPE= declaration (the following command
# only makes this replacement outside of comment-lines, identified by #, ! or @ as the first character)
sed -i '/^[#!@]/!s/EAM/MEAM/g' settingsMEAM
# Add the line POTFILEIN=potparas_EAM
echo "POTFILEIN=potparas_EAM" >> settingsMEAM
# If we got to our EAM potential by doing a continuation job, make sure we remove this flag for the MEAM job
# Note: this will also delete CONT in comment lines... /^[#!@]/!/CONT/d does not seem to work as I expected
sed -i '/CONT/d' settingsMEAM

for ((i=1; i<=$num; i++));
do
   mkdir MEAM/$i;

   cp settingsMEAM MEAM/$i/settings
   cp fitdbse MEAM/$i
   cp potparas_EAM MEAM/$i

   # Copy the vasprun.xml files
   for ((j=1; j<=$nVasprunFiles; j++));
   do
      cp ${vasprunFilename[$j]} MEAM/$i
   done

done

rm settingsMEAM

echo
echo "Finished."
