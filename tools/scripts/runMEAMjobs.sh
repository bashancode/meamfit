#!/bin/bash
echo
echo "MEAM job run utility"
echo "--------------------"
echo

for d in */ ; do
   echo "Entering "$d
   cd $d
   echo "Running MEAMfit > MEAMfit.out in screen"
   screen
   MEAMfitcode > MEAMfit.out
   screen -d
   exit
   stop
   cd ..
done
