set key font ",8"
set style line 1 linecolor rgb '#0060ad' linetype 1 linewidth 3
set xrange[0:300]
unset title
set xlabel "Atomic configuration"
set xtics scale 0 font ",8"
set ytics scale 0 font ",8"
set xlabel font ",8"
set ylabel font ",8"
set ylabel "Total energy of supercell (eV)"
set xtics nomirror
p './fitted_quantities.out' u 1:2 t "Potential", './fitted_quantities.out' u 1:3 w l t "DFT"
