
      program getpositionsfromMD

c
c     Andrew Ian Duff 2013-2014, Imperial College London
c
c     For an MD run, extracts energies, force and atomic positions
c     from a vasprun.xml file, and saves them as OUTCAR and POSCAR
c     files. The user can specify which sets of energies, force and
c     atomic positions are to be extracted (first ionic step to start
c     sampling; and no of ionic steps for which data is required).
c     User must supply a POSCAR file which will be used as a template
c     (lattice parameters, numbers of atoms, etc) when producing the
c     POSCAR files.
c
c     Files required: vasprun.xml, POSCAR
c     Files produced: numerically ordered POSCARx and OUTCARx files
c
c     Possible improvements:
c       Trackback to extract energy - might be better to search for the
c     relevant string within the last n lines rather than go back 19
c     lines, so as to make code more resiliant to formatting changes to 
c     vasprunn.xml
c       Automatic analysis of energy to find no. ionic steps necc. for
c     equilibrium and correlation length. Then automatically select
c     ionic iterations using this information (by default, although can
c     be over-riden)
c

      implicit none

      logical incompletecycle
      character*9 newstrng,newstrng2
      character*80 line,tmp,tmp2,numasstring,filename,header,strng(1:3),
     +        lineprev(18),filename2
      integer nwords,nsteps,nposcars,nspecies,natoms,i,nPOSCAR,check,
     +        diff,nposns,x,nequil,length,endoffile,nlast,correct,
     +        extractforces
      integer, allocatable:: nat(:)
      real(8) scale,energy,tr(3,3)
      real(8), allocatable:: coordinates(:,:),force(:,:)

      print *
      print *,'Program for extracting energies, force and positions'
      print *,'from vasprun.xml for an MD run.'
      print *

c     !Deleted 01 Apr 2014: Originally read in NSW from INCAR file,
c     !but there is no need, since it can be read in from the number
c     !of ionic cycles in the vasprun.xml file. Leave it here
c     !commented out for now in case I have use of it later.
c
c     !Determine number of ionic steps
c     open(50,file='INCAR')
c     do
c       read(50,'(a80)') line
c       call optionalarg(line,nwords)
c       if (nwords.eq.3) then
c          read(line,*) strng(1:3)
c          if (strng(1).eq.'NSW') then
c             read(line,*) strng(1:2),nsteps
c             exit
c          endif
c       endif
c     enddo
c     close(50)
c     print *,'Found NSW in INCAR, nsteps=',nsteps

      !Take opening data from POSCAR file
      open(unit=50,file='POSCAR')
      read(50,*) header
      read(50,*) scale
      read(50,*) tr(1:3,1)       !a lattice parameter
      read(50,*) tr(1:3,2)       !b lattice parameter
      read(50,*) tr(1:3,3)       !c lattice parameter
      read(50,'(a80)') line      !Numbers of atoms of each species
      close(50)
      call optionalarg(line,nspecies)
      allocate(nat(nspecies))
      read(line,*) nat(1:nspecies)
      natoms = sum( nat(1:nspecies) )
      allocate(coordinates(3,natoms),force(3,natoms))
      print *,'Read POSCAR (',natoms,' atoms)'
      print *

      !Check that there are the same number of ionic steps in vasprun.xml
      open(50,file='vasprun.xml')
      rewind(50)
      x=-1 !don't count first ionic step, since this is repeated
      do
        read(50,'(a80)',IOSTAT=endoffile) line
        if (endoffile.lt.0) then
          exit
        endif
        newstrng=line(17:25)
        newstrng2=line(18:26)
        if ( (newstrng(1:9).eq.'positions') .or.
     +       (newstrng2(1:9).eq.'positions') ) then
          x=x+1
        endif
      enddo
      close(50)
c     incompletecycle=.false.
c     if (x.eq.nsteps) then
c        print *,'Checked: same number of ionic steps in vasprun.xml'
c     else
c        print *,'Warning: vasprun.xml contains ',x,' ionic steps,'
c        print *,'rather than NSW (',nsteps,').'
c        print *,'  -> Taking nsteps=',x
         nsteps=x
c        incompletecycle=.true.
c     endif

      !Ask user which ionic steps they are interested in
      print *,'Vasprun.xml contains ',x,' ionic steps.'
      print *,'Which atomic config do you want to start sampling?'
      read *,nequil
      if (incompletecycle) then
        print *,'What is the last atomic config you want to consider?'
        read *,nlast
        if (nlast.gt.nsteps) then
          print *,'You cannot choose this to be larger than nsteps.'
          stop
        endif
      else
        nlast=nsteps
      endif
      print *,'How many ionic steps do you want to generate ',
     + 'POSCAR files for?'
      read *,nposcars
      check=mod(nlast-nequil,nposcars-1)
c      if (check.ne.0) then
c        print *,'ERROR: nsteps should be divisible by this number'
c        print *,'nlast=',nlast,', nequil=',nequil,', nposcars=',nposcars
cc        print *,'but nlast-nequil=',nlast-nequil
cc        print *,'and nposcars-1=',nposcars-1
cc        print *,'so that mod(nlast-nequil,nposcars-1)=',
cc     +                   mod(nlast-nequil,nposcars-1)
c        stop
c      endif
      diff=(nlast-nequil)/(nposcars-1)
      print *,'Reading ionic steps: ',nequil,', ',nequil+diff,
     + '...',nlast-diff,', ',nlast
      print *,'correct? (1=yes, 0=stop)'
      read *,correct
      if (correct.eq.0) then
        stop
      endif

      print *,'also extract forces? (1=yes, 0=no)'
      read *,extractforces

      !Skip past the first set of positions in vasprun.xml (these positions
      !are the same as the second)
      open(50,file='vasprun.xml')
      rewind(50)
      x=1
      do
        read(50,'(a80)') line
        x=x+1
        newstrng=line(17:25)
        newstrng2=line(18:26)
        if ( (newstrng(1:9).eq.'positions') .or. 
     +       (newstrng2(1:9).eq.'positions') ) then
          exit
        endif
      enddo
      print *

      nPOSCAR=1
      nposns=1
      do
        !Store previous lines so that once we find 'positions', we can
        !extract the corresponding energy for that ionic step.
        call storelines(lineprev)
        read(50,'(a80)') line
        x=x+1
        lineprev(1)=line
        newstrng=line(17:25)
        newstrng2=line(18:26)
        if ( (newstrng(1:9).eq.'positions') .or. 
     +       (newstrng2(1:9).eq.'positions') ) then
          if ( (mod(nposns-nequil,diff).eq.0) .and.
     +         (nposns.ge.nequil) ) then
            !---- Extract relevant data ----
            !Track back 18 lines to extract energy
            line=lineprev(18)
            read(line,*) strng(1:3)
            x=x+1
            tmp=strng(3) !Remove the last character from strng (<)
            length=len_trim(tmp)
            tmp=tmp(1:length-1)
            read (tmp,*) energy
            !Extract positions
            do i=1,natoms
              read(50,*) tmp,coordinates(1:2,i),tmp2
              x=x+1
              length=len_trim(tmp2) !Remove the last character from strng (<)
              tmp2=tmp2(1:length-1)
              read(tmp2,*) coordinates(3,i)
            enddo
            !Extract force
            if (extractforces.eq.1) then
             do i=1,3
               read(50,*)
               x=x+1
             enddo
             do i=1,natoms
               read(50,*) tmp,force(1:2,i),tmp2
               x=x+1
               length=len_trim(tmp2) !Remove the last character from strng (<)
               tmp2=tmp2(1:length-1)
               read(tmp2,*) force(3,i)
             enddo
            endif
            !---- Write data to POSCAR/OUTCAR ----
            !Write POSCAR
            print *,'Writing data for ',nposns,' (/',nsteps,
     +              ') set of ionic positions.'
            print *,'Data read-in ending on line ',x-1
            write(numasstring,'(I4)') nPOSCAR
            filename='POSCAR-' // trim(adjustl(numasstring))
            open(51,file=filename)
            write(51,*) header
            write(51,'(a2,f18.15)') '  ',scale
            write(51,'(a1,f18.15,a2,f18.15,a2,f18.15)') 
     +       ' ',tr(1,1),' ',tr(2,1),' ',tr(3,1)
            write(51,'(a1,f18.15,a2,f18.15,a2,f18.15)')
     +       ' ',tr(1,2),' ',tr(2,2),' ',tr(3,2)
            write(51,'(a1,f18.15,a2,f18.15,a2,f18.15)')
     +       ' ',tr(1,3),' ',tr(2,3),' ',tr(3,3)
            write(51,*) nat(1:nspecies)
            write(51,*) 'Direct'
            do i=1,natoms
               write(51,'(a1,f18.15,a2,f18.15,a2,f18.15)') 
     +         ' ',coordinates(1,i),'  ',coordinates(2,i),
     +         '  ',coordinates(3,i)
            enddo
            close(51)
            !Write OUTCAR
            write(numasstring,'(I4)') nPOSCAR
            filename='OUTCAR-' // trim(adjustl(numasstring))
            open(51,file=filename)
            write(51,'(A44,A19,F20.8)') 
     +       '  energy  without entropy=     not-read-in  ',
     +       'energy(sigma->0) = ',energy
            if (extractforces.eq.1) then
             write(51,*)
             write(51,'(A29,A41)') 
     +        ' POSITION                    ',
     +        '                   TOTAL-FORCE (eV/Angst)'
             write(51,'(A29,A41)')
     +        ' ----------------------------',
     +        '-------------------------------------------------------'
             do i=1,natoms
                write(51,'(a1,f12.8,a2,f12.8,a2,f12.8,
     +                     a1,f12.8,a2,f12.8,a2,f12.8)')
     +          ' ',coordinates(1,i),'  ',coordinates(2,i),
     +          '  ',coordinates(3,i),
     +          ' ',force(1,i),'  ',force(2,i),
     +          '  ',force(3,i)
             enddo
            endif
            close(51)
            if (nPOSCAR.eq.nposcars) then
              exit
            endif
            nPOSCAR=nPOSCAR+1
          endif
          nposns=nposns+1
        endif
      enddo
      close(50)

      print *,'Writing template fitdbse file (fitdbse_tmplt)'
      open(50,file='fitdbse_tmplt')
      write(50,'(I5,A42,A44)') 
     +     nPOSCAR,'  # Struc | Energies or forces to fit to |',
     +     ' What to fit | Relax struc | Weights'
      do i=1,nPOSCAR
        write(numasstring,'(I4)') i
        filename='POSCAR-' // trim(adjustl(numasstring))
        filename2='OUTCAR-' // trim(adjustl(numasstring))
        write(50,'(A15,A15,A19,A24)')
     +     filename,filename2,'          e        ',
     +     '       n               1'
      enddo
      close(50)

      print *,'Finished.'

      end program getpositionsfromMD

      subroutine optionalarg(line,nargs)

      implicit none

      integer nargs,length,i
      character line*(*)
      logical newarg

      intent(in) line
      intent(out) nargs

      !Analyze LINE
      length=len_trim(line)
      !Count number of sections in LINE that are separated by commas or
      !spaces
      nargs=0
      newarg=.true.
      do i=1,length
        if ((line(i:i).eq.',').or.(line(i:i).eq.' ').or.
     +        (line(i:i).eq.CHAR(9))) then
          newarg=.true.
        else
          if(newarg) nargs=nargs+1
          newarg=.false.
        endif
      enddo

      end subroutine optionalarg

      subroutine storelines(lineprev)

      integer i
      character*80 lineprev(18)

c     Store 18 previously read lines (so we can back-trace to read in
c     the energy upon finding the atomic positions)

      do i=18,2,-1
         lineprev(i)=lineprev(i-1)
      enddo

      end subroutine storelines
